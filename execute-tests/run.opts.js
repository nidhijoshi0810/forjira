const path = require('path');
const argvArr = process.argv.slice(2);
const {getOverridedConfigPath} = require('./override.configuration')

function removeConfigFromArgv() {
  let index = argvArr.findIndex(i => i === '-c')
  if(index < 0) {
    index = argvArr.findIndex(i => i === '--config')
  }
  // remove config opt
  argvArr.splice(index, 1)
  // remove config path opt
  argvArr.splice(index, 1)
}

function findSessionCount() {
  // clean up config
  removeConfigFromArgv()

  if(argvArr.includes('run-workers')) {
    const index = argvArr.findIndex(i => i === 'run-workers')
    const sessionsCount = Number(argvArr[index + 1])
    // remove run-workers from argv
    argvArr.splice(index, 1)
    // remove run-workers value from argv
    argvArr.splice(index, 1)
    return sessionsCount
  } else {
    return 1
  }
}

function formCommand(filePath) {
  return `${path.resolve(process.cwd(), './node_modules/.bin/codeceptjs')} run -c ${getOverridedConfigPath(filePath)} ${argvArr.join(' ')}`;
}

module.exports = {
  formCommand,
  maxSessionCount: findSessionCount()
};
