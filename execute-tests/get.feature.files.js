const {getSpecFilesArr} = require('process-rerun')
const {readFileSync} = require('fs')
const argv = require('minimist')(process.argv.slice(2))

function getFileContent(filePath) {
  return readFileSync(filePath).toString('utf8')
}

function getFeatureFiles() {
  const grep = argv.grep ? argv.grep.split(',') : [''];

  return getSpecFilesArr(process.cwd(), [], ['node_modules', 'Resources'])
    .filter(f => f.includes('.feature'))
    .filter(f => grep.some(i => getFileContent(f).includes(i)))
}

module.exports = {
  getFeatureFiles
}

