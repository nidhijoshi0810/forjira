const {getSpecFilesArr} = require('process-rerun')
const fs = require('fs')

function cleanUpTempConfigs() {
  getSpecFilesArr(process.cwd())
    .filter(c => c.includes('_temp.execution.config.js'))
    .forEach(fs.unlinkSync.bind(fs))
}

module.exports = {
  cleanUpTempConfigs
}
