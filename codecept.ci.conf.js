process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
// require("source-map-support").install(); 
// const startup = require("./dist/Framework/FrameworkUtilities/Startup/Startup").Startup;
const prop = require("./dist/Framework/FrameworkUtilities/config").prop; const PropertiesConfigurator = require("./dist/Framework/PropertiesConfigurator/PropertiesConfigurator");
// const addTimeStampToReport = require('./add_timestamp_report');
// const {startShareDataService} = require('./Share_data/startShareDataService');

// startShareDataService();
// addTimeStampToReport();
 PropertiesConfigurator.parseJson();
// require("./dist/Framework/CustomHelper/eventListeners").eventListener.init();
// const prop = global.confi_prop;
// startup.lang = 'en';
exports.config = {
  tests: "./*_test.js",
  output: "./output",
  helpers: {
    WebDriver: {
      url: prop.url,
      browser: prop.browser,
      host: "192.168.15.227",
      // host:"172.20.8.53",//vaish pc
      // host:"10.161.1.119",//not allowed to access from local only to gocd
      port: prop.port,
      restart: false,
      keepCookies : true,
      windowSize: prop.windowSize,
      waitForTimeout: 30000,
      smartWait: 10000,
      default_low_wait: prop.DEFAULT_LOW_WAIT,
      default_medium_wait: prop.DEFAULT_MEDIUM_WAIT,
      default_high_wait: prop.DEFAULT_HIGH_WAIT,
      desiredCapabilities: {
        chromeOptions: {
          excludeSwitches: ['enable-automation'],
          prefs: {
            download: {
              default_directory: "//localjump.zycus.net/public/Nidhi",
            }
          }
  
        },
        acceptInsecureCerts : true,
        'selenoid:options': {
          enableVNC: true,
          enableVideo: false
        }
      }
    },
   ChaiWrapper:
    {
      require: "codeceptjs-chai"
    },
    MyHelper:
    {
      require: "./myHelper.js",
    },
  },
  bootstrap: "./dist/Framework/FrameworkUtilities/Bootstrap/bootstrap.js",
  teardown: "./dist/Framework/FrameworkUtilities/Bootstrap/bootstrap.js",
  teardownAll: "./get_all_reports.js",
  include: {
    I: prop.stepFilePath,
  },
  multiple: {
    parallel: {
      // Splits tests into 2 chunks
      chunks: 2
    },
     sanityCases: {
      // Splits tests into 2 chunks
      chunks: 2
    }
  },
  gherkin: {
    features: './src/iSource/features/**/**.feature',
    steps: './dist/iSource/Implementation/**/**.js'
  },
 
  name: prop.projectName,
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true,
      uniqueScreenshotNames : true,
    },
    wdio: {
      enabled: false,
      services: ["selenium-standalone"]
    },
    autoDelay: {
      enabled: true,
      delayBefore: 1000,
      delayAfter: 500,
      methods: ['click', 'fillField', 'checkOption']
  },
    allure: {
      enabled: true
    },
    require: ["ts-node/register"]
  }
};