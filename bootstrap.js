const databaseOperations = require("./Framework/FrameworkUtilities/DatabaseOperations/databaseOperations");
const lmtVar = require("./Framework/FrameworkUtilities/i18nutil/readI18NProp")
const logger = require("./Framework/FrameworkUtilities/Logger/logger");
const {stopShareDataService} = require("./Share_data/startShareDataService")
module.exports = {
        bootstrap: async function () {
                console.log(global.output_dir)
                global.testData = await databaseOperations.getTestData();
                global.uiElements = await databaseOperations.getUiElementXpath();
                global.commonuiElements = await databaseOperations.getCommonUiElementXpath();
                global.lmt = await databaseOperations.getLMTDetails();
                global.automationLMT_Pair = await databaseOperations.getAutomationKey();
        },
        teardown: async function ()
        {
                stopShareDataService();
                // await databaseOperations.updateUSER(global.users.get("USERNAME"),"true");
        },
        bootstrapAll: async function()
        {
                console.log("inside bootstrapAll ");
        },
        teardownAll: async function()
        {
                // shared.killServerManager();
                console.log("inside tearDownAll ");
        }
}