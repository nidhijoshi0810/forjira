import "codeceptjs";

import { CommonLocators } from "./CommonLocators";
import { Startup } from "../../Framework/FrameworkUtilities/Startup/Startup";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword"
declare const inject: any;
const {I}=inject();
export class CommonFunctions{

    static async getFinalLocator(locator : string,stringToreplace : string){
        console.log("inside final locator");
        var finalLocator=locator.replace("<<dataname>>",stringToreplace);
        return finalLocator;
    }
    static async clickOnModalFooterButton(text : string){

        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.footerButtonXpath) as string,text)) ;
    }

    static async navigateToiosurceTabs(tabLabel : string){

      await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.tabXpath) as string,tabLabel));
    }

    static  async clickOnButton(buttonLabel : string){

        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,buttonLabel));
    }

    static async scrollToButton(buttonLabel : string){

        I.scrollIntoView(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,buttonLabel));
    }
    static async getTodaysDateInMMDDYYYYFormat(dayToAdd?:number){
        let today = new Date();
        let dd=0;
        if(dayToAdd && dayToAdd !=undefined){
            dd = today.getDate()+dayToAdd;
        }
        else{
            dd = today.getDate(); 
        }
      
        let mm = today.getMonth()+1; 
        let yyyy = today.getFullYear();
        if(dd<10) 
        {
            dd=parseInt('0'+dd.toString());
        } 
        
        if(mm<10) 
        {
            mm=parseInt('0'+mm.toString());
        } 
        console.log(mm+'/'+dd+'/'+yyyy);
        return mm+'/'+dd+'/'+yyyy;  
    }

    static async addMinutesToCurrentDate2(minutesToAdd:number) {
        let d = new Date();
        let h = d.getHours();
        let m = d.getMinutes() + minutesToAdd;
        if(m>59){
            h=h+1
            m=m-59
          } if(h===24)
          {
              h=12;
          }
        return  h.toString() + ':' + m.toString();    
    }
    static async addMinutesToCurrentDate(minutesToAdd:number) {
     
        let  indiaTime : Date = new Date()
           let d : Date = new Date(indiaTime.getTime()+(minutesToAdd+1)*60000);
          let h = d.getHours();
         let m = d.getMinutes()
        return  h.toString() + ':' + m.toString();    
    }
}