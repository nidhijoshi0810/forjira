
export class GlobalVariables {

    static scenarioName : string;
    static eventId : string;
    static auctionType : string;
    static QSeventId : string; 
    static QSeventName : string; 
    static contractName:string;
    static eventIDForAllProds : string;
    static EventTitle : string; 
    static ContractIDForAllProds : string;  
}