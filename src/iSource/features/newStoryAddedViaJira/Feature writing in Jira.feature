
Feature: Feature to written in jira
	Background:
		When User login to application

	Scenario: Scenario to be written in jirs
		Given User is logged in to the application
		And User navigates to iContract->Template screen
		When User initiates the template creation
		And User fills all details from the transition screen
		Then User should land on the template details screen in edit mode
