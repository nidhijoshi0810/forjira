@REQ_TSTNG-25
Feature: Authoring Contract scenario based on the application setting

   Create Contract in Authoring based on the workflow &
   Touchfree configuration in the app setting

@tagL1
Scenario: To verify whether user is able to initiate and approve Authoring Workflow
    Given logged in to iContract Login Page
    Given I Navigate to iContract Settings Tab followed by Product Configuration
    Then I verify Workflow is not bypassed in the app setting
    Given I navigate to Authoring Contract Tab
    When I initiate authoring contract for type subtype for which Authoring Workflow in confirgured
    Then I enter Contract Details
    When I select Contracting Party And Contract Person
    And I initiate the Authoring Workflow
    And I logout from existing user
    Then I should be aaprove the contract under Pending Authoring Approvals


@tagL2
Scenario: Create a Contract in Authoring with PDF and test send back to Author
    Given logged in to iContract Login Page
    Given I navigate to Authoring Contract Tab
    And I Initiate Authoring Contract with External Template as PDF
    Then I enter Contract Details
    When I select Contracting Party And Contract Person
    Then I initiate proceed to signing
    Then I click on Send Back to Author
    Then I verify if the Contract is sent to Authoring Stage or not
    Then I initiate proceed to signing

@tagL2
Scenario: To check Contract creation in Authoring when TouchFree is ON & workflow is not bypassed in app setting
    Given logged in to iContract Login Page
    Given I Navigate to iContract Settings Tab followed by Product Configuration
    Then I verify Touchfree is On in the app setting
    Then I verify Workflow is not bypassed in the app setting
    Given I navigate to Authoring Contract Tab
    Then I create a Contract with Authoring workflow & TouchFree bypassed
    Then I enter Contract Details
    When I select Contracting Party And Contract Person
    Then I initiate proceed to signing
    Then I click on Send Back to Author
    Then I verify if the Contract is sent to Authoring Stage or not
    Then I initiate proceed to signing

@tagL2
Scenario: To check Contract creation in Authoring when TouchFree is ON & workflow is not bypassed in app setting
    Given logged in to iContract Login Page
    Given I Navigate to iContract Settings Tab followed by Product Configuration
    Then I verify Touchfree is On in the app setting
    Then I verify Workflow is not bypassed in the app setting
    Given I navigate to Authoring Contract Tab
    Then I create a Contract with Authoring workflow not bypassed on transition & TouchFree not bypassed
    #When Contract Negotiation round is enabled then Auth workflow question does not appear & id wf is apllicable it will be triggered
    Then I enter the Contract Details
    When I select Contracting Party And Contract Person
    And I initiate the Authoring Workflow
    Then I approve the Contract
    Then I click on Send to Contracting Party button

@tagL2
Scenario: To check Contract creation in Authoring when TouchFree is ON & workflow is not bypassed in app setting & Negtiation is bypassed on transition page
    Given logged in to iContract Login Page
    Given I Navigate to iContract Settings Tab followed by Product Configuration
    Then I verify Touchfree is On in the app setting
    Then I verify Workflow is not bypassed in the app setting
    Given I navigate to Authoring Contract Tab
    Then I create a Contract with Authoring workflow enabled and Negotiation as No
    Then I enter Contract Details
    When I select Contracting Party And Contract Person
    And I initiate the Authoring Workflow
    Then I approve the Contract
    Then I initiate proceed to signing
    Then I click on Send Back to Author
    Then I verify if the Contract is sent to Authoring Stage or not
    Then I save the Contract
    And I initiate the Authoring Workflow
    Then I approve the Contract
    Then I initiate proceed to signing

@tagL2
Scenario: To check Contract creation in Authoring when TouchFree is OFF & workflow is bypassed in app setting
    Given logged in to iContract Login Page
    Given I Navigate to iContract Settings Tab followed by Product Configuration
    Then I verify Touchfree is OFF in the app setting
    Then I verify Workflow is bypassed in the app setting
    Given I navigate to Authoring Contract Tab
    Then I create a Contract in Authoring
    #When Auth WF is bypassed & Negotiation is OFF in the app setting then on the transition screen TouchFree & Authring Review question won't appear 
    Then I enter Contract Details
    When I select Contracting Party And Contract Person
    Then I send contract under Pending External Review

@tagL2
Scenario: To check Contract creation in Authoring when TouchFree is OFF & workflow is Not bypassed in app setting
   Given logged in to iContract Login Page
   Given I Navigate to iContract Settings Tab followed by Product Configuration
   Then I verify Touchfree is OFF in the app setting
   Then I verify Workflow is not bypassed in the app setting
   Given I navigate to Authoring Contract Tab
   Then I create a Contract in Authoring
   #When Auth WF is Not bypassed & Negotiation is OFF in the app setting then on the transition screen TouchFree & Authring Review question won't appear
   # & if workflow is configured & applicable then it will get triggered
   Then I enter Contract Details
   When I select Contracting Party And Contract Person
   And I initiate the Authoring Workflow
   Then I approve the Contract
   Then I click on Send to Contracting Party button

@tagL2
Scenario: To check Contract creation in Authoring when TouchFree is OFF & workflow is Not bypassed in app setting & workflow is not configured for the type subtype
   Given logged in to iContract Login Page
   Given I Navigate to iContract Settings Tab followed by Product Configuration
   Then I verify Touchfree is OFF in the app setting
   Then I verify Workflow is not bypassed in the app setting
   Given I navigate to Authoring Contract Tab
   Then I create a Contract with the type subtype for which workflow is not configured
   Then I enter Contract Details
   When I select Contracting Party And Contract Person
   Then I send contract under Pending External Review

@tagL2
Scenario: To check Contract Creation in Authoring when TouchFree is ON & Workflow is bypassed in app setting & Negotiation is On from Transition Screen
   Given logged in to iContract Login Page
   Given I Navigate to iContract Settings Tab followed by Product Configuration
   Then I verify Touchfree is On in the app setting
   Then I verify Workflow is bypassed in the app setting
   Given I navigate to Authoring Contract Tab
   Then I create a Contract for which Negotiation is Yes on the transition screen
   Then I enter Contract Details
   When I select Contracting Party And Contract Person
   Then I send contract under Pending External Review

@tagL2
Scenario: To check Contract Creation in Authoring when TouchFree is ON & Workflow is bypassed in app setting & Negotiation is Off from Transition Screen
   Given logged in to iContract Login Page
   Given I Navigate to iContract Settings Tab followed by Product Configuration
   Then I verify Touchfree is On in the app setting
   Then I verify Workflow is bypassed in the app setting
   Given I navigate to Authoring Contract Tab
   Then I create a Contract for which Negotiation is set as No on the transition screen
   Then I enter Contract Details
   When I select Contracting Party And Contract Person
   Then I initiate proceed to signing
   

@tagL2
Scenario: To check in the application setting Workflow is not bypassed
   Given logged in to iContract Login Page
   Given I Navigate to iContract Settings Tab followed by Product Configuration
   Then I verify Workflow is not bypassed in the app setting




