
Feature: Sample for uploading Demo feature
	Background:
		When User login to application

	Scenario: Uploading this feature directly from the bitbucket
		Given User is logged in to the application
		And User navigates to iContract->Template screen
		When User initiates the template creation
		And User fills all details from the transition screen
		Then User should land on the template details screen in edit mode
