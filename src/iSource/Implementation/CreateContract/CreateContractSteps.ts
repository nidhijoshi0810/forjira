import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { CreateContractFlow } from "../../POM/CreateContract/CreateContractFlow";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

Given('iSource- I click on Create Contract Button', async function () {

  
  await CreateContractFlow.clickCreateContractButton();
});

When('iSource- I fill contract name', async function () {
  await CreateContractFlow.typeContractName();
});

When('iSource- I select Contract type and sub type', async function () {
  await CreateContractFlow.selectContractType();
  await CreateContractFlow.selectContractSubType();
});

When('iSource- I Select Contract Owner', async function () {
  await CreateContractFlow.selectContractOwner();
});

Then('iSource- I click on Create Contract button in modal popup', async function () {
  await CreateContractFlow.clickCreateContractBtn();
});

Then('iSource- I verify contract ID is created or not', async function(){
  await CreateContractFlow.verifyContractCreation();
})

Then('iSource- I check contract ID on grid of supplier details', async function(){
  await CreateContractFlow.verifyContractIDOnGrid();
})