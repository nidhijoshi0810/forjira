import { SupplierEforum } from "../../POM/SupplierSideEforum/SupplierSideEforumFlow";

// const { I } = inject();


Given('Craete eforum from supplier side', async function(){
  await SupplierEforum.craeteEforum();
    
  });

When('iSource- I click on the created eforum', async function(){
  await SupplierEforum.clickOnCreatedEvent();
  await SupplierEforum.clickOnNewMessage();
})  

Then('iSource- I filled the mandatory fields and click on post', async function(){
  await SupplierEforum.enterMessage();
  await SupplierEforum.clickOnPost();
})

Then('iSource- I send the public message for approval', async function(){
  await SupplierEforum.clickOnSendForapprovalPopup();
})

Given('iSource- I am at eforum page', async function(){
  await SupplierEforum.clickOnListOfEforum();
 
})

Then('iSource- I send public message to buyer', async function(){
  await SupplierEforum.selectPublicDiscussionEfourm();
  await SupplierEforum.clickOnNewMessage();
 
})