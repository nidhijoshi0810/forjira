import "codeceptjs";
import { TEBPanelFlow } from "../../POM/TEBPanel/TEBPanelFlow";
declare const inject: any;

Given('iSource- I am on Add Panel Members section page', async function () {
await TEBPanelFlow.clickOnPanelAddMemberBtn();
});

When('iSource- I select Members for technical panel and commercial panel', async function () {
await TEBPanelFlow.addTechPanel();
await TEBPanelFlow.addCommPanel();
await TEBPanelFlow.addBothPanel();
});


Then('iSource- I click Done', async function () {
await TEBPanelFlow.clickDone();
});