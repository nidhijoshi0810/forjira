import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();

import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { AuctionMonitoringFlow } from "./../../POM/AuctionMonitoring/AuctionMonitoringFlow";


Given('iSource- I navigate go to auction summary page', async function(){
  await  AuctionMonitoringFlow.navigateToAuctionMonitoringPage();
});

Then('iSource- I stop auction by unchecking the Auto Extend option', async function(){
  await AuctionMonitoringFlow.uncheckExtendLotCheckbox();
});

Then('iSource- I click on overall progress tab', async function(){
  // I.amOnPage("https://partner-zcsauth.zycus.com/isource/#/events/206808/auction-monitoring/englishauction");
  await AuctionMonitoringFlow.overallProgressTab();
});

Then('iSource- I view Supplier bid graph', async function(){
  await AuctionMonitoringFlow.viewSupplierGraph();
});

Then('iSource- I view supplier rank', async function(){
  await AuctionMonitoringFlow.viewSupplierRank();
});


