import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { logger } from "./../../../Framework/FrameworkUtilities/Logger/logger";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { JapaneseAuctionMonitoring } from '../../POM/AuctionMonitoring/JapaneseAuctionMonitoringFlow';


Then('iSource- I navigate back to DDHome page', async function(){
    await JapaneseAuctionMonitoring.navigateToDDHomePage();
});

When('iSource- I see Current round', async function () {
    await JapaneseAuctionMonitoring.navigateToAuctionSummary();
    await JapaneseAuctionMonitoring.viewCurrentRound();
})

When('iSource- I see Total no of suppliers for current round', async function () {
    await JapaneseAuctionMonitoring.viewSupplierCount();
})

Then('iSource- I see Currrent round and Supplier count for the round', async function () {
    await JapaneseAuctionMonitoring.viewRoundAndSupplierCount();
})



When('iSource- I see starting bid price', async function () {
    await JapaneseAuctionMonitoring.viewStartBidPrice();
})

Then('iSource- I capture starting bid price', async function () {
    await JapaneseAuctionMonitoring.getStartBidPrice();
})



When('iSource- I view overall progress graph', async function () {
    await JapaneseAuctionMonitoring.navigateToOverAllProgressGraph(); 
})

Then('iSource- I see the no of rounds', async function () {
    
    await JapaneseAuctionMonitoring.seeNoOfBarsOnGraph();
})
// Then('iSource- I see suppliers accepting bid in each round', async function () {
//     await JapaneseAuctionMonitoring.enterBidPercentage();
//     await JapaneseAuctionMonitoring.clickSaveBtn();
// })


When('iSource- I see supplier details on grid', async function(){
    await JapaneseAuctionMonitoring.viewSupplierDetailsGrid();
})
Then('iSource- I see suppliers who have rejected & accepted bids', async function(){
    await JapaneseAuctionMonitoring.getSupplierDetails();
})

