import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { ShareEventFlow } from "../../POM/ShareEvent/ShareEventFlow";
import { DatabaseOperations } from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import { Logger } from "log4js";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";

   When('Select {string} in secondary action',async function(ButtonName : string){
    await ShareEventFlow.secondaryAction(ButtonName);
    I.wait(prop.DEFAULT_WAIT)
  });
  
  When('Apply filter as event type {string} and status {string}', async function(eventType : string,eventStatus : string){
    await ShareEventFlow.applyingFilter(eventType,eventStatus);
  });

  Then('iSource- I see clear all button', async function(){
    await ShareEventFlow.seeClearAllButton();
  });

  When('iSource- I search event for event type {string}',async function(eventType:string){
    let eventID:string= Startup.eventId_Map.get(eventType)as string;
    logger.info("EventId id ::::::",eventID)
    await ShareEventFlow.searchEvent(eventID);
    
   });

   Then('iSource- I search event',async function(){
    await ShareEventFlow.searchEvent("1610065145");
   });

   Then('iSource- I clear cookies', async function(){
    await I.clearCookie();
    await I.refreshPage();
   })