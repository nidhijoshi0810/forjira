import { iSourcelmt } from './../../../Framework/FrameworkUtilities/i18nutil/readI18NProp';
import { eventSettingsFlow } from './../../POM/eventSettings/eventSettingsFlow';
import { COE } from './../../../Framework/FrameworkUtilities/COE/COE';
import { ListingPagePrimaryFlow } from './../../POM/ListingPageFullSource/ListingPagePrimaryFlow';
import { QuestionnaireFlow } from './../../POM/Questionnaire/QuestionnaireFlow';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();

When("iSource- I navigate to listing grid",async function(){
    await ListingPagePrimaryFlow.navigateToListingGrid();
})

When("iSource- I search event on grid {string}",async function(eventType : string){
    await ListingPagePrimaryFlow.searchOnGrid(eventType);
})

Then("iSource- I check stage {string}",async function(stage : string){
    await ListingPagePrimaryFlow.verifyEventStage(stage);
})

Then("iSource- I check status {string}",async function(status : string){
    await ListingPagePrimaryFlow.verifyEventStatus(status);
})


Then("iSource- I see primary action as {string}",async function(primaryAction : string){
    await ListingPagePrimaryFlow.verifyPrimaryAction(primaryAction);
})


Then("iSource- I verify navigation for Create draft",async function(){
    await ListingPagePrimaryFlow.createDraftAsAction();
})

Then("iSource- I verify navigation for Edit draft",async function(){
    await ListingPagePrimaryFlow.editDraftAsAction();
})

Then("iSource- I verify navigation for Schedule event",async function(){
    await ListingPagePrimaryFlow.scheduleEventAsAction("Schedule event");
})

Then("iSource- I verify navigation for Pause event",async function(){
    await ListingPagePrimaryFlow.pauseEventAsAction();
})

Then("iSource- I verify navigation for Republish event",async function(){
    await ListingPagePrimaryFlow.scheduleEventAsAction("Republish event");
})

Then("iSource- I verify navigation for View responses",async function(){
    await ListingPagePrimaryFlow.viewResponsesAsAction();
})

Then("iSource- I verify navigation for Award",async function(){
    await ListingPagePrimaryFlow.awardEventAsAction();
})

Then("iSource- I verify navigation for View award summary",async function(){
    await ListingPagePrimaryFlow.viewAwardSummaryAsAction();
})

Then("iSource- I verify navigation for Manage scoresheets",async function(){
    await ListingPagePrimaryFlow.manageScoresheetsAsAction();
})

Then("iSource- I verify navigation for Evaluation Matrix",async function(){
    await ListingPagePrimaryFlow.manageScoresheetsAsAction1();
})

When("iSource- I reopen event",async function(){
    await ListingPagePrimaryFlow.reopenEvent();
})

When("iSource- I select reopen event for single supplier", async function(){
    await ListingPagePrimaryFlow.reopenEventSingleSupplier();
})

When("iSource- I reschedule reopened event", async function(){
    await ListingPagePrimaryFlow.rescheduleReopenEvent();
})

When("iSource- I click event on grid",async function(){
   await ListingPagePrimaryFlow.clickAndEnterEvent();
})

When("iSource- I conclude event",async function(){
   await ListingPagePrimaryFlow.concludeEvent();
})

Then("iSource- I pause event",async function(){
    await ListingPagePrimaryFlow.setPauseDateToCurrentDate();
})

// When("I reopen event",async function(){
    
// })

// When("When I move event to analysis",async function(){
    
// })
