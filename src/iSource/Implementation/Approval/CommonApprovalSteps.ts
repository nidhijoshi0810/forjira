import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {commonLandingFlow} from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import {ApprovalCommonFlow} from "../../POM/Approval/ApprovalCommonFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { loginFlow } from "../../../Framework/FrameworkUtilities/COE/Login/loginFlow";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

Given('iSource- I navigate to Approval Page',async function()  {
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "ApprovalAndReviewTab" ,"");
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    });
Then('iSource- I Switch to new Tab and load Home page',async function()  {
    I.openNewTab();
    I.amOnPage(Startup.testData.get("HomeURL"));
    });
Then('iSource- I Click on recall button',async function()  {
    await ApprovalCommonFlow.clickOnRecall();
    });
Then('iSource- I Click on the Secondary View button',async function()  {
    await ApprovalCommonFlow.ClickOnSecondaryView();
    });

Then('iSource- I Click on the SecondaryAction',async function()  {
    await ApprovalCommonFlow.ClickOnSecondaryAction();
    });
Then('iSource- I click on Fill comment box',async function()  {
    await ApprovalCommonFlow.fillDetailsOnCommentsBox();
    });
Then('iSource- I click on button {string}',async function(actionType:string)  {
    await ApprovalCommonFlow.clickOnCommentbutton(actionType);
    });
Then('iSource- I verify Event Status {string}',async function(status:string)  {
    await ApprovalCommonFlow.verifyEventStatus(status);
    });
Then('iSource- I verify award workflow Event Status {string}',async function(status:string)  {
    await ApprovalCommonFlow.verifyapproveawardEventStatus(status);
    });
Then('iSource- I verify Recalled award workflow Event Status {string}',async function(status:string)  {
    await ApprovalCommonFlow.verifyrecallawardEventStatus(status);
    });


Then('iSource- I verify Recalled Event Status {string}',async function(status:string)  {
    await ApprovalCommonFlow.verifyRecalledStatus(status);
    });
Then('iSource- I Click on the View button',async function()  {
    await ApprovalCommonFlow.clickOnView();
    });
Then('iSource- I Click on the Approve button',async function()  {
    await ApprovalCommonFlow.clickOnApprove();
    });
Then('iSource- I Click on the Reject button',async function()  {
    await ApprovalCommonFlow.clickOnReject();
    });
Given('iSource- I Click on the EventTitle',async function()  {
    await ApprovalCommonFlow.clickOnEventTitle();
    });
Then('iSource- I see Event details level',async function()  {
    await ApprovalCommonFlow.ViewEventDetails();
    });
Then('iSource- I click on OK button',async function()  {
    await ApprovalCommonFlow.clickOnOk();
    });
Then('iSource- I click on Send for Approval',async function()  {
    await ApprovalCommonFlow.clickSaveandGotoWorkflow();
    });
Then('iSource- I Click on the Delegate button',async function()  {
    await ApprovalCommonFlow.ClickOnDelegate();
    });
Then('iSource- I select user',async function()  {
    await ApprovalCommonFlow.selectUser();
    });
    
Then('logged in with another user',async function()  {
    I.clearCookie();
    await loginFlow.loginToApp(Startup.testData.get("delegateUserEmail"),Startup.testData.get("delegatePassword"));
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    await iSourcelmt.matchKeys().then(async function(map) {
    console.log("completed matchkeys")
    Startup.mst_final = map as Map<string, string>;})
    });

Then('iSource- I select Time period',async function()  {
    await ApprovalCommonFlow.selectDate();
    });
Then('iSource- I search for Event {string}', async function(eventType:string)  {
    await ApprovalCommonFlow.searchEvent(eventType);
    });