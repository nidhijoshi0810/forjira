import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { supplierHomeFlow} from "../../POM/SupplierHome/SupplierHomeFlow"
import {QuickSourceSupplierResponse} from "../../POM/SupplierResponse/SupplierResponseFlowQS"
const { I } = inject();

// const supplierHomeFlow = require("../../POM/SupplierHome/SupplierHomeFlow");
// const supplierResponseQS=require("../../POM/SupplierResponse/SupplierResponseFlowQS");

When("iSource- I submit response for Quick Source Inquiry from legacy", async function(){
    // await session('supplierResponseLegacyQS',{browser:Startup.testData.get("browser")},async function(){
        await supplierHomeFlow.loginTOSupplierSideLagacy();
         await supplierHomeFlow.searchEventAndEnterQS(GlobalVariables.QSeventId);
        await QuickSourceSupplierResponse.enterQuantity();
         await QuickSourceSupplierResponse.enterBidValue();
         await QuickSourceSupplierResponse.Questionresponse();
         await QuickSourceSupplierResponse.clickOnSubmit(GlobalVariables.QSeventId);
       
   
    // });

})
