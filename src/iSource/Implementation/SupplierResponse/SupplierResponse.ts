import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import "codeceptjs"; 
import { start } from "repl";
import {SupplierLogin} from "../../POM/SupplierLogin/SupplierLoginFlow";


declare const inject: any; 
const { I } = inject();

import {supplierHomeFlow} from "../../POM/SupplierHome/SupplierHomeFlow";
import {SupplierEforum} from "../../POM/SupplierSideEforum/SupplierSideEforumFlow";
import {SurrogateBidFlow} from "../../POM/SurrogateBid/SurrogateBidFlow";
import {supplierResponseSection} from "../../POM/SupplierResponse/SupplierResponseFlow";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";

// const supplierResponseSection = require("../../POM/SupplierResponse/SupplierResponseFlow")
Given("Logged in on supplier portal from ZSN",async function(){
    
  await SupplierLogin.loginToZSN();

        I.wait(20)
})

Given("Supplier is on view sourcing event screen",async function(){
       await supplierHomeFlow.selectMyEventFromSideMenu();

})
Given("Submit response from legacy {string}", async function(eventType : string){
  const surrogateBidvalue : boolean = false;
    //  await session('supplierResponseLegacy',{browser:Startup.testData.get("browser")},async function(){
       try {
        await supplierHomeFlow.loginTOSupplierSideLagacy();
        await supplierHomeFlow.searchEventAndEnter(eventType);
        await SurrogateBidFlow.clickOnAccept();
        await SurrogateBidFlow.clickOnConfirmParticipation();
        await supplierHomeFlow.clickOnPrepareResponseBtn();
        //await supplierResponseSection.FillResponse();
        await supplierResponseSection.FillResponse(eventType, surrogateBidvalue as boolean);
        await supplierResponseSection.clickOnGoToSubmitResponse();
        await supplierResponseSection.joinBidding(eventType);
        await supplierResponseSection.clickOnSubmitResponse();
        await supplierHomeFlow.logoutFromSupplierSideLegacy();




      //  await createEforumFlow.craeteEforum();

      console.log("Supplier side event id "+GlobalVariables.eventId)
       } catch (error) {
         console.log("Supplier response if failed "+error);
       }
    
    //  });
    

})
Given("iSource- I submit response through Surrogate Bid {string}", async function(eventType : string){
  const surrogateBidvalue : boolean = true;
    await SurrogateBidFlow.clickOnManagerSupplierTab();
    
    await SurrogateBidFlow.clickOnPlaceSurrogateBid();
        await SurrogateBidFlow.clickOnAccept();
        await SurrogateBidFlow.clickOnConfirmParticipation();
         await supplierHomeFlow.clickOnPrepareResponseBtn();
         //await supplierResponseSection.FillResponse(Startup.testData.get("defaultCurrentPricePlaceSurrogateBid") as string);
         await supplierResponseSection.FillResponse(eventType as string, surrogateBidvalue as boolean,Startup.testData.get("defaultCurrentPricePlaceSurrogateBid") as string);
         await supplierResponseSection.clickOnGoToSubmitResponse();
         await supplierResponseSection.clickOnSubmitResponse();
         await SurrogateBidFlow.clickOnReturnToCollect();
       

})


Given("Supplier selects event to participate", async function(){
   await supplierHomeFlow.searchEventAndEnter("RFP");
})
    Given("logout from the application", async function(){
       await SupplierLogin.logout();
    
})
Given("User accepts all terms and condition",async function(){
   await SurrogateBidFlow.clickOnAccept()
   

})

Given("User confirms participation in the event", async function(){
    await SurrogateBidFlow.clickOnConfirmParticipation();

})

Given("User fill all data in the section", async function(){
    await  supplierHomeFlow.clickOnPrepareResponseBtn();
      //await supplierResponseSection.FillResponse();
      await supplierResponseSection.clickOnGoToSubmitResponse();
     
  
})
Given("I submit response", async function(){
    await supplierResponseSection.clickOnSubmitResponse();
     
     })
Given("I join bidding",async function(){
    await  supplierResponseSection.joinBidding(GlobalVariables.auctionType);
     
     })

Given("User submits the response for an event",async function(){
  await supplierHomeFlow.searchEventAndEnter("RFP");

})

When('iSource- I search for {string} event in supplier',async function(eventType : string){
  await supplierHomeFlow.searchEventAndEnter(eventType);
})

When('Create the eforum for {string}', async function(eventType: string) {
    try {
        logger.info("eventType ==============>" + eventType)
        await SurrogateBidFlow.clickOnAccept();
        await SurrogateBidFlow.clickOnConfirmParticipation();
        console.log("Supplier side event id " + GlobalVariables.eventId)
    } catch (error) {
        console.log("Supplier response if failed " + error);
    }
})