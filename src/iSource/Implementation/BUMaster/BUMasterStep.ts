import  "codeceptjs";
declare  const inject:  any;
const { I } = inject();
import { BUMasterFlow } from "../../POM/BUMaster/BUMasterFlow";


When('iSource- I add pricing table for BU Master', async function () {
    await BUMasterFlow.AddPricingTableBUMaster();
});


When('iSource- I add segemnetation colum in pricing table for BU Master', async function () {
    await BUMasterFlow.AddSegmentationPricingTableBUMaster();
});

Then('iSource- I add BU Master Unit', async function(){
    await BUMasterFlow.selectBU();
}) 


Then('iSource- I verify BU selection', async function(){
    await BUMasterFlow.checkBU();
}) 

Then('iSource- I add Category Unit', async function(){
    await BUMasterFlow.selectCategory();
}) 

Then('iSource- I verify Category selection', async function(){
    await BUMasterFlow.checkCategory();
}) 