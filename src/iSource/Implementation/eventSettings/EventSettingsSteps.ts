import "codeceptjs"; 
declare const inject: any; 
const { I } = inject(); 
import { eventSettingsFlow } from "../../POM/eventSettings/eventSettingsFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

Given('iSource- I am on event settings page',async function(){
    await eventSettingsFlow.clickOnEventSettingsTab();
    
  });
  Given('iSource- I schedule event', async function(){
    await eventSettingsFlow.clickOnEventSettingsTab();
    await eventSettingsFlow.setCloseDateToCurrentDateAndTime(50);
    await eventSettingsFlow.clickOnEventSettingsTab();
    I.wait(prop.DEFAULT_LOW_WAIT);
   
  });
  Given('iSource- I reset start date time to current date time plus minutes {string}',async function(minutes : string){
   
    await eventSettingsFlow.resetStartDateAndTimeToCurrentDateAndTime(minutes);
    
  });
  Given('iSource- I reset close date time to current date time plus minutes {string} for RFxEvents',async function(minutes : string){
   
    await eventSettingsFlow.resetCloseDateAndTimeToCurrentDateAndTime(minutes);
   
  });
  Given('iSource- I reset close date time to current date time plus minutes {string}',async function(minutes : string){
   
    await eventSettingsFlow.clickOnEditSettings();
    await eventSettingsFlow.setCloseDateToCurrentDateAndTime(parseInt(minutes));
   
  });
  Given('iSource- I save edit settings for RFxEvents',async function(){
   
    await eventSettingsFlow.saveEditSettingsRFx();
    await eventSettingsFlow.clickOnSend();
    await I.waitForText(await iSourcelmt.getLabel("PauseButtonLabel"));
   
  });
  Given('iSource- I reset close date time to current date time plus minutes {string} for auction',async function(minutes : string){
   
    await eventSettingsFlow.resetCloseDateAndTimeToCurrentDateAndTimeauction(minutes);
   
  });
  When('iSource- I Select primary currency as {string}',async function(option : string){
   await eventSettingsFlow.selectPrimaryCurrency(option);
   });

   Then('iSource- I check primary currency is set as {string}',async function(option : string){
    await eventSettingsFlow.checkPrimaryCurrencyIsSet(option);
    });
    Then('iSource- I check event event status as {string}',async function(status : string){
      await eventSettingsFlow.checkEventStatus(status);
      });
  Given('iSource- I wait for {string}',async function(sec : string){
  I.wait(sec);
  });
  Given('iSource- I select multicurrency as {string}',async function(options : string){
   
    await eventSettingsFlow.selectMultiCurrencyOptions(options);
    
  });
  Given('iSource- I set latency hour as {string}',async function(hour : string){
    await eventSettingsFlow.setLatencyHour(hour);
  });
  Given('iSource- I set latency minute as {string}',async function(hour : string){
    
  });
  Given('Latency between lots should get set to specified units',async function(hour  : string){
    
  });
  When('iSource- I set start date time to current date time minute plus {string}', async function(minute:string) {
   await eventSettingsFlow.setStartDateToCurrentDate(minute)
   });
  When('iSource- I set close date to {string}',async function(dateString : string) {
   await eventSettingsFlow.setCloseDate(dateString)
  });
  
  When('iSource- I set close date to current date', async function() {
   await eventSettingsFlow.setCloseDateToCurrentDate()
  });
  When('iSource- I set close hour to {string}',async function(hour : string) {
   await eventSettingsFlow.setUserdefinedHour(hour);
  });
 
  Then('Date time should set',async  function() {
    
    console.log("Date is set succesfully.")
  });
  
  Given('iSource- I set hour to current hour', async function(){
     await eventSettingsFlow.setCurrentHour();
  });
  
  
  When('iSource- I set minute to current minute plus {string}',async function(minute : string) {
    await eventSettingsFlow.setCurrentMinute(minute)
  });
  
  When('iSource- I set meridiem to current meridiem', async function() {
   
    await eventSettingsFlow.setCurrentMeridiem()
  });

  When('iSource- I click on Ok button',async function() {
    await eventSettingsFlow.clickOnEventSettingsTab();
    // eventSettingsFlow.clickOK();
  });

  Given('iSource- I navigate to non pricing tab',async function() {
    await eventSettingsFlow.verifyAndClickOnNonpricingTypeTabUnderEventSettings();
   });
  
  