import "codeceptjs"; 
import { WhatIfAnalysisFlow } from "../../POM/ScoreSheet/WhatIfAnalysisFlow";
declare const inject: any; 


Given('iSource- I see Manage Scoresheet',async function(){
    await WhatIfAnalysisFlow.clickOnScoresheet();
    
  });

  
When('iSource- I see What If Analysis scenario',async function(){
  await WhatIfAnalysisFlow.clickOnWhatIfAnalysis();
  
});

Then('iSource- I fill scenario details',async function(){
  await WhatIfAnalysisFlow.fillScenarioDetails();
  
});

  
When('iSource- I see scores and supplier advantage',async function(){
  await WhatIfAnalysisFlow.seeScoresandSupplierAdvantage();
  
});

Then('iSource- I change scores',async function(){
  await WhatIfAnalysisFlow.changeScore();
  
});
Then('iSource- I change supplier advantage',async function(){
  await WhatIfAnalysisFlow.changeSupplierAdvantages();
  
});

Then('iSource- I save',async function(){
  await WhatIfAnalysisFlow.saveScore();
  
});