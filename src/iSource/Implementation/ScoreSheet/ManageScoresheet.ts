import "codeceptjs"; 
import { ManageScoresheetFlow } from "../../POM/ScoreSheet/ManageScoresheetFlow";
declare const inject: any; 



Given('iSource- I see Manage Scoresheet',async function(){
  await ManageScoresheetFlow.clickOnScoresheet();
  
});

When('iSource- I see Remind Scorers',async function(){
    await ManageScoresheetFlow.remindScorer();
    
  });

Then('iSource- I send reminder',async function(){
    await ManageScoresheetFlow.sendReminder();
    
  });


  When('iSource- I see Three dot button',async function(){
    await ManageScoresheetFlow.clickOnThreeDotButton();
    
  });


  Then('iSource- I copy scoresheet',async function(){
    await ManageScoresheetFlow.copyScoresheet();
    
  });

  When('iSource- I see reschedule scoresheet',async function(){
    await ManageScoresheetFlow.clickOnRescheduleDate();
    
  });

  Then('iSource- I reschedule scoresheet',async function(){
    await ManageScoresheetFlow.rescheduleScoresheet();
    
  });

  When('iSource- I export scoresheet',async function(){
    await ManageScoresheetFlow.clickOnExport();
    
  });

  When('iSource- I import scoresheet',async function(){
    await ManageScoresheetFlow.clickOnImport();
    
  });

  Then('iSource- I reschedule 360 degree scoresheet',async function(){
    await ManageScoresheetFlow.reschedule360ScoreSheet(); 
  });

