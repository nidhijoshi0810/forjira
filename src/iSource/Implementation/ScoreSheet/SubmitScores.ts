
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();

import {SubmitScoresFlow} from "../../POM/ScoreSheet/SubmitScoresFlow.js";


    When('iSource- I click on ManageScoresheet button',async function(){
    
        await SubmitScoresFlow.clickOnManageScoresheetButton();
    });

    When('iSource- I click on score button',async function(){
        await SubmitScoresFlow.clickOnScoringButton();

    });
    When('iSource- I click on clear score button',async function(){
        await SubmitScoresFlow.clickOnClearScoreButton();
   
    });

    When('iSource- I check view response',async function(){
        await SubmitScoresFlow.checkViewResponses();
    });
    
    When('iSource- I collapse section',async function(){
        await SubmitScoresFlow.collapseSection();
   
    });
    When('iSource- I expand section',async function(){
        await SubmitScoresFlow.expandSection();
   
    });
    When('iSource- I give quantitative scores',async function(){
        await SubmitScoresFlow.quantitativeScoring();
   
    });
    When('iSource- I give qualitative scores',async function(){
        await SubmitScoresFlow.qualitativeScoring();
   
    });



    When('iSource- I click on save score button',async function(){
        await SubmitScoresFlow.clickOnSaveSubmittedScoreButton();
   
    });


    When('iSource- I view full item table',async function(){
        await SubmitScoresFlow.viewFullItemTable()  
    })	

    When('iSource- I view full table',async function(){
        await SubmitScoresFlow.viewFullTable()
    })	

    When('iSource- I read supplier comment',async function(){
        await SubmitScoresFlow.readSupplierComment();
    })

    When('iSource- I enter comment',async function(){
        await SubmitScoresFlow.enterScorerComment();
    })

    When('iSource- I view automated score rule',async function(){
        await SubmitScoresFlow.viewAutomatedScoreRule();
    })

  

    When('iSource- I search by {string}', async function(searchType:string){
        if(searchType === 'Section Name'){
         await SubmitScoresFlow.searchBySectionName();
     } else{
          await SubmitScoresFlow.searchByQuestionName();
        }   
    })  

    Then('iSource- I check search result',async function(){
        await SubmitScoresFlow.checkSearchResult();
    })    

    When('iSource- I click on submit scores',async function(){
        await SubmitScoresFlow.submitScoreButton();
   
    });




 
