// const{I}=inject();
//const PublishScoreSheetFlow = require("\..\..\ScoreSheet\CreateScoresheetQuantitativeFlow");
// const PublishScoreSheetFlow=require("../../POM/ScoreSheet/PublishScoreSheetFlow")

import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {logger} from "../../../Framework/FrameworkUtilities/Logger/logger";
import {QuestionnaireFlow} from "../../POM/Questionnaire/QuestionnaireFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {TEBFlow} from "../../POM/Questionnaire/TEBFlow";
import {PublishScoreSheetFlow} from "../../POM/ScoreSheet/PublishScoreSheetFlow";


When('iSource- I Click on create Scoresheet button',async function(){
    
    await PublishScoreSheetFlow.clickOnCreateScoreSheetbutton();

  });
When('iSource- I change Scoresheet Name',async function(){
    await PublishScoreSheetFlow.changeScoresheetName();
   
});
When('iSource- I give Scoresheet description',async function(){
    await PublishScoreSheetFlow.changeScoresheetDescription();
   
});
When('iSource- I set Maximum Score Value',async function(){
    await PublishScoreSheetFlow.setMaxScoreValue();
   
});
When('iSource- I set scoring type as Quantitative',async function(){
    await PublishScoreSheetFlow.setScoringTypeAsQuantitative();
   
});

When('iSource- I change all sections scoring type to Qualitative individually',async function(){
    await PublishScoreSheetFlow.setScoringTypeAsQualitativeIndividually();
   
});

When('iSource- I set scoring type as Qualitative',async function(){
    await PublishScoreSheetFlow.setScoringTypeAsQualitative();
   
});

// save scoresheet

When('iSource- I Click on save scoresheet button on main page',async function(){
    await PublishScoreSheetFlow.saveScoreSheetOnMainPage();
   
});
Then ('iSource- I see scoresheet is created and saved with changes',async function(){
    await PublishScoreSheetFlow.validateChangesOnMainScoreSheetPage();
});


//section weightage

When('iSource- I assign weightages to sections as per choice',async function(){
    await PublishScoreSheetFlow.assignSectionWeightages();
   
});

Then ('iSource- I see remaining section weightage as 0',async function(){
    await PublishScoreSheetFlow.validateRemainingSectionWeightage();
});

//question weightages
When('iSource- I assign weightages to all type of questions',async function(){
    await PublishScoreSheetFlow.assignQuestionWeightages();
   
});

Then ('iSource- I see individual section weightage as completed',async function(){
    await PublishScoreSheetFlow.validateIndividualSectionWeightage();
});


//manage scorers
When('iSource- I select scorers',async function(){
    await PublishScoreSheetFlow.selectScorers();
   
});
When('iSource- I deselected scorers',async function(){
    await PublishScoreSheetFlow.deselectScorers();
   
});

Then('iSource- I see selected scorers in list of selected', async function(){
    await PublishScoreSheetFlow.validateScorersAddition();
})

// publish scoresheet
When('iSource- I Publish Scoresheet',async function(){
    await PublishScoreSheetFlow.publishScoreSheet();
   
});

Then('iSource- I am on event details page', async function(){
    await PublishScoreSheetFlow.validateWhetherOnEventDetailsPage();
});


// new line   