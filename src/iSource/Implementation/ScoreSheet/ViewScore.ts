
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();

import {ViewScoreFlow} from "../../POM/ScoreSheet/ViewScoreFlow.js";
import {SubmitScoresFlow} from "../../POM/ScoreSheet/SubmitScoresFlow.js";

// When('iSource- I click on ManageScoresheet button',async function(){
    
//     await SubmitScoresFlow.clickOnManageScoresheetButton();
    //since already on manage ss page
//   });

When('iSource- I click on view scores button',async function(){
    await ViewScoreFlow.clickOnViewScoreButton();
   
});
Then('iSource- I see submit score', async function(){
    await ViewScoreFlow.checkViewScore();
})
When('iSource- I click on weighted score tab',async function(){
    await ViewScoreFlow.clickOnWeightedScoreTab();
   
});
When('iSource- I click on absolute score tab',async function(){
    await ViewScoreFlow.clickOnAbsoluteScoreTab();
   
});
When('iSource- I click on settings icon',async function(){
    await ViewScoreFlow.clickOnSettingsIcon();
   
});

When('iSource- I remind scorers who have not scored', async function(){
    await ViewScoreFlow.remindScorersWhoHaveNotScored();
});

When('iSource- I use search text box',async function(){
    await ViewScoreFlow.useSearchTextBox();
   
});

When('iSource- I use apply and reset button',async function(){
    await ViewScoreFlow.applyResetButtons();
   
});

When('iSource- I click on export button for view score Page',async function(){
    await ViewScoreFlow.exportScoreButton();
   
});

When('iSource- I click on revoke score', async function(){
    await ViewScoreFlow.revokeScore();
});

When('iSource- I Export score from Manage ScoreSheet page', async function(){
    await ViewScoreFlow.exportImportScoreButton()
});          
