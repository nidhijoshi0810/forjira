import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { ListingPagePrimaryFlow } from "../../POM/ListingPageFullSource/ListingPagePrimaryFlow";

  When('iSource- I pause from secondary action', async function(){
    await ListingPagePrimaryFlow.setPauseDateToCurrentDate("SecondaryAction");
  });
