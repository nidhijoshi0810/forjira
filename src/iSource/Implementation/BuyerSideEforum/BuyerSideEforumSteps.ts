import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { BuyerEforum } from '../../POM/BuyerSideEforum/BuyerSideEforumFlow';
import { StringsReturn } from "webdriver";



      Given('iSource- I search publish event',async function()  {
        await BuyerEforum.searchForEvent();
        
      });
    
      Then('iSource- I select event',async function()  {
        await BuyerEforum.selectEvent()
        
      });

      Then('iSource- I navigate to eforum',async function()  {
        await BuyerEforum.navigateToEfroum();  
      });


      When('iSource- I create new topic as {string}',async function(type:string){
        await BuyerEforum.createNewTopic(type);
      });

      Then('iSource- I check topic is created as {string}',async function(type:string){
        await BuyerEforum.checkIfTopicIsCreated(type);
      })

      When('iSource- I select supplier',async function()  {
        await BuyerEforum.addSupplier();  
      });

      When('iSource- I enter post in public topic',async function(){
        await BuyerEforum.addPublicPostWithAttachment();
      })

      Then('iSource- I check post',async function(){
        await BuyerEforum.checkIfPostIsAdded();
      })
    

      // Then('iSource- I select type as {string}',async function(type:string)  {
      //   // await BuyerEforum.navigateToEfroum();  
      // });

      // Then('iSource- I enter Description',async function()  {
      //   await BuyerEforum.enterDiscription();  
      // });

      // Then('iSource- I enter email signature',async function()  {
      //   await BuyerEforum.enterSignature();  
      // });

      When('iSource- I click on delete topic', async function(){
         await BuyerEforum.deleteTopic();
      })

      When('iSource- I click on edit topic', async function(){
        await BuyerEforum.editTopic();
      })

      When('iSource- I select public topic', async function(){
        await BuyerEforum.selectPublicTopic();
      })

      When('iSource- I update supplier',async function(){
        await BuyerEforum.updateParticepent();
      })

      Then('iSource- I check supplier updated',async function(){
        await BuyerEforum.checkIfSupplierUpdated()
      })

      When('iSource- I select message',async function(){
        await BuyerEforum.clickActionMenuBtn();
      })

      When('iSource- I delete message',async function(){
        await BuyerEforum.selectDeleteOption();
      })

      Then('iSource- I check delete message',async function(){
        await BuyerEforum.checkIfMessageDeleted();
      })

      When('iSource- I edit message', async function(){
        await BuyerEforum.selectEditOption();
        await BuyerEforum.enterEditText();
        await BuyerEforum.clickDoneBtn();
      })

      Then('iSource- I check edit message',async function(){
        await BuyerEforum.checkIfMessageEdited()
      })

      When('iSource- I select message for approve',async function(){
        await BuyerEforum.checkForApprovalMessage();
      })
       
      When('iSource- I approve supplier posted message', async function(){
        await BuyerEforum.clickOnApprovePost();
      })

      When('iSource- I edit supplier posted message', async function(){
        await BuyerEforum.editApprovePost();
      })

      When('iSource- I delete supplier posted message', async function(){
        await BuyerEforum.clickOnReject();
      })

      Then('iSource- I check for new notification', async function(){
        await BuyerEforum.checkNotificationForNewMessage();
      })

      Given('iSource- I am on eforum page', async function(){
        await BuyerEforum.checkforCurrentPage();
      })

      Given('iSource- I am on public topic', async function(){
        await BuyerEforum.checkforCurrentPage();
      })

      Then('iSource- I check topic is edited',async function(){
        await BuyerEforum.checkIfTopicEdited();
      })
      Then('iSource- I check for new Public notification',async function(){
        await BuyerEforum.checkNotificationForNewPublicMessage();
      })

      Then('iSource- I check topic is deleted',async function(){
        await BuyerEforum.checkIfTopicDeleted();
      })

      Then('iSource- I view public topic supplier message',async function(){
        await BuyerEforum.clickOnPublicMessage();
      })

      Then('iSource- I verify public topic supplier message',async function(){
        await BuyerEforum.verifyPublicMessage();
      })



      