import "codeceptjs"; 
import { commonLandingFlow } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { SealedBidFlow } from "../../POM/AdminSetting_PostSendForApproval/SealedBidFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { WorkFlowFlow } from "../../POM/WorkFlow/WorkFlowFlow";
import {SupplierLogin} from "../../POM/SupplierLogin/SupplierLoginFlow";
import {supplierHomeFlow} from "../../POM/SupplierHome/SupplierHomeFlow";
import {SurrogateBidFlow} from "../../POM/SurrogateBid/SurrogateBidFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
declare const inject: any; 
const { I } = inject();

    
   

 Then('iSource- I go to event setting and select {string}', async function(labelToBeClicked : string){  
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await SealedBidFlow.clickOnEventSetting(labelToBeClicked);

   });

   When('iSource- I click on {string} of sealed bid and save the setting',async function(OptionName : string){
    await SealedBidFlow.YesNoOption(OptionName);
  });

  When('iSource- I click on {string} of open close date and save the setting',async function(OptionName1 : string){
    await SealedBidFlow.YesNoOption1(OptionName1);
  });

  When('iSource- I click on {string} of eforum and save the setting',async function(OptionName2 : string){
    await SealedBidFlow.YesNoOption2(OptionName2);
  });

  Then ('iSource- I search for Workflow by name {string}', async function(WorkflowName:string){
    await SealedBidFlow.searchWorkflowbyName(WorkflowName);
    await SealedBidFlow.clickOnAction1();
    await SealedBidFlow.ActivateWorkflow1();
  });

  Then ('iSource- I login from supplier side and enter into {string} event', async function(eventType : string){
    await SealedBidFlow.loginTOSupplierSideLagacy111();
        await supplierHomeFlow.searchEventAndEnter(eventType);
        await SurrogateBidFlow.clickOnAccept();
        await SurrogateBidFlow.clickOnConfirmParticipation();
  
  });

  Then ('iSource- I click on eforum link on supplier page and perform check', async function(){
    await SealedBidFlow.SuppEforumBtnClick();
  });

  When ('iSource- I see that sealed bid settings are not disabled', async function(){
    await SealedBidFlow.SealedBidCheck();
  });

  When ('iSource- I perform approval action', async function(){
    await SealedBidFlow.ApprAction();
  });

  Then ('iSource- I navigate back to home', async function(){
    await SealedBidFlow.HomeNav();
  });

  Then ('iSource- I click on workflow tab', async function(){
    await SealedBidFlow.WrkflwTab();
  });


   