import "codeceptjs"; 
declare const inject: any; 
import { GlobalVariables } from "../../dataCreation/GlobalVariables";

const { I } = inject();
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { BidAnalysisFlow } from "../../POM/BidAnalysis/BidAnalysisFlow";
import { AnalysisUsing360 } from "../../POM/BidAnalysis/AnalysisUsing360";
// import { awardSummaryFlow } from "../../POM/BidAnalysis/AwardSummaryFlow";
import { BidAnalysis } from "../../POM/BidAnalysis/BidAnalysisLocators";
import { CommonFunctions } from "./../../isourceCommonFunctions/CommonFunctions";
import {iSourcelmt  } from "./../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { AwardWordFlow } from "../../POM/AwardWorkFlow/AwardWorkFlow";
import { AwardSummary } from "../../POM/BidAnalysis/AwardSummaryFlow";


When ('iSource- I click on Move to Analysis button',async function(){
    await BidAnalysisFlow.clickOnMoveToAnalysisButton();
    
   });

   Then ('iSource- I move to {string} tab',async function(tabName : string){
await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabelFromKey(tabName) as string);
});
Then ('iSource- I open 360 degree scenario',async function(){
  await AnalysisUsing360.open360Scenario();
  });

  Then ('iSource- I select suppliers',async function(){
    await AnalysisUsing360.checkSupplierCheckbox();
    });
    Then ('iSource- I click on award event',async function(){
      await AnalysisUsing360.clickOnAwardEventFor360();
      });
   When ('iSource- I move to Pricing Analysis tab',async function(){

    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    // I.click(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalyzeTab),Startup.testData.get("analyzeTab")));
    await BidAnalysisFlow.clickAnalysisTabs("Pricing Analysis");
    
   });

   When ('iSource- I select a scenario to analyze', async function(){
    await BidAnalysisFlow.selectOneScenarioToAnalyze();
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
   });

   When ('iSource- I create optimized scenario by applying filter on suppliers and items included in BA', async function(){
    await BidAnalysisFlow.createOptimizedScenario();
   });
   Then('iSource- I save scenario as {string}', async function(scenarioName : string){
    await BidAnalysisFlow.SaveScenario(scenarioName);
   });
   When('iSource- I view award summary', async function(){
    await AwardSummary.clickOnViewAwradSummaryBtn();
    await AwardSummary.fetchAwardSummary();
   });
   When('iSource- I split quantity', async function(){
     I.wait(60);
    await BidAnalysisFlow.clickOnSplitQunatity();
    // await BidAnalysisFlow.clickOnApplyBtn();
   });
   When('iSource- I award the scenario', async function(){
    
    await BidAnalysisFlow.awardScenario();
    // await AwardWordFlow.awardEventUsingWorkFlow();
    //    await  BidAnalysisFlow.navigateToCommentsAndAttachmentsSection();
    //    await BidAnalysisFlow.enterAwardingComments();
    //    await BidAnalysisFlow.attachFileInAwarding();
    //    await BidAnalysisFlow.awardAndSend();
     });
   When('iSource- I award scenario using 360', async function(){
    
    // await BidAnalysisFlow.awardScenario();
    await AwardWordFlow.awardEventUsingWorkFlow();
       await  BidAnalysisFlow.navigateToCommentsAndAttachmentsSection();
       await BidAnalysisFlow.enterAwardingComments();
       await BidAnalysisFlow.attachFileInAwarding();
       await BidAnalysisFlow.awardAndSend();
     });
     When('iSource- I award scenario using workflow 360', async function(){
      await AwardWordFlow.awardEventUsingWorkFlowApproveRecall();
      });
     When('iSource- I send regret email', async function(){
    
      await BidAnalysisFlow.sendRegretEmail();
       });
   When('iSource- I add formula as {string}', async function(formula : string){
    
    await BidAnalysisFlow.modifyFormula(formula);
     });
   When('iSource- I modify the optimized scenario filters', async function(){
    await BidAnalysisFlow.modifyScenario();
   });

   When('iSource- I export a scenario', async function(){
    await BidAnalysisFlow.exportScenario();
   });

   When('iSource- I view {string} at all levels {string}', async function(scenarioName : string,customColumnAdded : string){
    await BidAnalysisFlow.checkScenarioAtAllLevels(scenarioName,customColumnAdded);
   });
   
   When('iSource- I delete scenario {string}', async function(scenarioName : string){
    await BidAnalysisFlow.deleteScenario(scenarioName);
   });

   When ('iSource- I navigate to Analyze screen', async function(){
    await BidAnalysisFlow.clickAnalysisMenu();
   });

   When('iSource- I create scheduled optimization', async function(){
   let currentSupplierCount= (await BidAnalysisFlow.getCurrentItemSupplierNumber())[0];
   let currentItemcount=(await BidAnalysisFlow.getCurrentItemSupplierNumber())[1];
  await BidAnalysisFlow.clickOnChangeScope();
  await BidAnalysisFlow.excludeSupplier();
  await BidAnalysisFlow.NavigateToItemTab();
  await BidAnalysisFlow.excludeItem();
  await BidAnalysisFlow.clickOnRunOptimizationBtn();
//   await BidAnalysisFlow.clickOnRunOptimizationNow();
  await  BidAnalysisFlow.enterDesc();
  await BidAnalysisFlow.clickOnScheduleBtn();
  await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneButtonLabel") as string);


   });

   When('Created scheduled scenrios are in lising section', async function(){
       await BidAnalysisFlow.clickAnalysisMenu();
       I.see(GlobalVariables.scenarioName, Startup.uiElements.get(BidAnalysis.scheduledScenario));
       
   });

   When('iSource- I move back to buyer side', async  function(){
        I.wait(prop,["DEFAULT_MEDIUM_WAIT"]);
   });

    When('iSource- I move to 360 degree Analysis tab', async function () {
      await BidAnalysisFlow.verify360DegreeAnalysis();
    });

    Then('iSource- I check for 360 degree analysis', async function () {
      await AnalysisUsing360.checkFor360Analysis();
    });

    Then('iSource- I move to Analysis for Cherry Pick', async function () {
      await BidAnalysisFlow.BOScenarioCherryPick();
    });

    // this.scenarioName = await BidAnalysisFlow.selectOneScenarioToAnalyze();
    