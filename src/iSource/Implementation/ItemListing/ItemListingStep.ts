import "codeceptjs";
import { QuestionnaireFlow } from "../../POM/Questionnaire/QuestionnaireFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { ItemListingFlow } from "../../POM/ItemListing/ItemListingFlow";
declare const inject: any;
const { I } = inject();


Then('iSource- I see add pricing table option', async function () {
    await ItemListingFlow.checkPricingTableIsPresent();

});

When('iSource- I delete pricing table', async function () {
    await ItemListingFlow.DeletePricingTable();
});

Then('iSource- I hide pricing column under TCB', async function () {
    // await QuestionnaireFlow.AddPricingTablewithTCB();
    await ItemListingFlow.hidePricingColUnderTCB();
    I.wait(prop.DEFAULT_WAIT)
});

Then('iSource- I see if the TCB is added', async function () {
    await ItemListingFlow.SeePricingTablewithTCB();
});

When('iSource- I add pricing table with TCB', async function () {
    await ItemListingFlow.AddPricingTablewithTCB();
});

Then('iSource- I see if the Item Specification is deleted', async function () {
    await ItemListingFlow.validateItemSpecificationIsNotPresent();
});

When('iSource- I delete pricing table with Item Specification', async function () {
    await ItemListingFlow.DeletePricingTablewithItemSpecification();
});

Then('iSource- I see if the Item Specification is added', async function () {
    await ItemListingFlow.validateItemSpecificationAddition();
});


When('iSource- I add pricing table with Item Specification', async function () {
    await ItemListingFlow.AddPricingTablewithItemSpecification();
});

Then('iSource- I see if the Custom Columns is deleted', async function () {
    await ItemListingFlow.CheckDeletePricingTablewithCustomColumns();
});


When('iSource- I delete pricing table with Custom Columns', async function () {
    await ItemListingFlow.DeletePricingTablewithCustomColumns();
});


Then('iSource- I see if the Custom Columns is added', async function () {
    await ItemListingFlow.SeePricingTablewithCustomColumns();
});


When('iSource- I add pricing table with Custom Columns', async function () {
    await ItemListingFlow.AddPricingTablewithCustomColumns();
});