import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { GlobalVariables } from "../dataCreation/GlobalVariables";
import { coe } from "../../Framework/FrameworkUtilities/COE/COE";
import { CreateQuickSourceEvent } from "../POM/CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import { SupplierGroupFlow } from "../POM/CreateQuickSourceEvent/CreateQuickSourceEventFlow";
import { CreateEventFlow } from "../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../Framework/FrameworkUtilities/Startup/Startup";
import { supplierHomeFlow } from "../POM/SupplierHome/SupplierHomeFlow";
import { prop } from "../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { CommonLandingAction } from "../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingAction";
import { iSourcelmt } from "../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { ShareEventFlow } from "../POM/ShareEvent/ShareEventFlow";
import {QuicksourceAdminFlow} from "../POM/QuicksourceAdmin/QuicksourceAdminFlow";

Then("iSource- I  navigate to quicksource admin settings",async function(){
    await QuicksourceAdminFlow.NavToQuickAdmin();
});

Then("iSource- I click on {string} of create potential supplier",async function(YesNoOption:string){
    await QuicksourceAdminFlow.ClickOnYesNo(YesNoOption);
});

Then("iSource- I click on potential supplier Settings",async function(YesNoOption:string){
    await QuicksourceAdminFlow.PotSuppSettings();
});

Then("iSource- I check if exception is displayed",async function(){
    await QuicksourceAdminFlow.itemDetailsException();
});

Then("iSource- I add a Quicksource item",async function(){
    await QuicksourceAdminFlow.QuickItem();
    await SupplierGroupFlow.addQuantity()
    await SupplierGroupFlow.addUOM()
    await SupplierGroupFlow.addCurrentPrice()
});

Then("iSource- I send without mandatory details",async function(){
    await QuicksourceAdminFlow.sendWithoutDetails();
});

Then("iSource- I click on recommended suppliers",async function(){
    await QuicksourceAdminFlow.RecommSuppButton();
});

Then("iSource- I add a recommended supplier",async function(){
    await QuicksourceAdminFlow.AddRecommSupp();
});

Then("iSource- I remove a recommended supplier",async function(){
    await QuicksourceAdminFlow.removeRecommSupp();
});

Then("iSource- I close smart assist popup",async function(){
    await QuicksourceAdminFlow.closeSmartAssist();
});

Then("iSource- I check if recommended supplier is added",async function(){
    await QuicksourceAdminFlow.recommAddCheck();
});

Then("iSource- I check if recommended supplier is removed",async function(){
    await QuicksourceAdminFlow.recommRemoveCheck();
});

Then("iSource- I try to set past date time",async function(){
    await QuicksourceAdminFlow.sendWithPastDateTime();
});

Then("iSource- I create a potential supplier when setting is No",async function(){
    await QuicksourceAdminFlow.PotentialSuppVerify();
});

Then("iSource- I try to award a supplier who has not sent quotes",async function(){
    await QuicksourceAdminFlow.AwardNoQuoteSupp();
});

Then("iSource- I check real time savings",async function(){
    await QuicksourceAdminFlow.realTimeSavings();
});

Then("iSource- I allocate specific quantity to the supplier",async function(){
    await QuicksourceAdminFlow.allocateSpecQty();
});

Then("iSource- I move to suppliers tab",async function(){
    await SupplierGroupFlow.clickOnSuppliersTab()
});
