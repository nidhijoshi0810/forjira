import "codeceptjs"; 
declare const inject: any; 
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { supplierHomeFlow } from"../../POM/SupplierHome/SupplierHomeFlow";
import { BidReco } from "./../../POM/BidReco/BidREcoFlow";
import { supplierResponseSection } from "../../POM/SupplierResponse/SupplierResponseFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";


When('iSource- I set bid reco start date time to current date time plus minutes {string}',async function(minutesToAdd:string)
{
  await BidReco.setBidRecoStartDateToCurrentDate(minutesToAdd);
});
When('iSource- I navigate to bid Reconciliation',async function()
{
  await BidReco.clickOnBidReco();
});
Then('iSource- I close bid reco datepicker',async function()
{
  await  BidReco.closeBidRecoDatePicker();
});

When('iSource- I set bid reco end date time to current date time plus minutes {string}',async function(minutesToAdd:string)
{
 await BidReco.setBidRecoEnndDateToCutrrentDateAndTime(minutesToAdd);
  
});
When('iSource- I open edit bid reconciliation popup',async function()
{
 await BidReco.clickOnEditBidReco();
  
});
Then('iSource- I schedule bid reco',async function(){
  await BidReco.clickOnSchedule();
  await BidReco.clickOnSend();
});

Then('iSource- I schedule edit bid reco',async function(){
  await BidReco.clickOnSchedule();
 
});
When("iSource- I logout from supplier side legacy",async function(){
  await supplierHomeFlow.logoutFromSupplierSideLegacy();

});
Then('iSource- I perform bid reco from leagcy for {string}',async function(eventName : string)
{
    // await session('BidRecoLegacy',{browser: prop.browser},async function(){
        try {
         await supplierHomeFlow.loginTOSupplierSideLagacy();
         await supplierHomeFlow.searchEventAndEnter(eventName);
         await supplierHomeFlow.clickOnPrepareResponseBtn();
         await BidReco.enterUnitCostBidreco();
         await supplierResponseSection.saveBidreco();
         await supplierResponseSection.clickOnGoToSubmitResponse();
 
       console.log("Supplier side event id "+GlobalVariables.eventId)
        } catch (error) {
          console.log("Supplier response if failed "+error);
        }
     
      // });
});

