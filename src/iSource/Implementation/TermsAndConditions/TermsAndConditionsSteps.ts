import "codeceptjs"; 
import { AttachmentsFlow } from "../../POM/EventLevelTermsAndCondtions/EventLevelTermsAndCondtionsFlow";
declare const inject: any;
const { I } = inject();

Given('iSource- I am on terms and condition section page',async function()  {
  await AttachmentsFlow.clickOnTandCondTab();
    
  });

  Then('iSource- I attach file in terms and condition',async function()  {
    await AttachmentsFlow.attachFile();
    
  });
  