import "codeceptjs"; 
declare const inject: any; 

import { AwardWordFlow } from "../../POM/AwardWorkFlow/AwardWorkFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";

When('iSource- I search for event',async function(){
    
    await AwardWordFlow.searchOnGrid("","")
    
});
Then('iSource- I am on Workflow Page',async function(){
    await AwardWordFlow.amOnWorkFlowPage()
});
Then('iSource- I Recall the Award work flow',async function(){
    await AwardWordFlow.RecallWorkflow()
});
Then('iSource- I Send for Approval',async function(){
    await AwardWordFlow.sendforApproval()
});
Then('iSource- I Accept Request',async function(){
    await AwardWordFlow.acceptRequest()
});
Then('iSource- I am on Analyze page',async function(){
    await AwardWordFlow.amonAnalyzepage()
})