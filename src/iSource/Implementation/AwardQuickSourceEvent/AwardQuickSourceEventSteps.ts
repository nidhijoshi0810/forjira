import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
declare const inject: any; 
const { I } = inject();
import { AwardQuickSource } from "../../POM/AwardQuickSourceEvent/AwardQuickSourceEventFlow";


Given('iSource- I am view Quicksource event details', async function() {
    await AwardQuickSource.eventDetails();
});
When('iSource- I click on Go Response', async function() {
    await AwardQuickSource.clickGoResponseButton();
});
When('iSource- I Select Supplier to Award', async function() {
    await AwardQuickSource.selectSupplierToAward();
});
When('iSource- I Award Scenario', async function() {
    await AwardQuickSource.clickAwardButton();
});
When('iSource- I click on view award note', async function() {
    await AwardQuickSource.clickViewAwardNote();
});