import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { SupplierGroupFlow } from "../../POM/SupplierGroup/SupplierGroupFlow";


import "codeceptjs"; 
import { ViewAllSuppliersSteps } from "../../POM/BayerSideSupplier/ViewAllSupplierFlow";
import { QuestionnaireFlow } from "../../POM/Questionnaire/QuestionnaireFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
declare const inject: any;
const { I } = inject();


When("I add additional details",async function(){
    await SupplierGroupFlow.addAditionalDetails()
    
});
When('iSource- I add suppliers',async function(){
   await  ViewAllSuppliersSteps.clickOnAddSuppliersTab();
   await ViewAllSuppliersSteps.navigateToViewAllSupliersPopup();
    await ViewAllSuppliersSteps.selectSuppliersFromGrid(Startup.testData.get("defaultSupplierText")as string);
    // ViewAllSuppliersSteps.performPublish()

});
When('iSource- I select all supplier in list',async function(){
    await SupplierGroupFlow.selectAllInSelectedSupplier()
});
When('iSource- I am on Supplier Group Popup',async function(){
    await SupplierGroupFlow.clickCreateGroupButton()
});
When('iSource- I enter supplier group name',async function(){
    await SupplierGroupFlow.enterGroupName()
});
When('iSource- I select supplier from Create Group',async function(){
    await SupplierGroupFlow.clickOnSelectAllOnCreateGroupPopup()
    await SupplierGroupFlow.clickOnCreateCreateSupplierGroup()
    await SupplierGroupFlow.clickOnDoneCreateGroup()
});
When('iSource- I select supplier group from view all suppliers',async function(){
    await SupplierGroupFlow.clickOnSupplierGroupTab()
    // SupplierGroupFlow.selectSupplierGroupFromGrid()
    await SupplierGroupFlow.selectSupplierGroupFromGriden("Group1","Group Name");
    
});
When('iSource- I close view all supplier popup',async function(){
    await SupplierGroupFlow.closeViewAllSupplierPopup();
});
When('iSource- I click on Freeze',async function(){
    await QuestionnaireFlow.clickFreeze()
});
Given('iSource- I navigate to View all supplier',async function(){
    await SupplierGroupFlow.navigateToViewAllSupliersPopup();
  });