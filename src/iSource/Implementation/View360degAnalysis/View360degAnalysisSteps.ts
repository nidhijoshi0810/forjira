import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { viewResponsesFlow } from "../../POM/viewResponses/ViewResponsesFlow";
import { view360degAnalysislow } from "../../POM/View360degAnalysis/View360degAnalysisFlow";


   Then('iSource- I see Supplier Details',async function(){    
    await view360degAnalysislow.SeeViewSupplierDetails();
  });