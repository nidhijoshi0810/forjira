import "codeceptjs"; 

import { commonLandingFlow } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { ClassicRedirectionFlow } from "../../POM/ClassicRedirection/ClassicRedirectionFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
declare const inject: any; 
const { I } = inject();

Given('iSource- I load Home page',async function(){
   I.amOnPage(Startup.testData.get("HomeURL"));
   });

Then('iSource- I verify am on Approval and Reviews page',async function(){
    await ClassicRedirectionFlow.verifyApproval();
    });

When('iSource- I navigate to Sourcing setting',async function(){
   await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "SettingsLabelKey", "SourcingSettingLabelKey");
  
});

Then('iSource- I am sourcing setting page and verify {string}',async function (verifyingFactor: string){
    await ClassicRedirectionFlow.verifySourcingSetting(verifyingFactor);
   // await ClassicRedirectionFlow.WindowSwitchfunction0();
   await I.closeCurrentTab();
});



When('iSource- I navigate to Quick Source Controls',async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "SettingsLabelKey", "Quick Source Controls");
     //  await ClassicRedirectionFlow.WindowSwitchfunction0();
});

When('iSource- I naviagte to Offline Exports',async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "Offline Exports","");
   // await ClassicRedirectionFlow.WindowSwitchfunction0();
 });

 When('iSource- I naviagte to Report',async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "ReportsLabelKey","");
    //await ClassicRedirectionFlow.WindowSwitchfunction0();
 });

 When('iSource- I naviagte to Manage User and Suppliers- Supplier Company List',async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "Manage Users and Suppliers", "Supplier Company List");
   // await ClassicRedirectionFlow.WindowSwitchfunction0();
 });

 When('iSource- I naviagte to Manage User and Suppliers- Supplier Contact Groups',async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "Manage Users and Suppliers", "Supplier Contact Groups");
   // await ClassicRedirectionFlow.WindowSwitchfunction0();
 });

 When('iSource- I naviagte to Manage User and Suppliers- Internal Users',async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "Manage Users and Suppliers", "Internal Users");
   // await ClassicRedirectionFlow.WindowSwitchfunction0();
 });