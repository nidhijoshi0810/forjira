import "codeceptjs";
import { EventSummaryFlow } from "../../POM/EventSummary/EventSummaryFlow";
declare const inject: any;

Then('iSource- I jump to event details via Create', async function () {
  await EventSummaryFlow.clickOnCreate();
});
Given('iSource- I am on Event Summary Page', async function () {
    await EventSummaryFlow.clickOnEventsummary();
});
 
Then('iSource- I verify am on event summary page', async function () {
  await EventSummaryFlow.verifyEventsummary();
});

Given('iSource- I click on Analysis', async function () {
  await EventSummaryFlow.clickOnAnalysis();
});

When('iSource- I filter data based on {string} as {string}', async function (filterType: string, subfilterType:string) {
    await EventSummaryFlow.selectSubfilter(filterType,subfilterType);
});
When('iSource- I filter data based on {string}', async function (filterType: string) {
  await EventSummaryFlow.selectBuyerfilter(filterType);
});
When('iSource- I apply filter based on {string} as actiivities performed by other users on that event {string}', async function (filterType: string, subfilterType: string) {
  await EventSummaryFlow.selectOtherUserfilter(filterType, subfilterType);
});

Then('iSource- I verify event stage as {string}',async function(Word:string){
  await EventSummaryFlow.verifyStage(Word);
});

Then('iSource- I verify supplier response status as {string}',async function(Word:string){
  await EventSummaryFlow.verifyResponse(Word);
});

Then('iSource- I verify the buyer name',async function(){
  await EventSummaryFlow.verifyBuyer();
});

Then('iSource- I verify the activities of other users',async function(){
  await EventSummaryFlow.verifyOtherUSers();
});

Then('iSource- I verify the Timeline Changes by seeing {string}',async function(Word:string){
  await EventSummaryFlow.verifyDate(Word);
});
 
Then('iSource- I verify the quickFilters by seeing {string}', async function(Word:string){
    await EventSummaryFlow.verifyResponse(Word);
});
    
Then('iSource- I clear the filter {string}',async function(filterType:string){
    await EventSummaryFlow.clearfilter(filterType);
  });

When('iSource- I download event summary details excel', async function () {
    await EventSummaryFlow.DownloadExcel();

});

