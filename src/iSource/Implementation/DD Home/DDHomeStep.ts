import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
import { CreateQuickSourceEvent } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import { SupplierGroupFlow } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventFlow";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { supplierHomeFlow } from "../../POM/SupplierHome/SupplierHomeFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { CommonLandingAction } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingAction";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { ShareEventFlow } from "../../POM/ShareEvent/ShareEventFlow";
import { DewHomeSearchField } from "dd-cc-zycus-automation/dist/components/dewHomeSearchField"
import { DDHomeIsource } from "../../POM/DD Home/DDHomeFlow";
import { commonLandingFlow } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourceDDHome } from "../../POM/DD Home/DDHomeLocators";


When('iSource- I search {string} on DD Home page', async function( dataToSearch: string ){
    await DDHomeIsource.DDHomeSearch(dataToSearch);
})

Then('iSource- Verify I see {string}', async function(textToVerify : string){
    await DDHomeIsource.verifyText(textToVerify);
})

When('iSource- navigate back to DDHome', async function(){
    await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
    I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(iSourceDDHome.SideMenuBar) as string,
                                                                                await iSourcelmt.getLabel("HOME_TABNAME") as string));    
})

When('iSource- navigate to My Desk', async function(){
    await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
    I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(iSourceDDHome.SideMenuBar) as string,
                                                                                await iSourcelmt.getLabel("MyDesk") as string)); 
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await I.wait(5);    
})

When('iSource- navigate tabSet {string}',async function(tabsetName : string){
    await DDHomeIsource.iSourceTabSetNavigation(tabsetName);
})

When('iSource- click on Sourcing Event', async function(){
    await I.refreshPage();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await DDHomeIsource.clickSourcingEvent();
    await I.wait(5);
})

Then('iSource- Verify I am on full source listing page', async function(){
    await DDHomeIsource.seeAllSourcingEvents();
})

When('iSource- Open Quick link customization', async function(){
    await DDHomeIsource.clickQuicklinkAndNavigateCustomizePopup();
    await I.wait(5);
})

When('iSource- Add Full Source to Quick Link and apply', async function(){
    await DDHomeIsource.addFullSourceToQuickLink();
    await I.wait(5);
})

When('iSource- Navigate to Full source using Quick link', async function(){
    await DDHomeIsource.NavigateUsingQuickLink();
    await I.wait(5);
})
