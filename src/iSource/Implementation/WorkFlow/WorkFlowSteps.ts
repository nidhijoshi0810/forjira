import "codeceptjs"; 
import { WorkFlowFlow } from "../../POM/WorkFlow/WorkFlowFlow";
declare const inject: any;
const { I } = inject();
// const CreateEventFlow = require("../../POM/CreateEvent/CreateEventFlow");
// const QuestionnaireLocators = require("../../POM/Questionnaire/QuestionnaireLocators");
Then('iSource- I Save and goto workflow else publish',async function(){
    await WorkFlowFlow.clickSaveandGotoFlowElsePuplish()
});
When('iSource- I select date for fullsource Last Date',async function(){
    await WorkFlowFlow.selectLastDateforFullsource()
});

Then('iSource- I click freeze',async function(){
    await WorkFlowFlow.clickFreeze()
})

Then('iSource- I click workflow defination tab',async function(){
    await WorkFlowFlow.clickWorkFlowDef();
})

Then ('iSource- I search for Workflow {string}', async function(WorkflowName:string){
    await WorkFlowFlow.searchWorkflow(WorkflowName);
  });
Then ('iSource- I click on workflow Action', async function(){
    await WorkFlowFlow.clickOnAction();
  });
Then ('iSource- I Activate workflow', async function(){
    await WorkFlowFlow.ActivateWorkflow();
  });
  Then ('iSource- I see workflow Activated', async function(){
    await WorkFlowFlow.VerifyActivateWorkflow();
  });
  Then ('iSource- I click on copy workflow', async function(){
    await WorkFlowFlow.CopyWorkflow();
  });
  Then ('iSource- I check whether I am on create copy workflow page',async function(){
    await WorkFlowFlow.CreateCopyCheck();
  });
  Then ('iSource- I click on workflow save', async function(){
    await WorkFlowFlow.saveWorkflow();
  });
  
  Then ('iSource- I Deactivate workflow', async function(){
    await WorkFlowFlow.DeactivateWorkflow();
  });
  Then ('iSource- I see workflow Deactivated', async function(){
    await WorkFlowFlow.VerifyDeactivateWorkflow();
  });