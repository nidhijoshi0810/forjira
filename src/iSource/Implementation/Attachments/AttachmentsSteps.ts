import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { AttachmentsFlow } from "../../POM/EventLevelAttachment/EventLevelAttachmentFlow";



Given('iSource- I am on attachmnets section page',async function()  {
    await AttachmentsFlow.clickOnAttachmentsTab();
    
  });

  Then('iSource- I upload file {string}', async function(name : string){
    await AttachmentsFlow.attachFile(name);
  });


  When('iSource- I select multiple attachment and download',async function(){
    await AttachmentsFlow.downloadMultipleAttachment();
  })

  When('iSource- I download all attachment',async function(){
    await AttachmentsFlow.downloadAllAttachment();
  })

  When('iSource- I select multiple attachment and delete',async function(){
    await AttachmentsFlow.deleteMultipleAttachment();
  })
  
  When('iSource- I delete all attachment',async function(){
    await AttachmentsFlow.deleteAllAttachment();
  }) 

  Then('iSource- I verify {string} attachment deleted',async function(value:string){
    await AttachmentsFlow.verifyDeleteAttachment(value);
  })