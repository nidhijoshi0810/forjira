import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { viewResponsesFlow } from "../../POM/viewResponses/ViewResponsesFlow";


   Then('iSource- I see View Responses',async function(){    
    await viewResponsesFlow.SeeViewResponses();
  });