import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { editEventDetailsFlow } from "../../POM/SecondaryActionListingPage/editEventDetails/editEventDetailsFlow";



   When('iSource- I Select {string} in secondary action',async function(ButtonName : string){
    await editEventDetailsFlow.secondaryAction(ButtonName);
    I.wait(5)
  });
  
  Then('iSource- I edit event name and description', async function(){
    await editEventDetailsFlow.editDetails();
  });

  When('iSource- I Apply filter as event type {string} and status {string}', async function(eventType : string,eventStatus : string){
    await editEventDetailsFlow.applyingFilter(eventType,eventStatus);
  });

  Then('iSource- I verify the event name and desc are updated', async function(){
    await editEventDetailsFlow.checkChanges();
  });


// stage
  
