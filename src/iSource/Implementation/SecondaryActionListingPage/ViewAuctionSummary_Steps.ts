import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { ViewAuctionSummaryFlow } from "../../POM/SecondaryActionListingPage/ViewAuctionSummaryFlow";
import { DatabaseOperations } from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";

   Then('iSource- I search event {string} using eventId',async function(eventType:string){
      let eventID:string= Startup.eventId_Map.get(eventType) as string;
      await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events")as string,eventID,await iSourcelmt.getLabel("EventColumnName")as string);
      await iSourcelmt.waitForLoadingSymbolNotDisplayed();
      I.wait(5);
  });

  When('clicking {string} in secondary action', async function(actionType:string){
      await ViewAuctionSummaryFlow.performSecondaryAction(actionType);
  });
  Then ('iSource- I check whether I am on view auction summary page',async function(){
   await ViewAuctionSummaryFlow.ViewAuctionSummaryCheck();
   I.wait(prop.DEFAULT_MEDIUM_WAIT);
 });
 Then ('iSource- I award Auction Event',async function(){
    await ViewAuctionSummaryFlow.awardEvent();
  });

