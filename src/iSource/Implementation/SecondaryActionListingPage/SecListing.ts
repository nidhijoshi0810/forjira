import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { SecListingFlow } from "../../POM/SecondaryActionListingPage/SecListingFlow";
import {DatabaseOperations} from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import {NegotiationFlow} from "../../POM/Negotiation/NegotiationFlow";
import { ViewAllSuppliersSteps } from "../../POM/BayerSideSupplier/ViewAllSupplierFlow";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { eventSettingsFlow } from "../../POM/eventSettings/eventSettingsFlow";


   When('iSource- Select {string} in secondary action',async function(ButtonName : string){
    await SecListingFlow.secondaryAction(ButtonName);
  });
  
  When('Apply filter as event type {string} and status {string}', async function(eventType : string,eventStatus : string){
    await SecListingFlow.applyingFilter(eventType,eventStatus);
  });

  Then ('iSource- I search for created event type {string}', async function(eventType:string){
    // let eventID:string= (await DatabaseOperations.getTestData()).get(eventType) as string;
    let eventID:string= await Startup.eventId_Map.get(eventType)as string;
    await SecListingFlow.searchEvent(eventID);
  });


  Then ('iSource- I see full source listing page',async function(){
    await SecListingFlow.FullSourceListingPage();
  });

  Then ('iSource- I check whether I am on create copy page',async function(){
    await SecListingFlow.CreateCopyCheck();
  });

  Then ('iSource- I check whether I am on view activity page',async function(){
    await SecListingFlow.ViewActivityCheck();
  });

  Then ('iSource- I check whether I am on multiround page',async function(){
    await SecListingFlow.MultiroundCheck();
  });

  Then ('iSource- I check whether I am on conclude page',async function(){
    await SecListingFlow.ConcludeCheck();
  });

  Then ('iSource- I navigate to analysis page',async function(){
    await SecListingFlow.AnalysisClick();
  });

  Then ('iSource- I click on conclude event',async function(){
    await SecListingFlow.ConcludeClick();
  });

  Then ('iSource- I fill comments for concluding event',async function(){
    await SecListingFlow.ConcludeComments();
  });

  Then ('iSource- I click on conclude button',async function(){
    await SecListingFlow.ConcludeBtnClick();

  });

  Then ('iSource- I conclude the created event',async function(){
    await SecListingFlow.ConcludeClick();
    await SecListingFlow.ConcludeComments();
    await SecListingFlow.ConcludeBtnClick();

  });

  When('iSource- I start nego',async function(){
    await NegotiationFlow.clickOnNegotiate()
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await NegotiationFlow.clickOnStartNegotiate()
        await NegotiationFlow.downloadBidDocumentFromMenuIcon()
  });

  Then('iSource- I click on back arrow',async function(){
    await SecListingFlow.BackToManageSuppBackArr();
  });

  Then('iSource- I reopen the closed event',async function(){
      await SecListingFlow.ReopenEvent11();
  });

  
  Then('iSource- I perform awarding',async function(){
    await SecListingFlow.AwardingActions();
});

  Then('iSource- I add suppliers in paused event',async function(supplierName : string){
    I.wait(prop.DEFAULT_LOW_WAIT)
    
    await ViewAllSuppliersSteps.clickOnAddSuppliersTab();
    
    await SecListingFlow.selectExtraSuppliers(supplierName);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed() 
  });


  Then('iSource- I unfreeze an event',async function(){
    await SecListingFlow.UnfreezeEvent();
});


  Then('iSource- I republish with a different date and time',async function(){
    await eventSettingsFlow.clickOnEventSettingsTab();
    await eventSettingsFlow.setCloseDateToCurrentDateAndTime(60);
    await eventSettingsFlow.clickOnEventSettingsTab();
    await SecListingFlow.RepublishButtonClick();
    I.wait(prop.DEFAULT_LOW_WAIT);
});

Then('iSource- I click on view lot details',async function(){
  await SecListingFlow.viewLotDetails();
});

Then('iSource- I verify whether I am on lot details page',async function(){
  await SecListingFlow.viewLotDetailsVerify();
});

Then('iSource- I click on view lot settings',async function(){
  await SecListingFlow.viewLotSettings();
});

Then('iSource- I verify whether lot settings popup has opened',async function(){
  await SecListingFlow.viewLotSettingsVerify();
});

Then('iSource- I close the define settings popup',async function(){
  await SecListingFlow.closeDefineSettingsPopup();
});

Then('iSource- I click on Restart Bid reconciliation',async function(){
  await SecListingFlow.restartBidRecoButton();
});
