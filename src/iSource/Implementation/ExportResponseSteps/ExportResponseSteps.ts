import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { ExportResponseFlow } from "../../POM/ExportResponseFlow/ExportResponseFlow";



  Then('iSource- I click on export non pricing responses', async function(){
    await ExportResponseFlow.ClickonExportNonPricingResp();
  });