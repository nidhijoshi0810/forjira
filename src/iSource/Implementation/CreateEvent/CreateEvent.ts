import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { SurrogateBidFlow } from "../../POM/SurrogateBid/SurrogateBidFlow";
import { ExportFlow } from "../../POM/Export/ExportFlow";
import { BidAnalysisFlow } from "../../POM/BidAnalysis/BidAnalysisFlow";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {DatabaseOperations} from "./../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import { stat } from "fs";

Given ('iSource- I am on full source event page',function()
{
  I.amOnPage(Startup.testData.get("DDS_iSource_Url"));
  
});

When ('iSource- I click on {string} button',async function(createButton : string){
  await CreateEventFlow.clickOnCreateEvent();
  I.wait(prop.DEFAULT_MEDIUM_WAIT)
});

Then('iSource- I am On create Sourcing event page',async function(){
  await CreateEventFlow.verifyEventPage();
});



When('iSource- I create event of type {string} and subtype of {string}',async function(eventType : string,testEvent : string,auctionType : string){
  await iSourcelmt.waitForLoadingSymbolNotDisplayed();
  await CreateEventFlow.clickOnCreateEvent();
  await CreateEventFlow.fillEventTitle();
  await CreateEventFlow.fillEventDesc();
  await CreateEventFlow.selecteventTypeAuction(eventType,auctionType);
  await CreateEventFlow.checkTestEvent(testEvent);
  await CreateEventFlow.clickCreateButton();
  var eventId=await CreateEventFlow.getEventID();
 GlobalVariables.eventIDForAllProds = eventId as string;
//  I.insertIntoDB(eventType,eventId);
 Startup.eventId_Map.set(eventType,eventId);
 
  console.log("**********event ID"+eventId);
  
});

When('iSource- I create IAnalyze event of type {string} and subtype of {string}',async function(eventType : string,testEvent : string,auctionType : string){
  await iSourcelmt.waitForLoadingSymbolNotDisplayed();
  await CreateEventFlow.fillEventTitle();
  await CreateEventFlow.fillEventDesc();
  await CreateEventFlow.selecteventTypeAuction(eventType,auctionType);
  await CreateEventFlow.checkTestEvent(testEvent);
  await CreateEventFlow.clickCreateButton();
 var eventId=await CreateEventFlow.getEventID();
 Startup.eventId_Map.set(eventType,eventId);
  console.log("**********event ID"+eventId);
  
});

When('iSource- I select Auction type as {string} with auction subtype as {string}',async function(auctionType : string,auctionSubType :  string){
await CreateEventFlow.selectauctypeAndAuctionsubtype(auctionType,auctionSubType);
await CreateEventFlow.clickCreateButton();
 var eventId=await CreateEventFlow.getEventID();
 Startup.eventId_Map.set(auctionType+""+auctionSubType,eventId);
//  I.insertIntoDB(auctionType+""+auctionSubType,eventId);
  console.log("**********event ID"+eventId);
});
When('iSource- I create event of type {string} and subtype as {string}',async function(eventType: string,testEvent : string){
  await CreateEventFlow.clickOnCreateEvent();
  await CreateEventFlow.fillEventTitle();
  await CreateEventFlow.fillEventDesc();
  await CreateEventFlow.selectEventType(eventType);
  await CreateEventFlow.checkTestEvent(testEvent);
 });
 When('iSource- I search and open event for event type {string}',async function(eventType:string){
  // let eventID:string= (await DatabaseOperations.getTestData()).get(eventType) as string;
  let eventID:string= Startup.eventId_Map.get(eventType)as string;
  // let eventID:string = "1310213627";
  await CreateEventFlow.searchEventAndOpenEvent(eventID);
 });


When('iSource- I create event of type {string} {string}',async function(eventType : string,auctionType : string){
  await CreateEventFlow.clickOnCreateEvent();
  CreateEventFlow.fillEventTitle();
  CreateEventFlow.fillEventDesc();
  GlobalVariables.auctionType=auctionType;
  CreateEventFlow.selecteventTypeAuction(eventType,auctionType);
});
When('iSource- I select Auction subtype as {string}',function(auctionSubType : string){
  CreateEventFlow.selectauctionSubType(auctionSubType);
});
When('iSource- I select Event subtype as {string}',async function(testEvent : string){
  CreateEventFlow.checkTestEvent(testEvent);
  CreateEventFlow.clickCreateButton();
  GlobalVariables.eventId=await CreateEventFlow.getEventID();
});
  When('iSource- I Fill additional details',async function(){
    I.wait(prop.DEFAULT_LOW_WAIT)
    // await CreateEventFlow.verifyEventName();
    await CreateEventFlow.clickOnAdditionalDetailsTab();
    //await CreateEventFlow.SelectAllAdditionalDropdown();
});
When('iSource- I See Event Name',async function(){

await CreateEventFlow.verifyEventName();
});

When('iSource- I click on {string} Tab',async function(additionalDetailTab : string){
await CreateEventFlow.clickOnAdditionalDetailsTab();
//I.wait(Startup.confi_prop.DEFAULT_MEDIUM_WAIT);
});

When('iSource- I select all dropdown values',async function(){
  await CreateEventFlow.SelectAllAdditionalDropdown();
});
//*********************Surrogate Bid************************************************************
When('iSource- I open event',async function(){
  
   await SurrogateBidFlow.searchOnGrid();
  await CreateEventFlow.OpenEvent("4566");
 
});
When('iSource- I click on ManagerSupplier Tab',async function(){
  await SurrogateBidFlow.clickOnManagerSupplierTab();
})
When ('iSource- I click on Surrogate Bid',async function(){
 await  SurrogateBidFlow.surrogateBid();
});

Then ('iSource- I am on Supplier Page',function(){
  I.amOnPage(Startup.testData.get("DDS_iSource_Supplier_URL"));
});

When('iSource- I click on Accept button',async function(){
await SurrogateBidFlow.clickOnAccept();
});

When('iSource- I click on Confirm Participation button',async function(){
await SurrogateBidFlow.clickOnConfirmParticipation();
});
When('iSource- I click on Return To Collect button',async function(){
 await SurrogateBidFlow.clickOnReturnToCollect();
});

//**********************************Export*******************************************************
When('iSource- I select Supplier',async function(){
  await ExportFlow.selectCheckbox();
});
When('iSource- I click on Export button',async function(){
  await ExportFlow.clickOnExport();
});
  When('iSource- I click on View Response Button',async function(){
  await ExportFlow.clickOnViewResponse();
  });

  When('iSource- I verify view response for RFI',async function(){
    await ExportFlow.verifyViewResponseForRFI();
  })
  When('iSource- I verify view response for Non RFI',async function(){
    
    await ExportFlow.verifyViewResponseForNonRFI();
  })
  When('iSource- I verify status Response Received',async function(){
await ExportFlow.verifyResponseReceived();
  });
//***********************************************Bid Analysis*************************************
When ('iSource- I click on Move to Analysis button',async function(){
 await BidAnalysisFlow.clickOnMoveToAnalysisButton();
 
});

// When('iSource- I click on 360 degree Analysis tab',function(){

// });
When ('iSource- I click on {string}Tab',async function(Tab : string){
  await BidAnalysisFlow.clickAnalysisTabs(Tab);
});
When('iSource- I click on Analyze button',async function(){
  await BidAnalysisFlow.clickOnSingleAnalyze();
});
When('iSource- I click on Scenarios analyze button', async function(){
  await BidAnalysisFlow.clickAnalyzebutton();
});
Then('iSource- I see Scenario title on page',async function(){

})

//***********************************************View All Supplier Popup*************************************
Given ('iSource- I am on View All Supplier Popup',async function(){
  await CreateEventFlow.viewAllSupplierPopup();
});

When ('iSource- I search for Supplier by {string}',async function(name:string){
  await CreateEventFlow.searchByCompnyContactName(name);
});

Then('iSource- I should verify the Search result for {string}',async function(name:string){
  await CreateEventFlow.verifyCompnyContactName(name);
})

Then('iSource- I remove supplier from event', async function(){
await CreateEventFlow.removeSupplier();
})

When('iSource- I search and open event for modify closure date {string}', async function(eventType:string){
    let eventID:string= Startup.eventId_Map.get(eventType)as string;
    await CreateEventFlow.searchEventAndOpenEventforModifyingDate(eventID);
 });

          
