import  "codeceptjs";
declare  const inject:  any;
const { I } = inject();
import { CreateCopyEventFlow } from "../../POM/CreateEvent/CreateCopyEventFlow";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

When('iSource- I select template for event {string}', async function (event: string) {
    await CreateCopyEventFlow.selectTemplate(event);
});

When('iSource- I select the Auction type as {string} with auction subtype as {string} and select template as {string}',
    async function (auctionType: string, auctionSubType: string, auctionTemplate: string) {
        await CreateEventFlow.selectauctypeAndAuctionsubtype(auctionType, auctionSubType);
        await CreateCopyEventFlow.selectTemplate(auctionTemplate);
    });

Given('iSource- I create a event of type {string} and subtype of {string} for the template', async function (eventType: string, testEvent: string) {
    let auctionType = eventType;
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CreateEventFlow.clickOnCreateEvent();
    await CreateEventFlow.fillEventTitle();
    await CreateEventFlow.fillEventDesc();
    await CreateEventFlow.selecteventTypeAuction(eventType, auctionType);
    await CreateEventFlow.checkTestEvent(testEvent);
});

Then('iSource- I should see Template selected {string}', async function (eventType: string) {
    await CreateCopyEventFlow.seeTheTemplate(eventType);
});

