import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
declare const inject: any; 
const { I } = inject();
import { MultiRoundsFlow } from "../../POM/CreateMultiRoundEvent/CreateMultiRoundEventFlow";
import { ShareEventFlow } from "../../POM/ShareEvent/ShareEventFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";



When('I fill create event of type {string} and subtype of {string}', async function () {
  await MultiRoundsFlow.createEvent();
});

When('I See ELMD Attachments', async function () {
  await MultiRoundsFlow.attachmentPage();
});

Then('I Select ELMD Attachments', async function () {
  await MultiRoundsFlow.selectAttachmentPage();
});

When('I See Sections', async function () {
  await MultiRoundsFlow.sectionPage();
});

Then('I select Sections', async function () {
  await MultiRoundsFlow.selectNoOfSection();
});

When('I See Tnc Attachments', async function () {
  await MultiRoundsFlow.TnCAttchPage();
});

Then('I Select Tnc Attachments', async function () {
  await MultiRoundsFlow.selectTnCAttch();
});

When('I See Suppler carry Forward', async function () {
  await MultiRoundsFlow.supplierPage();
});

Then('I select suppliers carry Forward', async function () {
  await MultiRoundsFlow.selectSupplierPage();
});

When('I Select Send Regret mail', async function () {
  await MultiRoundsFlow.sendRegretMail();
});

// When('I See Secoresheet Carry Forward', async function () {
//   await MultiRoundsFlow.scoreSheetPage();
// });
// When('I Select Scoresheet Carry Forward', async function () {
//   await MultiRoundsFlow.selectScoreSheetPage();
// });

Then('I should see Multiround event created', async function () {
  await MultiRoundsFlow.verifyMultiRoundEventCreated();
});

When('I should verify No of Sections', async function () {
  await MultiRoundsFlow.verifyNumberOfSection();
});

When('I should verfy Attachments carry Forwrd', async function () {
  await MultiRoundsFlow.verifyAttchCarryForward();
});

Then('iSource- I click cancel icon',async function(){
  await MultiRoundsFlow.clickCancelIcon();
})

Then('iSource- I click on analyze tab',async function(){
  await MultiRoundsFlow.clickOnAnalyzeTab();
})