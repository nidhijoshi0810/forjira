import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {commonLandingFlow} from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import {SupplierInfoFlow} from "../../POM/SupplierInfo/SupplierInfoFlow";


Given('iSource- I navigate to Source Setting',async function()  {
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "SettingsLabelKey" ,"SourcingSettingLabelKey");
  });
Given('iSource- I Click on Supplier Info Tab and select Supplier Contact',async function()  { 
    await SupplierInfoFlow.supplierInfoTab();
  });
Given('iSource- I select checkbox for Supplier',async function()  {
    await SupplierInfoFlow.selectChcekBox();
    });
Then('iSource- I click on save',async function()  {
    await SupplierInfoFlow.next();
    });
Then('iSource- I click on save and apply',async function()  {
    await SupplierInfoFlow.SaveAndApply();
    });
Given('iSource- I Verify all parameter and their Order',async function()  {
    await SupplierInfoFlow.Verify();
    });