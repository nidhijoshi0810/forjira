import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {ShowSupplierFlow} from "../../POM/SupplierInfo/ShowSupplierFlow";

Given('iSource- I Click on UserList Tab',async function()  { 
    await ShowSupplierFlow.userListTab();
  });
Given('iSource- I search based on userId',async function()  { 
  await ShowSupplierFlow.searchUser();
  });
Given('iSource- I uncheck show Supplier',async function()  { 
  await ShowSupplierFlow.uncheckShowSupplier();
  });

Given('iSource- I check show Supplier',async function()  { 
  await ShowSupplierFlow.checkShowSupplier();
  });

Then('iSource- I save userList',async function()  { 
  await ShowSupplierFlow.saveUserList();
  });

Then('iSource- I verify supplier name visibility',async function()  { 
  await ShowSupplierFlow.supplierNameVisibility();
  });
Then('iSource- I verify supplier name not Visible {string}',async function(supplierName:string)  { 
  await ShowSupplierFlow.supplierNameNotVisibile(supplierName);
  });