import "codeceptjs"; 
declare const inject: any; 
const { I } = inject(); 
import {NegotiationFlow} from "../../POM/Negotiation/NegotiationFlow";
import {SurrogateBidFlow} from "../../POM/SurrogateBid/SurrogateBidFlow";
import {NegotiationLocators} from "../../POM/Negotiation/NegotiationLocators";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
When('iSource- I am on Manage Suppliers page of event',async function(){

    await COE.searchOnGrid();
    I.click(Startup.uiElements.get(NegotiationLocators.First_record_name))
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
    await   SurrogateBidFlow.clickOnManagerSupplierTab();
     I.wait(20)

});

When('iSource- I negotiate from Buyer side',async function(){
    await NegotiationFlow.negotiateFromBuyer()
});

When('selects event to participte from Supplier side {string}',async function(eventType : string){
    await session('supplier',{browser:prop.browser},async function(){
    await NegotiationFlow.searchEventAndEnterEvent(eventType)
    });
});

Then('iSource- I Reject Request from Buyer side',async function(){
    await NegotiationFlow.RejectBidFromBuyer()
});

When('iSource- I start Negotiation from Buyer side',async function(){
    await NegotiationFlow.startNegotiation()
});

Then('iSource- I Accept Request from Supplier side',async function(){
    await session('supplier',{browser:prop.browser},async function(){
    await NegotiationFlow.acceptBidFromSupplier()
    });
});

Then('iSource- I Reject Request from Supplier side',async function(){
    await session('supplier',{browser:prop.browser},async function(){
    await NegotiationFlow.rejectBidFromSupplier()
    });
});

Then('iSource- I counter Bid from supplier side',async function(){
    await session('supplier',{browser:prop.browser} ,async function(){
    await NegotiationFlow.createCounterBidFromSupplier()
        });
});

Then('iSource- I Counter Bid from Buyer side',async function(){

    await NegotiationFlow.createCounterBidFromBuyer()
});

When('iSource- I Accept Request from Buyer side',async function(){
    I.wait(20)
    await NegotiationFlow.AcceptBidFromBuyer()
})
Given('logged on iSource supplier Page legacy',async function(){
    // From "features\Myevents.feature" {"line":5,"column":13}
     await session('supplier',{browser:prop.browser},async function(){
    I.amOnPage(Startup.testData.get('supp_url'));
    I.seeElement("//div[@id='loginDiv']//input[@id='USER']")
    I.click("//div[@id='loginDiv']//input[@id='USER']")
    I.fillField("//div[@id='loginDiv']//input[@id='USER']",Startup.testData.get("supp_username"))
    I.fillField("//div[@id='loginDiv']//input[@id='USER_EMAIL']",Startup.testData.get("supp_email"))
    I.click("//input[@value='Password']")
    I.fillField("//div[@id='loginDiv']//input[@id='password']",Startup.testData.get("supp_password"))
    I.click("//div[@id='loginDiv']//input[@id='imgLoginButton']")
    I.amOnPage(Startup.testData.get("supp_DDS_iSource_Url"));
  
});
  });