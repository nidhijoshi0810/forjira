import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { BOScenarioFlow } from "../../POM/BOScenarios/BOScenarioFlow";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { BidAnalysisFlow } from "../../POM/BidAnalysis/BidAnalysisFlow";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "./../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

When('iSource- I click on Analyze for Best 2 Scenario', async function () {
  await BOScenarioFlow.clickOnSingleAnalyzeForBest2Scenario();
});

Then('iSource- I Should Check and verify Supplier view as event level', async function () {
  logger.info("verifyEventLevel method")
  await BOScenarioFlow.verifyEventLevel();
});

Then('iSource- I Should verify Supplier view as Section level', async function () {
  await BOScenarioFlow.verifySectionLevel();
});

Then('iSource- I Should verify Supplier view as Supplier level', async function () {
  await BOScenarioFlow.verifySupplierLevel();
});

Then('iSource- I Should verify Supplier view as Item level', async function () {
  await BOScenarioFlow.verifyItemLevel();
});

Given('iSource- I navigate to analyze cherry picking page', async () => {
  await BOScenarioFlow.navigateToAnalyzeCherryPicking();
});

When('iSource- I click to Change Scope', async () => {
  await BOScenarioFlow.changeScope();
})

When('iSource- I Navigate to Item tab', async () => {
  await BidAnalysisFlow.excludeSupplier();
  await BidAnalysisFlow.NavigateToItemTab();
})

When('iSource- I deselect one Items', async () => {
  await BidAnalysisFlow.excludeItem();
})

Then('Click on Run Optimization', async () => {
  await BidAnalysisFlow.clickOnRunOptimizationBtn();
  await BidAnalysisFlow.clickOnRunOptimizationNow();
  await BidAnalysisFlow.enterDesc();
  await BidAnalysisFlow.clickOnRunNowBtn();
})

Given('iSource- I am on Schedule scenario for Optimization', async () => {
  await BOScenarioFlow.changeScope();  
  await BidAnalysisFlow.excludeSupplier();
  await BidAnalysisFlow.NavigateToItemTab();
  await BidAnalysisFlow.excludeItem();
  await BidAnalysisFlow.clickOnRunOptimizationBtn();
  await BOScenarioFlow.scheduleScnarioForOptimization();
});

When('iSource- I Select Schedule scenaio', async () => {
  await BOScenarioFlow.selectScheduleScenario();
})

When('iSource- I Update scenario name', async () => {
  await BOScenarioFlow.updatescenarioName();
  await BidAnalysisFlow.enterDesc();
})

When('iSource- I Scehdule Scenario', async () => {
  await BidAnalysisFlow.clickOnScheduleBtn();
  await iSourcelmt.waitForLoadingSymbolNotDisplayed()
  await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneButtonLabel") as string);
})

Given('iSource- I am on custom scenario with strategy Best 2 suppliers', async function () {
  await BOScenarioFlow.customBest2();
});

Then('iSource- I make allowcation for Cherry pick', async function() {
    await BOScenarioFlow.makeAllocationToItemLevel()
    await BOScenarioFlow.exportScenario()
});