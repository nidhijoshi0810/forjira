import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
import { CreateQuickSourceEvent } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import { SupplierGroupFlow } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventFlow";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { supplierHomeFlow } from "../../POM/SupplierHome/SupplierHomeFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { CommonLandingAction } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingAction";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { ShareEventFlow } from "../../POM/ShareEvent/ShareEventFlow";


When('iSource- I enter Event Name', async function () {
    await SupplierGroupFlow.addEventName()
})
When('iSource- I add Item Details', async function () {
    await SupplierGroupFlow.addItemDetails()
});
When('iSource- I Remove the added item', async function () {
    await SupplierGroupFlow.removeItemDetails()
});

When('iSource- I add Quicksource supplier', async function () {
    await SupplierGroupFlow.addSupplier()
});


When('iSource- I added Item', async function () {
    await SupplierGroupFlow.additemsQS()
});
When('iSource- I added question in Quicksource', async function () {
    await SupplierGroupFlow.questionadded()
});

When('iSource- I Remove added question in Quicksource', async function () {
    await SupplierGroupFlow.removequestion()
});
When('iSource- I check question is removed', async function () {
    await SupplierGroupFlow.checkremovequestion()
});

When('iSource- I create new suppliers', async function () {
    await SupplierGroupFlow.createNewSupplier()
});

When('iSource- I am on Opening Message Supplier', async function () {
    await SupplierGroupFlow.openingmessageheader()
});
When('iSource- I enter Opening message', async function () {
    await SupplierGroupFlow.openingmessage()
});
When('iSource- I add Supplier', async function () {
    await SupplierGroupFlow.addSupplier()
});

When('iSource- I click on View all Supplier', async function () {
    await SupplierGroupFlow.openviewallSupplier()
});

When('iSource- I am on Supplier tab', async function () {
    await SupplierGroupFlow.navigatetoSuppliertab()
});

When('iSource- I verify opening message', async function () {
    await SupplierGroupFlow.verifyopeingmessage()
});

When('iSource- I Click on add Collabrator popup', async () => {
    await SupplierGroupFlow.Addcollabratorpopup()
});
When('iSource- I select Collabrator', async () => {
    await SupplierGroupFlow.Selectcollabrator()
});

When('iSource- I verify collaborator Selected', async () => {
    await SupplierGroupFlow.verifycollabratoradded()
});

When('iSource- I Click on view all collaborator', async () => {
    await SupplierGroupFlow.openviewallcollbratorpopup()
});
When('iSource- I search for collaborator by {string}', async (name: string) => {
    await SupplierGroupFlow.Searchcollabrator(name)
});

When('iSource- I  verify the Search result for {string}', async (name: string) => {
    await SupplierGroupFlow.verifysearchCollabrator(name)
});

When('iSource- I check Item is removed', async () => {
    await SupplierGroupFlow.checkremoveditem()
});

When('iSource- I click on selected Link', async () => {
    await SupplierGroupFlow.ShowSelectedlist()
});
When('iSource- I am on Collabrator tab', async () => {
    await SupplierGroupFlow.NavigatetoCollbrator()
});

When('iSource- I should able to see selected Collabrator', async () => {
    await SupplierGroupFlow.verifyselectedresult()
});
When('iSource- I Add Collabrator', async () => {
    await SupplierGroupFlow.addcollabrator()
});
When('iSource- I click on Add CollabratorButton', async () => {
    await SupplierGroupFlow.addcollabratorresponsetab()
});
When('iSource- I verify collaborator added', async () => {
    await SupplierGroupFlow.sucessmessage()
});
When('iSource- I Click On Sort By {string}', async (name: string) => {
    await SupplierGroupFlow.sortcolumn(name)
});
Given('iSource- I save as Draft', async function () {
    await SupplierGroupFlow.clickSaveAsDraft();
});
When('iSource- I click on send', async function () {
    await SupplierGroupFlow.clickSend()
});

Then('iSource- I see Event Name and Event ID', async function () {

    GlobalVariables.QSeventName = await CreateEventFlow.getEventName();
    var eventName = await CreateEventFlow.getEventName();
    Startup.eventId_Map.set("QSeventName", eventName);
    await I.see(eventName);

    GlobalVariables.QSeventId = await CreateEventFlow.getEventID();
    var eventId = await CreateEventFlow.getEventID();
    Startup.eventId_Map.set("QSeventId", eventId);
});

When('iSource- I select date for Last Date', async function () {
    await SupplierGroupFlow.selectLastDate()
});
When('iSource- I Send Reminder', async function () {
    await SupplierGroupFlow.sendreminder()
});

When('iSource- I click on Export', async function () {
    await SupplierGroupFlow.clickonExportResponses();
});

When('iSource- I modify closure date day to current date time plus 2 for quicksource', async () => {
    await SupplierGroupFlow.selectModifyDate()
});

Then('iSource- I navigate back to quicksource listing page', async function () {
    I.wait(prop.DEFAULT_WAIT)
    await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU) as string);
    await I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
    await I.click((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>", await iSourcelmt.getLabel("Quick Source") as string))

});

When('iSource- I search for quicksource event', async function () {
    GlobalVariables.QSeventId = await CreateEventFlow.getEventID();
    var eventId = await CreateEventFlow.getEventID();
    Startup.eventId_Map.set("QSeventId", eventId);
    I.wait(prop.DEFAULT_WAIT)
    await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU) as string);
    await I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
    await I.click((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>", await iSourcelmt.getLabel("Quick Source") as string))
    await ShareEventFlow.searchEvent(eventId);

});


Then("iSource- I verify navigation for Edit Event", async function () {
    await SupplierGroupFlow.editEventAsAction();
});

Then("iSource- I verify whether modify closure date pop up has opened", async function () {
    await SupplierGroupFlow.ModifyClDateCheck();
});

Then("iSource- I check whether I am on View Inquiry page", async function () {
    await SupplierGroupFlow.ViewInquiryCheck();
});

Then("iSource- I check whether I am on Copy page", async function () {
    await SupplierGroupFlow.CopyCheck();
});

When('iSource- I search for event type {string}', async function (eventType: string) {
    let eventID: string = Startup.eventId_Map.get(eventType) as string;
    await ShareEventFlow.searchEvent(eventID);
});

Then("iSource- I check whether I am on view response page", async function () {
    await SupplierGroupFlow.ViewResponseCheck();
});

Then("iSource- I verify navigation for View Response", async function () {
    await SupplierGroupFlow.viewResponseAsAction();
});

Then("iSource- I check whether I am on activity page of quicksource", async function () {
    await SupplierGroupFlow.QuickActivityChk();
});

Then("iSource- I close the quicksource event", async function () {
    await SupplierGroupFlow.CloseQuickEvent();
});

Then("iSource- I verify navigation for Award Event", async function () {
    await SupplierGroupFlow.AwardEventAsAction();
});

Then("iSource- I reopen the quicksource event", async function () {
    await SupplierGroupFlow.selectModifyDate()
});

Then("iSource- I search for added collaborators by name", async function () {
    await SupplierGroupFlow.searchCollabOnGridByName()
});

Then("iSource- I search for added collaborators by email id", async function () {
    await SupplierGroupFlow.searchCollabOnGridByEmail()
});

Then("iSource- I add a collab", async function () {
    await SupplierGroupFlow.AddCollabForGridSearch()
});

Then("iSource- I add suppliers in an open event from responses tab", async function () {
    await SupplierGroupFlow.addSuppliersFromResponsesTab()
});



