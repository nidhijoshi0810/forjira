import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {loginFlow} from "./../../../Framework/FrameworkUtilities/COE/Login/loginFlow";
import {commonLandingFlow} from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { loginAction } from "../../../Framework/FrameworkUtilities/COE/Login/loginAction";
import { z } from "actionbot-wrapper/z";


Given('logged in on iSource Page',async function ()  {
  // I.clearCookie();
    if(process.env.dsso)
    {
      I.wait(prop.DEFAULT_MEDIUM_WAIT);
      await loginFlow.loginWithDSSO();
      I.wait(prop.DEFAULT_MEDIUM_WAIT);
      await iSourcelmt.matchKeys().then(async function(map) {
        console.log("completed matchkeys")
        Startup.mst_final = map as Map<string, string>;
    })
    }
    else{
    await loginFlow.loginToApp();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    await iSourcelmt.matchKeys().then(async function(map) {
        console.log("completed matchkeys")
        Startup.mst_final = map as Map<string, string>;
    })
}

});
Then('I use logout from DDiSource',async function(){
   await loginFlow.logoutDDiSource();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
  });
Given('I navigate to Quick Source',async function(){
  await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "QUICKSOURCE_TABNAME", "");
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
  }); 

  Given('iSource- I navigate to Full Source', async function(){
    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "FULLSOURCE_TABNAME", "");
     await  iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.waitForText(await iSourcelmt.getLabel("FULLSOURCE_TABNAME"));
    }); 

    // Given('I navigate to Full Source', async function(){
    //   console.log("inside method");
    //   // pause();
    //    await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "FULLSOURCE_TABNAME", "" );
    //    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    //    I.waitForText(await iSourcelmt.getLabel("FULLSOURCE_TABNAME"));
    //   });

    When('I navigate to Approval and Reviews', async function(){
      await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "Approvals & Reviews", "");
       await  iSourcelmt.waitForLoadingSymbolNotDisplayed()
      
      }); 

Given('logged in on iSource supplier Page from legacy',  function() {
  // From "features\Myevents.feature" {"line":5,"column":13}
  I.amOnPage(Startup.testData.get("supp_url"));
  I.seeElement(Startup.uiElements.get(loginAction.usernameInputFiled))
  I.click(Startup.uiElements.get(loginAction.usernameInputFiled))
  I.fillField(Startup.uiElements.get(loginAction.usernameInputFiled),Startup.testData.get("supp_username"))
  I.fillField(Startup.uiElements.get(loginAction.usernameEmailInputFiled),Startup.testData.get("supp_email"))
  I.click("//input[@value='Password']")
  I.fillField("//div[@id='loginDiv']//input[@id='password']",Startup.testData.get("supp_password"))
  I.click("//div[@id='loginDiv']//input[@id='imgLoginButton']")
  I.amOnPage(Startup.testData.get("supp_DDS_iSource_Url"));


});

  Then('iSource- I refresh page',async function(){
    I.refreshPage();
   await iSourcelmt.waitForLoadingSymbolNotDisplayed();
      I.wait(prop.DEFAULT_WAIT)
  });









