
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { FilterOnListingFlow } from "../../POM/FilterOnListing/FilterOnListingFlow";


When('iSource- I click on filter and search in {string} Column in QuickSource event listing', async function(columnName: string){
    await FilterOnListingFlow.filterOnGrid(columnName);
    await FilterOnListingFlow.checkboxSearch(columnName, Startup.testData.get("QS_Owner_Name") as string);
});