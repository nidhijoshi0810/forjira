
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { FilterOnListingFlow } from "../../POM/FilterOnListing/FilterOnListingFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";


Given('iSource- I Click on fullsource listing page', async function(columnName: string){
    await FilterOnListingFlow.ClickOnfullsource();
   
});


When('iSource- I click on filter and search in {string} Column in event listing', async function(columnName: string){
    await FilterOnListingFlow.filterOnGrid(columnName);
    await FilterOnListingFlow.checkboxSearch(columnName, Startup.testData.get("Owner_Name") as string);
});

Then('iSource- I verify owner is present in {string} column', async function(columnName: string){
    await FilterOnListingFlow.verifyOwner();
})

When('iSource- I click on filter and search in {string} Column for {string} in event listing', async function(columnName: string, dataToFilter: string){
    await FilterOnListingFlow.filterOnGrid(columnName);
    await FilterOnListingFlow.checkboxSearch(columnName, dataToFilter);
});

Then('iSource- I clear filter', async function(dataToFilter: string, columnName: string){
      await FilterOnListingFlow.clearFilter();
    })

Then('iSource- I verify Event Type {string} is present in {string} column', async function(dataToFilter: string, columnName: string){

await FilterOnListingFlow.verifyType(dataToFilter); 
  
})

Then('iSource- I verify Status {string} is present in {string} column', async function(dataToFilter: string, columnName: string){
    
    await FilterOnListingFlow.verifyStatus(dataToFilter); 
  
})

When('iSource- I click on filter and search on {string} date for {string} in event listing', async function(columnName: string, dataToFilter: string){
    await FilterOnListingFlow.filterOnGridCreateDateInDate(columnName, dataToFilter);
});
Then('iSource- I verify date {string} is present in {string} column', async function(dataToFilter: string, columnName: string){
    
    await FilterOnListingFlow.verifyOnDateFilter(dataToFilter); 
   
})

When('iSource- I click on filter and search on {string} date in date within for {string} in event listing', async function(columnName: string, dataToFilter: string){
    await FilterOnListingFlow.filterOnGridCreateDateInDateWithin(columnName, dataToFilter);
});
When('iSource- I click on filter and search on {string} date in date period from {string} to {string} in event listing', async function(columnName: string, fromDate: string, toDate: string){
    await FilterOnListingFlow.filterOnGridCreateDateInDatePeriod(columnName, fromDate, toDate);
});

When('iSource- I toggle between Fullsource and Quicksource tab', async function(){
    await FilterOnListingFlow.toggleBetweenTabs();
})

Then('iSource- I verify filters are retain',async function(){
    await FilterOnListingFlow.verifyFilterRetain();
})
