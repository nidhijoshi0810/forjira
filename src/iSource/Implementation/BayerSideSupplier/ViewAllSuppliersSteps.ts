import "codeceptjs"; 
declare const inject: any; 
const {I} = inject();

import { ViewAllSuppliersSteps } from "../../POM/BayerSideSupplier/ViewAllSupplierFlow";
import { WorkFlowFlow } from "../../POM/WorkFlow/WorkFlowFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

Given('iSource- I am on suppliers page',async function(){
    await ViewAllSuppliersSteps.clickOnAddSuppliersTab();
});

Given('iSource- I navigate to View all supplier popup',async function(){
  await ViewAllSuppliersSteps.navigateToViewAllSupliersPopup();
});
Given('iSource- I select suppliers from view all suppliers',async function(){
  await ViewAllSuppliersSteps.selectSuppliersFromGrid();
});

Then('iSource- I Move to event details page', async function(){
  
   await WorkFlowFlow.clickEventDetailTab();
  });
  Then('iSource- I Publish event', async function(){
    //  await ViewAllSuppliersSteps.performPublish();
     await WorkFlowFlow.clickSaveandGotoFlowElsePuplish();
    });
  Given('iSource- I select suppliers {string}',async function(supplierName : string){
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_LOW_WAIT)
    await ViewAllSuppliersSteps.clickOnAddSuppliersTab();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    await ViewAllSuppliersSteps.navigateToViewAllSupliersPopup();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    await ViewAllSuppliersSteps.selectSuppliersFromGrid(supplierName);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    // await ViewAllSuppliersSteps.selectSuppliersFromGrid(global.testData.get("SupplierName"));
  });
