import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { ViewAllSupplierPopupflow } from "../../POM/BayerSideSupplier/viewAllsupplierpopupflow";

Given('iSource- I am on View All Supplier Popup of QS', async function () {
    await ViewAllSupplierPopupflow.viewAllSupplierPopupQS();
});
Given('iSource- I am on View All Supplier Popup', async function () {
    await ViewAllSupplierPopupflow.viewAllSupplierPopup();
});

When('iSource- I search for Supplier by {string}', async function (name: string) {
    await ViewAllSupplierPopupflow.searchByCompnyContactName(name);
});
// When('iSource- I search for Supplier by {string}', async function (name: string) {
//     await ViewAllSupplierPopupflow.searchByCompnyContactNameQS(name);
// });


Then('iSource- I should verify the Search result for {string}', async function (name: string) {
    await ViewAllSupplierPopupflow.verifyCompnyContactName(name);
})

Then('iSource- I remove supplier from event', async () => {
    await ViewAllSupplierPopupflow.removeSupplier();
})

Then('iSource- I move to view all supplier popup', async () => {
    await ViewAllSupplierPopupflow.moveTOViewAllSupplier();
})

Then('iSource- I navigate to add supplier page', async () => {
    await ViewAllSupplierPopupflow.goBackToAddSupplierPage();
});