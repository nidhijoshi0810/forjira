import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { EventSharingFlow } from "../../../../POM/FS-Admin Setting/Event Setting/Event Sharing Setting/EventSharingSettingFlow";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

    
   Then('iSource- I select Event Sharing on dropdown and select all users',async function(){
    await EventSharingFlow.ClickEventSharingAndSelectAllUser();
    I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I select Event Sharing on dropdown and select No other users',async function(){
    //I.wait(20)
    await EventSharingFlow.ClickEventSharingAndSelectNoOtherUser();
    I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I see shared',async function(){
    await EventSharingFlow.seeShared();
  });