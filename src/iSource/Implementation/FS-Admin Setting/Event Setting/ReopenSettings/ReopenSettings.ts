import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
// reopen settings flow
import { reopenSettingsFlow } from "../../../../POM/FS-Admin Setting/Event Setting/ReopenSetting/reopenSettingsFlow";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

When('iSource- I select reopen event settings on dropdown and enable external comments',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await reopenSettingsFlow.externalComments();
   })

   
When('iSource- I select reopen event settings on dropdown and enable internal comments',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await reopenSettingsFlow.internalComments();
   })

