import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
// broadcast settings flow
import { broadcastSettingsFlow } from "../../../../POM/FS-Admin Setting/Event Setting/EventInviteTypeSetting/broadcastSettingsFlow";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

When('iSource- I select Event invite type on dropdown and check allow broadcast option',async function(){
    await broadcastSettingsFlow.allowBroadcast();
   I.wait(prop.DEFAULT_WAIT)
  });

  When('iSource- I select YES for Broadcast - All Suppliers',async function(){
    await broadcastSettingsFlow.broadcastToSuppliers();
   I.wait(prop.DEFAULT_WAIT)
  });

  
  When('iSource- I select Event invite type on dropdown and check broadcast default option',async function(){
    await broadcastSettingsFlow.broadcastDefault();
   I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I check option YES is selected',async function(){
    await broadcastSettingsFlow.checkYES();
    I.wait(prop.DEFAULT_WAIT)
  });

 