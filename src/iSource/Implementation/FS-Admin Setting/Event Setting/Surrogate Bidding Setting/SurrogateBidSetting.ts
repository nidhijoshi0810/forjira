import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { SurrogateBidSettingFlow } from "../../../../POM/FS-Admin Setting/Event Setting/Surrogate Bidding Setting/SurrogateBidSettingFlow";
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { CommonLandingAction } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingAction";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import { logger } from "../../../../../Framework/FrameworkUtilities/Logger/logger";
import { SurrogateBidFlow } from "../../../../POM/SurrogateBid/SurrogateBidFlow";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
declare const inject: any; 
const { I } = inject();

Given('navigate to Admin Setting',async function(){
   await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "SettingsLabelKey", "SourcingSettingLabelKey");
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    
});

Then('switch TAB',async function(){  
  await SurrogateBidSettingFlow.switchTab();
});

Then('switch to previous Tab',async function(){  
  await SurrogateBidSettingFlow.switchToPreviousTab();
});

Then('iSource- I go to event setting and {string}', async function(labelToBeClicked : string){  
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await SurrogateBidSettingFlow.clickOnEventSetting(labelToBeClicked);
   });

   Then('iSource- I navigate back to Full Source', async function(){
      I.wait(prop.DEFAULT_WAIT)
      await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
        await I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
        await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>",await iSourcelmt.getLabel("FULLSOURCE_TABNAME") as string))

   });

   Then('iSource- I go to Admin Setting from Isource', async function(){
    I.wait(prop.DEFAULT_WAIT)
    await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
      I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
      await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>",await iSourcelmt.getLabel("SettingsLabelKey") as string))
  await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.SUBMENU_LOCATOR) as string ).replace("<<subMenuName>>",await iSourcelmt.getLabel("SourcingSettingLabelKey") as string))

 });
    
   Then('iSource- I select surrogate Bidd on dropdown and select no',async function(){
    await SurrogateBidSettingFlow.ClickSurrogateBidAndSelectNo();
    I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I select surrogate Bidd on dropdown and select YES',async function(){
    await SurrogateBidSettingFlow.ClickSurrogateBidAndSelectYes();
    I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I see All Sourcing Event',async function(){
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await SurrogateBidSettingFlow.ISeeAllSourcingEvent();
  });

  Then('iSource- I navigate to manage supplier tab and dont see  place surrogate bid', async function(){
    await SurrogateBidFlow.clickOnManagerSupplierTab();
    await SurrogateBidSettingFlow.dontSeePlaceSurrogatebid();
  });