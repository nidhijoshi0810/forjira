import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
// Negotiate settings flow
import { NegotiationFlow } from "../../../../POM/Negotiation/NegotiationFlow";
import { SurrogateBidFlow } from "../../../../POM/SurrogateBid/SurrogateBidFlow";
import { negoSettingsFlow } from "../../../../POM/FS-Admin Setting/Event Setting/NegotiateSetting/negoSettingsFlow";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

When('iSource- I select Negotiate settings on dropdown and check enable negotiate',async function(){
    await negoSettingsFlow.enableNego();
   I.wait(prop.DEFAULT_WAIT)
  });

When('iSource- I navigate to Manage Suppliers page of event',async function(){
    await SurrogateBidFlow.clickOnManagerSupplierTab();
   I.wait(prop.DEFAULT_WAIT)
});

When('iSource- I click negotiate from Buyer side',async function(){
    await NegotiationFlow.clickOnNegotiate();
})

When('iSource- I check for negotiate action',async function(){
    await negoSettingsFlow.checkNegoAction();
   I.wait(prop.DEFAULT_WAIT)
})

When('iSource- I select Negotiate settings on dropdown and check enable BAFO',async function(){
    await negoSettingsFlow.enableBAFO();
    I.wait(prop.DEFAULT_WAIT)
})

When('iSource- I click on nego and check for request offer option',async function(){
    await negoSettingsFlow.checkBAFO();
    I.wait(prop.DEFAULT_WAIT)
})

