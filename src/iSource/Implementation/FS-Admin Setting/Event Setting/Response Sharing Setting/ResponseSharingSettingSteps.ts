import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { RespSharingFlow } from "../../../../POM/FS-Admin Setting/Event Setting/Response Sharing Setting/ResponseSharingSettingFlow";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

    
   Then('iSource- I select Response Sharing on dropdown and Check Mandatory Sealing of Supplier Response',async function(){
    await RespSharingFlow.ClickRespSharingAndCheckOption();
   I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I select Response Sharing on dropdown and UnCheck Mandatory Sealing of Supplier Response',async function(){
    //I.wait(20)
    await RespSharingFlow.ClickRespSharingAndUncheckOption();
   I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I see seal bid DISABLED', async function(){
    await RespSharingFlow.seeSealBidDisabled()
  });