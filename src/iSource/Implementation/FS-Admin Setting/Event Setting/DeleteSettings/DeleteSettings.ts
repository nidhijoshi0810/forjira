import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
// Delete Settings Flow
import { deleteSettingsFlow } from "../../../../POM/FS-Admin Setting/Event Setting/DeleteSetting/deleteSettingsFlow";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

Then('iSource- I select {string} under delete settings', async function(labelToBeClicked : string){  
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.subDeleteSetting(labelToBeClicked);
    

   });

   Then('iSource- I select Owner check settings',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.checkOwnerDelete();
   })

Then('iSource- I click on delete',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.deleteEvent();
})

Then('iSource- I select delete comments mandatory option',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.checkMandatoryComments();
})

Then('iSource- I enter delete comments',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.enterDeleteComments();
})

When('iSource- I click on secondary actions button',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.secondaryActionsButton();
})

Then('iSource- I check for delete event option',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.deleteButtonVerify();
})

Then('iSource- I delete settings are saved',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.saveDeleteSettings();
})

Then('iSource- I check for no records found',async function(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await deleteSettingsFlow.noRecordsFound();
})

// stage