import "codeceptjs"; 
import { commonLandingFlow } from "../../../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { EventRulesFlow } from "../../../../../iSource/POM/FS-Admin Setting/Event Setting/Event Rules Setting/EventRulesSettingFlow"
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const { I } = inject();

    
   Then('iSource- I select Event Rules on dropdown and select NO',async function(){
    await EventRulesFlow.ClickEventRulesAndSelectNo();
    I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I select Event Rules on dropdown and select YES',async function(){
    await EventRulesFlow.ClickEventRulesAndSelectYes();
    I.wait(prop.DEFAULT_WAIT)
  });

  Then('iSource- I create potential supplier', async function(){
    await EventRulesFlow.CreatePotentialSupplier();
  })