
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
// import { SortListingFlow } from "../../POM/SortListing/SortListingFlow";

Given('iSource- I am on QuickSource listing page', async function(){
    I.see(await iSourcelmt.getLabel("QUICKSOURCE_TABNAME"));
  })