import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { logger } from "./../../../Framework/FrameworkUtilities/Logger/logger";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { DutchAuctionMonitoring } from '../../POM/AuctionMonitoringForDutch/DutchMonitoringFlow';

When('iSource- I search auction event', async function () {
    await DutchAuctionMonitoring.searchAuctionEvent();
})

Then('iSource- I navigate to auction summary', async function () {
    await DutchAuctionMonitoring.navigateToAuctionSummary();
})

When('iSource- I see starting bid price', async function () {
    await DutchAuctionMonitoring.viewStartingBidPrice();
})

Then('iSource- I see reverse bid price', async function () {
    await DutchAuctionMonitoring.viewReverseBidPrice();
})

When('iSource- I see current bid price', async function () {
    await DutchAuctionMonitoring.viewCurrentBidPrice();
})

When('iSource- I click on change bid price button', async function () {
    await DutchAuctionMonitoring.clickChangeBidBtn();
})

When('iSource- I enter Bid price manually', async function () {
    await DutchAuctionMonitoring.enterBidPercentage();
    await DutchAuctionMonitoring.clickSaveBtn();
})

Then('iSource- I verify changed bid price', async function () {
    await DutchAuctionMonitoring.verifySavedBidPrice();
})

When('iSource- I see supplier details', async function () {
    await DutchAuctionMonitoring.viewSupplierDetail();
})
Then('iSource- I verify supplier', async function () {
    await DutchAuctionMonitoring.verifySupplier();
})

When('iSource- I pause auction', async function () {
    await DutchAuctionMonitoring.pauseAuction();
})

Then('iSource- I verify auction is paused', async function () {
    await DutchAuctionMonitoring.verifyAuctionIsPaused()
})

When('iSource- I disable auto extention', async function () {
    await DutchAuctionMonitoring.enableOrDisableAutoExtention();
})

Then('iSource- I verify auto extention is disable', async function () {
    await DutchAuctionMonitoring.verifyDisableAutoExtention();
})

When('iSource- I enable auto extention', async function () {
    await DutchAuctionMonitoring.enableOrDisableAutoExtention();
})

Then('iSource- I verify auto extention is enable', async function () {
    await DutchAuctionMonitoring.verifyEnableAutoExtention();
})

Then('iSource- I navigate to Monitoring Auction', async function(){
    await DutchAuctionMonitoring.navigateToMontoringTab();
})

Given('iSource- I am on monitoring auction tab', async function(){
    await DutchAuctionMonitoring.checkForCurrentTab();
}) 

Then('iSource- I set duration per lot',async function(){
    await DutchAuctionMonitoring.setDurationPerLot();
})