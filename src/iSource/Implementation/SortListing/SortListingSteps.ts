import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { SortListingFlow } from "../../POM/SortListing/SortListingFlow";


  Given('iSource- I am on fullsource listing page', async function(){
    I.see(await iSourcelmt.getLabel("FULLSOURCE_TABNAME"));
  })

  When('iSource- I click on {string} sort icon', async function(columnName:string){
    I.wait(prop.DEFAULT_LOW_WAIT);
    await SortListingFlow.AscendingVerification(columnName);
    I.wait(prop.DEFAULT_LOW_WAIT);
  });

  Then('iSource- I verify Ascending sort on {string}', async function(columnName: string){
      I.wait(prop.DEFAULT_LOW_WAIT);
      await SortListingFlow.sortingVerified(columnName)
  });

  Then('iSource- I click on {string} sort icon', async function(columnName:string){
    I.wait(prop.DEFAULT_LOW_WAIT);
    await SortListingFlow.DescendingVerification(columnName);
    I.wait(prop.DEFAULT_LOW_WAIT);
  });

  Then('iSource- I verify Descending sort on {string}', async function(columnName:string){
    I.wait(prop.DEFAULT_LOW_WAIT);
    await SortListingFlow.sortingVerified(columnName)
  });

  When('iSource- Search fullsource event by {string} as {string}', async function(type: string,data:string){
    await SortListingFlow.searchEvent(type,data);
});

Then('iSource- I verify fullsource search result for {string} as {string}', async function(type:string, data:string){
    await SortListingFlow.verifySearch(type,data)
});