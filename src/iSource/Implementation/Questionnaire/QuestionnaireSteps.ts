import { Startup } from './../../../Framework/FrameworkUtilities/Startup/Startup';
import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { logger } from "./../../../Framework/FrameworkUtilities/Logger/logger";
import { QuestionnaireFlow } from "../../POM/Questionnaire/QuestionnaireFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { TEBFlow } from "../../POM/Questionnaire/TEBFlow";
import {FlexiItemTableFlow} from "../../POM/FlexiItemTable/FlexiItemTableFlow";
import { iSourcelmt } from '../../../Framework/FrameworkUtilities/i18nutil/readI18NProp';

Given('iSource- I am on Questionnarire page', async function () {
    
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await QuestionnaireFlow.clickCreateQuestionnaire()
    logger.info("Clicked on Create Questionanaire");
});


Then('iSource- I select section type as {string}', async function (secType: string) {
    await TEBFlow.selectSectionType(secType);

});



When("iSource- I add Question with Non mandatory", async function () {
    //await QuestionnaireFlow.addSection()
    I.wait(prop.DEFAULT_LOW_WAIT)
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_Default");
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_Numeric");
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_MultiChoice");
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_YesNo");
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_SingleChoice");
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_Attachment");
    await QuestionnaireFlow.selectQuestionTypeWithNonMandatory("QuestionType_Comment");
    await QuestionnaireFlow.addQuestionTypeTableForNonMandatory();

});
When("iSource- I add Question with mandatory", async function () {
    //await QuestionnaireFlow.addSection();
    await QuestionnaireFlow.selectQuestionTypeWithMandatory("QuestionType_Default");
    await QuestionnaireFlow.selectQuestionTypeWithMandatory("QuestionType_Numeric");
    await QuestionnaireFlow.selectQuestionTypeWithMandatory("QuestionType_MultiChoice");
    await QuestionnaireFlow.selectQuestionTypeWithMandatory("QuestionType_YesNo");
    await QuestionnaireFlow.selectQuestionTypeWithMandatory("QuestionType_SingleChoice");
    await QuestionnaireFlow.selectQuestionTypeWithMandatory("QuestionType_Attachment");
    await QuestionnaireFlow.addQuestionTypeTableForMandatory();



});
Then('iSource- I do not see add pricing table option', async function () {
    await QuestionnaireFlow.checkPricingTableIsNotPresent();

});
Then("iSource- I add pricing table for award workflow", async function () {
    await QuestionnaireFlow.AddPricingTableAwardWorkflow();
});
Then("iSource- I add pricing table for RFX", async function () {

    // await QuestionnaireFlow.addSection()
    await QuestionnaireFlow.AddPricingTableRFPandRFQ();
});

Then('add pricing table for English Auction with Line Item', async function () {
    await QuestionnaireFlow.addSection();
    // await QuestionnaireFlow.AddPricingTableEnglishAuction();
    await QuestionnaireFlow.AddPricingTableEngAuctionlineItem();
});

Then('iSource- I add pricing table for English Auction', async function () {
    await QuestionnaireFlow.addSection();
    await QuestionnaireFlow.AddPricingTableEnglishAuction();
});
Then('iSource- I add pricing table for Dutch Auction', async function () {
    await QuestionnaireFlow.addSection();
    await QuestionnaireFlow.AddPricingTableDutchandJapneseAuction();
});
Then('iSource- I add pricing table for Japanese Auction', async function () {
    await QuestionnaireFlow.addSection();
    await QuestionnaireFlow.AddPricingTableDutchandJapneseAuction();
});

When('iSource- I add section with section name', async function () {
    let secName = Startup.testData.get('sectionNameDeletionQuestionnaire');
    await QuestionnaireFlow.addSection(secName);
})

When('iSource- I add section', async function (secName?: string) {
    
    await QuestionnaireFlow.addSection(secName);
});

Then('iSource- I click on Done', async function () {
    await QuestionnaireFlow.clickOnDone()
});

//---------------------------------------------------

Then('iSource- I add Flexi Item Table', async function () {
    await FlexiItemTableFlow.addFlexiItemTable();
});

Then('iSource- I create mapping for Flexi Item', async function () {
    await FlexiItemTableFlow.CreateMappingOfFlexiItemTable();
});

Then('iSource- I go to section {string}', async function( sectionNo : string ){
    await FlexiItemTableFlow.navigateToSection(sectionNo);
});
Then('iSource- I edit mapping for existing Flexi Item table', async function(){
    await FlexiItemTableFlow.editMappingForExistingTable();
});
Then('iSource- I unlock mapping for Flexi Item table', async function(){
    await FlexiItemTableFlow.unlockFlexiExcelMapping();
})
Then('iSource- I lock mapping for Flexi Item table', async function(){
    await FlexiItemTableFlow.lockFlexiExcelMapping();
})
Then('iSource- I override mapping for existing Flexi Item Table', async function(){
    await FlexiItemTableFlow.overrideFlexiExcelMapping();
})
//---------------------------------------------------

Then('iSource- I save draft', async function () {
    await QuestionnaireFlow.clickSaveAsDraft()
    // I.see("//dew-alert/div[contains(@class,'alert')]")
});
Then('iSource- I Freeze Event', async function () {
    await QuestionnaireFlow.clickFreeze()
});


When('iSource- I Click on Define Setting', async function () {
    await QuestionnaireFlow.clickDefineSetting();
});

When('iSource- I add Auction price', async function () {
    await QuestionnaireFlow.addAuctionStartingPrice();

});
When('iSource- I add Japnese Auction Price', async function () {
    await QuestionnaireFlow.addJapneseAuctionstartingPrice();
});
When('iSource- I add duration per round as {string}', async function (option: string) {
    await QuestionnaireFlow.setDurationPerRound(option);
});
When('iSource- I add Reserve price', async function () {
    await QuestionnaireFlow.addAuctionreservePrice();

})
When('iSource- I add Bid Increment percentage', async function () {
    await QuestionnaireFlow.addBidIncrementPercentage();
});

Then('iSource- I click Save on Define Setting Popup', async function () {
    await QuestionnaireFlow.clickSaveOnDefineSetting()

});

Then('iSource- I navigate to modify Questionnarire', async function () {
    await QuestionnaireFlow.modifyQuestionnaire();
})

When('iSource- I delete all questions from current section', async function () {
    await QuestionnaireFlow.deleteAllQuestionsFromCurrentSection();
})

Then('iSource- I verify all questions deleted from current section', async function () {
    await QuestionnaireFlow.verifyDeleteAllQuestionsFromCurrentSection();
})

When('iSource- I delete current section', async function () {
    await QuestionnaireFlow.deleteSection();
})

When('iSource- I verify current section is deleted', async function () {
    await QuestionnaireFlow.verifySectionDeletion();
})

Then('iSource- I copy current section', async function () {
    await QuestionnaireFlow.copySection();
})

Then('iSource- I verify copy of current section', async function () {
    await QuestionnaireFlow.verifyCopySection();
})

When('iSource- I export Questionnaire to excel', async function () {
    await QuestionnaireFlow.exportQuestionnaireToExcel();
})

When('iSource- I export Questionnaire to Word', async function () {
    await QuestionnaireFlow.exportQuestionnaireToWord();
})

When('iSource- I export Questionnaire with blank template', async function () {
    await QuestionnaireFlow.exportQuestionnaireToWord();
})

When('iSource- I import Questionnaire with Append to current Draft option', async function () {
    await QuestionnaireFlow.importUsingAppendToCurrentDraftOption();
})

When('iSource- I import Questionnaire with Overwrite to current Draft option', async function () {
    await QuestionnaireFlow.importUsingOverwriteToCurrentDraftOption();
})

When('iSource- I add Question with Marked as hidden', async function () {
    await QuestionnaireFlow.markedAsHidden("QuestionType_Default");
    await QuestionnaireFlow.markedAsHidden("QuestionType_Numeric");
    await QuestionnaireFlow.markedAsHidden("QuestionType_MultiChoice");
    await QuestionnaireFlow.markedAsHidden("QuestionType_YesNo");
    await QuestionnaireFlow.markedAsHidden("QuestionType_SingleChoice");
    await QuestionnaireFlow.markedAsHidden("QuestionType_Attachment");
    await QuestionnaireFlow.addQuestionTypeTableMarkedAsHidden();
})

When('iSource- I add Question without supplier comments', async function () {
    await QuestionnaireFlow.selectQuestionTypeWithoutSupplierComments("QuestionType_Default");
    await QuestionnaireFlow.selectQuestionTypeWithoutSupplierComments("QuestionType_Numeric");
    await QuestionnaireFlow.selectQuestionTypeWithoutSupplierComments("QuestionType_MultiChoice");
    await QuestionnaireFlow.selectQuestionTypeWithoutSupplierComments("QuestionType_YesNo");
    await QuestionnaireFlow.selectQuestionTypeWithoutSupplierComments("QuestionType_SingleChoice");
    await QuestionnaireFlow.selectQuestionTypeWithoutSupplierComments("QuestionType_Attachment");
    await QuestionnaireFlow.addQuestionTypeTableWithoutSupplierComments();
})

When('iSource- I add Question of type {string}', async function (type: string) {
    await QuestionnaireFlow.addQuestion(type);
})








