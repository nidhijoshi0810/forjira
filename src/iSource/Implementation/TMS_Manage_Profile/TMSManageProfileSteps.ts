import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {loginFlow} from "./../../../Framework/FrameworkUtilities/COE/Login/loginFlow";
import {commonLandingFlow} from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { z } from "actionbot-wrapper/z";
import {TMSManageProfile} from "../../POM/TMS_Manage_Profile/TMSManageProfileFlow";



Then('iSource- I move to manage profile page',async function(){
    await TMSManageProfile.moveToManageProfilePage();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
  });

Then('iSource- I get current user Email-Id',async function(){
    await TMSManageProfile.grabEmailId();
  });

Then('iSource- I get current user User Name',async function(){
    await TMSManageProfile.grabUserName();   
  });

Then('iSource- I set user location deatils',async function(){
    
  });

Then('iSource- I set user currency as {string}',async function(currency : string){
      await TMSManageProfile.setUserCurrency(currency);
  });

Then('iSource- I set user time zone as {string}',async function(timeZone : string){
      await TMSManageProfile.setTimeZone(timeZone);     
  });

Then('iSource- I set decimal precision as {string}',async function(decimalPrecision : number){
      await TMSManageProfile.setDecimalPrecision(decimalPrecision);
  });

Then('iSource- I verify changes made by Manage Profile', async function(){
      await TMSManageProfile.verifyCurrencyTimeZoneDecimalPrecision();
})






