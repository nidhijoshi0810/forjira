
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { QuickSourceSearch } from "../../POM/QuickSourceSearch/QuickSourceSearchFlow";


When('iSource- Search QuickSource event by {string} as {string}', async function(type: string,data:string){
    await QuickSourceSearch.searchEvent(type,data);
});

Then('iSource- I verify search result for {string} as {string}', async function(type:string, data:string){
    await QuickSourceSearch.verifySearch(type,data)
});
