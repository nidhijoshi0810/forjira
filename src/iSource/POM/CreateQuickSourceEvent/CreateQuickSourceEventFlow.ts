import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { CreateQuickSourceEvent } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import { ViewAllSuppliersSteps } from "../../POM/BayerSideSupplier/ViewAllSupplierFlow";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import { DewCheckbox } from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield"
import { Wait } from "dd-cc-zycus-automation/dist/components/dewWait";
import { SortColumn } from "dd-cc-zycus-automation/dist/components/sortColumn"
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup"
import { eventSettingsFlow } from "./../eventSettings/eventSettingsFlow";
import { DatePicker } from "dd-cc-zycus-automation/dist/components/datePicker"
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonLocators } from './../../isourceCommonFunctions/CommonLocators';
import { listingPageElements } from "../ListingPageFullSource/ListingPagePrimaryLocators";

export class SupplierGroupFlow {

    
    static async addEventName() {
        I.wait(10)
        var Eventname = await COE.randomString(10);
        I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.EventNameInpuBox), Eventname)
    }
    static async addItemDetails() {
        await SupplierGroupFlow.addDecription()
        await SupplierGroupFlow.addQuantity()
        await SupplierGroupFlow.addUOM()
        await SupplierGroupFlow.addCurrentPrice()

    }
    static async additemsQS() {
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.additemsQS) as string);
    }

    static async removeItemDetails() {

        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.QSremoveaddeditem) as string);
    }
    static async checkremoveditem() {
        I.dontSee(Startup.uiElements.get(CreateQuickSourceEvent.QSremoveaddeditem))
    }


    static async questionadded() {
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("QSquestiontab") as string);
        let Questtext: string = COE.randomString(10) as string;
        //  TextField.enterText(await iSourcelmt.getLabel("Questionsforsuppliers") as string,Questtext) ;
        // failed due to CC 
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateQuickSourceEvent.QSQuestion) as string, Questtext)
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.addquestion) as string);

    }
    static async openingmessage() {
        let QSQuestionText: string = await COE.randomString(10) as string;
        await TextArea.enterText(await iSourcelmt.getLabel("OpeningMessage") as string, QSQuestionText);
        // TextArea.enterTextUsingLocator(Startup.uiElements.get(CreateQuickSourceEvent.OpeningMsgInputBox) as string,QSQuestionText);
    }

    static async Selectcollabrator() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await DewCheckbox.selectAllCheckboxInGrid();
        
        // I.click(Startup.uiElements.get(CreateQuickSourceEvent.Select_all_addCollbratorName)); //passed
        //await SupplierGroupFlow.addcollabrator();
        await logger.info("Selected all Collabrator");
        Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
    }
    static async verifycollabratoradded() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Verifycollabratoradded) as string, await iSourcelmt.getLabel("InternalCollaboratorsName") as string));
    }
    static async Searchcollabrator(name: string) {
        Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let Search = await iSourcelmt.getLabel("Search") as string;
        let Collabratorname = await Startup.testData.get("Sarchinternalcollabrator") as string;
        let collbratorColumn = await iSourcelmt.getLabel(name) as string;
        await ModalPopUp.applyGridSearch(Search, Collabratorname, collbratorColumn);
        // await DewCheckbox.selectCheckboxInRow() // this was suggested by vidita satam
    }
    static async verifysearchCollabrator(name: string) {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Verifycollabrator) as string, await Startup.testData.get("Sarchinternalcollabrator") as string));
        await ModalPopUp.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.closepopup) as string);
    }
    static async SortColumnbyusername() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await SortColumn.filterByAscending("Internal Collaborators Name")
        logger.info("sorted User Name by ascending order")
    }
    static async SortColumnbyemailid() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await SortColumn.filterByAscending("Collaborators Email Id")
        logger.info("sorted Email Id by ascending order")

    }
    static async sortcolumn(name: string) {
        await SortColumn.filterByAscending(await iSourcelmt.getLabel(name) as string)
        await ModalPopUp.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.closepopup) as string);
    }

    static async sendreminder() {

        if (await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Sendreminder) as string, await iSourcelmt.getLabel("SendReminder") as string), true) {
            await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Sendreminder) as string, await iSourcelmt.getLabel("SendReminder") as string));
            logger.info("Reminder sent successfully")

        } else {
            logger.info("Remainder not send ")
        }
    }
    static async clickonExportResponses() {
        await DewButton.click(await iSourcelmt.getLabel("ExportResponses") as string);
    }
    static async addcollabrator() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Addcollbratorbutton) as string, await iSourcelmt.getLabel("AddCollaborator") as string));
    }
    static async addcollabratorresponsetab() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Addcollbratorresponsebutton) as string, await iSourcelmt.getLabel("AddCollaborator") as string));
    }
    static async sucessmessage() {
        await I.seeElement(Startup.uiElements.get(CreateQuickSourceEvent.successMessage));
    }
    static async NavigatetoCollbrator() {
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("Collaborators") as string);
    }
    static async navigatetoSuppliertab() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.AddSuppliertab) as string, await iSourcelmt.getLabel("AddSuppliers") as string))

    }
    static async openviewallSupplier() {
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
    }
    static async ShowSelectedlist() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Selectedlink) as string, await iSourcelmt.getLabel("Selected") as string))

    }
    static async verifyselectedresult() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.showall) as string, await iSourcelmt.getLabel("showall") as string))
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.closepopup) as string);

    }
    static async verifyopeingmessage() {
        await I.dontSee(Startup.uiElements.get(CreateQuickSourceEvent.placehoder) as string)
    }

    static async openviewallcollbratorpopup() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.ViewAllcollabrators) as string, await iSourcelmt.getLabel("ViewAllcollabrators") as string))

    }
    static async Addcollabratorpopup() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Addcollbratorbuttononresp) as string, await iSourcelmt.getLabel("AddCollaborator") as string))

    }
    static async removequestion() {
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.removequestion) as string);
    }
    static async checkremovequestion() {
        await I.dontSee(Startup.uiElements.get(CreateQuickSourceEvent.removequestion))

    }
    static async openingmessageheader() {

        await I.scrollIntoView(await Startup.uiElements.get(CreateQuickSourceEvent.QSopeningMessageheader) as string);
    }
    static async addDecription() {
        var EventDesc = await COE.randomString(10);
        await I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.DescriptionInputBox), EventDesc)
    }
    static async addQuantity() {

        await I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.QuantityInputBox), Startup.testData.get("defaultQuantityQS"));
    }
    static async addUOM() {
        await I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.UOMInputBox), Startup.testData.get("UOM"))
    }
    static async addCurrentPrice() {
        await I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.CurrentPriceInputBox), Startup.testData.get("Currentprice"))
    }
    static async addSupplier() {
        await SupplierGroupFlow.clickOnSuppliersTab()
        await ViewAllSuppliersSteps.navigateToViewAllSupliersPopup()
        await SupplierGroupFlow.selectSuppliersFromGridQS(Startup.testData.get("defaultSupplierText") as string);
    }
    static async viewAllSupplierPopupQS() {

        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.viewAllSupplierPopupqs) as string);
    }
    static async selectSuppliersFromGrid(supplierName: string) {
        let columnName: string = await iSourcelmt.getLabel("Supplier Company") as string;
        if (supplierName) {
            await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.SuppPopup) as string, supplierName, columnName);
            await SupplierGroupFlow.selectAllSupplierCheckbox();
            await ViewAllSuppliersSteps.clickOnAddSupplierButton();
        } else {
            await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.SuppPopup) as string, "audi", columnName);
            await SupplierGroupFlow.selectAllSupplierCheckbox();
            await ViewAllSuppliersSteps.clickOnAddSupplierButton();
        }
    }
//added below function only for Quicksource 
    static async selectSuppliersFromGridQS(supplierName: string) {
        let columnName: string = await iSourcelmt.getLabel("Supplier Company") as string;
        let Search: string = await iSourcelmt.getLabel("Search") as string;
       await ModalPopUp.applyGridSearch(Search,supplierName,columnName);
       await SupplierGroupFlow.selectAllSupplierCheckbox();
       await ViewAllSuppliersSteps.clickOnAddSupplierButton();
 
        // if (supplierName) {
        //     await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.searchSuppliersInputOfGridPopupqs) as string, supplierName, columnName);
        //     await SupplierGroupFlow.selectAllSupplierCheckbox();
        //     await ViewAllSuppliersSteps.clickOnAddSupplierButton();
        // } else {
        //     await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.searchSuppliersInputOfGridPopupqs) as string, "audi", columnName);
        //     await SupplierGroupFlow.selectAllSupplierCheckbox();
        //     await ViewAllSuppliersSteps.clickOnAddSupplierButton();
        // }
    }
    static async selectAllSupplierCheckbox() {
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.Select_all_checkbox_AllSuppliers_QuickSource) as string);
        logger.info("Select all checkbox is checked.");
    }



    static async clickOnSuppliersTab() {
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CreateQuickSourceEvent.tabXpath) as string, await iSourcelmt.getLabel("SupplierLabel") as string));
    }

    static async createNewSupplier() {
        let supplierName: string = await COE.randomString("10") as string;
        var supplierMailid = (supplierName + "@gmail.com");
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.CreateSupplierSearchField) as string);
        I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.CreateSupplierSearchField), Startup.testData.get("SearchSupplierInput"))
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.SuggestNewSupplierXpath) as string, await iSourcelmt.getLabel("SuggestNewSupplierButton") as string));
        await SupplierGroupFlow.setSupplierName(supplierName)
        await SupplierGroupFlow.setSupplieContact()
        await SupplierGroupFlow.setMailId(supplierMailid)
        I.wait(10)
        await SupplierGroupFlow.clickDoneOnCreateNewSupplier()
        I.wait(5)

    }

    static async setSupplierName(supplierName: string) {
        I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.SupplierNameInputField), supplierName)
    }
    static async setSupplieContact() {
        I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.SupplierContactName), Startup.testData.get("ContactInput"))
    }
    static async setMailId(supplierMailid: string) {
        I.fillField(Startup.uiElements.get(CreateQuickSourceEvent.SupplierMailId), supplierMailid)
    }

    static async clickDoneOnCreateNewSupplier() {
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.DoneButton) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async clickSaveAsDraft() {
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("Save as Draft") as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }

    static async clickSend() {
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.SendButton) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await I.waitForText(await iSourcelmt.getLabel("Go To Responses"));

    }

    static async clickOnAttachmentTab() {
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CreateQuickSourceEvent.tabXpath) as string, await iSourcelmt.getLabel("AttachmentLabel") as string));

    }
    static async selectLastDate() {
        await SupplierGroupFlow.clickOnAttachmentTab()
        await (DatePicker.selectDate(Startup.uiElements.get(CreateQuickSourceEvent.LastDateInputField) as string, await (eventSettingsFlow.getCurrentDate(2))));
        

        I.wait(prop.DEFAULT_LOW_WAIT)
        await SupplierGroupFlow.clickOnAttachmentTab()
    }
    
    static async selectModifyDate(){
        await DatePicker.selectDateAndTime(await Startup.uiElements.get(CreateQuickSourceEvent.ModifyDate) as string,
        (await CommonFunctions.getTodaysDateInMMDDYYYYFormat(2)),(await CommonFunctions.addMinutesToCurrentDate(2)));
        I.wait(prop.DEFAULT_LOW_WAIT) ;
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.ModifyClosureDateOutClick) as string, await iSourcelmt.getLabel("Modify Closure Date") as string));
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.ModifyButton) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.ModifyDone) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed(); 
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }


    static async editEventAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.primaryActionEdit) as string, await iSourcelmt.getLabel("Edit Event") as string));
         await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("Event Details") as string));
         I.wait(prop.DEFAULT_LOW_WAIT) ;
     }

     static async ModifyClDateCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Modify") as string);
        await I.pressKey(`Escape`); 
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }

    static async ViewInquiryCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Event Details") as string);
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }

    static async CopyCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Copy") as string);
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }

    static async ViewResponseCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("SupplierDetails") as string);
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }


    static async viewResponseAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.primaryActionEdit) as string, await iSourcelmt.getLabel("View_Response") as string));
         await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("SupplierDetails") as string));
         I.wait(prop.DEFAULT_LOW_WAIT) ;
     }

     static async QuickActivityChk(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Activity") as string);
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }
     
    static async CloseQuickEvent(){
        await DatePicker.selectDateAndTime(await Startup.uiElements.get(CreateQuickSourceEvent.ModifyDate) as string,
        (await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(2)));
        I.wait(prop.DEFAULT_LOW_WAIT) ;
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.ModifyClosureDateOutClick) as string, await iSourcelmt.getLabel("Modify Closure Date") as string));
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.ModifyButton) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.ModifyDone) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed(); 
        I.wait(prop.DEFAULT_LOW_WAIT) ;
    }


    static async AwardEventAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.primaryActionEdit) as string, await iSourcelmt.getLabel("Award Event") as string));
         await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("SupplierDetails") as string));
         I.wait(prop.DEFAULT_LOW_WAIT) ;
     }

     static async searchCollabOnGridByName(){
        I.wait(prop.DEFAULT_LOW_WAIT) ;
        let CollabColumnName=await iSourcelmt.getLabel("Internal Collaborators Name");
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.SearchCollabOnGrid) as string);
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.SearchCollabOnGrid) as string,Startup.testData.get("CollabSearchOnGrid") as string,CollabColumnName as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.CloseSearchCollabGrid) as string);
     }

     static async searchCollabOnGridByEmail(){
        I.wait(prop.DEFAULT_LOW_WAIT) ;
        let CollabColumnName=await iSourcelmt.getLabel("Collaborators Email Id");
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.SearchCollabOnGrid) as string);
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.SearchCollabOnGrid) as string,Startup.testData.get("CollabSearchOnGridEmail") as string,CollabColumnName as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateQuickSourceEvent.CloseSearchCollabGrid) as string);
     }

     static async AddCollabForGridSearch(){
        I.wait(prop.DEFAULT_LOW_WAIT) ;
        let CollabColumnName=await iSourcelmt.getLabel("Internal Collaborators Name");
        let Search: string = await iSourcelmt.getLabel("Search") as string;
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(CreateQuickSourceEvent.searchBoxForViewAllCollabratorPopup) as string,Startup.testData.get("CollabSearchOnGrid") as string,CollabColumnName as string);
        await DewCheckbox.selectAllCheckboxInGrid();
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.Addcollbratorbutton) as string, await iSourcelmt.getLabel("AddCollaborator") as string));
     }


     static async addSuppliersFromResponsesTab(){
        I.wait(prop.DEFAULT_LOW_WAIT) ;
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.primaryActionEdit) as string, await iSourcelmt.getLabel("Add Supplier") as string));
       await DewCheckbox.selectAllCheckboxInGrid();
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.primaryActionEdit) as string, await iSourcelmt.getLabel("AddSuppliers") as string));
       await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
     }



}
    
// module.exports = new SupplierGroupFlow();