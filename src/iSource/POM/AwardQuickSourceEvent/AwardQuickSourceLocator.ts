export let awardQuickSourceElements = {
    eventDetailss: "QuickSource/eventDetails",
    clickGoResponseButtom: "QuickSource/clickGoResponseButtom",
    selectCheckBoxSuppliers: "QuickSource/selectCheckBoxSuppliers",
    awardButton: "QuickSource/clickAwardButton",
    reasonForAward: "QuickSource/reasonForAward",
    awardNote: "QuickSource/awardNote",
    clickSaveButton: "QuickSource/saveButton",
    awardSupplierReason: "QuickSource/awardSupplierReason",
    awardSupplierNote: "QuickSource/awardSupplierNote",
    clickViewAwardNote: "QuickSource/viewAwardNote",
    clickExportResponse: "QuickSource/exportResponse",
    inputField: "QuickSource/chnageInputField" ,
    closeawardnote:"QuickSource/closeawardnote"
}