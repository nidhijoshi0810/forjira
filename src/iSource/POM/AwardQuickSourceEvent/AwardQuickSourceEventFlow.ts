import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { awardQuickSourceElements } from "./AwardQuickSourceLocator";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import {Checkbox} from "dd-cc-zycus-automation/components/checkbox";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";

export class AwardQuickSource {

    
    static async eventDetails() {
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(awardQuickSourceElements.eventDetailss) as string, await iSourcelmt.getLabel("Event Details") as string));
    }

    static async clickGoResponseButton(){
        await CommonKeyword.clickElement(Startup.uiElements.get(awardQuickSourceElements.clickGoResponseButtom) as string);
    }

    static async selectSupplierToAward(){
        await CommonKeyword.clickElement(Startup.uiElements.get(awardQuickSourceElements.selectCheckBoxSuppliers) as string);
    }

    static async clickAwardButton() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(awardQuickSourceElements.awardButton) as string, await iSourcelmt.getLabel("Award") as string));
        I.wait(prop.DEFAULT_LOW_WAIT);
        await TextArea.enterTextUsingLocator(Startup.uiElements.get(awardQuickSourceElements.awardSupplierReason) as string, Startup.testData.get("awardReason") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await TextArea.enterTextUsingLocator(Startup.uiElements.get(awardQuickSourceElements.awardSupplierNote) as string, Startup.testData.get("noteAwardSupplier") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(awardQuickSourceElements.clickSaveButton) as string, await iSourcelmt.getLabel("saveButton") as string));
    }

    static async clickViewAwardNote(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(awardQuickSourceElements.clickViewAwardNote) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(awardQuickSourceElements.closeawardnote)as string);
        await I.pressKey(`Escape`); 
        I.wait(prop.DEFAULT_LOW_WAIT);
    }

}