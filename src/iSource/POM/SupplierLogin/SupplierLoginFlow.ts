import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { supplierLoginAction } from "./SupplierLoginAction";
import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
declare const inject: any; 
const {I} = inject();

export class SupplierLogin{

    static async loginToZSN(){
        try{
        I.amOnPage(Startup.testData.get("zsnLogin"));
        I.fillField(Startup.uiElements.get(supplierLoginAction.UserName), Startup.testData.get("ZSN_UserName"))
        I.fillField(Startup.uiElements.get(supplierLoginAction.Password), Startup.testData.get("ZSN_UserPassword"))
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierLoginAction.SignIn) as string);
        // I.click(Startup.uiElements.get(supplierLoginAction.SignIn))
    }

        catch(err){
            await SupplierLogin.logout()
        }
    }
    static async logout(){
        await CommonKeyword.clickLabel("#headerDisplayName");
        await CommonKeyword.clickLabel("#logout");
        // I.click("#headerDisplayName")
        // I.click("#logout")
        I.see("#login-email")
   
}
    }

// module.exports = new SupplierLogin