import "codeceptjs";
declare const inject: any;
const { I } = inject();

import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CreateContractLocators } from "../CreateContract/CreateContractLocator"
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { Wait } from "dd-cc-zycus-automation/dist/components/dewWait"
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { TextArea } from 'dd-cc-zycus-automation/dist/components/textArea';
import { Footer } from 'dd-cc-zycus-automation/dist/components/Footer';
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { strict } from "assert";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { TMSManageProfile } from "../TMS_Manage_Profile/TMSManageProfileFlow"
export class CreateContractFlow {

    static ContractID: string;
    static async clickCreateContractButton() {
      
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(CreateContractLocators.CreateContractButton) as string, await iSourcelmt.getLabel("Create Contract") as string));

    }

    static async typeContractName() {
         GlobalVariables.contractName=(await COE.randomString(10))as string;
        await Wait.waitForDefaultTimeout(prop.DEFAULT_LOW_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        // await TextArea.enterTextUsingLocator(await Startup.uiElements.get(CreateContractLocators.ContractNameTextbox) as string, await Startup.testData.get("AutomatedContractCreation") as string);
        await TextArea.enterTextUsingLocator(await Startup.uiElements.get(CreateContractLocators.ContractNameTextbox) as string,  GlobalVariables.contractName);

    }

    static async selectContractType() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateContractLocators.ContractTypeDropdown) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateContractLocators.ContractTypeDropdownValue) as string);
    }

    static async selectContractSubType() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateContractLocators.ContractSubTypeDropdown) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateContractLocators.ContractSubTypeDropdownValue) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }

    static async selectContractOwner() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_MEDIUM_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        // await TMSManageProfile.moveToManageProfilePage();
        // var contractOwner: string = await TMSManageProfile.grabUserName();
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateContractLocators.ContractOwnerTextBox) as string);
        I.fillField((Startup.uiElements.get(CreateContractLocators.ContractOwnerTextBox) as string), "self");
        I.wait(prop.DEFAULT_HIGH_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(CreateContractLocators.ContractOwnerDDValue) as string);
        await Wait.waitForDefaultTimeout(prop.DEFAULT_LOW_WAIT);
    }

    static async clickCreateContractBtn() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await Footer.clickModalFooterButton(await iSourcelmt.getLabel("Create Contract") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        
    }

    static async verifyContractCreation(){

        var IDone : string = await DewElement.grabTextFrom((Startup.uiElements.get(CreateContractLocators.ContractIDValue) as string));  //;
        var contractNo : string = IDone.split(" ")[1].split(":")[0];
        logger.info(contractNo +" contractNo");
        CreateContractFlow.ContractID = contractNo;
        logger.info(contractNo +" contractNo" + " " + CreateContractFlow.ContractID);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator( Startup.uiElements.get(CreateContractLocators.ContractDoneButton) as string, await iSourcelmt.getLabel("DoneLabelKey") as string));
        GlobalVariables.ContractIDForAllProds = contractNo;
    }

    static async verifyContractIDOnGrid(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeElement(Startup.uiElements.get(CreateContractLocators.ContractOnGrid) as string);
    }

}



