export let CreateContractLocators = {
    CreateContractButton: "CreateContract/createContractBtn",
    ContractNameTextbox: "CreateContract/contractTextbox",
    ContractTypeDropdown: "CreateContract/contractTypeDropdown",
    ContractTypeDropdownValue: "CreateContract/contractTypeDDValue",
    ContractSubTypeDropdown: "CreateContract/contractSubTypeDropdown",
    ContractSubTypeDropdownValue: "CreateContract/ContractSubTypeDDValue",
    ContractOwnerTextBox: "CreateContract/ContractOwnerDropdown",
    ContractOwnerDDValue: "CreateContract/ContractOwnerDDValue",
    CreateContractContractButton: "CreateContract/createContractButton",
    ContractDoneButton: "CreateContract/ContractDoneButton",
    ContractIDValue : "CreateContract/ContractIDValue",
    ContractOnGrid : "CreateContract/ContractOnGrid",
}