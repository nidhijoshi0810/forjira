import { CommonLocators } from './../../isourceCommonFunctions/CommonLocators';
import { listingPageElements } from './../ListingPageFullSource/ListingPagePrimaryLocators';
import "codeceptjs"; 
declare const inject: any;
const { I } = inject();
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter"
import { createEventElements } from "./CreateEventLocator";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { GlobalVariables } from "../../dataCreation/GlobalVariables";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions"
import { loginAction } from "../../../Framework/FrameworkUtilities/COE/Login/loginAction";
import{CommonKeyword}  from "dd-cc-zycus-automation/dist/components/commonKeyword";
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element"
import {Wait} from"dd-cc-zycus-automation/dist/components/dewWait"
import { Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
import { CONNREFUSED } from 'dns';

export class CreateEventFlow{

  static async searchEventAndOpenEvent(eventID:string){
     I.wait(prop.DEFAULT_WAIT);
     await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events")as string,eventID,await iSourcelmt.getLabel("EventColumnName")as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    //@Rushabh commmented as the codecept method is failing even when the element exists in dom
    //await I.waitForText(eventID);
    await CreateEventFlow.OpenEvent(eventID);
  }
static async OpenEvent(eventID:string){
  await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(createEventElements.eventNameColumnRecordXpath)as string,eventID))
  // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(createEventElements.eventNameColumnRecordXpath)as string,eventID));
  await iSourcelmt.waitForLoadingSymbolNotDisplayed();
  //@Rushabh commmented as the codecept method is failing even when the element exists in dom
  //await I.waitForText(await iSourcelmt.getLabel("EventDetailsTab"));
}
    static async clickOnCreateEvent(){
      await iSourcelmt.waitForLoadingSymbolNotDisplayed()
      I.wait(prop.DEFAULT_LOW_WAIT);  
      await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("CreateEventbutton")  as string);
      //  I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.CreateEventButton)as string,await iSourcelmt.getLabel("CreateEventbutton")as string)); 
   }
    static async verifyEventPage(){
        I.see("Create Sourcing Event")
   }
    static async fillEventTitle(){
        // await CommonKeyword.enterText(Startup.uiElements.get(createEventElements.EventName) as string, Startup.testData.get("EventName") as string);
       GlobalVariables.EventTitle=Startup.testData.get("EventName") as string + COE.randomString(5);
      await Startup.testData.set("EventName",GlobalVariables.EventTitle);
      I.fillField(Startup.uiElements.get(createEventElements.EventName),Startup.testData.get("EventName") as string);
    }
    static async verifyEventName()
    {
      I.waitForText(Startup.testData.get("EventName"));
    }
   static async fillEventDesc(){
    // await CommonKeyword.enterText(Startup.uiElements.get(createEventElements.EventDesc) as string, Startup.testData.get("EventName") as string);
    I.fillField(Startup.uiElements.get(createEventElements.EventDesc),Startup.testData.get("EventName"));
   }
   static async selectEventType(eventType : string){
     await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.EventTypeRadio) as string, await iSourcelmt.getLabelFromKey(eventType) as string));
    // I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.EventTypeRadio) as string, await iSourcelmt.getLabelFromKey(eventType) as string));
   }
   static async selectauctypeAndAuctionsubtype(auctionType : string,auctionSubType : string){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionType) as string, await iSourcelmt.getLabelFromKey(auctionType) as string));
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionType) as string, await iSourcelmt.getLabelFromKey(auctionType) as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionSubType) as string, await iSourcelmt.getLabelFromKey(auctionSubType) as string));
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionSubType) as string, await iSourcelmt.getLabelFromKey(auctionSubType) as string));
     
    }
static async selecteventTypeAuction(eventType : string,auctionType : string){
  if(eventType==="Auction"){
    I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.EventTypeRadio) as string, await iSourcelmt.getLabelFromKey(eventType) as string));
    I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionType) as string ,await iSourcelmt.getLabelFromKey(auctionType) as string));
  }
  else{
    I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.EventTypeRadio) as string, await iSourcelmt.getLabelFromKey(eventType) as string));
  }
}
static async selectauctionSubType(auctionSubType : string){
  I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionSubType) as string, await iSourcelmt.getLabelFromKey(auctionSubType) as string));
  I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AuctionSubType) as string, await iSourcelmt.getLabelFromKey(auctionSubType) as string));
}
    
   static async checkTestEvent(testEvent : string){
      // I.wait(prop.DEFAULT_WAIT);
      await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.TestEventCheckbox) as string,testEvent));
      //  I.click(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.TestEventCheckbox) as string,testEvent));
   }
   static async clickCreateButton(){
       I.wait(prop.DEFAULT_WAIT);
       await CommonKeyword.clickElement(await Startup.uiElements.get(createEventElements.CreateButton) as string);
      //  I.click(Startup.uiElements.get(createEventElements.CreateButton));
       iSourcelmt.waitForLoadingSymbolNotDisplayed()
       
      // I.see(Startup.uiElements.get(createEventElements.EventID));
   }
 static async getEventID(){
   I.wait(4);
   await iSourcelmt.waitForLoadingSymbolNotDisplayed();
   I.waitForText(await iSourcelmt.getLabel("Event Details"));
   var title =await I.grabTextFrom(Startup.uiElements.get(createEventElements.EventID));
   GlobalVariables.eventId=title.replace(/\D/g,"");
    logger.info("Current event id is "+GlobalVariables.eventId);
    return GlobalVariables.eventId;
   }

   static async getEventName(){
    I.wait(4);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.waitForText(await iSourcelmt.getLabel("Event Details"));
    var title =await I.grabTextFrom(Startup.uiElements.get(createEventElements.SpotLightEventName));
   // GlobalVariables.eventId=title.replace(/\D/g,"");
     logger.info("Current event name is "+title);
     return title;
    }
    
   static async clickOnAdditionalDetailsTab()
   {
     if(await I.checkIfVisible(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AdditionalDeatailsTab) as string, await iSourcelmt.getLabel("AdditionalDetailsTabLabel") as string))){
     
      await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(createEventElements.AdditionalDeatailsTab) as string, await iSourcelmt.getLabel("AdditionalDetailsTabLabel") as string));
     await CreateEventFlow.SelectAllAdditionalDropdown();
     await CreateEventFlow.selectFlexiFormDetails();
     }
     else{
       console.log("Additional details are not configured")
     }
   }
  

    static async SelectAllAdditionalDropdown()
   {
     console.log("inside fill additional details.");
    var dropdowns = new Array(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.AdditionalDetailsDropdown)as string));
     console.log("inside fill additional details."+dropdowns);
     for(let i=1;i<=dropdowns.length; i++){
        I.click(locate(Startup.uiElements.get(createEventElements.AdditionalDetailsDropdown)as string).at(i));
        I.clearField(locate(Startup.uiElements.get(createEventElements.AdditionalDetailsDropdown)as string).at(i));
        I.fillField(locate(Startup.uiElements.get(createEventElements.AdditionalDetailsDropdown)as string).at(i)," ");

        I.seeElement(Startup.uiElements.get(createEventElements.dopdownDiv)as string);
       let enteries= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.dopdownDiv)as string);{
         if(enteries >1){
          I.click(locate(Startup.uiElements.get(createEventElements.dopdownDiv)as string).at(2));
         }else{
          I.click(locate(Startup.uiElements.get(createEventElements.dopdownDiv)as string).at(1));
         }
       }
       

     }
    }
     static async  selectFlexiFormDetails(){

      var status=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.flexiformSection) as string);
      if(status==1){
        console.log("Flexiform details is present.");
       var inputfields= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.flexiformInputFiled) as string);
       console.log("input fileds "+ inputfields);
       if(inputfields>0){
        console.log("inside if loop ");
        for(let i=1;i<=inputfields; i++){
          console.log("inside for loop");
          I.fillField(locate(Startup.uiElements.get(createEventElements.flexiformInputFiled) as string).at(i),"test");
       }
      }
      var numericfields= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.flexiformNumericTypeFiled)as string);
      if(numericfields>0){
       
        for(let i=1;i<=numericfields; i++){
          
          I.fillField(locate(Startup.uiElements.get(createEventElements.flexiformNumericTypeFiled) as string).at(i),"40");
       }
      }
      let textArea= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.flexiformTextArea)as string);
      if(textArea>0){
        for(let i=1;i<=textArea; i++){
          I.fillField(locate(Startup.uiElements.get(createEventElements.flexiformTextArea) as string).at(i),"Automation run");
       }
      }
      let radioBtns= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.flexiformRadioBtn)as string);
      if(radioBtns>0){
        for(let i=1;i<=textArea; i++){
         I.click(Startup.uiElements.get(createEventElements.flexiformRadioBtnOption) as string);
       }
      }
      var multiselectfields= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(createEventElements.flexiformMultiselect)as string);
      if(multiselectfields>0){
       
        for(let i=1;i<=multiselectfields; i++){
          console.log("inside for loop");
          I.click(locate(Startup.uiElements.get(createEventElements.flexiformMultiselect)as string).at(i));
          await CommonKeyword.clickElement(await Startup.uiElements.get(createEventElements.flexiformMultiselectOption) as string);
       }
      }
     }
 
    }

    //view All Supplier popup
    static async viewAllSupplierPopup(){
      logger.info("viewallsupplierpopup");
      await Wait.waitForDefaultTimeout(10);
      await I.seeElement(await Startup.uiElements.get(createEventElements.viewAllSupplierPopup) as string);
    }
 
    static async searchByCompnyContactName(name:string){
      await Wait.waitForDefaultTimeout(5);
      iSourcelmt.waitForLoadingSymbolNotDisplayed();
      let supplierData = await Startup.testData.get("defaultSupplierText") as string;
      let supplierColumn = "Supplier Column";
      if(name == "company name"){
        supplierColumn = await iSourcelmt.getLabel("supplierColumnName") as string;
      }else if(name == "contact name"){
        supplierColumn = await iSourcelmt.getLabel("supplierColumnName") as string;
      }
      await CommonKeyword.clickElement(await Startup.uiElements.get(createEventElements.viewAllSupplierPopup) as string)
      // await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search")as string,supplierData,supplierColumn);
      await COE.searchOnGridProvidedLocator(await Startup.uiElements.get(createEventElements.searchBoxForViewAllSupplierPopup) as string, supplierData, supplierColumn);
    }

    static async verifyCompnyContactName(name:string){
      iSourcelmt.waitForLoadingSymbolNotDisplayed();
      logger.info("verified Supplier company and contact name");
      await Wait.waitForDefaultTimeout(5);
      await I.seeElement(await Startup.uiElements.get(createEventElements.verifyCopmanyOrContactName) as string);
    }

    static async removeSupplier() {
      let firstSupplier = await I.grabTextFrom(await Startup.uiElements.get(createEventElements.grabFirstElementInSuppluier) as string);
      logger.info("firstSuupplier" + firstSupplier);
      await Wait.waitForDefaultTimeout(2);
      await CommonKeyword.clickElement(await Startup.uiElements.get(createEventElements.deleteSupplier) as string);
      let removedSupplier = await I.grabTextFrom(await Startup.uiElements.get(createEventElements.grabFirstElementInSuppluier) as string);
      logger.info("secondSuupplier" + removedSupplier);
      await Wait.waitForDefaultTimeout(2);
      if (firstSupplier !== removedSupplier) {
        logger.info("Removed Successfully");
      }
    }

  static async searchEventAndOpenEventforModifyingDate(eventID: string) {
    I.wait(prop.DEFAULT_WAIT);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events") as string, eventID, await iSourcelmt.getLabel("EventColumnName") as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
  }
   
   
}

// module.exports = new CreateEventFlow();