const { I } = inject();

export let createCopyEventElements = {
  SearchATemplate : "template/searchATemplate",
  SeeATemplate : "template/seeATemplate",
  SelectATemplate : "template/selectATemplate",
}