import "codeceptjs";
declare const  inject: any;
const { I } = inject();
import { createCopyEventElements } from "./CreateCopyEventLocator";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup"
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';


export class CreateCopyEventFlow {

  static async selectTemplate(event: string) {
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await ModalPopUp.applyGridSearch(await iSourcelmt.getLabel("Search") as string, await iSourcelmt.getLabel(event) as string, await iSourcelmt.getLabel("EventName") as string);
  }

  static async seeTheTemplate(eventType: string) {
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CommonKeyword.clickElement(await Startup.uiElements.get(createCopyEventElements.SeeATemplate) as string);
    await CommonKeyword.clickElement(await Startup.uiElements.get(createCopyEventElements.SelectATemplate) as string);
    await CreateEventFlow.clickCreateButton();
    var eventId=await CreateEventFlow.getEventID();
    Startup.eventId_Map.set(eventType,eventId);
    console.log("**********event ID"+eventId);
  }
  
}
