const { I } = inject();



export let createEventElements = {
     CreateEventButton:"FullSourceEventPage/CreateEventButton",
     EventName:"ISource/createEventPage/EventNameTextBox",
     EventDesc:"ISource/createEventPage/EventDescriptionTextBox",
     EventTypeRadio:"FullSourceEventPage/EvenTypeRadio",
     TestEventCheckbox:"FullSourceEventPage/MarkasTestCheckbox",
     CreateButton:"ISource/createEventPage/EnabledCreateButton",
     EventID:"ISource/EventDetailsPageCreateModeBeforeFreeze/BasicDetails/EventIdInSpotlight",
     SpotLightEventName:"ISource/BasicDetails/EventNameSpotlight",
     AuctionType:"FullSourceEventPage/AuctionType",
     AuctionSubType:"FullSourceEventPage/AuctionSubType",
     AdditionalDeatailsTab:"FullSourceEventPage/Additional_Details_Tab",
     AdditionalDetailsDropdown:"FullSourceEventPage/AdditionalDetailDropDown",
     SelectedOptionXpath:"FullSourceEventPage/SelectAdditionalDropdown",
     flexiformSection:"Additional details/flexiformSection",
     flexiformInputFiled:"Additional details/flexiformInputFiled",
     flexiformNumericTypeFiled:"Additional details/flexiformNumericTypeFiled",
     flexiformMultiselect:"Additional details/flexiformMultiselect",
     flexiformMultiselectOption:"Additional details/flexiformMultiselectOption",
     dopdownDiv:"common/dopdownDiv",
     eventNameColumnRecordXpath:"listingPage/eventNameColumnRecordXpath",
     viewAllSupplierPopup:"viewAllsupplierpopup/verifyViewAllSupplier",
     searchBoxForViewAllSupplierPopup:"viewAllsupplierpopup/searchBox",
     flexiformTextArea:"AdditionalDetails/flexiformTextArea",
     flexiformRadioBtn:"AdditionalDetails/flexiformRadioBtn",
     flexiformRadioBtnOption:"AdditionalDetails/flexiformRadioBtnOption",
     verifyCopmanyOrContactName:"viewAllsupplierpopup/verifyCompanyNameorContactName",
     deleteSupplier:"viewAllsupplierpopup/deleteSupplier",
     grabFirstElementInSuppluier:"viewAllsupplierpopup/grabTheElement",   
    
}