import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { SupplierHomeAction } from "./SupplierHomeAction";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { DatabaseOperations } from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import { commonLandingFlow } from '../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow';
import { SupplierLogin } from "../../POM/SupplierLogin/SupplierLoginFlow"
import "codeceptjs"; 
import { SurrogateBidFlow } from "../SurrogateBid/SurrogateBidFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
declare const inject: any; 
const {I} = inject();
// const elements = require("../../POM/SurrogateBidFlow/SurrogateBidLocators")
export  class supplierHomeFlow {

    static async selectMyEventFromSideMenu(){
        try{
        await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.sideMenuMyEventOption) as string);
        // I.click(Startup.uiElements.get(SupplierHomeAction.sideMenuMyEventOption))
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("ViewSourcingEvents") as string);
        // I.click(await iSourcelmt.getLabel("ViewSourcingEvents"))
        
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        // I.waitForElement(Startup.testData.get("SupplierNameForResponse"))
        // I.click(Startup.testData.get("SupplierNameForResponse"), "#zsp-submenus-id");
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        I.waitForElement(Startup.uiElements.get(SupplierHomeAction.SupplierEventList))
        // I.click(Startup.uiElements.get(supplierHomeAction.SupplierEventList))
            }catch(err){
                await SupplierLogin.logout()
            }
    }
    static async loginTOSupplierSideLagacy(){
        I.stopBrowser();
        I.startBrowser();
        I.amOnPage(Startup.testData.get("supp_url"));
        I.seeElement(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled));
        await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled) as string);
        I.fillField(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled),Startup.testData.get("supp_username"));
        I.fillField(Startup.uiElements.get(SupplierHomeAction.usernameEmailInputFiled),Startup.testData.get("supp_email"));
        await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.passwordClick) as string);
        I.fillField(Startup.uiElements.get(SupplierHomeAction.passwordInputField),Startup.testData.get("supp_password"))
        await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.loginBtn) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
       
        if ( await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SupplierHomeAction.dialogueOnListingPage) as string)> 0) {
                await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.dialogueOnListingPage) as string);

        }

        if ( await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SupplierHomeAction.clearFilterXpath) as string) > 0) {
            await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.clearFilterXpath) as string);

        }
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        I.seeElement(Startup.uiElements.get(SupplierHomeAction.searchEventID));
    }

    static async searchEventAndEnter(eventType : string){
        try{
            // var eventID= (await DatabaseOperations.getTestData()).get(eventType);
            var eventID= Startup.eventId_Map.get(eventType);
           console.log("Response is getting submitted for event "+eventID);
       I.fillField(Startup.uiElements.get(SupplierHomeAction.searchEventID), eventID)
    //    I.fillField(Startup.uiElements.get(SupplierHomeAction.searchEventID), "1110269916")
        I.pressKey('Enter');
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("EnterEventLabel") as string);
        
        }catch(err){
            throw err;
           // logout.logout()
            }
    }

    static async searchEventAndEnterQS(eventID : string){
        try{
            console.log("Response is getting submitted for event "+eventID);
       I.fillField(Startup.uiElements.get(SupplierHomeAction.searchEventID), eventID)
    //    I.fillField(Startup.uiElements.get(SupplierHomeAction.searchEventID), "1610058648")
        I.pressKey('Enter');
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("EnterInquiryLabel") as string);
        // I.click(await iSourcelmt.getLabel("EnterInquiryLabel"));
       await I.waitForText(await iSourcelmt.getLabel("Request Overview"));
        }catch(err){
            throw err;
           // logout.logout()
            }
    }
    static async AcceptTermsCondition(){
       await SurrogateBidFlow.clickOnAccept();

    }
   static async clickOnPrepareResponseBtn(){
    //I.wait(prop.DEFAULT_LOW_WAIT);
    //await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.prepareResponseBtn) as string);
    I.wait(prop.DEFAULT_LOW_WAIT);
        I.waitForElement(Startup.uiElements.get(SupplierHomeAction.firstPrepareResponseBtnXpath));
        await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.firstPrepareResponseBtnXpath) as string);
        // await I.click(Startup.uiElements.get(SupplierHomeAction.firstPrepareResponseBtnXpath));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT);
    }

    static async confirmParticipation(){
        
     await   SurrogateBidFlow.clickOnConfirmParticipation()
    }
    static async logoutFromSupplierSideLegacy(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.supplierlogoutbtnxpath) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        I.seeElement(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled));
    }

}

// module.exports = new supplierHomeFlow