export let SupplierHomeAction = {
    sideMenuMyEventOption : "SupplierHome/sideMenu/MyEvent",
    SupplierEventList : "SupplierGrid/SuplierEventList_Grid",
    searchEventID : "SupplierGrid/searchEventInput",
    enterEventButton : "SupplierGrid/Enter_Event_Label",
    SupplierName : "SupplierGrid/supplierTabName",
    firstPrepareResponseBtnXpath:"SupplierPrepare response/firstPrepareResponseBtnXpath",
    usernameInputFiled:"legacySupplierSide/usernameInputFiled",
    usernameEmailInputFiled:"legacySupplierSide/usernameEmailInputFiled",
    passwordClick:"legacySupplierSide/passwordClick",
    passwordInputField:"legacySupplierSide/passwordInputField",
    loginBtn:"legacySupplierSide/loginBtn",
    clearFilterXpath:"supplierResponse/clearFilterXpath",
    dialogueOnListingPage:"supplierResponse/dialogueOnListingPage",
    prepareResponseBtn: "supplierResponse/prepareResponseBtn",
    supplierlogoutbtnxpath:"SupplierHome/supplierlogoutbtnxpath"
}