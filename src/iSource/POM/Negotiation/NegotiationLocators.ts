export let NegotiationLocators = {
searchEventID : "SupplierGrid/searchEventInput",
EditRoundButtonXpath:"ISource/Negotiation/Buyerside/EditRoundButtonXpath",
MenuIcon:"ISource/Negotiation/Buyerside/MenuIcon",
DownloadBidDocumentDropdownoption:"ISource/Negotiation/Buyerside/DownloadBidDocumentDropdownoption",
Start_NegotiationButton:"ISource/Negotiation/Buyerside/Start_NegotiationButton",
Negotiatebutton:"ISource/Negotiation/Buyerside/Negotiatebutton",
SendForApprovalButton:"ISource/Negotiation/Buyerside/SendForApprovalButton",
First_record_name:"CommonComponent/Grid Component/General/First_record_name",
RejectButton:"ISource/Negotiation/Buyerside/RejectButton",
AcceptButton:"ISource/Negotiation/Buyerside/AcceptButton",
CounterBidButton:"ISource/Negotiation/Buyerside/CounterBidButton",
BrowserButton:"ISource/Negotiation/Buyerside/BrowserButton",
SaveRoundButton:"ISource/Negotiation/Buyerside/SaveRoundButton",
DoneButtonSendApproval:"ISource/Negotiation/Buyerside/DoneButtonSendApproval",
DropDownText:"ISource/Negotiation/Supplierside/DropDownText",
ActionDropdown:"ISource/Negotiation/Supplierside/ActionDropdown",
SubmitButton:"ISource/Negotiation/Supplierside/SubmitButton",
CommentInputbox:"ISource/Negotiation/Supplierside/CommentInputbox",
CommentInputboxBuyer:"ISource/Negotiation/Buyerside/CommentInputAcceptBuyer",
BidButtonOnPopup:"ISource/Negotiation/Buyerside/AcceptBidOnPopup",
DoneOnPopup:"ISource/Negotiation/Buyerside/DoneOnPopup",
PlusIconOnBuyer:"ISource/Negotiation/Buyerside/PlusIconOnBuyer",




}