import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import {NegotiationLocators} from "../../POM/Negotiation/NegotiationLocators";
import {coe, COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import {surrogateBidlocator} from "../../POM/SurrogateBid/SurrogateBidLocators";
import {SupplierHomeAction} from "../../POM/SupplierHome/SupplierHomeAction";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { DatabaseOperations } from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import { prop } from "../../../Framework/FrameworkUtilities/config";

export class NegotiationFlow{

    static async clickOnNegotiate(){
        I.click(Startup.uiElements.get(NegotiationLocators.Negotiatebutton))
    }
    static async clickOnStartNegotiate(){
        I.click(Startup.uiElements.get(NegotiationLocators.Start_NegotiationButton))
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }

    static async downloadBidDocumentFromMenuIcon(){
        await NegotiationFlow.clickOnMenuIcon()
        await NegotiationFlow.clickOnDowloadBidDocument()
    }
    static async clickOnMenuIcon(){
        I.scrollIntoView(Startup.uiElements.get(NegotiationLocators.MenuIcon))
        I.click(Startup.uiElements.get(NegotiationLocators.MenuIcon))
    }
    static async clickOnDowloadBidDocument(){
        I.click(Startup.uiElements.get(NegotiationLocators.DownloadBidDocumentDropdownoption))
    }
    static async clickEditRound(){
        I.click(Startup.uiElements.get(NegotiationLocators.EditRoundButtonXpath))
    }
    static async uploadDocumnet(){
        // this.clickOnBrowser()
        // I.attachFile(Startup.uiElements.get(NegotiationLocators.BrowserButton),"/Downloads");
        // this.clickOnSaveRound()
        // I.cleanDir('C:/Users/divyashree/Downloads');

    }
    static async clickOnBrowser(){
        I.click(Startup.uiElements.get(NegotiationLocators.BrowserButton))
    }
    static async clickOnSaveRound(){
        I.click(Startup.uiElements.get(NegotiationLocators.SaveRoundButton))
    }
    static async clickSendOnApproval(){
        I.click(Startup.uiElements.get(NegotiationLocators.SendForApprovalButton))
    }
    static async clickDone(){
        I.click(Startup.uiElements.get(NegotiationLocators.DoneButtonSendApproval))
    }

    static async negotiateFromBuyer(){
        await NegotiationFlow.clickOnNegotiate()
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        // let url =  I.grabCurrentUrl();
        // console.log(`Current URL is---------------------- [${url}]`);
        await NegotiationFlow.clickOnStartNegotiate()
        await NegotiationFlow.downloadBidDocumentFromMenuIcon()
        await NegotiationFlow.clickEditRound()
        I.wait(60)
        // this.uploadDocumnet()
        await NegotiationFlow.clickSendOnApproval()
        await NegotiationFlow.clickDone()
    }

    static async startNegotiation(){
        I.refreshPage();
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await NegotiationFlow.clickOnStartNegotiate()
        await NegotiationFlow.downloadBidDocumentFromMenuIcon()
        await NegotiationFlow.clickEditRound()
        I.wait(30)
        // this.uploadDocumnet()
        await NegotiationFlow.clickSendOnApproval()
        await NegotiationFlow.clickDone()
    }
    static async AcceptBidFromBuyer(){
        await NegotiationFlow.clickAcceptButton_Buyerside()
        await NegotiationFlow.setCommentBuyerside()
        await NegotiationFlow.clickonAcceptBidOnPopup()
        await NegotiationFlow.clickDoneOnReject_Accept()
    }
    static async clickAcceptButton_Buyerside(){
        I.click(Startup.uiElements.get(NegotiationLocators.AcceptButton))
    }
    static async setCommentBuyerside(){
        var commenttext =  await COE.randomString(10)
        I.fillField(Startup.uiElements.get(NegotiationLocators.CommentInputboxBuyer),commenttext)
    }
    static async clickonAcceptBidOnPopup(){
        I.click(Startup.uiElements.get(NegotiationLocators.BidButtonOnPopup))
    }
    static async clickDoneOnReject_Accept(){
        I.click(Startup.uiElements.get(NegotiationLocators.DoneOnPopup))

    }
    static async RejectBidFromBuyer(){
        I.click(Startup.uiElements.get(NegotiationLocators.RejectButton))
        await NegotiationFlow.setCommentBuyerside()
        await NegotiationFlow.clickonAcceptBidOnPopup()
        await NegotiationFlow.clickDoneOnReject_Accept()
    }
    static async clickOnCounterBidFromBuyer(){
        I.click(Startup.uiElements.get(NegotiationLocators.CounterBidButton))
    }
    static async clickOnPlusIcon(){
        I.click(Startup.uiElements.get(NegotiationLocators.PlusIconOnBuyer))
    }
    static async createCounterBidFromBuyer(){
        await NegotiationFlow.clickOnCounterBidFromBuyer()
        await NegotiationFlow.clickOnPlusIcon()
        await NegotiationFlow.downloadBidDocumentFromMenuIcon()
        await NegotiationFlow.clickEditRound()
        I.wait(30)
        // this.uploadDocumnet()
        await NegotiationFlow.clickSendOnApproval()
        await NegotiationFlow.clickDone()



    }
// -----------------------------supplier side function-------------------
   static async rejectBidFromSupplier(){
    await NegotiationFlow.selectRejectInAction()
    await NegotiationFlow.setcomment()
        I.wait(prop.DEFAULT_LOW_WAIT)
        await NegotiationFlow.submit()
    }
    static async acceptBidFromSupplier(){
        I.refreshPage();
        await NegotiationFlow.selectAcceptInAction()
        await NegotiationFlow.setcomment()
        await NegotiationFlow.submit()
    }
    static async createCounterBidFromSupplier(){
        I.refreshPage();
        I.wait(prop.DEFAULT_LOW_WAIT)
        await NegotiationFlow.selectCounterBidInAction()
        await NegotiationFlow.selectExportInAction()
        I.wait(60)
        // this.selectImportInAction()
        await NegotiationFlow.selectsendforApprovalInAction()
        I.wait(prop.DEFAULT_MEDIUM_WAIT)

    }
   
    static async selectCounterBidInAction(){
        I.scrollIntoView(Startup.uiElements.get(NegotiationLocators.ActionDropdown))
        await NegotiationFlow.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown) as string , await iSourcelmt.getLabel("CounterBidOption") as string, "");
    }
    static async selectExportInAction(){
        await NegotiationFlow.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown) as string , await iSourcelmt.getLabel("ExportOption")as string,"");
    }
    static async selectImportInAction(){
        await NegotiationFlow.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown) as string , await iSourcelmt.getLabel("ImportOption")as string,"");
    }
    static async selectAcceptInAction(){
        await NegotiationFlow.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown) as string , await iSourcelmt.getLabel("AcceptOption")as string,"");
    }
    static async selectsendforApprovalInAction(){
        await NegotiationFlow.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown) as string , await iSourcelmt.getLabel("SendforApprovalLabel")as string,"");
    }
    static async selectRejectInAction(){
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await NegotiationFlow.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown) as string , await iSourcelmt.getLabel("RejectOption")as string,Startup.testData.get("ActionOption_Default") as string);
    }
    static async setcomment(){
       var commenttext =  await COE.randomString(10)
        I.fillField(Startup.uiElements.get(NegotiationLocators.CommentInputbox),commenttext)
    }

    static async submit(){
        I.click(Startup.uiElements.get(NegotiationLocators.SubmitButton))
    }


    static async searchEventAndEnterEvent(eventType : string){
        try{
        //I.fillField(Startup.uiElements.get(supplierHomeAction.searchEventID), eventID)
        // var dataTosearch=Startup.testData.get("EventIdData");
        var eventID= (await DatabaseOperations.getTestData()).get(eventType);
        I.fillField(Startup.uiElements.get(SupplierHomeAction.searchEventID),eventID)
        I.pressKey('Enter');
         I.wait(prop.DEFAULT_LOW_WAIT)
         
        I.click(iSourcelmt.getLabel("EnterEventLabel"));
        //I.waitForElement(Startup.uiElements.get(surrogateBidlocator.AcceptButton));
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        // this.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown),iSourcelmt.getLabel("RejectOption"));
        // this.SelectFromDropDownWithXpath(Startup.uiElements.get(NegotiationLocators.ActionDropdown),iSourcelmt.getLabel("RejectOption"),Startup.testData.get("ActionOption_Default"));

        }catch(err){
            throw err;
           // logout.logout()
            }
    }

    static async searchOnGrid(dataTosearch : string,colunmName : string)
    {
        // dataTosearch="1310194642";
        // var dataTosearch=Startup.testData.get("EventIdData");
        colunmName=Startup.testData.get("EventID") as string;
        I.seeElement(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid));
        I.click(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid));
        I.fillField(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid),Startup.testData.get("EventIdData"));
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName)as string,colunmName));
        I.click(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName)as string,colunmName));
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }

    static async replaceValueInAString(xpath : string,valueToReplacewith: string) 
    {
   return  xpath.replace("<<dataname>>",valueToReplacewith);
    }
    static async SelectFromDropDownWithXpath(formControlName : string,ValueToSelect: string,TestDataDefaultValueKeyFromDB: string)
{
I.click(formControlName);
I.wait(prop.DEFAULT_LOW_WAIT)
console.log("i clicked************************")
if(!ValueToSelect)
                 {       
                    console.log("xapth-----------------"+Startup.uiElements.get(NegotiationLocators.DropDownText))
                   // I.scrollIntoView(this.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.DropDownText),TestDataDefaultValueKeyFromDB));
                    I.click(await NegotiationFlow.replaceValueInAString(Startup.uiElements.get(NegotiationLocators.DropDownText) as string,TestDataDefaultValueKeyFromDB));
                    return TestDataDefaultValueKeyFromDB;
                 }
                  else
                    {
                        logger.info("ValueToSelect=================="+ValueToSelect);
                        logger.info("ValueToSelect=================="+TestDataDefaultValueKeyFromDB);
                        logger.info("xapth-----------------"+Startup.uiElements.get(NegotiationLocators.DropDownText))
                          I.scrollIntoView(await NegotiationFlow.replaceValueInAString(Startup.uiElements.get(NegotiationLocators.DropDownText) as string,ValueToSelect));
                        I.click(await NegotiationFlow.replaceValueInAString(Startup.uiElements.get(NegotiationLocators.DropDownText) as string,ValueToSelect));
                        return ValueToSelect;
                    }
}

}
// module.exports = new NegotiationFlow();