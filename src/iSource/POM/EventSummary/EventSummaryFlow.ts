import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { EventsummaryLocators } from "./EventsummaryLocators";

import { prop } from "../../../Framework/FrameworkUtilities/config";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { Filter } from "dd-cc-zycus-automation/dist/components/filter";
import { DewCheckbox } from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";


export class EventSummaryFlow {
  static async verifyEventsummary() {
  
        I.wait(prop.DEFAULT_LOW_WAIT)
      //  await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('TimlineKey') as string);
       // await DewElement.verifyIfISeeText('Timeline');

       await DewElement.checkIfElementPresent(Startup.uiElements.get(EventsummaryLocators.SearchIcon) as string);
        console.log("I am on event summary page")
  }

 static async clickOnCreate(){
   //a[contains(text(),'Create')]
   I.wait(prop.DEFAULT_WAIT)
  // await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventsummaryLocators.Create) as string, await iSourcelmt.getLabel('CreateLabelKey') as string) as string);
  await CommonKeyword.clickLabel( await iSourcelmt.getLabel('CreateLabelKey') as string)
   iSourcelmt.waitForLoadingSymbolNotDisplayed()
   I.wait(prop.DEFAULT_WAIT)
 }

 static async clickOnAnalysis() {
  I.wait(prop.DEFAULT_WAIT)
 await CommonKeyword.clickLabel( await iSourcelmt.getLabel('AnalysisLabelKey') as string)
 iSourcelmt.waitForLoadingSymbolNotDisplayed()
 I.wait(prop.DEFAULT_WAIT)
}

  static async clickOnEventsummary() {
    iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_WAIT)
    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventsummaryLocators.Eventsummary) as string, await iSourcelmt.getLabel('EventSummaryKey') as string) as string);
    iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_WAIT)
  }

  static async selectOtherUserfilter(filterType: string, selectSubfilter: string) {
    await Filter.applyFilter(await iSourcelmt.getLabel(filterType) as string);
    I.wait(prop.DEFAULT_LOW_WAIT)

    await DewCheckbox.selectCheckbox("Activities of Other Users Only");
    await DewButton.click(await iSourcelmt.getLabel("Apply") as string);

    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_WAIT)
  }

  static async selectSubfilter(filterType: string, selectSubfilter: string) {
    await Filter.applyFilter(await iSourcelmt.getLabel(filterType) as string);
    I.wait(prop.DEFAULT_LOW_WAIT)

    await DewCheckbox.selectCheckbox(await iSourcelmt.getLabel(selectSubfilter) as string);
    await DewButton.click(await iSourcelmt.getLabel("Apply") as string);

    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_WAIT)
  }

  static async selectBuyerfilter(filterType: string) {

    await CommonKeyword.clickElement(Startup.uiElements.get(EventsummaryLocators.Userbutton) as string);
    let buyer = await I.grabTextFrom(Startup.uiElements.get(EventsummaryLocators.UserName) as string);
    console.log(buyer + "Hi  buyer name")
    let x =  await iSourcelmt.getLabel("HiLabelKey")as string;
    let buyerName = buyer.replace(x+", ", "");
     console.log(buyerName + " Modified buyer name")
    
    await Filter.applyFilter(await iSourcelmt.getLabel(filterType) as string);

    I.wait(prop.DEFAULT_LOW_WAIT)

    await DewCheckbox.selectCheckbox(buyerName);
    await DewButton.click(await iSourcelmt.getLabel("Apply") as string);

    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_WAIT)
  }

  static async verifyBuyer() {
    I.wait(prop.DEFAULT_LOW_WAIT)
    let totalEnteries = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(EventsummaryLocators.Email) as string);
    console.log(totalEnteries);
    for (let index = 1; index <= totalEnteries; index++) {
      let entry = (await Startup.uiElements.get(EventsummaryLocators.EmailIndex) as string).replace("<<index>>", "" + index);
      if ((await I.grabTextFrom(entry)).includes(Startup.users.get("USERNAME") as string)) {
        console.log(entry);
      }
    }
  }


  static async verifyOtherUSers() {
    I.wait(prop.DEFAULT_LOW_WAIT)
    let totalEnteries = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(EventsummaryLocators.Email) as string);
    console.log(totalEnteries);
    for (let index = 1; index <= totalEnteries; index++) {
      let entry = (await Startup.uiElements.get(EventsummaryLocators.EmailIndex) as string).replace("<<index>>", "" + index);
      if (!((await I.grabTextFrom(entry)).includes(Startup.users.get("USERNAME") as string))) {
        console.log(entry);
      }
    }
  }

  static async verifyStage(word: string) {
    I.wait(prop.DEFAULT_LOW_WAIT)
    let totalEnteries = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(EventsummaryLocators.Stage) as string);
    console.log(totalEnteries);
    for (let index = 1; index <= totalEnteries; index++) {
      let entry = (await Startup.uiElements.get(EventsummaryLocators.StageIndex) as string).replace("<<index>>", "" + index);
      if ((await I.grabTextFrom(entry)).includes(word)) {
        console.log(entry);
      }
    }
  }

  static async verifyResponse(word: string) {
    I.wait(prop.DEFAULT_LOW_WAIT)
    let responseSubmitted = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(EventsummaryLocators.Response) as string);
    console.log(responseSubmitted);
    for (let index = 1; index <= responseSubmitted; index++) {
      let entry = (await Startup.uiElements.get(EventsummaryLocators.ResponseIndex) as string).replace("<<index>>", "" + index);
      if ((await I.grabTextFrom(entry)).includes(word)) {
        console.log(entry);
      }
    }
  }

  static async verifyDate(word: string) {
    I.wait(prop.DEFAULT_LOW_WAIT)
    let responseSubmitted = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(EventsummaryLocators.Date) as string);
    console.log(responseSubmitted);
    for (let index = 1; index <= responseSubmitted; index++) {
      let entry = (await Startup.uiElements.get(EventsummaryLocators.DateIndex) as string).replace("<<index>>", "" + index);
      if ((await I.grabTextFrom(entry)).includes(word)) {
        console.log(entry);
      }
    }
  }

  static async clearfilter(filterType: string) {
    await Filter.applyFilter(await iSourcelmt.getLabel(filterType) as string);
    await DewButton.click(await iSourcelmt.getLabel("Clear All") as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_LOW_WAIT)
    iSourcelmt.waitForLoadingSymbolNotDisplayed()
  }


  static async DownloadExcel() {

    await CommonKeyword.clickElement(Startup.uiElements.get(EventsummaryLocators.ExcelDownload) as string);
    iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_WAIT)
  }

}
