import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { sectionAction } from "./SupplierResponseAction";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";


import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
declare const inject: any; 

const { I } = inject ();
export class supplierResponseSection { 
   
    static async clickOnGoToSubmitResponse(){
        I.click(Startup.uiElements.get(sectionAction.goToSubmitResponseBtn));
        I.wait(prop.DEFAULT_LOW_WAIT);
    }
    

    static async clickOnGoToSubmitResponseOnPrepareresponsePage(){
        I.wait(prop.DEFAULT_WAIT)
        I.click(Startup.uiElements.get(sectionAction.gotoSubmitResponseButnOnPrepareResponse));
    }

    static async clickOnmatchAndAcceptDutchOption(){
        I.wait(3);
        I.click(Startup.uiElements.get(sectionAction.MatchAndAcceptActiveBidCost));
        I.wait(3);
        await I.click(Startup.uiElements.get(sectionAction.Ok_button));
        I.wait(3);
        I.click(Startup.uiElements.get(sectionAction.backToSectionListingLinkXpath));
        I.wait(3);
    }


    static async clickOnSubmitBid(){
        I.wait(3);
        I.click(Startup.uiElements.get(sectionAction.submitBidXpath));
        I.wait(prop.DEFAULT_LOW_WAIT);
        await I.click(Startup.uiElements.get(sectionAction.Ok_button));
        I.wait(3);
        I.click(Startup.uiElements.get(sectionAction.backToSectionListingLinkXpath));
        I.wait(3);
    }

    static async clickOnSubmitResponse(){
        I.wait(3);
        I.click(Startup.uiElements.get(sectionAction.submitResponseBtn));
        I.wait(prop.DEFAULT_WAIT);
        await I.click(Startup.uiElements.get(sectionAction.Ok_button));
        I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(sectionAction.viewResponseButton) as string,"View Response"));
    }
    static async acceptBid(){
        I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(sectionAction.acceptRejectChoice) as string,await iSourcelmt.getLabel("AcceptLabelKey") as string));
        console.log("Bid is accepted.");
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await I.click(Startup.uiElements.get(sectionAction.Ok_button));
        I.wait(3);
        I.seeElement(Startup.uiElements.get(sectionAction.backToSectionListingLinkXpathJap));
        await I.click(Startup.uiElements.get(sectionAction.backToSectionListingLinkXpathJap));
        I.wait(3);
    }
    static async joinBidding(eventType : string){
        var auctionType=(eventType.replace("Reverse","")).replace("Forward","")
        console.log("current autcion type "+auctionType)
        I.wait(3);
        var status = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.joinBiddingButtonXpath) as string);
        console.log("join bidding "+status);
        if(status==1) {
            I.click(Startup.uiElements.get(sectionAction.joinBiddingButtonXpath));
        switch (auctionType) {
            case "English":
                await supplierResponseSection.clickOnSubmitBid();
                break;
        case "Japanese":
            await supplierResponseSection.acceptBid();
            break;

        case "Dutch":
            await supplierResponseSection.clickOnmatchAndAcceptDutchOption();
            break;
            default:
                console.log("shared autcion type is not mathcing with predefined cases, kindly check.");
                break;
        }
       
      
        await supplierResponseSection.clickOnGoToSubmitResponseOnPrepareresponsePage()
    }
}
static async FillResponse(eventType : string,surrogateBidvalue : boolean,unitcost?:string){

  try {
      I.wait(6);

  
    await I.waitForElement(Startup.uiElements.get(sectionAction.sections));
    var sections = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.sections) as string);
    let breakthechain : number = 0
    for(let i=1;i<=sections; i++){ 
       
        I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(sectionAction.getSection) as string,""+i));
        I.wait(prop.DEFAULT_LOW_WAIT);
         await supplierResponseSection.selectMultiChoiceOption();//working
         await supplierResponseSection.fillTextTypeQuestion();//working
         await supplierResponseSection.fillSingleChoiceQuestion();//working
         await supplierResponseSection.fillTableTypeQuestion();
         await supplierResponseSection.setAttachmentTypeQuestion();
         await supplierResponseSection.fillNumericTypeQuestion();
         console.log(breakthechain) + "Before Fillunit cost";
         breakthechain = await supplierResponseSection.fillUnitcost(eventType,surrogateBidvalue,breakthechain,i);
         console.log(breakthechain + " after fillunit cost");
         await supplierResponseSection.fillYesNoTypeQuestion();
         await supplierResponseSection.saveResponse(breakthechain);
         
   }

   
  } catch (error) {
   
      throw new Error(error.message);
  }

    }  
        static async saveResponse(breakthechain : number){
            console.log("I am insitde saveResponse Function")
            if (breakthechain == 0) {
        I.wait(3);
        await I.click(Startup.uiElements.get(sectionAction.saveResponseButton));
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await I.click(Startup.uiElements.get(sectionAction.Ok_button));
            }
            else {}
    }
 
    static async saveBidreco(){
        I.wait(3);
        await I.click(Startup.uiElements.get(sectionAction.saveBidrecoButton));
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await I.click(Startup.uiElements.get(sectionAction.Ok_button));
    }
    static async checkPrimaryCurrencyAtSupplierSide(option : string){
        if(option){
            I.seeElement(await CommonFunctions.getFinalLocator(I.getElement(sectionAction.primaryCurrencySetAtSupplierSide),option));
            logger.info("Primary currency is set as "+option);
        }
        else{
            var defaultPrimaryOption=I.getData("primaryCurrencyOption");
            I.seeElement(await CommonFunctions.getFinalLocator(I.getElement(sectionAction.primaryCurrencySetAtSupplierSide),defaultPrimaryOption));
            logger.info("Primary currency is set as "+defaultPrimaryOption);
        }
    }
    static async fillUnitcost(eventType:string,surrogateBidvalue:boolean,breakthechain:number,i:number,unitcost ?: string) : Promise<number> {
        console.log(eventType + "===================Is EVENTTYPE Value")
        if (eventType == "DutchReverse" && surrogateBidvalue && i == 3) {
        breakthechain = 1;
        //supplierHomeFlow.breakthechain = 1;
        return breakthechain;
        }
        else {
        var currentPriceUniutBox = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.unitCostInputXpath) as string);
       if(currentPriceUniutBox>0){
        //    await this.checkPrimaryCurrencyAtSupplierSide();
       console.log("Number of pricing rows are "+currentPriceUniutBox);
        var prices : string[];
        if(unitcost){
            
            prices=unitcost.split(",");
        }else{
            prices= (Startup.testData.get("defaultCurrentPrice")as string).split(",");
        }
      
        for (let index = 0; index < currentPriceUniutBox; index++) {
            console.log("input "+prices[index]);
           
           I.fillField(locate(Startup.uiElements.get(sectionAction.unitCostInputXpath) as string).at(index+1),prices[index]);
          }
       }
       return breakthechain;
console.log("Inside Else Loop in fill Response value = ", breakthechain)
    }
    }
    static async fillTableTypeQuestion(){
        var text = (Startup.testData.get("defaultTablePrice") as string).split(",");

       var tableTypeElement = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.tableTypeInputboxXpath)as string);

       console.log("table type elemnt "+tableTypeElement);
       for(let i=0;i<tableTypeElement; i++){

           I.fillField(locate(Startup.uiElements.get(sectionAction.tableTypeInputboxXpath) as string).at(i+1), text[i]);

       }
   }
   static async fillNumericTypeQuestion(){
    var text = Startup.testData.get("defaultNumericValue");

   var numericTypeElement = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.numericInputEleXpath)  as string);

   console.log("numeric type elemnt "+numericTypeElement);
   for(let i=1;i<=numericTypeElement; i++){

       I.fillField(locate(Startup.uiElements.get(sectionAction.numericInputEleXpath)as string).at(i), text);

   }
}

   static async setAttachmentTypeQuestion(){
    
   var attachmentTypeElement = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.addattachmentButtonXpath) as string);

   console.log("attachment type elemnt "+attachmentTypeElement);
   for(let i=0;i<attachmentTypeElement; i++){

       I.scrollIntoView(locate(Startup.uiElements.get(sectionAction.addattachmentButtonXpath)as string).at(i+1));
       I.click(locate(Startup.uiElements.get(sectionAction.addattachmentButtonXpath)as string).at(i+1));
       I.wait(prop.DEFAULT_LOW_WAIT);
       I.switchTo(Startup.uiElements.get(sectionAction.attachmentFrame));
       I.attachFile("//div[@id='fileUploadDiv']//form[contains(@id,'fileupload')]//input[@type='file']","/DataFiles/iSource/AutomationPresentation.pptx"); 
       I.wait(prop.DEFAULT_LOW_WAIT);
       I.switchTo();
       I.click(Startup.uiElements.get("ISource/SupplieSide/OkButtonAttachmentPopup"));

   }
}
    static async fillTextTypeQuestion(){
         var text = Startup.testData.get("defaultQuestionTypeAnswer");

		var textTypeElement = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.textTypeQuestion) as string);

        console.log("txt type elemnt "+textTypeElement);
		for(let i=0;i<textTypeElement; i++){

			I.fillField(locate(Startup.uiElements.get(sectionAction.textTypeQuestion)as string).at(i+1), text);

        }
    }
    static async fillSingleChoiceQuestion(){
        I.wait(5)
        var singleChoiceTypeElement = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.singleTypeQuestionDropdown)  as string);
       
        console.log("single choice type elemnt "+singleChoiceTypeElement);
		for(let i=0;i<singleChoiceTypeElement; i++){
            // I.scrollIntoView(locate(Startup.uiElements.get(sectionAction.singleTypeQuestionDropdown)).at(i+1));
           await I.click(locate(Startup.uiElements.get(sectionAction.singleTypeQuestionDropdown)as string).at(i+1));
         //  I.clickElement(Startup.uiElements.get(sectionAction.singleChiceOptionDefault) as string);
            I.click(Startup.uiElements.get(sectionAction.singleChiceOptionDefault));

        }
    }
    static async selectMultiChoiceOption (){
        var multiTypeQuestions = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.multiChiceOptionsInputXpath) as string);
        console.log("multi choice type elemnt "+multiTypeQuestions);
		for(let i=1;i<multiTypeQuestions; i +=3){
            var ele=await CommonFunctions.getFinalLocator(Startup.uiElements.get(sectionAction.multiChoiceOption)as string,""+(i+1))
            await I.click(ele);
            //await CommonKeyword.clickElement(ele);
        }
    }
    // static async setAttachmentTypeQuestions(){
    //     var AttachmentTypeQuestions = await I.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.AttachmentTypeQuestions));

	// 	for(let i=0;i<AttachmentTypeQuestions; i++){
    //        await I.click(locate(Startup.uiElements.get(sectionAction.singleTypeQuestionDropdown)as string).at(i+1));
    //        I.click(Startup.uiElements.get(sectionAction.singleChiceOptionDefault));

    //     }
    // }
    static async fillYesNoTypeQuestion(){
        var yesNoTypeElements = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.yesNoChoiceOptionDropDwon) as string);
        console.log("Yes/No choice type elemnt "+yesNoTypeElements);
		try {
            for(let i=0;i<yesNoTypeElements; i++){
                await I.click(locate(Startup.uiElements.get(sectionAction.yesNoChoiceOptionDropDwon)as string).at(i+1));
                //await CommonKeyword.clickElement(Startup.uiElements.get(sectionAction.yesNoChoiceOptionDropDwon) as string);
                 I.click(Startup.uiElements.get(sectionAction.yesNoChoiceOptionDropDwon) as string);
               
             }
     
          } catch (error) {
            throw error;
        }
    }

    // static async executeJSClick(i: number){
      
    //     I.executeScript(function(i : number ) {
    //     //var xpath=
    //     document.evaluate("//div[@id='sectionBox']//li[2]", document, null, 9, null). singleNodeValue.click();
         
    //     }); 
    //     I.wait(prop.DEFAULT_MEDIUM_WAIT);
    // }
    
}

// module.exports = new supplierResponseSection;