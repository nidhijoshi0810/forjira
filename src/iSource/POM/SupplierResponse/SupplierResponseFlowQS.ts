import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { QSSupplierSideLocators } from "./SupplierResponseActionQS";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";

import "codeceptjs";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { logger } from '../../../Framework/FrameworkUtilities/Logger/logger';
declare const inject: any;
const { I } = inject();

export class QuickSourceSupplierResponse {

  static async enterQuantity(quantity?: number) {

    var count = await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QSSupplierSideLocators.quantityInputField) as string, "" + 1));
    var prices: number = 0;
    if (quantity) {
      prices = quantity;
    } else {
      prices = parseInt(Startup.testData.get("defaultQuantityQS") as string);
    }
    for (let index = 1; index <= count; index++) {
      I.fillField(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QSSupplierSideLocators.quantityInputField) as string, "" + index), "" + prices * index);

    }
  }
  static async Questionresponse(Questionres?: string) {
    var count :Number = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QSSupplierSideLocators.questioncountQS) as string);
    I.scrollIntoView(Startup.uiElements.get(QSSupplierSideLocators.questioncountQS) as string)
    I.wait(10)
    logger.info("**** Debg Started "+count);
    for (let index = 1; index <= count; index++) {
      logger.info("DEBUG::"+Startup.uiElements.get(QSSupplierSideLocators.questioncountQS) as string+'--'+index+"--"+(Startup.testData.get("QuestionResponseQS") as string));
      I.fillField(locate(await Startup.uiElements.get(QSSupplierSideLocators.questioncountQS) as string).at(index), await Startup.testData.get("QuestionResponseQS") as string);

    }
  }
  static async enterBidValue(BidValue?: number) {

    var count = await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QSSupplierSideLocators.priceInputField) as string, "" + 1));
    var prices: number = 0;
    if (BidValue) {
      prices = BidValue;
    } else {
      prices = parseInt(Startup.testData.get("defaultPriceSupplier") as string);
    }
    for (let index = 1; index <= count; index++) {
      I.fillField(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QSSupplierSideLocators.priceInputField) as string, "" + index), "" + prices * 20);

    }
  }

  static async clickOnSubmit(id: string) {
    await CommonKeyword.clickLabel(await iSourcelmt.getLabel("SUBMITLabelKey") as string);
    // I.click(await iSourcelmt.getLabel("SUBMITLabelKey"));
    I.wait(4);
    I.seeElement(Startup.uiElements.get(QSSupplierSideLocators.GotITElement));
    //I.waitForText(await iSourcelmt.getLabel("UPDATELabelKey"));
    console.log("Response submitted succesfully for " + id + " .");
    I.wait(4);
  }
}

// module.exports=new QuickSourceSupplierResponse();