
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {coe} from "../../../Framework/FrameworkUtilities/COE/COE";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {eventSettingsFlow} from "../eventSettings/eventSettingsFlow.js";
import {DatePickerFlow} from "../../../Framework/FrameworkUtilities/COE/DatePicker/DatePickerFlow";
import { loginAction } from "../../../Framework/FrameworkUtilities/COE/Login/loginAction";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker";
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element"; //clickElement
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";  
// import{textfield}  from "dd-cc-zycus-automation/dist/components/textfield";   
import { TMSManageProfileLocators } from "./TMSManageProfileLocators";
import {TextField} from "dd-cc-zycus-automation/dist/components/textfield"
import { commonLandingFlow } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import { Console } from "console";


export class TMSManageProfile{

    static UserEmailId : string;
    static UserProfileName : string;
    static UserCurrency : string;
    static UserDecimalPrecision : string;

    static async moveToManageProfilePage(){
        await  CommonKeyword.clickElement(Startup.uiElements.get(loginAction.profileDropdown)as string);
          var noOfDD : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(loginAction.dopdownDiv)) as string);
          if(noOfDD > 0)
          {
              I.wait(5);
              I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(loginAction.logoutDropdownOption)as string,await iSourcelmt.getLabel("Manage Profile")as string)); 
              await iSourcelmt.waitForLoadingSymbolNotDisplayed()
              I.wait(5);
          }
  
      }

      static async grabEmailId(){
          var emailID : string = await DewElement.grabTextFrom((Startup.uiElements.get(TMSManageProfileLocators.emailID) as string));
          this.UserEmailId = emailID;
          logger.info(" email ID "+emailID);
          return emailID;
        //   //dew-section[contains(@section-heading,'User Details')]//dew-row[2]//dew-col[4]//dew-input-container[2]/span
      }

      static async grabUserName(){
        var userName : string = await DewElement.grabValueFrom((Startup.uiElements.get(TMSManageProfileLocators.userName) as string));
        this.UserProfileName = userName;
        logger.info(" userName "+userName);
        return userName;
        // 
      }
      
      static async setTimeZoneCurrency(defaultValue : string, valueToReplace ?: string){

        var commonDropDown : string = "//div[contains(text(),'- ')]"; // //div[contains(text(),'- ')] //div[contains(text(),'- (GMT')]
        var a : string = "- ";
        var b : any = valueToReplace;
        var array = new Array();
        var x : any = await this.grabValues(commonDropDown);
        array = Array.from(x)
        logger.info(x.length);
        
        if (b!=" " || b!=b) { 
            await CommonKeyword.click(((commonDropDown).replace(a,b)));
          } 
          
        else {     
            await CommonKeyword.click(((commonDropDown).replace(a,defaultValue)));
          }

      }

      static async setTimeZone(timeZone ?: string){
          var ISTZone : string = "IST - (GMT+05:30)";
          I.wait(prop.DEFAULT_LOW_WAIT);
          // await textfield.enterTextUsingLocator("(//dew-section[contains(@section-heading,'User Preferences')]//input)[1]", " ");
          I.scrollIntoView((Startup.uiElements.get(TMSManageProfileLocators.CurrencyTimeZone) as string)+"[1]");
          I.fillField((Startup.uiElements.get(TMSManageProfileLocators.CurrencyTimeZone) as string)+"[1]", " ");
          I.wait(prop.DEFAULT_MEDIUM_WAIT);
          await this.setTimeZoneCurrency(ISTZone, timeZone);
          
      }

      static async setUserCurrency(currency ?: string){
          var INR : string = "INR - Indian Rupee";
          I.wait(prop.DEFAULT_LOW_WAIT);
          I.fillField((Startup.uiElements.get(TMSManageProfileLocators.CurrencyTimeZone) as string)+"[2]", "i");
          I.wait(prop.DEFAULT_MEDIUM_WAIT);
          await this.setTimeZoneCurrency(INR ,currency);
          I.wait(prop.DEFAULT_LOW_WAIT);
          var value : string = await DewElement.grabValueFrom((Startup.uiElements.get(TMSManageProfileLocators.CurrencyTimeZone) as string)+"[2]");
          this.UserCurrency = value;
          logger.info(value + " User currency"+ this.UserCurrency)

      }

      static async setDecimalPrecision(decimalPrecision ?: number){

        if(decimalPrecision!=undefined && decimalPrecision <5 && decimalPrecision >0)
        {
          var xpath : string = ("//div[contains(@title,'3')]").replace("3", decimalPrecision as any)
          await CommonKeyword.click(Startup.uiElements.get(TMSManageProfileLocators.DecimalPrecision) as string);
          await CommonKeyword.click(xpath);
        }
        else{
          var xpath : string = ("//div[contains(@title,'3')]").replace("3", "5")
          await CommonKeyword.click(Startup.uiElements.get(TMSManageProfileLocators.DecimalPrecision) as string);
          await CommonKeyword.click(xpath);
        }
        var value : string = await DewElement.grabValueFrom((Startup.uiElements.get(TMSManageProfileLocators.DecimalPrecision) as string));
        this.UserDecimalPrecision = value;
        await this.saveChanges();
      }
      static async grabValues( xpath : string){
        var x : number = await DewElement.grabNumberOfVisibleElements(xpath);
        logger.info(" value of x"+x)
        var TimeZoneCurrencyValues = new Array(x);
        logger.info(TimeZoneCurrencyValues.length)
        return TimeZoneCurrencyValues;
      }

      static async saveChanges(){
        await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("SaveButton") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
      }

      static async verifyCurrencyTimeZoneDecimalPrecision(){
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.navigateToiosurceTabs( await iSourcelmt.getLabel("Event Settings") as string);
        var currency : string = await DewElement.grabValueFrom(Startup.uiElements.get(TMSManageProfileLocators.EventDetailsPageCurrency) as string);
        var decimalPrecision : string = await DewElement.grabTextFrom(Startup.uiElements.get(TMSManageProfileLocators.EventDetailsDecimalPrecision) as string);
        logger.info(this.UserCurrency+" event details curency" + currency + " : "+this.UserDecimalPrecision +" Dp "+ decimalPrecision);
        if((currency.toString()==this.UserCurrency.toString()) && (decimalPrecision.toString().trim()==this.UserDecimalPrecision.toString().trim()))
        {
          logger.info("manage profile settings are saved successfully")
        }

      }

    }

    