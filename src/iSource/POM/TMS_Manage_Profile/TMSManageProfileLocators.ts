export let TMSManageProfileLocators = {

    emailID : "TMSManageProfile/emailID",
    userName : "TMSManageProfile/userName",
    CurrencyTimeZone : "TMSManageProfile/CurrencyTimeZone",
    EventDetailsPageCurrency : "TMSManageProfile/EventDetailsPageCurrency",
    DecimalPrecision : "TMSManageProfile/DecimalPrecision",
    EventDetailsDecimalPrecision : "TMSManageProfile/EventDetailsDecimalPrecision",

}