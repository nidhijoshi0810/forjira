import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { QuestionnaireFlow } from "../Questionnaire/QuestionnaireFlow";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { QuestionnaireLocators } from "../Questionnaire/QuestionnaireLocators";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";

export class ItemListingFlow {

    static async checkPricingTableIsPresent() {
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfIDontSeeElement(await iSourcelmt.getLabel("AddPricingTableLabelKey") as string);
        console.log("Add pricingtable is present.");
    }

    static async DeletePricingTable() {
        await QuestionnaireFlow.clickAddPricingTable();
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SelectPricingTableButton) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
       // await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.DoneBtn) as string);
       await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneButtonLabel") as string)
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.deleteBtn) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.YesButton_AddSection) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
    }
    static async hidePricingColUnderTCB() {
        I.wait(prop.DEFAULT_LOW_WAIT)
        // I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.iconView));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.iconView) as string);
        // I.click(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon));
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("OKButton") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomOKButton)as string, await iSourcelmt.getLabel("OKButton") as string));
        await I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.iconView));
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async SeePricingTablewithTCB() {
        I.wait(prop.DEFAULT_WAIT);
       await DewElement.checkIfElementVisible(await Startup.uiElements.get(QuestionnaireLocators.iconView) as string);
    }
    static async AddPricingTablewithTCB() {
        await QuestionnaireFlow.clickAddPricingTable();
        await ItemListingFlow.checkTCBCheckBox();
        I.wait(prop.DEFAULT_WAIT);
    }

    static async checkTCBCheckBox() {
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SelectPricingTableButton) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.TCBCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT);

    }
    static async SeePricingTablewithCustomColumns() {
        I.wait(prop.DEFAULT_WAIT);
       await DewElement.checkIfElementVisible(await iSourcelmt.getLabel("CustomColumnsKey") as string);
    }
    
    static async validateItemSpecificationAddition() {
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.checkIfElementPresent(await iSourcelmt.getLabel("ItemSpecificationKey") as string);
    }
    static async validateItemSpecificationIsNotPresent() {
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfIDontSeeElement(await iSourcelmt.getLabel("ItemSpecificationKey") as string);
    }
    static async AddPricingTablewithItemSpecification() {
        await QuestionnaireFlow.clickAddPricingTable();
        await ItemListingFlow.checkItemSpecification();
    }
    
    static async checkItemSpecification() {
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SelectPricingTableButton)as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
       await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.ItemSpecificationCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async DeletePricingTablewithItemSpecification() {
        await ItemListingFlow.uncheckItemSpecification();
    }
    static async uncheckItemSpecification() {
        I.wait(prop.DEFAULT_LOW_WAIT);
       await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SettingButton) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
      await  CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.ItemSpecificationCheckBox) as string);
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
    }

    static async AddPricingTablewithCustomColumns() {
        await QuestionnaireFlow.clickAddPricingTable();
        await ItemListingFlow.checkCustomColumnCheckBox1();
        await ItemListingFlow.hideCustomColumns();
        I.wait(prop.DEFAULT_WAIT);
    }
    static async hideCustomColumns() {
        //I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("OKButton") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.scrollIntoView(await Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon));
        I.wait(prop.DEFAULT_LOW_WAIT)
    }
    static async checkCustomColumnCheckBox1() {
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SelectPricingTableButton) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.CustomColumnCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT);
        await I.clearField("//input[contains(@placeholder ,'<Custom Column Desc.>')]");
        // await I.fillField(Startup.uiElements.get(QuestionnaireLocators.CustomColumnName) as string, Startup.testData.get("CustomColumn"));
        await I.fillField("//input[contains(@placeholder ,'<Custom Column Desc.>')]", Startup.testData.get("CustomColumn"));

        I.wait(prop.DEFAULT_LOW_WAIT);

    }
    static async CheckDeletePricingTablewithTCB() {
        I.wait(prop.DEFAULT_WAIT);
       await DewElement.checkIfElementVisible( await Startup.uiElements.get(QuestionnaireLocators.iconView) as string);
    }
    static async DeletePricingTablewithCustomColumns() {
        await ItemListingFlow.uncheckCustomColumns();
    }
    static async uncheckCustomColumns() {
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SettingButton) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.CustomColumnCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()

    }

    static async CheckDeletePricingTablewithCustomColumns() {
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfIDontSeeElement(await iSourcelmt.getLabel("CustomColumnsKey") as string);
    }
}