
export let TEbPanelLocators = {
    tebPanelAddMemberBtn:"TEBPanel/TEBPanelAddMemberBTN",
    tebPanelSearch : 'TEBPanel/TEBPanelSearch',
    tebPanelTechCheckBox: 'TEBPanel/TEBPanelTechChkBox',
    tebPanelCommCheckBox: 'TEBPanel/TEBPanelCommChkBox',
    tebPanelEmailSearch:'TEBPanel/TEBPanelEmailSearch',
    tebPanelDone : 'TEBPanel/TEBPanelDoneButton'
 
    
}