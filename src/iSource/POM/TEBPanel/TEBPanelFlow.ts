import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { TEbPanelLocators } from "./TEBPanelLocator";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { HeaderFilter } from 'dd-cc-zycus-automation/dist/components/headerFilter'
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";

export class TEBPanelFlow {

    static async clickOnPanelAddMemberBtn() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(TEbPanelLocators.tebPanelAddMemberBtn)as string,
                                                         await iSourcelmt.getLabel("AddPanelMembersKey")as string))

        iSourcelmt.waitForLoadingSymbolNotDisplayed()
    }

    static async addTechPanel() {

        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabelFromKey("Search") as string,
            Startup.testData.get("TebPanelSearchTech") as string, await iSourcelmt.getLabelFromKey("Email Id") as string);

        await iSourcelmt.waitForLoadingSymbolNotDisplayed()

        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(TEbPanelLocators.tebPanelTechCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async addCommPanel() {

        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabelFromKey("Search") as string,
            Startup.testData.get("TebPanelSearchComm") as string, await iSourcelmt.getLabelFromKey("Email Id") as string);
        
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()

        await CommonKeyword.clickElement(Startup.uiElements.get(TEbPanelLocators.tebPanelCommCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)

    }

    static async addBothPanel() {

        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabelFromKey("Search") as string,
            Startup.testData.get("TebPanelSearchBoth") as string, await iSourcelmt.getLabelFromKey("Email Id") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()

        await CommonKeyword.clickElement(Startup.uiElements.get(TEbPanelLocators.tebPanelTechCheckBox) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(TEbPanelLocators.tebPanelCommCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)

    }

    static async clickDone() {
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
        //await CommonKeyword.clickElement(Startup.uiElements.get(TEbPanelLocators.tebPanelDone) as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        DewElement.checkIfElementPresent(Startup.uiElements.get(TEbPanelLocators.tebPanelAddMemberBtn) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
    }


}