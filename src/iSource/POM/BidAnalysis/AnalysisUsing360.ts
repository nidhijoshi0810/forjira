import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
declare const inject: any; 
const { I } = inject();
import {logger}  from "../../../Framework/FrameworkUtilities/Logger/logger"
import {iSourcelmt}  from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp"
import {CommonFunctions}  from "./../../isourceCommonFunctions/CommonFunctions"
import {BidAnalysis}  from "./BidAnalysisLocators"
import {AwardWordFlow}  from "../../POM/AwardWorkFlow/AwardWorkFlow"
import {BidAnalysisFlow}  from "./BidAnalysisFlow"

export class AnalysisUsing360{

    static async open360Scenario(){

        console.log("********"+Startup.uiElements.get(BidAnalysis.anaylzeBtn360));
        var status=await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.anaylzeBtn360)as string,await iSourcelmt.getLabel("ReloadLabelKey") as string));
        if(status>0){
            await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.anaylzeBtn360)as string,await iSourcelmt.getLabel("ReloadLabelKey") as string));
           await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.anaylzeBtn360)as string,await iSourcelmt.getLabel("analyzeTab") as string));
        }
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.anaylzeBtn360)as string,await iSourcelmt.getLabel("analyzeTab") as string));
       await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.waitForText(await iSourcelmt.getLabel("EventLevelLebelKey") as string);
    }
    static  async checkSupplierCheckbox(){
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.supplierCheckbox)as string);  

      await  iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static  async clickOnAwardEventFor360(){
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.awardButtonXpath) as string, await iSourcelmt.getLabel("AwardEventLabelKey") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.waitForText(await iSourcelmt.getLabel("EventNotAwardedLabelKey"));
        // I.see(await iSourcelmt.getLabel("EventNotAwardedLabelKey") as string, Startup.uiElements.get(BidAnalysis.scenarioName) as string);
    }

    static async awardUsing360(){
       
    await AwardWordFlow.awardEventUsingWorkFlow();
   await  BidAnalysisFlow.navigateToCommentsAndAttachmentsSection();
   await BidAnalysisFlow.enterAwardingComments();
   await BidAnalysisFlow.attachFileInAwarding();
   await BidAnalysisFlow.awardAndSend();
    }

    static async checkFor360Analysis() {
        await BidAnalysisFlow.eventLevel()
        await BidAnalysisFlow.selectWeightage()
        await BidAnalysisFlow.sectionLevel()
        await BidAnalysisFlow.lineItemLevel()
        await BidAnalysisFlow.makeAllocation360();
        await BidAnalysisFlow.checkPreviewSection()
    }
}
// module.exports=new AnalysisUsing360();