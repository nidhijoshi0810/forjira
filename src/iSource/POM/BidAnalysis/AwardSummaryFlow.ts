import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

import "codeceptjs"; 
declare const inject: any; 

const { I } = inject();

import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import {BidAnalysis} from "./BidAnalysisLocators"
export class AwardSummary{
   
   static  async clickOnViewAwradSummaryBtn(){
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("AwardSummaryLabelKey")as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
       await I.waitForText(await iSourcelmt.getLabel("SupplierAwardSummaryLabelKey") as string);
    }
    static  async fetchAwardSummary(){
        var list=new Array();
        var size=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(BidAnalysis.awardSummaryTable) as string);
        for(let i=1;i<=size; i++){
            var text=await I.grabTextFrom(locate(Startup.uiElements.get(BidAnalysis.awardSummaryTable) as string).at(i));
        
        list.push(text);
        }

        list.forEach(entry => {
           
            logger.info(entry);
        });
    }
}
// module.exports=new AwardSummary();