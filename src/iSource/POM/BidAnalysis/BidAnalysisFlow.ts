import { listingPageElements } from './../ListingPageFullSource/ListingPagePrimaryLocators';
import { DatabaseOperations } from './../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations';
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { CommonLocators } from './../../isourceCommonFunctions/CommonLocators';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { BidAnalysis } from "./BidAnalysisLocators";
import { BOScenarioElements } from "../BOScenarios/BOScenarioLocator"

import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {AwardWordFlow} from "../../POM/AwardWorkFlow/AwardWorkFlow";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";

export class BidAnalysisFlow{

    static async clickOnChangeScope(){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ChangeScopeLabelKey") as string);
        I.waitForText(await iSourcelmt.getLabel("ChangeScopeLabelKey"));
    }
    static async enterAwardingComments(){
        I.fillField("textarea",(await DatabaseOperations.getTestData()).get("awardingcomment"));
        // I.fillField("textarea",I.testData.get("awardingcomment"))
    }
    static async navigateToCommentsAndAttachmentsSection(){
        await CommonFunctions.navigateToiosurceTabs( await iSourcelmt.getLabel("CommentsAndAttachmentsLabelKey")as string);
    }
    static async attachFileInAwarding(name?:string){

        let label=await iSourcelmt.getLabel("UploadFailedLabelKey")as string;
        if(name){
            if(name.length>1){
            var names=new Array(name.split(","));
            names.forEach(async (fileName) => {
                I.attachFile("//dew-file-upload//input[@id='file1']",fileName);
                I.wait(prop.DEFAULT_MEDIUM_WAIT);
                 I.dontSeeElement(await CommonFunctions.getFinalLocator("*[contains(text(),'<<dataname>>')]", label) as string);
                I.seeElement(Startup.uiElements.get(BidAnalysis.attachmnetDeleteButton));
            });
        }else{
            I.attachFile("//dew-file-upload//input[@id='file1']","/DataFiles/iSource/WORD FILE.docx"); // /DataFiles/iSource/WORD FILE.docx   ./Resources/WORD FILE.docx
            I.wait(prop.DEFAULT_MEDIUM_WAIT);
             I.dontSeeElement(await CommonFunctions.getFinalLocator("//*[contains(text(),'<<dataname>>>>')]", label));
            I.seeElement(Startup.uiElements.get(BidAnalysis.attachmnetDeleteButton));
        }
     
        }else{
     
       I.attachFile("//dew-file-upload//input[@id='file1']","/DataFiles/iSource/WORD FILE.docx"); // /DataFiles/iSource/WORD FILE.docx   ./Resources/WORD FILE.docx
       I.wait(prop.DEFAULT_MEDIUM_WAIT);
        I.dontSeeElement(await CommonFunctions.getFinalLocator("//*[contains(text(),'<<dataname>>')]",label));
    //    I.seeElement(I.getElement(BidAnalysis.attachmnetDeleteButton));
        I.seeElement(Startup.uiElements.get(BidAnalysis.attachmnetDeleteButton));
    }
}
static async awardAndSend(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("awardButtonLabel")as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    let number=await I.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.awardConfirmPopup)as string,await iSourcelmt.getLabel("You are awarding")as string));
   if(number>0){
    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("YesLabelKey")as string)
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
   }
    I.wait(prop.DEFAULT_WAIT);
    await I.waitForText(await iSourcelmt.getLabel("Award Mail"));
    await CommonFunctions.clickOnModalFooterButton( await iSourcelmt.getLabel("sendLabelKey")as string);
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("DoneButtonLabel")as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.breadcrumFullSource) as string, await iSourcelmt.getLabel('FULLSOURCE_TABNAME') as string));    
    await I.waitForText(await iSourcelmt.getLabel("AwardedLabelKey"));
}
    static async getCurrentItemSupplierNumber(){
        var label=await I.grabAttributeFrom(Startup.uiElements.get(BidAnalysis.scenarioHeaderXpath),"value");
        var array=new Array();
        var supplierString=label[label.length-1];
        var ItemString=label[label.length-2];
        console.log("*******str "+supplierString);
        var supplierNumber=supplierString.replace(/[a-zA-Z]/g,"");
        array.push(supplierNumber.split("/")[0])
        
        var itemNumber=ItemString.replace(/[a-zA-Z]/g,"");
        array.push(itemNumber.split("/")[0]);
        return array;
    }
    
    static async excludeSupplier(){
      let number=  await I.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.exludedSupplierCheckbox) as string,Startup.testData.get("excludeSupplierName") as string));
        if(number>1){
    I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.exludedSupplierCheckbox) as string,Startup.testData.get("excludeSupplierName") as string));
    }
    }
    static async excludeItem(){
        
        // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.exludedSupplierCheckbox) as string,Startup.testData.get("itemToExclude") as string));
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.itemFirstCheckbox)as string);
    }
    static async NavigateToItemTab(){
    //    await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ItemTabKey") as string);

       await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.itemTabBA)as string,  await iSourcelmt.getLabel("ItemTabKey") as string))
     
        }
   static async clickOnRunOptimizationBtn(){

    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("RunOptimizationLabelKey") as string);
    }

    static async clickOnRunOptimizationNow(){
        I.click(Startup.uiElements.get(BidAnalysis.RunoptimizationnowRadioBtnxpath));

    }
    static async enterDesc(){
        let Text : string="";
       
            Text=Startup.testData.get("scenarioName") as string;
       
        I.fillField(Startup.uiElements.get(BidAnalysis.DescriptionFiledXpath),Text);
    }
    static async clickOnRunNowBtn(){
        await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("RunNowLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
    }

    static async clickOnScheduleBtn(){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("scheduleScenario") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
    }


    static async SaveScenario(scenarioName : string){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveAsLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.waitForText(await iSourcelmt.getLabel("SaveAsLabelKey"));
        await BidAnalysisFlow.enterScenarioName(scenarioName);
        await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("SaveAsLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneButtonLabel") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        I.see(scenarioName, Startup.uiElements.get(BidAnalysis.scenarioName));
    }
    static async enterScenarioName(name : string ){
        let  scenarioName="";
        if(!name){
            scenarioName=Startup.testData.get("scenarioName") as string;
        }else{
            scenarioName=name;
        }
        I.fillField(Startup.uiElements.get(BidAnalysis.ScenarioNameFiledXpath),scenarioName);
    }
    static async createOptimizedScenario(desc?: string){
    //    let currentSupplierCount= (await BidAnalysisFlow.getCurrentItemSupplierNumber())[0];
    //    let currentItemcount=(await BidAnalysisFlow.getCurrentItemSupplierNumber())[1];
     await BidAnalysisFlow.clickOnChangeScope();
     await BidAnalysisFlow.excludeSupplier();
     await BidAnalysisFlow.NavigateToItemTab();
     await BidAnalysisFlow.excludeItem();
     await BidAnalysisFlow.clickOnRunOptimizationBtn();
     await BidAnalysisFlow.clickOnRunOptimizationNow();
     await  await BidAnalysisFlow.enterDesc();
     await BidAnalysisFlow.clickOnRunNowBtn();
    //  currentSupplierCount===(await BidAnalysisFlow.getCurrentItemSupplierNumber())[0]+1;
    //  currentItemcount===(await BidAnalysisFlow.getCurrentItemSupplierNumber())[1]+1;
    }
static async clickOnMoveToAnalysisButton(){
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await I.wait(prop.DEFAULT_LOW_WAIT)
    await I.waitForText(Startup.testData.get("MoveToAnalysisButton"));
    await I.click(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.moveToAnalysisButton) as string,await iSourcelmt.getLabel("MoveToAnalysisButton") as string));
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await I.wait(prop.DEFAULT_LOW_WAIT)
    await I.waitForText(Startup.testData.get("YesButton"));
    await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.YesPopUpButton) as string,await iSourcelmt.getLabel("YesLabelKey") as string));
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.tabXpath) as string, await iSourcelmt.getLabel('analyzeTab') as string))
}

 static async clickAnalysisTabs(Tabs : string){
     await I.wait(prop.DEFAULT_MEDIUM_WAIT);
     await iSourcelmt.waitForLoadingSymbolNotDisplayed();
     await I.seeElement(await Startup.uiElements.get(BOScenarioElements.pricingAnalysis) as string);
     await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.pricingAnalysis) as string);
    }

static async clickOnSingleAnalyze(){
    await CommonFunctions.clickOnButton(Startup.testData.get("AnalyzeButton") as string);
    // I.click(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalyzeSingleButton) as string,Startup.testData.get("AnalyzeButton") as string));
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_LOW_WAIT)
    I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.VerifySupplierDetails) as string, await iSourcelmt.getLabel("SupplierDetails") as string))
    await BidAnalysisFlow.clickAnalysisMenu();
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_LOW_WAIT)
}

static async clickAnalyzebutton(){
    I.wait(10);
    var analyzeButton = new Array(await DewElement.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalyzeButton) as string,Startup.testData.get("AnalyzeButton") as string)));
    for(let i=1;i<=analyzeButton.length; i++){
        I.click(locate(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalyzeButton) as string,Startup.testData.get("AnalyzeButton")as string)).at(i));
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        if(i==1){
            await BidAnalysisFlow.verifyCherryScenariotitle();
        }
        else if(i==2){
            await BidAnalysisFlow.verifyBest1ScenarioTitle();
        }
        else{
            await BidAnalysisFlow.verifyBest2ScenarioTitle();
        }
        await BidAnalysisFlow.clickAnalysisMenu();
        //I.click(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalysisMenu),Startup.testData.get("AnalysisTab")));
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
     }
}

static async verifyCherryScenariotitle(){
    I.waitForText(Startup.testData.get("CherryPickingScenario"));
}
static async verifyBest1ScenarioTitle(){
    I.waitForText(Startup.testData.get("Best1Scenario"));
}
static async verifyBest2ScenarioTitle(){
    I.waitForText(Startup.testData.get("Best2Scenario"));
}

static async clickAnalysisMenu(){
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalysisMenu) as string,await iSourcelmt.getLabel("AnalysisLabelKey") as string));
}

static async selectOneScenarioToAnalyze(){
    await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.AnalyzeButtonForScenario) as string,await iSourcelmt.getLabel("analyzeTab") as string));
}

static async modifyScenario(){
    await BidAnalysisFlow.clickOnChangeScope();
    I.fillField(Startup.uiElements.get(BidAnalysis.maxSavingInpt), "30");
    await BidAnalysisFlow.clickOnRunOptimizationBtn();
    await BidAnalysisFlow.clickOnRunOptimizationNow();
    await  await BidAnalysisFlow.enterDesc();
    await BidAnalysisFlow.clickOnRunNowBtn();

}

static async exportScenario(){
    await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.exportScenarioButton) as string);
    // I.click(Startup.uiElements.get(BidAnalysis.exportScenarioButton))
}

static async clickOnSendRegretEmail(){
    await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("SendRegretEmailLebelKey")as string);
    I.waitForText( await iSourcelmt.getLabel("SelectsupplierstosendregretmailLabelKey")as string);
}

static async checkSupplierCheckbox(){
    await CommonKeyword.clickElement(await Startup.uiElements.get(BidAnalysis.regretEmailSupplierCheckbox) as string);
    // I.checkOption(I.getElement(BidAnalysis.regretEmailSupplierCheckbox));
    await CommonFunctions.clickOnModalFooterButton( await iSourcelmt.getLabel("nextLabelKey")as string);
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CommonFunctions.clickOnModalFooterButton( await iSourcelmt.getLabel("sendLabelKey")as string);
   await iSourcelmt.waitForLoadingSymbolNotDisplayed();
}

static async sendRegretEmail(){
    await BidAnalysisFlow.clickOnSendRegretEmail();
    await BidAnalysisFlow.checkSupplierCheckbox(); 
}

static async clickOnSplitQunatity(){

   var splitNoOfItems : number = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(BidAnalysis.SplitQuantityButton) as string);
    for(let j=1; j<=splitNoOfItems; j++)
    {
        //I.click( await iSourcelmt.getLabel("SplitQuantityLabelKey"));  
        await CommonKeyword.clickElement((Startup.uiElements.get(BidAnalysis.SplitQuantityButton) as string)+"["+j+"]"); 
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        var inputBoxes : number = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(BidAnalysis.InputBoxForSplitQuantity) as string);
        for(let i=1; i<=inputBoxes; i++)        // 
        {
            I.clearField((Startup.uiElements.get(BidAnalysis.InputBoxForSplitQuantity) as string)+"["+i+"]");
            I.fillField((Startup.uiElements.get(BidAnalysis.InputBoxForSplitQuantity) as string)+"["+i+"]", 1);
            
        }
        await I.waitForText( await iSourcelmt.getLabel("ApplyLabelKey"))
        await this.clickOnApplyBtn();
    }
    
}

static async clickOnApplyBtn(){
    await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.ApplyButtonOnSplitQtyPopUp) as string); 
  await  iSourcelmt.waitForLoadingSymbolNotDisplayed();
}

static async modifyFormula(formula:any){
    await BidAnalysisFlow.clickOnChangeScope();
    await BidAnalysisFlow.NavigateToCostElementTab();
    await BidAnalysisFlow.clickOnAddFormula();
    await BidAnalysisFlow.addFormula(formula);
    await BidAnalysisFlow.clickOnRunOptimizationBtn()
    await BidAnalysisFlow.clickOnRunOptimizationNow();
    await BidAnalysisFlow.clickOnRunNowBtn();
}

static async NavigateToCostElementTab(){
    await CommonFunctions.navigateToiosurceTabs( await iSourcelmt.getLabel("costElemenetLabelKey")as string);

     }

   static  async clickOnAddFormula(){

        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddFormulaLabelKey")as string);
        I.seeElement("isource-add-formula");
        I.scrollIntoView("isource-add-formula")
    }

    static async addFormula(formula:any){

        var string=formula.split(",");
        for (let index = 0; index < string.length; index=index+2) {
          await this.clickOnSelectOperantBtn();
            I.click(string[index])
    
            if((index+1)<string.length){
            I.click(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.operandOption)as string,string[index+1]));
            }
            
        }
        await CommonFunctions.clickOnButton(  await iSourcelmt.getLabel("ValidateLabelKey")as string);
        await this.saveFormula();
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.addedCuetomFormulaEle)as string,   await iSourcelmt.getLabel("CustomFormulaLabelKey")as string));
        // I.click(await COE.replaceValueInAString(I.getElement(BidAnalysis.addedCuetomFormulaEle),   await iSourcelmt.getLabel("CustomFormulaLabelKey")as string));
        
    }

    static async clickOnSelectOperantBtn(){
          
        await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("SelectOperandLabelKey")as string);
    }

    static async saveFormula(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.saveFormaulaXpath)as string, await iSourcelmt.getLabel("SaveButtonLabel")as string));
    }

    static async checkScenarioAtAllLevels(scenarioName:any,customColumnAdded:any){
        await BidAnalysisFlow.checkScenarioAt( await iSourcelmt.getLabel("EventLevelLebelKey"),scenarioName);
        await BidAnalysisFlow.checkScenarioAt( await iSourcelmt.getLabel("SectionLevelLabelKey"),scenarioName);
        await BidAnalysisFlow.checkScenarioAt( await iSourcelmt.getLabel("ItemLevel"),scenarioName);
        await BidAnalysisFlow.checkScenarioAt( await iSourcelmt.getLabel("SupplierLevel"),scenarioName);
        if(customColumnAdded==="True" ||customColumnAdded==="true" ){
            await this.checkScenarioAt( await iSourcelmt.getLabel("CustomLevel"),scenarioName);
        }
       
       
    }

   static async checkScenarioAt(tabName:any,scenarioName:any){
        await CommonFunctions.navigateToiosurceTabs(tabName);
       await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        I.see(scenarioName,Startup.uiElements.get(BidAnalysis.scenarioName));
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.tabActiveLink)as string,tabName));
    }

    static async awardScenario(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.awardButtonXpath)as string, await iSourcelmt.getLabel("awardButtonLabel")as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await AwardWordFlow.awardEventUsingWorkFlow();
        await BidAnalysisFlow.navigateToCommentsAndAttachmentsSection();
       await BidAnalysisFlow.enterAwardingComments();
       await BidAnalysisFlow.attachFileInAwarding();
       await BidAnalysisFlow.awardAndSend();
        }

        static async deleteScenario(scenarioName:any){

            var index= await BidAnalysisFlow.getIndexOfSpecificScenario("ScenarioNameLabelKey",scenarioName);
            await BidAnalysisFlow.openActionsMenu(index);
            await BidAnalysisFlow.clickOnScenarioSecondaryAction( await iSourcelmt.getLabel("DeleteOption")as string);
            I.waitForText( await iSourcelmt.getLabel("YesLabelKey")as string);
            await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("YesLabelKey")as string);
            iSourcelmt.waitForLoadingSymbolNotDisplayed();
         
         }

         static async clickOnScenarioSecondaryAction(actionName:any){
            I.click(await CommonFunctions.getFinalLocator (Startup.uiElements.get(BidAnalysis.scenarioSecondaryAction)as string,actionName));
        }

        static async openActionsMenu(index:any){
            I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(BidAnalysis.actionsMenuAnalyze)as string,index));
            I.seeElement(Startup.uiElements.get(BidAnalysis.actionsMenuDiv));
        }

        static async getIndexOfSpecificScenario(columnNameKey:any,stringToMatch:any){
            I.wait(prop.DEFAULT_LOW_WAIT)
            var supplierEnteries=await DewElement.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.SupplierNamesRows)as string, await iSourcelmt.getLabel(columnNameKey)as string));
            var i=0;
            for (let index = 1; index <= supplierEnteries; index++) {
          
               var entry=await COE.replaceValueInAString((Startup.uiElements.get(BidAnalysis.specificRowOfSupplier)as string).replace("<<index>>",index.toString()), await iSourcelmt.getLabel(columnNameKey)as string);
                 if( (await I.grabTextFrom(entry)).includes(stringToMatch)){
              
                i=index;
               break;
            }
            }
            return i;
         }

    static async verify360DegreeAnalysis() {
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.tabXpath) as string, await iSourcelmt.getLabel('analyzeTab') as string))
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.clickOn360DegreeAnalysis) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.degreeAnalyze) as string);
    }

    static async eventLevel() {
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.eventLevel) as string)
    }

    static async selectWeightage() {
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.popOver) as string)
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.scoresheetSelectionDropdown) as string)
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.selectScoreSheet) as string)
        await I.dragSlider(Startup.uiElements.get(BidAnalysis.dragSlider) as string, 2);
    }

    static async sectionLevel() {
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.sectionLevel) as string)
    }

    static async lineItemLevel() {
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.lineItemLevel) as string)
    }

    static async checkPreviewSection() {
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.checkPreviewSection) as string)

    }

    static async checkCherryPick(){
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.clickAnalysis) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    
    }

        static async BOScenarioCherryPick(){
        await BidAnalysisFlow.checkCherryPick()
    }

    static async makeAllocation360() {
        let supplierDetailsText: any;
        for(let i=1; i<=3;i++){
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            supplierDetailsText = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(BidAnalysis.grabsupplierDetailsText) as string)
            if(supplierDetailsText >=1 ){
                break;
            }else{
             await CommonKeyword.clickElement(await Startup.uiElements.get(BidAnalysis.selectIcon) as string)
             await iSourcelmt.waitForLoadingSymbolNotDisplayed();
             let checking = Startup.uiElements.get(await BidAnalysis.selectItemTable) as string;
             let modifiedChecking = `${checking}[${i+1}]`
             await CommonKeyword.clickElement(modifiedChecking)
            }
        }
        await TextField.enterText(await iSourcelmt.getLabel("Quantity") as string,1);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.grabsupplierDetailsText) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(BidAnalysis.yesForSelect) as string)
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }


}
// module.exports = new BidAnalysisFlow();