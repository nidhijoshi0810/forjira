import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { DewButton } from 'dd-cc-zycus-automation/dist/components/dewButton';
import "codeceptjs"; 
declare const inject: any; 

const { I } = inject();
import { AuctionMonitoringLocators } from "./AuctionMonitoringLocators";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger"
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

export class AuctionMonitoringFlow{

   static async navigateToAuctionMonitoringPage(){

    await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AuctionMonitoringTabLabelKey")as string);
    }


   static async uncheckExtendLotCheckbox(){
        await CommonKeyword.clickElement(Startup.uiElements.get(AuctionMonitoringLocators.extendCheckBox) as string);
        await DewButton.click(await iSourcelmt.getLabel("YesLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        // I.click(Startup.uiElements.get(AuctionMonitoringLocators.extendCheckBox));
    }

    static async overallProgressTab(){
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(AuctionMonitoringLocators.OverallProgressTab) as string, await iSourcelmt.getLabel("Overall Progress") as string));
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async viewSupplierGraph(){
        await DewElement.verifyIfISeeElementInDOM(Startup.uiElements.get(AuctionMonitoringLocators.graphView) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async viewSupplierRank(){
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Rank") as string)
        I.wait(prop.DEFAULT_LOW_WAIT)
    }
}
// module.exports=new AuctionMonitoringFlow();