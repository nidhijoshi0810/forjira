import "codeceptjs";
import { JapaneseAuctionMonitoringLocator } from "./JapaneseAuctionMonitoringLocator";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
declare const inject: any;
const { I } = inject();
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import { DewCheckbox } from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { HeaderFilter } from "dd-cc-zycus-automation/dist/components/headerFilter";
import { DewFileUpload } from "dd-cc-zycus-automation/dist/components/dewFileUpload";
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalPopUp";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { CommonLandingAction } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingAction";


export class JapaneseAuctionMonitoring {


    static async navigateToDDHomePage(){

        I.wait(prop.DEFAULT_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
        await DewElement.verifyIfISeeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE) as string);
        await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>",await iSourcelmt.getLabel("HOME_TABNAME") as string))

    };

    static async navigateToAuctionSummary() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT)
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AuctionMonitoringTabLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }

    static async viewCurrentRound() {
        I.wait(prop.DEFAULT_WAIT)
        await DewElement.verifyIfISeeElement((await CommonFunctions.getFinalLocator((Startup.uiElements.get(JapaneseAuctionMonitoringLocator.CurrentRound)as string), await iSourcelmt.getLabel("CurrentRoundLabelKey") as string)));
        
    }
    static async viewSupplierCount() {
        I.wait(prop.DEFAULT_WAIT)
        await DewElement.verifyIfISeeElement((await CommonFunctions.getFinalLocator((Startup.uiElements.get(JapaneseAuctionMonitoringLocator.CurrentRound)as string), await iSourcelmt.getLabel("TotalSuppliersLabelKey") as string)));
        
    }
    static async viewRoundAndSupplierCount() {
        
        var currentRoundNo : string = await DewElement.grabTextFrom(((await CommonFunctions.getFinalLocator((await Startup.uiElements.get(JapaneseAuctionMonitoringLocator.CurrentRound)as string), await iSourcelmt.getLabel("CurrentRoundLabelKey") as string)))+"//following-sibling::span");
        var TotalNofOfSupps : string = await DewElement.grabTextFrom(((await CommonFunctions.getFinalLocator((await Startup.uiElements.get(JapaneseAuctionMonitoringLocator.CurrentRound)as string), await iSourcelmt.getLabel("TotalSuppliersLabelKey") as string)))+"//following-sibling::span");
        logger.info("the current round is "+currentRoundNo)
        logger.info(" and no of supps is "+TotalNofOfSupps)
    }


    static async viewStartBidPrice() {

        await DewElement.verifyIfISeeElement((await CommonFunctions.getFinalLocator((Startup.uiElements.get(JapaneseAuctionMonitoringLocator.StartingBidPrice)as string), await iSourcelmt.getLabel("StartingBidPriceLabelKey") as string)));
        
    }

    static async getStartBidPrice() {

        await DewElement.grabTextFrom(((await CommonFunctions.getFinalLocator((await Startup.uiElements.get(JapaneseAuctionMonitoringLocator.StartingBidPrice)as string), await iSourcelmt.getLabel("StartingBidPriceLabelKey") as string)))+"//following-sibling::span");
        
    }

    static async navigateToOverAllProgressGraph(){

        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("OverAllProgressLabelKey") as string);
    } 

    static async seeNoOfBarsOnGraph(){

        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("OverAllProgressLabelKey") as string);
    } 

    static async viewSupplierDetailsGrid(){

        await DewElement.verifyIfISeeElement(Startup.uiElements.get(JapaneseAuctionMonitoringLocator.SupplierGridDetailsRow) as string);
    }

    static async getSupplierDetails(){

    var noOfRows : number = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(JapaneseAuctionMonitoringLocator.SupplierGridDetailsRow) as string);
    
    if(noOfRows>=1)
    {
        for(var i : number=1; i<=noOfRows; i++)
    { 
        for(var j : number=1; j<=3; j++)
        {   
            let suppName : string;
            let loggedInStatus : string;
            let bidAcceptanceStatus : string;
            if(j==1)
            {
               suppName = await DewElement.grabTextFrom((Startup.uiElements.get(JapaneseAuctionMonitoringLocator.SupplierGridDetailsRow) as string)+"["+i+"]"+"//dew-col["+j+"]");
               logger.info("Supplier name :"+suppName)
            }
            else if(j==2)
            {
                loggedInStatus = await DewElement.grabTextFrom((Startup.uiElements.get(JapaneseAuctionMonitoringLocator.SupplierGridDetailsRow) as string)+"["+i+"]"+"//dew-col["+j+"]");
                logger.info("logged in status :"+loggedInStatus)
            }
            else if(j==3)
            {
                bidAcceptanceStatus = await DewElement.grabTextFrom((Startup.uiElements.get(JapaneseAuctionMonitoringLocator.SupplierGridDetailsRow) as string)+"["+i+"]"+"//dew-col["+j+"]");
                logger.info(" Bid acceptance status :"+bidAcceptanceStatus)
            }
        }  
    }
    }

    }

}