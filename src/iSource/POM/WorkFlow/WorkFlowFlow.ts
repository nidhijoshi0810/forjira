import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { workflowlocator } from "./WorkFlowLocators";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { eventSettingsFlow } from "../eventSettings/eventSettingsFlow";
import { CreateQuickSourceEvent } from "../CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import { QuestionnaireLocators } from "../Questionnaire/QuestionnaireLocators";
import { Locators } from "../BayerSideSupplier/ViewAllSupplierLocators";
import { ViewAllSuppliersSteps } from "../BayerSideSupplier/ViewAllSupplierFlow";
import {DewElement} from "dd-cc-zycus-automation/dist/components/element";


import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { TextField } from 'dd-cc-zycus-automation/dist/components/textfield';
//declare const inject: any; 
const { I } = inject();
export class WorkFlowFlow{

    static async clickSaveandGotoFlowElsePuplish(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        var element=await DewElement.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(Locators.publishBtnXpath) as string, await iSourcelmt.getLabel("PublishLabelKey") as string));

        if(element === 0){
           element=await I.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(Locators.publishBtnXpath) as string, await iSourcelmt.getLabel("RepublishEventButtonLabel") as string));
        }
        console.log(element);
        //CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.publishBtnXpath) as string,Locators.PublishLabelKey);
            if(element>0){
                // I.click(element);
                // logger.info("Clicked on publish button.");
                // iSourcelmt.waitForLoadingSymbolNotDisplayed();
                // await ViewAllSuppliersSteps.clickOnSend();
                await ViewAllSuppliersSteps.performPublish();
                }else{
                   await WorkFlowFlow.clickOnSaveandGotoflow()
                   await WorkFlowFlow.AcceptWorkflow()
                   await WorkFlowFlow.clickEventDetailTab();
                   await ViewAllSuppliersSteps.performPublish();
                }
            }

            static async clickSaveandGotoFlowElseRePublish(){
                I.wait(prop.DEFAULT_LOW_WAIT)
                let element=await I.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(Locators.publishBtnXpath) as string, await iSourcelmt.getLabel("RepublishEventButtonLabel") as string));
                console.log(element);
                    if(element>0){
                        await ViewAllSuppliersSteps.performPublish();
                        }else{
                           await WorkFlowFlow.clickOnSaveandGotoflow()
                           await WorkFlowFlow.AcceptWorkflow()
                           await WorkFlowFlow.clickEventDetailTab();
                           await ViewAllSuppliersSteps.performPublish();
                        }
            }


    static async  clickOnSaveandGotoflow(){
          await WorkFlowFlow.clickSaveandGotoWorkflow()
          await WorkFlowFlow.clickSendforApproval()
          await WorkFlowFlow.setCommentInput()
          await WorkFlowFlow.clickSendforApprovalOnPopup()
            }
    static async AcceptWorkflow(){
        await WorkFlowFlow.clickApproveOnWorkflow()
        await WorkFlowFlow.setApproveCommentInput()
        await WorkFlowFlow.clickApproveButtonOnPopup()
        await WorkFlowFlow.clickOk()
    }
    static async clickOk(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(workflowlocator.OkButton) as string)
        // I.click(Startup.uiElements.get(workflowlocator.OkButton))
    }
    static async clickEventDetailTab(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("EventDetailsTab") as string);
        // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(workflowlocator.tabXpath) as string,await iSourcelmt.getLabel("EventDetailsTab") as string));
        console.log("clicked on Event detail");
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT);
    }
    static async clickApproveOnWorkflow(){
        // iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(20)
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ApproveButton") as string);
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(workflowlocator.ApproveButtonOnWorkflowpage) as string,await iSourcelmt.getLabel("ApproveButton") as string));

    }
    static async setApproveCommentInput(){
        I.fillField(Startup.uiElements.get(workflowlocator.CommentInputBox),Startup.testData.get("Approvetext"))
    }
    static async clickApproveButtonOnPopup(){
        await CommonKeyword.clickElement(Startup.uiElements.get(workflowlocator.ApproveButtonOnApprovePopup) as string);
        // I.click(Startup.uiElements.get(workflowlocator.ApproveButtonOnApprovePopup))
        console.log("Workflow approved")

    }

    static async clickSaveandGotoWorkflow(){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("Saveandgotoworkflow") as string);
        // I.click(await iSourcelmt.getLabel("Saveandgotoworkflow"))
        console.log("clicked on Save and Go to workflow");
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();

    }
    static async clickSendforApproval(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(workflowlocator.SendforApprovalButton) as string);
        // I.click(Startup.uiElements.get(workflowlocator.SendforApprovalButton));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async setCommentInput(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        var comment = await COE.randomString(10)
        await I.fillField(Startup.uiElements.get(workflowlocator.CommentInputBox),comment);
    }
    static async clickSendforApprovalOnPopup(){
        await CommonKeyword.clickElement(Startup.uiElements.get(workflowlocator.SendforApprovalPopupButton) as string);
        // I.click(Startup.uiElements.get(workflowlocator.SendforApprovalPopupButton));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }

    static  async selectLastDateforFullsource(){
        await eventSettingsFlow.clickOnEventSettingsTab();
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.LastDateInputField) as string);
        // I.click(Startup.uiElements.get(CreateQuickSourceEvent.LastDateInputField))
        await CommonFunctions.clickOnButton(Startup.testData.get("LastDate") as string);
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.SelectDate) as string  ,Startup.testData.get("LastDate") as string))
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.OkButton_LastDate) as string);
        // I.click(Startup.uiElements.get(CreateQuickSourceEvent.OkButton_LastDate))
    }


    static async clickFreeze(){
        I.wait(prop.DEFAULT_WAIT);
        var status=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuestionnaireLocators.GetUpdatedInformationButtonXpath) as string);
        if(status==1)
        {
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.GetUpdatedInformationButtonXpath) as string);
            // I.click(Startup.uiElements.get(QuestionnaireLocators.GetUpdatedInformationButtonXpath));
        }
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.FreezeButton) as string);
        // I.click(Startup.uiElements.get(QuestionnaireLocators.FreezeButton));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.clickOnButton(Startup.testData.get("DoneButton") as string);
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.DoneButtonAfterFreeze) as string,Startup.testData.get("DoneButton") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    
    static async sendforApproval(){
        await WorkFlowFlow.clickSendforApproval()
        await WorkFlowFlow.setCommentInput()
        await WorkFlowFlow.clickSendforApprovalOnPopup()
        await WorkFlowFlow.clickOk()
    }

    static async acceptRequest(){
        await WorkFlowFlow.AcceptWorkflow()
    }

    static async clickOnViewAwardSummary(){
        await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("ViewAwardSummaryLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        logger.info("Clicked on view award summary button.");
    }
    static async clickWorkFlowDef(){
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
+       await I.switchToNextTab(1);
        await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.workFlowTab) as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async searchWorkflow(workflowName:string){
        await TextField.enterTextUsingLocator(await Startup.uiElements.get(workflowlocator.searchbox) as string,workflowName);
        I.pressKey('Enter');
    }
    static async clickOnAction(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.actionButton) as string);
    }
    static async CopyWorkflow(){
            await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.copyWorkflow) as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed()
            I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }
    static async CreateCopyCheck(){
        I.seeElement(`//div[contains(@class,'subTitleRight')]//h2[contains(@class,'titleGridHeadingAdm') 
        and contains(text(),"Create")]`);
        // I.seeElement(`//div[contains(@class,'subTitleRight')]//h2[contains(@class,'titleGridHeadingAdm') 
        // and contains(text(), iSourcelmt.getLabel("CreateLabelKey") as string)]`);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }
    static async saveWorkflow(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.saveWorkflowBtn) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }
    static async ActivateWorkflow(){
        let status: string = await I.grabTextFrom(await Startup.uiElements.get(workflowlocator.workFlowStatus) as string);
        if(status.localeCompare("Activate") == 0){
            await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.activate) as string);
        }
    }

    static async VerifyActivateWorkflow(){
        I.seeElement(`//tr[contains(@class, "filterGridTblTd")]/td[contains(text(), "Active")]`);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }
    static async DeactivateWorkflow(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.Deactivate1) as string);
    }
    static async VerifyDeactivateWorkflow(){
        I.seeElement(`//tr[contains(@class, "filterGridTblTd")]/td[contains(text(), "Inactive")]`);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }

}
// module.exports = new WorkFlowFlow();