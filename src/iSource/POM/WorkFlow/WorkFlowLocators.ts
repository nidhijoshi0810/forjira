export let workflowlocator = {
    tabXpath:"EventDetailsPage/tabXpath",
    SendforApprovalButton:"Isource/WorkFlow/SendforApprovalButton",
    CommentInputBox:"Isource/WorkFlow/CommentInputBox",
    SendforApprovalPopupButton:"Isource/WorkFlow/SendforApprovalPopupButton",
    ApproveButtonOnApprovePopup:"Isource/WorkFlow/ApproveButtonOnApprovePopup",
    ApproveButtonOnWorkflowpage:"Isource/WorkFlow/ApproveBuutonOnWorkflowpage",
    ApproveCommentCommentInputField:"Isource/WorkFlow/ApproveCommentCommentInputField",
    OkButton:"Isource/WorkFlow/OkButton",
    workFlowTab:"Isource/WorkFlow/WorkFlowTab",
    searchbox:"Isource/WorkFlow/Searchbox",
    activate:"Isource/WorkFlow/Activate",
    copyWorkflow:"Isource/WorkFlow/CopyWorkflow",
    actionButton:"Isource/WorkFlow/Action",
    Deactivate1:"Isource/WorkFlow/Deactivate",
    workFlowStatus:"Isource/WorkFlow/WorkFlowStatus",
    saveWorkflowBtn:"Isource/WorkFlow/saveWorkflowBtn"
}