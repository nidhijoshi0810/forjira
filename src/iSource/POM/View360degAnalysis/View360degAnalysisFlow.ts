
declare const inject: any; 
const { I } = inject();
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
export class view360degAnalysislow{
    static async SeeViewSupplierDetails() {       
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "SupplierDetails" ) as string)
    }

}
