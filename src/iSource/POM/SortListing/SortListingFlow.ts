import { iSourcelmt } from './../../../Framework/FrameworkUtilities/i18nutil/readI18NProp';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { SortColumn } from "dd-cc-zycus-automation/dist/components/sortColumn";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { HeaderFilter } from "dd-cc-zycus-automation/dist/components/headerFilter";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import {SortListingLocator} from "./SortListingLocators"

var searchResult:string;
export class SortListingFlow {

    static async AscendingVerification(columnName: string){
        await SortColumn.filterByAscending(await iSourcelmt.getLabel(columnName) as string); //ddcc function
    }

    static async DescendingVerification(columnName: string){
        await SortColumn.filterByDescending(await iSourcelmt.getLabel(columnName) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
    }

    static async sortingVerified(columnName: string){
        I.seeElement(`//span[contains(text(),'${await iSourcelmt.getLabel(columnName) as string}')]//following-sibling::img[contains(@alt,'sort ascending icon')] | //span[contains(text(),'${await iSourcelmt.getLabel(columnName) as string}')]//following-sibling::img[contains(@alt,'sort descending icon')]`);
        I.wait(prop.DEFAULT_LOW_WAIT);
    }

    static async searchEvent(type:string,data:string) {
        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events")as string, Startup.testData.get(data)as string,type);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }

    static async verifySearch(type:string,data:string){

        if(type === 'Event ID'){
            searchResult = await DewElement.grabTextFrom(Startup.uiElements.get(SortListingLocator.eventIdLabel)as string);
        }else if(type === 'Event Name'){
            searchResult = await DewElement.grabTextFrom(Startup.uiElements.get(SortListingLocator.eventNameLabel)as string);
        }else if(type === 'Event Owner'){
            searchResult = await DewElement.grabTextFrom(Startup.uiElements.get(SortListingLocator.ownerLabel)as string);
        }
        else{
            searchResult = '';
        }

        if(searchResult === Startup.testData.get(data)as string){
            logger.info('========>Search Result Matches')
        }else{
            logger.info('========>Search Result Not Matches')
        }
    }


}


