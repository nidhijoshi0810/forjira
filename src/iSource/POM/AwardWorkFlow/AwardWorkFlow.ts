import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { WorkFlowFlow } from "../../POM/WorkFlow/WorkFlowFlow";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger"
import { workflowlocator } from "../../POM/WorkFlow/WorkFlowLocators";
import { Awardworkflowlocator } from "../../POM/AwardWorkFlow/AwardWorkFlowLocators";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { surrogateBidlocator } from "../../POM/SurrogateBid/SurrogateBidLocators"
import { NegotiationLocators } from "../../POM/Negotiation/NegotiationLocators"
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {GenerateRandom} from "dd-cc-zycus-automation/dist/components/generateRandom"

export class AwardWordFlow{

    static async searchOnGrid(dataTosearch : string,colunmName : string ){
        dataTosearch="1610061229";
        // var dataTosearch=Startup.testData.get("EventIdData");
        // colunmName =Startup.testData.get("EventID") as string;
        colunmName ="Event ID";
        I.seeElement(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid));
        I.click(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid));
        I.fillField(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid),dataTosearch);
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName)as string,colunmName));
        I.click(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName) as string,colunmName));
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }

    static async amOnWorkFlowPage(){
          // await CommonKeyword.clickElement(Startup.uiElements.get(NegotiationLocators.First_record_name) as string);
        // I.click(Startup.uiElements.get(NegotiationLocators.First_record_name))
        await AwardWordFlow.clickWorkFlowTab()
        
    }
    static async clickWorkFlowTab(){
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(workflowlocator.tabXpath) as string,await iSourcelmt.getLabel("WorkFlowTab") as string));
        // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(workflowlocator.tabXpath) as string,await iSourcelmt.getLabel("WorkFlowTab") as string));
        console.log("clicked on Workflow")
    }

    static async RecallWorkflow(){
        await AwardWordFlow.clickRecallButton()
        await AwardWordFlow.setResetComment()
        await AwardWordFlow.clickRecallButtonOnPopup()
        await WorkFlowFlow.clickOk()
    }

    static async clickRecallButton(){
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(Awardworkflowlocator.RecallButtonOnworkflowpage) as string);
        // I.click(Startup.uiElements.get(Awardworkflowlocator.RecallButtonOnworkflowpage))
    }
    static async setResetComment(){
        var commontext =await GenerateRandom.generateRandomString(10);
        // var commontext =await COE.randomString(10);
        I.fillField(Startup.uiElements.get(Awardworkflowlocator.RecallCommentInputField),commontext);
    }
    static async clickRecallButtonOnPopup(){
      await CommonKeyword.clickElement(Startup.uiElements.get(Awardworkflowlocator.RecallButtonOnPopup) as string);
    // I.click(Startup.uiElements.get(Awardworkflowlocator.RecallButtonOnPopup))  
    console.log("Workflow Recall successuful")

  }
  static async sendforApproval(){
    await WorkFlowFlow.clickSendforApproval()
    await WorkFlowFlow.setCommentInput()
    await WorkFlowFlow.clickSendforApprovalOnPopup()
    await WorkFlowFlow.clickOk()
  }
  static async acceptRequest(){
    await WorkFlowFlow.AcceptWorkflow()
  }
  static async amonAnalyzepage(){
    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(workflowlocator.tabXpath) as string, await iSourcelmt.getLabel("AnalyzeTab") as string));
    // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(workflowlocator.tabXpath) as string, await iSourcelmt.getLabel("AnalyzeTab") as string));
    console.log("clicked on Analyze Tab")
  }
  static async awardEventUsingWorkFlow(){
    I.wait(5);        
    var count=await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(surrogateBidlocator.buttonSpanXpath)as string,await iSourcelmt.getLabel("Saveandgotoworkflow")as string));
      if(count>0){
        await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("Saveandgotoworkflow")as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await WorkFlowFlow.sendforApproval();
        await WorkFlowFlow.acceptRequest();
        await WorkFlowFlow.clickOnViewAwardSummary();
      }
    }

    static async awardEventUsingWorkFlowApproveRecall(){
      I.wait(5);        
      var count=await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(surrogateBidlocator.buttonSpanXpath)as string,await iSourcelmt.getLabel("Saveandgotoworkflow")as string));
        if(count>0){
          await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("Saveandgotoworkflow")as string);
          await iSourcelmt.waitForLoadingSymbolNotDisplayed();
          await WorkFlowFlow.sendforApproval();
          await WorkFlowFlow.acceptRequest();
        }
      }
    
    
}
