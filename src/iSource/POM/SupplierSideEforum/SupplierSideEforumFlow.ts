import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { eforumLocators } from "./SupplierSideEforumLocators";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
declare const inject: any; 
const { I } = inject();
export class SupplierEforum{

    static async craeteEforum(){
        await SupplierEforum.navigateToEforumPage();
        await SupplierEforum.enterEforumName("");
        await SupplierEforum.enterEforumDesc("");
        await SupplierEforum.enterEforumSignature("");
        // await SupplierEforum.selectCutomField();
        await SupplierEforum.saveEforum();
        await SupplierEforum.checkEformCraeted();
    }
    static async navigateToEforumPage(){
        // await CommonKeyword.clickElement('//tr//tr[1]//td[2]//label[1]');
        await CommonKeyword.clickElement(Startup.uiElements.get(eforumLocators.goToEforumButton) as string);
        // I.click(Startup.uiElements.get(eforumLocators.goToEforumButton));
        I.seeElement(Startup.uiElements.get(eforumLocators.NeweForumButton));
        await CommonKeyword.clickElement(Startup.uiElements.get(eforumLocators.NeweForumButton) as string);
        // I.click(Startup.uiElements.get(eforumLocators.NeweForumButton));
        I.wait(prop.DEFAULT_WAIT);
        I.seeElement(Startup.uiElements.get(eforumLocators.NeweForumNameInputTxtbox));
    }

    static async enterEforumName(name : string){
        if(name!==""){
        I.fillField(Startup.uiElements.get(eforumLocators.NeweForumNameInputTxtbox),name);
        }else{
            I.fillField(Startup.uiElements.get(eforumLocators.NeweForumNameInputTxtbox),Startup.testData.get("eForumName"));  
        }
    }
   

    static async enterEforumDesc(desc : string){
        if(desc!==""){
        I.fillField(Startup.uiElements.get(eforumLocators.NeweForumNameDescTxtbox),desc);
        }else{
            I.fillField(Startup.uiElements.get(eforumLocators.NeweForumNameDescTxtbox),Startup.testData.get("eForumDescription"));
        }
    }
    static async  checkEformCraeted(){

        await I.seeElement(Startup.uiElements.get(eforumLocators.forumList));
    }
    static async enterEforumSignature(signature : string ){
        if(signature!==""){
        I.fillField(Startup.uiElements.get(eforumLocators.SignatureTextbox),signature);
        }else{
            I.fillField(Startup.uiElements.get(eforumLocators.SignatureTextbox),Startup.testData.get("eForumSignature"));

        }
    }
    
    static async selectCutomField(){
        await CommonKeyword.clickElement(Startup.uiElements.get(eforumLocators.customFormFieldsdropdown) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(eforumLocators.customFormFieldsdropdownOption) as string);
        // I.click(Startup.uiElements.get(eforumLocators.customFormFieldsdropdown));
        // I.click(Startup.uiElements.get(eforumLocators.customFormFieldsdropdownOption));

    }
    static async saveEforum(){
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("SaveButtonLabel") as string);
    }
    
    static async clickOnCreatedEvent(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.createdEvent) as string);
    }

    static async clickOnNewMessage(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.newMessage) as string);
    }
    static async enterMessage(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await I.fillField(await Startup.uiElements.get(eforumLocators.enterTheMessage) as string,'test data');

    }
    static async clickOnPost(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.postingTheMessage) as string);
    }
    static async clickOnListOfEforum(){
        await CommonKeyword.clickElement('//a[contains(text(),"List of eForum")]');
    }
   

    static async selectPublicDiscussionEfourm(){
   
    let totalEnteries = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(eforumLocators.PublicDiscussionGrid) as string)
    console.log(totalEnteries);
    for (let index = 1; index <= totalEnteries; index++) {
      let entry = (await Startup.uiElements.get(eforumLocators.PublicDiscussionGridIndex) as string).replace("<<index>>", "" + index);
      if ((await I.grabTextFrom(entry)).includes("Public Discussion")) {
        console.log(entry);
        let indexClick = index-2
        await CommonKeyword.clickElement((await Startup.uiElements.get(eforumLocators.PublicDiscussionGridIndex) as string).replace("<<index>>", "" + indexClick));
        break;
      }
    }
    }

    static async clickOnSendForapprovalPopup(){
        //await CommonKeyword.clickElement("//input[contains(@id,'popup_ok')]");
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.PopUpOkSupp) as string)
    }
}
// module.exports=new SupplierEforum();