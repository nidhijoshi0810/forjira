export let eforumLocators = {
    goToEforumButton:"SupplierSide/eforum/goToEforumButtonId",
    NeweForumButton:"SupplierSide/eforum/NeweForumButton",
    NeweForumNameInputTxtbox:"SupplierSide/eforum/NeweForumNameInputTxtbox",
    NeweForumNameDescTxtbox:"SupplierSide/eforum/NeweForumNameDescTxtbox",
    SignatureTextbox:"SupplierSide/eforum/SignatureTextbox",
    customFormFieldsdropdown:"SupplierSide/eforum/customFormFieldsdropdown",
    customFormFieldsdropdownOption:"SupplierSide/eforum/customFormFieldsdropdownOption",
    forumList:"SupplierSide/eforum/forumList",
    createdEvent:"SupplierSide/eforum/createdEvent",
    newMessage:"SupplierSide/eforum/newMessage",
    enterTheMessage:"SupplierSide/eforum/enterTheMessage",
    postingTheMessage:"SupplierSide/eforum/postingTheMessage",
    PopUpOkSupp:"eFourm/PopUpOkSupp",
    PublicDiscussionGridIndex:"efourm/PublicDiscussionGridIndex",
    PublicDiscussionGrid:"efourm/PublicDiscussionGrid"
}