import "codeceptjs"; 
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
declare const inject: any; 

const { I } = inject();


export class ClassicRedirectionFlow{
  
    static async verifyApproval(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Approval") as string);
        console.log("Approval and review page")
    }


    static async WindowSwitchfunction1(){
        const windows = await I.grabAllWindowHandles();
        await I.switchToWindow( windows[1] );
        console.log("Window switched")
    }

    static async WindowSwitchfunction0(){
        const windows = await I.grabAllWindowHandles();
        await I.switchToWindow( windows[0] );
        console.log("Window switched")
    }


    static async verifySourcingSetting(verifyingFactor){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
       await ClassicRedirectionFlow.WindowSwitchfunction1();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel(verifyingFactor) as string);
        console.log("Settings clicked and verified")
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }


}