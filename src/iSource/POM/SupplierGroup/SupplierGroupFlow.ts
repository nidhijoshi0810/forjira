import { COE } from './../../../Framework/FrameworkUtilities/COE/COE';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { supplierGroupLocators } from "./SupplierGroupLocators";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { CreateEventFlow } from "../../POM/CreateEvent/CreateEventFlow";
import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { iSourcelmt } from '../../../Framework/FrameworkUtilities/i18nutil/readI18NProp';
declare const inject: any;
const { I } = inject();

export class SupplierGroupFlow{
    static async selectAllInSelectedSupplier(){
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.SelectAllCheckbox_SelectedSupplier) as string);
    }
    static async  clickCreateGroupButton(){
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.CreateGroupButton) as string);
    }
    static async  enterGroupName(){
        await I.fillField(Startup.uiElements.get(supplierGroupLocators.GroupName),Startup.testData.get("GroupName"))
    }
    static async  clickOnSelectAllOnCreateGroupPopup(){
        await  await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.SelectAllCreateGroupPopup) as string);
    }
    static async  clickOnCreateCreateSupplierGroup(){
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.CreateButtonCreateSupplierGroup) as string);
    }
    static async  clickOnDoneCreateGroup(){
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.DoneButton_CreateGroup) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }
    static async  clickOnSupplierGroupTab(){
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.SupplierGroupTabOnPopup) as string);
    }
    static async  closeViewAllSupplierPopup(){
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.closeIconViewAllSUpplierPopup) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.waitForText(await iSourcelmt.getLabel("ViewAllSuppliers"));
    }
    static async  addAditionalDetails(){
        await CreateEventFlow.verifyEventName();
        await CreateEventFlow.clickOnAdditionalDetailsTab();
        await CreateEventFlow.SelectAllAdditionalDropdown();
        await CreateEventFlow.verifyEventName();
        await CreateEventFlow.clickOnAdditionalDetailsTab();
        await CreateEventFlow.SelectAllAdditionalDropdown();
    }
    static async  navigateToViewAllSupliersPopup(){
        I.scrollIntoView(Startup.uiElements.get(supplierGroupLocators.View_all_Suppliers_hyperlink))
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(supplierGroupLocators.View_all_Suppliers_hyperlink) as string);
        // I.click(Startup.uiElements.get(supplierGroupLocators.View_all_Suppliers_hyperlink));
        I.wait(prop.DEFAULT_LOW_WAIT)
        I.seeElement(Startup.uiElements.get(supplierGroupLocators.All_Supplier_popup));
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }
    static async  selectSupplierGroupFromGriden(supplierName : string,columnName : string){
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(supplierGroupLocators.searchSuppliersInputOfGridPopup) as string,supplierName as string,columnName as string);

        // await searchOnGridMethod.selectSupplierGroupFromGriden(Startup.uiElements.get(supplierGroupLocators.searchSuppliersInputOfGridPopup),supplierName,columnName);

    }

}

// module.exports = new SupplierGroupFlow();