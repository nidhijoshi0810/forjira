export let supplierGroupLocators = {
    SelectAllCheckbox_SelectedSupplier:"SupplierPage/SelectAllCheckbox_SelectedSupplier",
    CreateGroupButton:"SupplierPage/CreateGroupButton",
    GroupName:"SupplierPage/GroupName",
    SelectAllCreateGroupPopup:"SupplierPage/SelectAllCreateGroupPopup",
    CreateButtonCreateSupplierGroup:"SupplierPage/CreateButtonCreateSupplierGroup",
    DoneButton_CreateGroup:"SupplierPage/DoneButton_CreateGroup",
    SupplierGroupTabOnPopup:"SupplierPage/SupplierGroupTabOnPopup",
    View_all_Suppliers_hyperlink:"ISource/Suppliers_Objects/View_all_Suppliers_hyperlink",
    All_Supplier_popup:"ISource/Suppliers_Objects/All_Supplier_pop-up",
    searchSuppliersInputOfGridPopup:"ISource/Suppliers_Objects/searchSuppliersInputOfGrid",
    closeIconViewAllSUpplierPopup:"ViewAllSupplier/closeIconViewAllSUpplierPopup"
}