import "codeceptjs"; 
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { SealedBidLocator } from "../../POM/AdminSetting_PostSendForApproval/SealedBidLocator";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import { workflowlocator } from "../../POM/WorkFlow/WorkFlowLocators";
import { WorkFlowFlow } from "../../POM/WorkFlow/WorkFlowFlow";
import { CommonLandingAction } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingAction";
import {SurrogateBidFlow} from "../../POM/SurrogateBid/SurrogateBidFlow";
import { SupplierHomeAction } from "../SupplierHome/SupplierHomeAction";
declare const inject: any; 
const { I } = inject();
export class SealedBidFlow{
    

    
    static async clickOnEventSetting(labelToBeClicked : string) {

        await I.wait(prop.DEFAULT_WAIT);
        
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.eventSettingOp) as string);

        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.eventSettingDropdown) as string);
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickLabel(labelToBeClicked);
        I.wait(prop.DEFAULT_WAIT);

    }

    static async YesNoOption(OptionName : string) {

        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(SealedBidLocator.YesNOSealedBid) as string,OptionName));
        if(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SealedBidLocator.saveButton) as string+"[not(@disabled)]")> 0)
        {
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.OKButton) as string);
        }
        else
        {
        I.wait(prop.DEFAULT_WAIT);
        }

    }

    static async YesNoOption1(OptionName1 : string) {

        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(SealedBidLocator.YesNoOpenOpenCloseDate) as string,OptionName1));
        if(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SealedBidLocator.saveButton) as string+"[not(@disabled)]")> 0)
        {
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.OKButton) as string);
        }
        else
        {
        I.wait(prop.DEFAULT_WAIT);
        }

    }

    static async YesNoOption2(OptionName2 : string) {

        await I.wait(prop.DEFAULT_WAIT);

        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(SealedBidLocator.EforumYesNo) as string,OptionName2));
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.OKButton) as string);

        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.UploadLogoSetting) as string);

    }

        static async searchWorkflowbyName(workflowName:string){
            await I.fillField(Startup.uiElements.get(workflowlocator.searchbox),workflowName);
            I.pressKey('Enter');
            I.wait(prop.DEFAULT_WAIT);

    }

    static async clickOnAction1(){
        await I.click(Startup.uiElements.get(workflowlocator.actionButton));
    }
    static async ActivateWorkflow1(){
        let status: string = await I.grabTextFrom(Startup.uiElements.get(workflowlocator.activate));
        if(status.localeCompare("Activate") == 0){
            await I.click(Startup.uiElements.get(workflowlocator.activate));
            I.wait(prop.DEFAULT_WAIT);
        }
        else
        {
        I.wait(prop.DEFAULT_WAIT);
        }
}


static async clickOnAction2(){
    await I.click(Startup.uiElements.get(workflowlocator.actionButton));
}


static async SuppEforumBtnClick(){
await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.SuppSideEforum) as string);
}

static async SealedBidCheck(){
    var a : Boolean;
    a=await DewElement.grabAttributeFrom(Startup.uiElements.get(SealedBidLocator.SealedRadioCheck) as string, "disabled");
    console.log("Value of a"+a);
    if(a==false)
    {
        console.log("Sealed bid is enabled")
    }
    else{
        console.log("Sealed bid is disabled")
    }
    
    }

    static async ApprAction(){
        await WorkFlowFlow.clickSaveandGotoWorkflow()
          await WorkFlowFlow.clickSendforApproval()
          await WorkFlowFlow.setCommentInput()
          await WorkFlowFlow.clickSendforApprovalOnPopup()
          await WorkFlowFlow.AcceptWorkflow()
        }

        
        static async HomeNav(){
            I.wait(prop.DEFAULT_WAIT)
            await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
        await I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
        await I.click((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>",await iSourcelmt.getLabel("HOME_TABNAME") as string))
            }

            static async loginTOSupplierSideLagacy111(){
                I.amOnPage(Startup.testData.get("supp_url"));
                await I.clearCookie();
                await I.refreshPage();
               I.seeElement(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled));
               await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled) as string);
               I.fillField(Startup.uiElements.get(SupplierHomeAction.usernameInputFiled),Startup.testData.get("supp_username"));
               I.fillField(Startup.uiElements.get(SupplierHomeAction.usernameEmailInputFiled),Startup.testData.get("supp_email"));
               await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.passwordClick) as string);
               I.fillField(Startup.uiElements.get(SupplierHomeAction.passwordInputField),Startup.testData.get("supp_password"))
               await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.loginBtn) as string);
                I.wait(prop.DEFAULT_MEDIUM_WAIT)

                if ( await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SupplierHomeAction.dialogueOnListingPage) as string)> 0) {
                    await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.dialogueOnListingPage) as string);
        
            }
        
            if ( await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SupplierHomeAction.clearFilterXpath) as string) > 0) {
                await CommonKeyword.clickElement(Startup.uiElements.get(SupplierHomeAction.clearFilterXpath) as string);
        
            }
          I.wait(prop.DEFAULT_MEDIUM_WAIT);
          I.seeElement(Startup.uiElements.get(SupplierHomeAction.searchEventID));
            }

            static async WrkflwTab(){
                await iSourcelmt.waitForLoadingSymbolNotDisplayed();
                await CommonKeyword.clickElement(await Startup.uiElements.get(workflowlocator.workFlowTab) as string);
                await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            }

            

}