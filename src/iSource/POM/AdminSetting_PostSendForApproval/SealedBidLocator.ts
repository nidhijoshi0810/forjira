export let SealedBidLocator = {
    eventSettingOp : "iSourceAdmin/Options/EventSettingOption",
    eventSettingDropdown : "Admin/EventSettingDropDown",
    SurrogateBidSetting : "Admin/EventSettingDropdownOption/SurrogateBidSetting",
    saveButton : "Admin/EventSettingDropdownOption/SaveButton",
    OKButton : "Admin/EventSettingDropdownOption/OKButton",
    selectYES : "Admin/EventSetting/SurrogateBidSetting/YES",
    selectNO : "Admin/EventSetting/SurrogateBidSetting/NO",
    disabledSealBid : "EventSettings/SealBidDisabled",
    EforumMsgVisYes: "Admin/EventSettingDropdownOption/EforumMsgVisibility/YES",
    YesNOSealedBid: "Admin/SealedBidPostApproval/RadioBtn/YesNoRadioBtn",
    YesNoOpenOpenCloseDate: "Admin/OpenCloseDatePostApproval/RadioBtn/YesNoRadioBtn1",
    EforumYesNo: "Admin/EforumMessageVisibility/RadioBtn/YesNoRadioBtn3",
    DropdownAdminSetting: "AdminSettings/DropdownValues/OptionsFromDropDown",
    SuppSideEforum: "Legacy/SuppSide/Eforum/EforumThreadSupp",
    SealedRadioCheck: "EventSettings/SealedBidCheck/SealedBidRadioCheck",
    UploadLogoSetting: "Legacy/UploadLogo/UploadLogoLeg"
   
}
   