import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { HeaderFilter } from "dd-cc-zycus-automation/dist/components/headerFilter";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { QuickSourceLocator } from "./QuickSourceLocator";

var searchResult:string;

export class QuickSourceSearch{
    static async searchEvent(type:string,data:string) {
        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events")as string, Startup.testData.get(data)as string,type);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }

    static async verifySearch(type:string,data:string){

        if(type === 'Event ID'){
            searchResult = await DewElement.grabTextFrom(Startup.uiElements.get(QuickSourceLocator.eventIdLabel)as string);
        }else if(type === 'Event Name'){
            searchResult = await DewElement.grabTextFrom(Startup.uiElements.get(QuickSourceLocator.eventNameLabel)as string);
        }else{
            searchResult = '';
        }

        if(searchResult === Startup.testData.get(data)as string){
            logger.info('========>Search Result Matches')
        }else{
            logger.info('========>Search Result Not Matches')
        }
    }
}