export let ShowSupplierLocators = {
    userList:"iSource/Settings/Sourcing/UserList",
    supplierName:"iSource/Settings/Sourcing/SupplierName",
    supplierFullName:"iSource/Settings/Sourcing/SupplierFullName",
    userIdSearchBox:"iSource/Settings/Sourcing/UserIdSearchBox",
    showSupplierCheckbox:"iSource/Settings/Sourcing/ShowSupplierCheckbox",
    save:"iSource/Settings/Sourcing/Save",
    supplierVerification:"iSource/Settings/Sourcing/SupplierVerification"
}