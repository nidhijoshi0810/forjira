import "codeceptjs"; 
declare const inject: any; 

const { I } = inject();
import { SupplierInfoLocators } from "./SupplierInfoLocators";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger"
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { ViewAllSuppliersSteps } from "../BayerSideSupplier/ViewAllSupplierFlow";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
let suplierDetails:string[];
export class SupplierInfoFlow{



    static async supplierInfoTab(){
        await I.switchToNextTab(1);
        await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.supplierInfoList) as string);
        await I.click(await Startup.uiElements.get(SupplierInfoLocators.selectSupplierContact)as string);
    }
    static async selectChcekBox(){
        let supplierLevel: string =  await I.grabTextFrom(await Startup.uiElements.get(SupplierInfoLocators.supplierLevel) as string);
        if(supplierLevel.localeCompare("10") == 0)
       {
            await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.checkedCheckBox)as string);
            await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.unchekedCheckBox) as string);
        }
        else{
            await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.unchekedCheckBox) as string);
        }
    }
    static async next(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.next) as string);
    }
    static async SaveAndApply(){
        suplierDetails = await I.grabTextFrom(await Startup.uiElements.get(SupplierInfoLocators.parameterOrder) as string);
        console.log("The log is here ::" + suplierDetails);
        await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.saveAndApply) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(SupplierInfoLocators.okLevel) as string);
        await I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }

    static async Verify(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_LOW_WAIT);
        await ViewAllSuppliersSteps.clickOnAddSuppliersTab();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await ViewAllSuppliersSteps.navigateToViewAllSupliersPopup();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_MEDIUM_WAIT);
        console.log(suplierDetails);
        for(let i = 1;i<suplierDetails.length+1;i++) {
            await I.scrollIntoView("("+await Startup.uiElements.get(SupplierInfoLocators.addSupplierHeader) as string+")["+i+"]");
            let modifiedSuplierDetails :string = await I.grabTextFrom("("+await Startup.uiElements.get(SupplierInfoLocators.addSupplierHeader) as string+")["+i+"]");
            if(modifiedSuplierDetails.localeCompare(suplierDetails[i-1]) == 0)
                {
                    console.log("Data Matched :"+modifiedSuplierDetails);
                }
                else{
                    throw new Error("there is an Exception with Supplier details");
                }
        }

    }

}