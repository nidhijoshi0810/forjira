export let SupplierInfoLocators = {
    supplierInfoList:"iSource/Settings/Sourcing/SupplierInfo/SupplierInfoList",
    selectSupplierContact:"iSource/Settings/Sourcing/SupplierInfo/SelectSupplierContact",
    supplierLevel:"iSource/Settings/Sourcing/SupplierInfo/SupplierLevel",
    checkedCheckBox:"iSource/Settings/Sourcing/SupplierInfo/CheckedCheckBox",
    unchekedCheckBox:"iSource/Settings/Sourcing/SupplierInfo/UnchekedCheckBox",
    next:"iSource/Settings/Sourcing/SupplierInfo/Next",
    saveAndApply:"iSource/Settings/Sourcing/SupplierInfo/SaveAndApply",
    okLevel:"iSource/Settings/Sourcing/SupplierInfo/OKLevel",
    parameterOrder:"iSource/Settings/Sourcing/SupplierInfo/ParameterOrder",
    addSupplierHeader:"iSource/Settings/Sourcing/SupplierInfo/addSupplierHeader"
}