import "codeceptjs"; 
declare const inject: any; 

const { I } = inject();
import { ShowSupplierLocators } from "./ShowSupplierLocators";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger"
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

let supplierFullName: string = "";
let supplierName: string = "";

export class ShowSupplierFlow{

    static async userListTab(){
        await I.switchToNextTab(1);
        await CommonKeyword.clickElement(await Startup.uiElements.get(ShowSupplierLocators.userList) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async searchUser(){
        await TextField.enterTextUsingLocator(await Startup.uiElements.get(ShowSupplierLocators.userIdSearchBox) as string,await Startup.testData.get("ShowSupplierUserId") as string);
        I.pressKey('Enter');
        await I.wait(prop.DEFAULT_LOW_WAIT);
    }
    static async uncheckShowSupplier(){
        await I.uncheckOption(await Startup.uiElements.get(ShowSupplierLocators.showSupplierCheckbox) as string);
    }
    static async checkShowSupplier(){
        await I.checkOption(await Startup.uiElements.get(ShowSupplierLocators.showSupplierCheckbox) as string);
    }
    static async saveUserList(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(ShowSupplierLocators.save) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async supplierNameNotVisibile(hiddenValue:string){
        await I.scrollIntoView(await Startup.uiElements.get(ShowSupplierLocators.supplierName) as string);
        let supplierVerificationPath : string =await Startup.uiElements.get(ShowSupplierLocators.supplierVerification) as string
        let tempName :string = await CommonFunctions.getFinalLocator(supplierVerificationPath,supplierName) as string;
        let tempfullName :string = await CommonFunctions.getFinalLocator(supplierVerificationPath,supplierName) as string;
        DewElement.verifyIfIDontSeeElement(tempName);
        DewElement.verifyIfIDontSeeElement(tempfullName);
        let suppName: string = await I.grabAttributeFrom(await Startup.uiElements.get(ShowSupplierLocators.supplierName) as string, "title");
        let suppFullName: string = await I.grabAttributeFrom(await Startup.uiElements.get(ShowSupplierLocators.supplierFullName) as string, "title");
        logger.info("Visible supplierName :"+suppName);
        logger.info("Visible supplierFullName :"+ suppFullName);
    }
    static async supplierNameVisibility(){
        await I.scrollIntoView(await Startup.uiElements.get(ShowSupplierLocators.supplierName) as string);
        supplierName = await I.grabAttributeFrom(await Startup.uiElements.get(ShowSupplierLocators.supplierName) as string,"title");
        supplierFullName = await I.grabAttributeFrom(await Startup.uiElements.get(ShowSupplierLocators.supplierFullName) as string,"title");
        let supplierVerificationPath : string =await Startup.uiElements.get(ShowSupplierLocators.supplierVerification) as string
        let tempName :string = await CommonFunctions.getFinalLocator(supplierVerificationPath,supplierName) as string;
        let tempfullName :string = await CommonFunctions.getFinalLocator(supplierVerificationPath,supplierName) as string;
        DewElement.verifyIfISeeElement(tempName);
        DewElement.verifyIfISeeElement(tempfullName);
        console.log("suppName:: "+supplierName);
        console.log("suppFullName:: "+supplierFullName);
    }

}