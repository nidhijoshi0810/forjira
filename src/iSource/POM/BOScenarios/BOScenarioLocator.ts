
export let BOScenarioElements = {
    pricingAnalysis:"BidOptimization/pricingAnalysis",
    Bid2ScenarioAnalyzeButton: "BidOptimization/Bid2scenarioanalyzeButton",
    verifyEventLevel: "BidOptimization/VerifyEventLevel",
    verifySectionLevel: "BidOptimization/VerifysectionLevel",
    verifySupplierLevel: "BidOptimization/VerifySupplierLevel",
    verifyItemLevel: "BidOptimization/VerifyItemLevel",
    cancelTheElement: "BidOptimization/cancelTheElement",
    navigateToCherryAnalyze: "BidOptimization/navigateToCherryAnalyze",
    changeScope:"BidOptimization/CherryPickchangeScope",
    seeScheduleHeader:"BidOptimization/seeScheduleHeader",
    selectInNextMonth:"BidOptimization/selectInNextMonth",
    customBest2Supplier:"BidOptimization/customBest2Supplier",
    splitQuantity:"BidOptimization/splitQuantity",
    splitQuantityInput: "BidOptimization/splitQuantityInput",
    itemPopupApply: "BidOptimization/itemPopupApply",
    exportScenarioItem: "BidOptimization/exportScenarioItem"
}