import "codeceptjs";
declare const inject: any;
const { I } = inject();

import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { BOScenarioElements } from "../BOScenarios/BOScenarioLocator"
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { Wait } from "dd-cc-zycus-automation/dist/components/dewWait";
import { DewRadioButton } from "dd-cc-zycus-automation/dist/components/dewRadioButton"
import { DatePicker } from "dd-cc-zycus-automation/dist/components/datePicker";
import { TextField } from 'dd-cc-zycus-automation/dist/components/textfield';
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import {Footer} from 'dd-cc-zycus-automation/dist/components/footer'
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";



export class BOScenarioFlow {

    static async clickOnSingleAnalyzeForBest2Scenario() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.Bid2ScenarioAnalyzeButton) as string, await iSourcelmt.getLabel("analyzeTab") as string));
    }

    static async verifyEventLevel() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.verifyEventLevel) as string);
    }

    static async verifySectionLevel() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.verifySectionLevel) as string);
    }

    static async verifySupplierLevel() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.verifySupplierLevel) as string);
    }

    static async verifyItemLevel() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.verifyItemLevel) as string);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.cancelTheElement) as string , await iSourcelmt.getLabel("Cancel") as string));
    }
    

    static async navigateToAnalyzeCherryPicking() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_MEDIUM_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.navigateToCherryAnalyze) as string, await iSourcelmt.getLabel("analyzeTab") as string));
    }

    static async makeAllocationToItemLevel() {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_LOW_WAIT);
        let index: number = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(BOScenarioElements.splitQuantity) as string);
        if (index >= 1) {
            for (let i = 1; i <= index; i++) {
                let splitString = await Startup.uiElements.get(BOScenarioElements.splitQuantity) as string;
                let combinedSplitString = `${splitString}[${i}]`
                await CommonKeyword.clickElement(combinedSplitString);
                let idx = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(BOScenarioElements.splitQuantityInput) as string)
                let combinedInputString = await Startup.uiElements.get(BOScenarioElements.splitQuantityInput) as string;
                if (idx >= 1) {
                    for (let i = 1; i <= idx; i++) {
                        let data = `${combinedInputString}[${i}]`
                        await TextField.enterTextUsingLocator(data, Startup.testData.get("MaxScore") as string);
                    }
                }
                await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.itemPopupApply) as string);
            }
        } else {
            console.log("There is no split quantity to allocate")
        }
    }

    static async exportScenario(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.exportScenarioItem) as string);
    }

    static async changeScope() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await Startup.uiElements.get(BOScenarioElements.changeScope) as string);
    }

    static async scheduleScnarioForOptimization() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.seeElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.seeScheduleHeader) as string, await iSourcelmt.getLabel("Schedule scenario for Optimization") as string));
    }

    static async selectScheduleScenario() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewRadioButton.selectRadioButton(await iSourcelmt.getLabel("scheduleOptimization") as string);
    }

    static async updatescenarioName() {
        await TextField.enterText(await iSourcelmt.getLabel("ScenarioName") as string, await Startup.testData.get("testdataforScenario") as string);
        await DatePicker.selectInNextMonth(await Startup.uiElements.get(BOScenarioElements.selectInNextMonth) as string, await Startup.testData.get("LastDate") as string,false)
    }

    static async customBest2() {
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.cancelTheElement) as string , await iSourcelmt.getLabel("Cancel") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.Bid2ScenarioAnalyzeButton) as string, await iSourcelmt.getLabel("analyzeTab") as string));  
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()      
        await I.seeElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(BOScenarioElements.customBest2Supplier) as string, await iSourcelmt.getLabel("Best 2 Suppliers") as string));
    }
}
