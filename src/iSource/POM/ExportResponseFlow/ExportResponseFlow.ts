import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
//import {ViewSummaryLocators} from "./ViewSummaryLocators";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { Stats } from "fs";
import { ShareEventLocator } from "../ShareEvent/ShareEventLocator";
import { ExportResponseLocator } from "./ExportResponseLocator";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { Footer } from "dd-cc-zycus-automation/dist/components/Footer";
export class ExportResponseFlow{
    static async ClickonExportNonPricingResp() {
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(ExportResponseLocator.ExportNonPricingResponsesInCSV) as string);
        // await CommonKeyword.clickElement(Startup.uiElements.get(ExportResponseLocator.ExportButton) as string);
        await Footer.clickModalFooterButton(await iSourcelmt.getLabel("ExportOption") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(ExportResponseLocator.closeExportPopupButton) as string);
        await I.wait(prop.DEFAULT_LOW_WAIT);
    }
    
}