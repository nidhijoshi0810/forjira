export let ExportResponseLocator ={
    ExportButton : "secondaryaction/ExportPopUp/ExportButton",
    ExportNonPricingResponsesInCSV : "secondaryaction/ExportPopUp/ExportNonPricingResponsesInCSV",
    closeExportPopupButton:"secondaryaction/ExportPopUp/closeExportPopupButton"
}