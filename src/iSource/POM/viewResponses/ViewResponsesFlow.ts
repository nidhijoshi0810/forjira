
declare const inject: any; 
const { I } = inject();
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { ShareEventLocator } from "../ShareEvent/ShareEventLocator";
import { ViewResponseLocator } from "./ViewResponsesLocator";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
export class viewResponsesFlow{
    static async SeeViewResponses() {       
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await  DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "View responses" ) as string)
    }

}
