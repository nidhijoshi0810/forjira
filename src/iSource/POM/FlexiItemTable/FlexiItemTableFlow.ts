
import { DewFileUpload } from 'dd-cc-zycus-automation/dist/components/dewFileUpload';
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { DragAndDrop } from 'dd-cc-zycus-automation/dist/components/dragAndDrop';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { CommonLocators } from '../../isourceCommonFunctions/CommonLocators';
import {FlexiItemTableLocators } from "./FlexiItemTableLocators";

export class FlexiItemTableFlow{

// by vaishnavi

static async addFlexiItemTable(){

    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("AddPricingTableLabelKey") as string);
    await CommonKeyword.click("(//button//div//span[contains(text(), 'Select')])[2]");
    await DewFileUpload.uploadFile("/DataFiles/iSource/Flexi_with_all_cominationwith_CP.xlsx", "Flexi_with_all_cominationwith_CP.xlsx");
    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
}

static async CreateMappingOfFlexiItemTable(){

    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await this.clickOnCreateMappingAndSelectSheet();
    await this.SelectRowNoAndMapParentChild();
    await this.MapOtherRowsFromDropdown();
    await this.MapOtherColumnsByDragAndDrop();
    await this.MapSupplierMandatoryNonMandatory();
    
}

static async MovetoNextStep(){
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MappingNextStepRightArrow) as string);
}

static async clickOnCreateMappingAndSelectSheet(){

    await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("CreateMappingLabelKey") as string);
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.SheetOneRadioButton) as string);
    await this.MovetoNextStep();

}

static async SelectRowNoAndMapParentChild(){

    I.fillField((Startup.uiElements.get(FlexiItemTableLocators.RowNumberForColumnMapping)), 1);
    await this.MovetoNextStep();
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.ParentChildRadioButton) as string);
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.ParentChildDropdown) as string);
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.ParentChildDropdownValueFlag) as string);
    await this.MovetoNextStep();

  
}

static async MapOtherRowsFromDropdown(){

    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+1+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapItemNo) as string);
    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+2+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapItemName) as string);
    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+4+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapQuantity) as string);
    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+5+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapUOM) as string);
    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+6+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapTotalCost) as string);
    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+7+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapCurrentPrice) as string);
    await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+8+"]");
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapSupplierBid) as string);
    // await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+9+"]");
    // await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapNoBid) as string);
    // await CommonKeyword.click("("+(Startup.uiElements.get(FlexiItemTableLocators.MappingOtherColumnsDropdown))+")["+10+"]");
    // await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MapCategory) as string);
    // I.scrollIntoView()
    await this.MovetoNextStep();

}

static async MapOtherColumnsByDragAndDrop(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    // await DragAndDrop.performDragAndDrop("NPDF1", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToNPDF) as string);
    // await DragAndDrop.performDragAndDrop("NPDF2", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToNPDF) as string);
    await DragAndDrop.performDragAndDrop("Unit Price 1", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToUnitCost) as string);
    await DragAndDrop.performDragAndDrop("FP1", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToUnitCost) as string);
    await DragAndDrop.performDragAndDrop("UP2", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToFixedCost) as string);
    await DragAndDrop.performDragAndDrop("FP2", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToFixedCost) as string);
    await DragAndDrop.performDragAndDrop("Total", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToFormula) as string);
    await this.MovetoNextStep();
}

static async saveMapping(){

    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveLabelKey") as string);
    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("YesLabelKey") as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    // await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    var xpath : string = await CommonFunctions.getFinalLocator(Startup.uiElements.get(FlexiItemTableLocators.MappingSuccessDoneButton)as string, await iSourcelmt.getLabel("DoneLabelKey")as string);
    await CommonKeyword.click(xpath);
    // I.wait(prop.DEFAULT_MEDIUM_WAIT);
    // await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
}

static async MapSupplierMandatoryNonMandatory(){

    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.SupplierMandatoryCheckBox) as string);
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.LockButton) as string);
    await this.saveMapping();
}

static async navigateToSection(sectionNo : string){
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.click( "("+(Startup.uiElements.get(FlexiItemTableLocators.SectionToReach) as string)+")["+sectionNo+"]");
    // var xpath : string = await CommonFunctions.getFinalLocator(Startup.uiElements.get(FlexiItemTableLocators.SectionToReach) as string, await iSourcelmt.getLabel("ViewQuestionsLabelKey") as string);
    // //span[contains(@title,'Add')]    //span[contains(text(),'<<dataname>>')]
}
static async editMappingForExistingTable(){

    await CommonFunctions.clickOnButton( await iSourcelmt.getLabel("EditMappingLabelKey") as string);
    // await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.MappingBackLeftArrow) as string);
    await this.MovetoNextStep();
    I.wait(prop.DEFAULT_LOW_WAIT);
    await this.MovetoNextStep();
    I.wait(prop.DEFAULT_LOW_WAIT);
    await this.MovetoNextStep();
    I.wait(prop.DEFAULT_LOW_WAIT);
    await this.MovetoNextStep();
    I.wait(prop.DEFAULT_LOW_WAIT);
    await DragAndDrop.performDragAndDrop("NPDF1", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToNPDF) as string);
    await DragAndDrop.performDragAndDrop("NPDF2", Startup.uiElements.get(FlexiItemTableLocators.DragAndDropToNPDF) as string);
    await this.MovetoNextStep();
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.SupplierMandatoryCheckBox) as string);
    await this.saveMapping();
}

static async unlockFlexiExcelMapping(){

    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.unlockIcon) as string);

}

static async lockFlexiExcelMapping(){

    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.LockIcon) as string);

}

static async overrideFlexiExcelMapping(){

    await this.unlockFlexiExcelMapping();
    I.wait(prop.DEFAULT_LOW_WAIT);
    await CommonKeyword.click(Startup.uiElements.get(FlexiItemTableLocators.ImportOverrideExcel) as string);
    await DewFileUpload.uploadFile("/DataFiles/iSource/Flexi_with_all_cominationwith_CP.xlsx", "Flexi_with_all_cominationwith_CP.xlsx");
    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await this.CreateMappingOfFlexiItemTable();

}

}