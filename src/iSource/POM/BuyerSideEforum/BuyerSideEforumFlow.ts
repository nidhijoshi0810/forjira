import "codeceptjs";
import { eforumLocators } from "./BuyerSideEforumLocators";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
declare const inject: any;
const { I } = inject();
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { DewDropdown } from "dd-cc-zycus-automation/dist/components/dewDropdown";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import { DewCheckbox } from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { DewFileUpload } from "dd-cc-zycus-automation/dist/components/dewFileUpload";
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";

var elementCountAfterDelete:Number;
var elementCountBeforeDelete:Number;
var topicCountBefore:Number;
var topicCountAfter:Number;
export class BuyerEforum {


    static async searchForEvent() {
        I.wait(prop.DEFAULT_WAIT);
        await I.fillField(await Startup.uiElements.get(eforumLocators.EventSearchBox)as string, await Startup.testData.get('eforum_eventid')as string);
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.searchByEventId)as string)

    }

    static async selectEvent() {
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.selectEvent)as string)

    }

    static async navigateToEfroum() {
        I.wait(prop.DEFAULT_WAIT)
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.navigateToEfroum)as string)
        
    }

    static async createNewTopic(type: string) {
        await BuyerEforum.clickNewTopicBtn();
        await BuyerEforum.enterTopicName(await Startup.testData.get('eforum_topicName')as string);
        await BuyerEforum.selectType(type)
        await BuyerEforum.enterDiscription(await Startup.testData.get('eforum_topicDescription')as string);
        await BuyerEforum.enterSignature(await Startup.testData.get('eforum_topicSignature')as string);
    }

    static async addSupplier() {
        I.wait(prop.DEFAULT_WAIT);
        await BuyerEforum.clickNext();
        await BuyerEforum.selectSupplier();
        await BuyerEforum.clickAdd();
    }

    static async clickNewTopicBtn() {
        I.wait(prop.DEFAULT_WAIT)
        await DewButton.click(await iSourcelmt.getLabel('NewTopic')as string);
    }

    static async enterTopicName(name: string) {        
        await TextField.enterTextUsingPlaceHolder(await iSourcelmt.getLabel('NameLabel')as string, name)
    }

    static async selectType(topicType: string) {        
        await DewDropdown.selectByText(await Startup.uiElements.get(eforumLocators.topicTypeDropdown)as string, topicType)
        if (topicType === 'Public') {
            //chekboxcode
            await DewCheckbox.selectCheckbox(await iSourcelmt.getLabel('MessageApproval')as string);
        }
    }

    static async enterDiscription(discription: string) {
        await TextArea.enterTextWithPlaceHolder(await iSourcelmt.getLabel('Description')as string, discription) //iSourcelmt //testdata
    }

    static async enterSignature(signature: string) {
        await TextArea.enterTextUsingLocator(await Startup.uiElements.get(eforumLocators.signature)as string, signature)
    }

    static async clickNext() {
        await DewButton.click(await iSourcelmt.getLabel('nextLabelKey')as string);
    }

    static async selectSupplier() {
        await ModalPopUp.applyGridSearch(await iSourcelmt.getLabel("Search") as string,await Startup.testData.get('eforum_Supplier')as string, await iSourcelmt.getLabel('SupplierNameKey')as string)
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.selectAllCheckbox)as string)
        
    }

    static async clickAdd() {
        I.wait(prop.DEFAULT_WAIT)
        await DewButton.click(await iSourcelmt.getLabel('AddLabel')as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
        await DewButton.click(await iSourcelmt.getLabel('OkbuttonText')as string)

    }

    static async addPublicPostWithAttachment() {
        I.wait(prop.DEFAULT_WAIT)
        await TextArea.enterTextUsingLocator(await Startup.uiElements.get(eforumLocators.addPostTextbox)as string, await Startup.testData.get('eforum_postData')as string);
        await DewButton.click(await iSourcelmt.getLabel('AddAttachments')as string);
        // await DewFileUpload.uploadFile('batman.jpg', 'batman.jpg');
        
        let filepath : string = "/DataFiles/iSource/WORD FILE.docx";
        await  DewFileUpload.uploadFile(filepath,"WORD FILE.docx");

        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.saveBtnClick)as string)
        await DewButton.click(await iSourcelmt.getLabel('PostLabelKey')as string);
    }

    static async deleteTopic() {
        topicCountBefore = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(eforumLocators.checkTopicDeleted)as string);

        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.secondaryAction)as string)
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.deleteTopic)as string)
        await DewButton.click(await iSourcelmt.getLabel('YesLabelKey') as string);
    }

    static async editTopic() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.secondaryAction)as string)
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.editTopic)as string)

        await BuyerEforum.enterTopicName(await Startup.testData.get('eforum_topicName')as string);
        await BuyerEforum.enterDiscription(await Startup.testData.get('eforum_topicDescription')as string);
        await BuyerEforum.enterSignature(await Startup.testData.get('eforum_topicSignature')as string);
        await DewButton.click(await iSourcelmt.getLabel('SaveButtonLabel')as string);
        // I.wait(prop.DEFAULT_LOW_WAIT)
        // await DewButton.click(await iSourcelmt.getLabel('OkbuttonText')as string)
    }



    static async selectPublicTopic() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.selectPublic)as string)
    }
    static async updateParticepent() {
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.editSupplier)as string)
        await ModalPopUp.applyGridSearch(await iSourcelmt.getLabel("Search") as string,await Startup.testData.get('eforum_SupplierUpdate')as string, await iSourcelmt.getLabel('SupplierNameKey')as string)
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.selectAllUpdateSupplier)as string)
        await DewButton.click(await iSourcelmt.getLabel('OkbuttonText')as string)
    }


    static async clickActionMenuBtn(){
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.clickActionMenu)as string)

    }

    static async selectDeleteOption(){
        elementCountBeforeDelete = await DewElement.grabNumberOfVisibleElements(`//div[contains(@class,'message-container')]//dew-section`);
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.deletePost)as string)
        await DewButton.click(await iSourcelmt.getLabel('YesLabelKey')as string)
        I.wait(prop.DEFAULT_WAIT)
    }

    static async selectEditOption(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(eforumLocators.editPost)as string)
    }

    static async enterEditText(){
        await TextField.enterTextUsingPlaceHolder(await iSourcelmt.getLabel('ReplyLabel')as string, await Startup.testData.get('eforum_editMessage')as string)
    }

    static async clickDoneBtn(){
        await DewButton.click(await iSourcelmt.getLabel('DoneButtonLabel')as string)
    }

    static async clickOnApprovePost(){
        await DewButton.click(await iSourcelmt.getLabel('ApproveAndPost')as string);
    }

    static async clickOnReject(){
        await DewButton.click(await iSourcelmt.getLabel('RejectKey')as string);
        await DewButton.click(await iSourcelmt.getLabel('YesLabelKey')as string)
    }

    static async editApprovePost(){
        await TextField.enterTextUsingLocator(await Startup.uiElements.get(eforumLocators.editApprovePost)as string,await Startup.testData.get('eforum_editedPost')as string)
    }

    static async checkForApprovalMessage(){
        await CommonKeyword.scrollIntoView(await Startup.uiElements.get(eforumLocators.approvePostBtn)as string)
        // await I.scrollIntoView(await Startup.uiElements.get(eforumLocators.approvePostBtn)as string)
        // await I.seeElement(await Startup.uiElements.get(eforumLocators.approvePostBtn)as string);
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.approvePostBtn)as string)    
    }

    static async checkNotificationForNewMessage(){
        // await I.seeElement(await Startup.uiElements.get(eforumLocators.newMessageNotification)as string);  
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.newMessageNotification)as string)

    }

    static async checkforCurrentPage() {
        await I.seeInCurrentUrl('/collaboration');
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.allTopicsLabel)as string);
    }

    static async checkforPublicTopic(){
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.publicTopicActive)as string);
    }

    static async checkIfTopicIsCreated(topicType:string){
        I.wait(prop.DEFAULT_WAIT)
        if(topicType === 'Public'){
            await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkPublicTopic)as string);
            await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkTopicName)as string);
        }else if(topicType === 'Information'){
            await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkInformationTopic)as string);
            await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkTopicName)as string);
        }

    }

    static async checkIfPostIsAdded() {
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkTopicName)as string);
    }

    static async checkIfSupplierUpdated(){
        I.wait(prop.DEFAULT_LOW_WAIT)
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkAdditionSuccessful)as string);
        
        await DewButton.click(await iSourcelmt.getLabel('OkbuttonText')as string)
        
    }

    static async checkIfMessageEdited(){
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkEditMessage)as string);
    }

    static async checkIfMessageDeleted(){
        elementCountAfterDelete = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(eforumLocators.checkMessageDeleted)as string);
        
        if(elementCountBeforeDelete>elementCountAfterDelete){
            logger.info('Message Deleted Successfully');
        }else{
            logger.info('Message Not Deleted Successfully')
        }
    }

    static async checkIfTopicEdited(){
        I.wait(prop.DEFAULT_LOW_WAIT)
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.checkTopicEdited)as string);
        await DewButton.click(await iSourcelmt.getLabel('OkbuttonText')as string)
        
    }

    static async checkIfTopicDeleted(){
        topicCountAfter = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(eforumLocators.checkTopicDeleted)as string);
        console.log(4455,topicCountBefore,topicCountAfter);
        
        if(topicCountBefore>topicCountAfter){
            logger.info('Topic Deleted Successfully');
        }
    }

    static async clickOnPublicMessage(){
        let totalEnteries = await DewElement.getNumberOfElementsPresentInDom(await Startup.uiElements.get(eforumLocators.TopicLabel) as string);
        console.log(totalEnteries);

        for (let index = 1; index <= totalEnteries; index++) {
            let entry = (await Startup.uiElements.get(eforumLocators.TopicLabelIndex) as string).replace("<<index>>", "" + index);
            if ((await I.grabTextFrom(entry)).includes(await iSourcelmt.getLabel("Public") as string)) {
              await CommonKeyword.clickElement(entry);
              console.log("public message clicked")
            }
          }
    }

    static async verifyPublicMessage() {
       await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Reject") as string);
        console.log("Public message of supplier verified")
      }

      static async checkNotificationForNewPublicMessage(){
        console.log("Public message notification verified")
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(eforumLocators.newPublicMessageNotification)as string)
       
    }

}
