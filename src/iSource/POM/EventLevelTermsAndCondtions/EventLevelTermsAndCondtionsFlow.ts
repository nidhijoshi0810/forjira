import { DewFileUpload } from 'dd-cc-zycus-automation/dist/components/dewFileUpload';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {EventLevelTermsAndCondtions} from "./EventLevelTermsAndCondtions";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";

export class AttachmentsFlow{

    static async clickOnTandCondTab(){
        //console.log("***************"+iSourcelmt.getLabel("AttachmentsTabLabel"));
        //I.click(await CommonFunctions.getFinalLocator(.uiElements.get(attachmentsElements.tabXpath),iSourcelmt.getLabel("AttachmentsTabLabel")));
       await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("TermsAndConditionsTabLabel") as string);
        // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventLevelTermsAndCondtions.tabXpath) as string, "Terms & Conditions"));
    }
    static async attachFile(){

       //I.attachFile(.uiElements.get(attachmentsElements.AttachmentsBrowseButton),"AutomationPresentation.pptx");//selector needs to be typeof `string` or `function`
       let filepath : string = "/DataFiles/iSource/WORD FILE.docx";
    //  await DewFileUpload.uploadFile(filepath,"WORD FILE.docx");
       I.attachFile("//dew-tab[@id='termsConditions']//isource-attachments//dew-add-attachments//dew-file-upload//input[@id='file1']","/DataFiles/iSource/WORD FILE.docx");
       I.wait(prop.DEFAULT_LOW_WAIT);
       logger.info("File uploaded in Terms And Condition section.");
    }
}
// module.exports = new AttachmentsFlow();