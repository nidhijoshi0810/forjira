export let QuicksourceAdminLocator = {
QuickAdminSett: "QuickSourceControls/Settings",
PotentialSuppYesNo: "QuickSourceControls/PotentialSuppYesNo",
ItmeDetailsException: "Quicksource/Exception/ItemDetails/ItmeDetailsException",
RecommendedAddRemove: "Quicksource/RecommendedSuppliers/RecommendedAddRemove",
CloseSmartAssist: "Quicksource/CloseSmartAssist",
RecommSuppA: "Quicksource/RecommSuppA",
RecommSuppB: "Quicksource/RecommSuppB",
RecommRemoveCheck: "Quicksource/RecommRemoveCheck",
SuppSearchnSelect: "Quicksource/SuppSearchnSelect",
SuggestNewSuppCheck: "Quicksource/SuggestNewSuppCheck",
NoOfStatus: "Quicksource/NoOfStatus",
NoOfCheckbox: "Quicksource/NoOfCheckbox",
NoOfStatusN: "Quicksource/NoOfStatusN",
realTimeSavings: "Quicksource/realTimeSavings",
specificQty: "Quicksource/specificQty",
dateNotDisabled: "Quicksource/dateNotDisabled",
dateDisabled: "Quicksource/dateDisabled"
}