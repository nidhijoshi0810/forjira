import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import {Checkbox} from "dd-cc-zycus-automation/components/checkbox";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import{commonLandingFlow} from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow"
import {SealedBidLocator} from "../AdminSetting_PostSendForApproval/SealedBidLocator"
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import {QuicksourceAdminLocator} from "../QuicksourceAdmin/QuicksourceAdminLocator"
import { verify } from "crypto";
import { CreateQuickSourceEvent } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import {CreateScoreSheetLocator} from "../ScoreSheet/CreateScoreSheetLocator"
import { listingPageElements } from "../../POM/ListingPageFullSource/ListingPagePrimaryLocators";
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup";
import { StringifyOptions } from "querystring";
import { DatePicker } from "dd-cc-zycus-automation/dist/components/datePicker"
import { eventSettingsFlow } from "./../eventSettings/eventSettingsFlow";
import { SupplierGroupFlow } from "../../POM/CreateQuickSourceEvent/CreateQuickSourceEventFlow";
import { strict } from "assert";
import { awardQuickSourceElements } from "../AwardQuickSourceEvent/AwardQuickSourceLocator";

export class QuicksourceAdminFlow {


    static async NavToQuickAdmin(){
        await commonLandingFlow.navigateToTab("ISOURCE_APPNAME", "SettingsLabelKey", "Quick Source Controls");
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_WAIT)
     }

     static async ClickOnYesNo(YesNoOption : string) {
        await I.wait(prop.DEFAULT_WAIT);
        const webelement=await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuicksourceAdminLocator.PotentialSuppYesNo) as string,YesNoOption);
        CommonKeyword.click(webelement);
        if(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(SealedBidLocator.saveButton) as string+"[not(@disabled)]")> 0)
        {
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SealedBidLocator.OKButton) as string);
        }
        else
        {
        I.wait(prop.DEFAULT_WAIT);
        }

    }

    static async PotSuppSettings(){
        I.wait(prop.DEFAULT_WAIT)
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuicksourceAdminLocator.QuickAdminSett) as string,await iSourcelmt.getLabel("SettingsLabelKey") as string));
     }
    
     static async itemDetailsException(){
        I.wait(prop.DEFAULT_WAIT)
        await DewElement.verifyIfISeeElement(Startup.uiElements.get(QuicksourceAdminLocator.ItmeDetailsException)as string);
   }

     static async QuickItem(){
        I.wait(prop.DEFAULT_WAIT)
        await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateQuickSourceEvent.DescriptionInputBox)) as string), "Pen");
        I.wait(prop.DEFAULT_WAIT)
     }

     static async sendWithoutDetails() {
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.SendButton) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT)

    }

    static async RecommSuppButton() {
      I.wait(prop.DEFAULT_MEDIUM_WAIT)
      await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(listingPageElements.primaryAction) as string,await iSourcelmt.getLabel("Recommended Suppliers") as string));
      I.wait(prop.DEFAULT_MEDIUM_WAIT)

  }
   static a : string;
  static async AddRecommSupp() {
   await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuicksourceAdminLocator.RecommendedAddRemove) as string,await iSourcelmt.getLabel("AddButton") as string));
   this.a= await DewElement.grabTextFrom(Startup.uiElements.get(QuicksourceAdminLocator.RecommSuppA)as string);
   I.wait(prop.DEFAULT_MEDIUM_WAIT)

}

static async removeRecommSupp() {
   await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuicksourceAdminLocator.RecommendedAddRemove) as string,await iSourcelmt.getLabel("Remove") as string));
   I.wait(prop.DEFAULT_MEDIUM_WAIT)

}

static async recommAddCheck() {
   await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CreateQuickSourceEvent.tabXpath) as string, await iSourcelmt.getLabel("SupplierLabel") as string));
   I.wait(prop.DEFAULT_Standard_WAIT)
   const b = await DewElement.grabTextFrom(Startup.uiElements.get(QuicksourceAdminLocator.RecommSuppB)as string);
   console.log("+++++++++++++++++++++++++++++"+b);
   if(b.includes(this.a))
   {
      console.log("Recommended supplier has been added :-)");
   }
   else
   {
      console.log("Recommended supplier has not been added :-(");
   }
}

static async recommRemoveCheck() {
   await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CreateQuickSourceEvent.tabXpath) as string, await iSourcelmt.getLabel("SupplierLabel") as string));
   I.wait(prop.DEFAULT_MEDIUM_WAIT)
   const b=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuicksourceAdminLocator.RecommRemoveCheck)as string)

   if(b > 0)
   {
      console.log("Recommended supplier has been removed :-)");
   }
   else
   {
      console.log("Recommended supplier has not been removed :-(");
   }
}

static async closeSmartAssist() {
   await CommonKeyword.clickElement(Startup.uiElements.get(QuicksourceAdminLocator.CloseSmartAssist)as string)
   I.wait(prop.DEFAULT_MEDIUM_WAIT)

}

static async sendWithPastDateTime() {
   await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.LastDateInputField) as string);
   const c:number=await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(QuicksourceAdminLocator.dateNotDisabled) as string));
   const d:number=await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(QuicksourceAdminLocator.dateDisabled) as string));

   console.log("Not dis+++++"+c);
   console.log("Dis+++++"+d);
   var last : number = c-1;
   var xpath : string = (Startup.uiElements.get(QuicksourceAdminLocator.dateDisabled) as string+"["+d+"]");
   var isOrNot : boolean = await DewElement.checkIfElementPresent(xpath);
   if( isOrNot )
   {
      console.log("Cannot select PAST Date and Time");
   }
   else
   {
      console.log("Something is wrong. Past date is selected");
   }
   await CommonKeyword.clickElement(Startup.uiElements.get(CreateQuickSourceEvent.LastDateInputField) as string),(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(-2));
   I.wait(prop.DEFAULT_Standard_WAIT)
}

static async PotentialSuppVerify() {
   await TextField.enterTextUsingLocator(((Startup.uiElements.get(QuicksourceAdminLocator.SuppSearchnSelect)) as string), "jhg");
   const e: number=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuicksourceAdminLocator.SuggestNewSuppCheck) as string)
   console.log("Count of suggest new supplier"+e);
   if(e==0)
   {
      console.log("Sorry!!! POTENTIAL SUPPLIER could not be created. Change the admin settings to proceed");
   }
   else
   {
      console.log("Product bug")
   }

   I.wait(prop.DEFAULT_MEDIUM_WAIT)

}


static async AwardNoQuoteSupp() {
   I.wait(prop.DEFAULT_MEDIUM_WAIT)
   var status:number= await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuicksourceAdminLocator.NoOfStatusN) as string)
   I.wait(prop.DEFAULT_MEDIUM_WAIT)
   var ckbox: number= await DewElement.getNumberOfElementsPresentInDom(Startup.uiElements.get(QuicksourceAdminLocator.NoOfCheckbox) as string)
   I.wait(prop.DEFAULT_MEDIUM_WAIT)
   console.log("status++++++++"+status);
   console.log("checkbox++++++++"+ckbox);
   if(status==ckbox)
   {
      for(var i=1;i<status;i++)
      {
         var statuspendin:boolean =await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuicksourceAdminLocator.NoOfStatus) as string, await iSourcelmt.getLabel("Pending") as string));
         var checkdis:boolean=await DewElement.checkIfElementPresent((Startup.uiElements.get(QuicksourceAdminLocator.NoOfCheckbox) as string)+"[@disabled]");
         if(statuspendin && checkdis)
         {
            console.log("Cannot award this supplier");
         }
         else
         {
            console.log("Can award this supplier");
         }
      }

   }
   I.wait(prop.DEFAULT_MEDIUM_WAIT)

}

static async realTimeSavings() {
   var savingsBefore:string=await DewElement.grabTextFrom(Startup.uiElements.get(QuicksourceAdminLocator.realTimeSavings) as string)
   console.log("++++++++++"+savingsBefore)
   await CommonKeyword.clickElement(Startup.uiElements.get(awardQuickSourceElements.selectCheckBoxSuppliers) as string);
   var savingsAfter:string=await DewElement.grabTextFrom(Startup.uiElements.get(QuicksourceAdminLocator.realTimeSavings) as string)
   console.log("++++++++++"+savingsAfter)
   I.wait(prop.DEFAULT_MEDIUM_WAIT)
}

static async allocateSpecQty() {
   await TextField.enterTextUsingLocator(((Startup.uiElements.get(QuicksourceAdminLocator.specificQty)) as string), "2");
}




}