const { I } = inject();



export let listingPageElements = {
    stageName : "PrimaryAction/stageName", 
    statusName : "PrimaryAction/statusName", 
    primaryAction: "PrimaryAction/primaryAction",
    clickOnEvent: "PrimaryAction/clickOnEvent",
    breadcrumFullSource: "PrimaryAction/breadcrumFullSource",
    reopenEventModalHeader:"PrimaryAction/reopenEventModalHeader",
    awardAction :"PrimaryAction/awardAction",
    stakeHolderComment:"PrimaryAction/stakeHolderComment",
    pauseEventModalHeader:"PrimaryAction/pauseEventModalHeader",
    viewResponsesSpotlightTitle:"PrimaryAction/viewResponsesSpotlightTitle",
    pauseReasonTextArea:"PrimaryAction/pauseReasonTextArea",
    pauseDateTime:"PrimaryAction/pauseDateTime",
    closePauseModal:"PrimaryAction/closePauseModal",
    stakeHolderCommentReopen:"PrimaryAction/stakeHolderCommentReopen",
    supplierCommentReopen:"PrimaryAction/supplierCommentReopen",
    doneButtonOnSuccessfullyPausedPopup:"EventSuccessfullyPausedPopup/DoneButton",
    pauseDateAlert:"PrimaryAction/pauseDateAlert",
    isFullReopenCheckbox:"PrimaryAction/isFullReopen",
    firstSupplierCheckbox:"PrimaryAction/firstSupplier"
}