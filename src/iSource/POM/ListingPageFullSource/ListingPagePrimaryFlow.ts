import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { GenerateRandom } from 'dd-cc-zycus-automation/dist/components/generateRandom';
import { Locators } from './../BayerSideSupplier/ViewAllSupplierLocators';
import { CommonLocators } from './../../isourceCommonFunctions/CommonLocators';
import "codeceptjs"; 
declare const inject: any;
const { I } = inject();
import { DatabaseOperations } from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { COE } from '../../../Framework/FrameworkUtilities/COE/COE';
import { surrogateBidlocator } from '../SurrogateBid/SurrogateBidLocators';
import { stat } from "fs";
import { listingPageElements } from "./ListingPagePrimaryLocators";
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter"; 
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { BidAnalysis } from '../BidAnalysis/BidAnalysisLocators';

export class ListingPagePrimaryFlow {

    static async searchOnGrid(eventType : string)
    {   
        var EventID =  Startup.eventId_Map.get(eventType);
        // var EventID = "1310213627";
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid)  as string);
        I.fillField(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid),EventID);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName) as string,await iSourcelmt.getLabel("EventColumnName") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName) as string,await iSourcelmt.getLabel("EventColumnName") as string));
    }

    static async clickAndEnterEvent(){
        var EventName = (await DatabaseOperations.getTestData()).get('EventName');
        // var EventName = "TEST REMINDER";
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.clickOnEvent) as string, EventName as string));
    }

    static async verifyEventStage(stage : string){
        I.wait(prop.DEFAULT_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.stageName) as string, await iSourcelmt.getLabel(stage) as string));
    }

    static async verifyEventStatus(status : string){
        I.wait(prop.DEFAULT_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.statusName) as string, await iSourcelmt.getLabel(status) as string));
        await I.see(status);
    }

    static async verifyPrimaryAction(primaryAction : string){
        I.wait(prop.DEFAULT_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel(primaryAction) as string));
    }

    static async createDraftAsAction(){
       await iSourcelmt.waitForLoadingSymbolNotDisplayed();
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Create draft")as string));
       await I.waitForText(await iSourcelmt.getLabel("AddSectionLabelKey")as string);
       await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("AddSectionLabelKey")as string));
    }

    static async editDraftAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Edit draft") as string));
         await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("AddSectionLabelKey") as string));
     }
 
    static async scheduleEventAsAction(primaryAction : string){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel(primaryAction) as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.tabXpath) as string, await iSourcelmt.getLabel('Event Details') as string));
    }

    static async viewResponsesAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("View responses") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.see((await iSourcelmt.getLabel("ViewResponsesLabelKey") as string), Startup.uiElements.get(listingPageElements.viewResponsesSpotlightTitle) as string);
    }

    static async pauseEventAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Pause event") as string));
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.pauseEventModalHeader) as string, await iSourcelmt.getLabel("PauseButtonLabel") as string));
        await DewElement.checkIfElementPresent(Startup.uiElements.get(listingPageElements.closePauseModal) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(listingPageElements.closePauseModal) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
    }

    static async awardEventAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Award") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.tabXpath) as string, await iSourcelmt.getLabel('analyzeTab') as string));
    }

    static async viewAwardSummaryAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("View award summary") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.awardAction) as string,await iSourcelmt.getLabel('SupplierAwardSummaryLabelKey') as string));
    }

    static async manageScoresheetsAsAction(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Manage scoresheets") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let tabNameFirstPart : string  = await iSourcelmt.getLabel('ManageScoresheetsLabelKey') as string;
        let tabNameSecondPart : string  = await iSourcelmt.getLabel('ScoreSheetLabelKey') as string;
        let tabName : string  = tabNameFirstPart.split(' ')[0] + ' ' + tabNameSecondPart;
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.tabXpath) as string, tabName));
    }

    static async manageScoresheetsAsAction1(){
        // await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Manage scoresheets") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.primaryAction) as string, await iSourcelmt.getLabel("Evaluation Matrix") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let tabNameFirstPart : string  = await iSourcelmt.getLabel('ManageScoresheetsLabelKey') as string;
        let tabNameSecondPart : string  = await iSourcelmt.getLabel('ScoreSheetLabelKey') as string;
        let tabName : string  = tabNameFirstPart.split(' ')[0] + ' ' + tabNameSecondPart;
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.tabXpath) as string, tabName));
    }

    static async reopenEvent(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ReopenEventLabel") as string);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.reopenEventModalHeader) as string,await iSourcelmt.getLabel("ReopenEventLabel") as string));
        await DatePicker.selectDateAndTime((Startup.uiElements.get(listingPageElements.pauseDateTime) as string),(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(2)));
         I.wait(prop.DEFAULT_WAIT);
         await I.fillField(await Startup.uiElements.get(listingPageElements.supplierCommentReopen) as string, await Startup.testData.get("externalCommnet"));
         await I.fillField(await Startup.uiElements.get(listingPageElements.stakeHolderCommentReopen) as string, await Startup.testData.get("externalCommnet"));
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
    }

    static async reopenEventSingleSupplier(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ReopenEventLabel") as string);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.reopenEventModalHeader) as string,await iSourcelmt.getLabel("ReopenEventLabel") as string));
        await CommonKeyword.clickElement(Startup.uiElements.get(listingPageElements.isFullReopenCheckbox) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(listingPageElements.firstSupplierCheckbox) as string);
        await DatePicker.selectDateAndTime((Startup.uiElements.get(listingPageElements.pauseDateTime) as string),(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(5)));
         I.wait(prop.DEFAULT_WAIT);
         await I.fillField(await Startup.uiElements.get(listingPageElements.supplierCommentReopen) as string, await Startup.testData.get("externalCommnet"));
         await I.fillField(await Startup.uiElements.get(listingPageElements.stakeHolderCommentReopen) as string, await Startup.testData.get("externalCommnet"));
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
    }

    static async rescheduleReopenEvent(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ReopenEventLabel") as string);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.reopenEventModalHeader) as string,await iSourcelmt.getLabel("ReopenEventLabel") as string));
        await DatePicker.selectDateAndTime((Startup.uiElements.get(listingPageElements.pauseDateTime) as string),(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(2)));
         I.wait(prop.DEFAULT_WAIT);
         await I.fillField(await Startup.uiElements.get(listingPageElements.supplierCommentReopen) as string, await Startup.testData.get("externalCommnet"));
         await I.fillField(await Startup.uiElements.get(listingPageElements.stakeHolderCommentReopen) as string, await Startup.testData.get("externalCommnet"));
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
    }

    static async concludeEvent(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("ConcludeEventButtonLabel") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel("ConcludeEventButtonLabel") as string));
        await DewElement.checkIfElementPresent(await Startup.uiElements.get(listingPageElements.supplierCommentReopen) as string);
        await I.fillField(await Startup.uiElements.get(listingPageElements.stakeHolderComment) as string, await Startup.testData.get("externalCommnet"));
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnModalFooterButton((await iSourcelmt.getLabel("ConcludeEventButtonLabel") as string).split(' ')[0]);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
    }

    static async navigateToListingGrid(){
        I.wait(prop.DEFAULT_WAIT);
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.breadcrumFullSource) as string, await iSourcelmt.getLabel('FULLSOURCE_TABNAME') as string));
       await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }

    static async setPauseDateToCurrentDate(SecondaryAction?:string){
        if(!SecondaryAction){
            I.wait(prop.DEFAULT_MEDIUM_WAIT);
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("PauseButtonLabel") as string);
        }
        await DatePicker.selectDateAndTime(Startup.uiElements.get(listingPageElements.pauseDateTime) as string,(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(4)));
        await I.fillField(Startup.uiElements.get(listingPageElements.pauseReasonTextArea) as string,await Startup.testData.get("externalCommnet"));
        await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("PauseButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(listingPageElements.doneButtonOnSuccessfullyPausedPopup) as string,await iSourcelmt.getLabel("DoneLabelKey") as string));
        
        // let dateErrorAlertElement : number = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(listingPageElements.pauseDateAlert) as string);
        // if(dateErrorAlertElement > 0){
        //     await DatePicker.selectDateAndTime(Startup.uiElements.get(listingPageElements.pauseDateTime) as string,(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(2)));
        //     await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("PauseButtonLabel") as string);
        //     iSourcelmt.waitForLoadingSymbolNotDisplayed();
        //     I.wait(prop.DEFAULT_WAIT);
        //     await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
        //     iSourcelmt.waitForLoadingSymbolNotDisplayed();
        //     await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(listingPageElements.doneButtonOnSuccessfullyPausedPopup) as string,await iSourcelmt.getLabel("DoneLabelKey") as string));
        // }else{
            // iSourcelmt.waitForLoadingSymbolNotDisplayed();
            // I.wait(prop.DEFAULT_WAIT);
            // await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
            // iSourcelmt.waitForLoadingSymbolNotDisplayed();
            // await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(listingPageElements.doneButtonOnSuccessfullyPausedPopup) as string,await iSourcelmt.getLabel("DoneLabelKey") as string));
        // }
    }
}
// module.exports = new ListingPagePrimaryRFIFlow();