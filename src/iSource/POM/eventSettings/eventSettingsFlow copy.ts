import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {eventSettingsLocators} from "./eventSettingsLocators";
import {DatePickerFlow} from "../../../Framework/FrameworkUtilities/COE/DatePicker/DatePickerFlow";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {CommonLocators} from "../../isourceCommonFunctions/CommonLocators";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import {DatePickerLocators} from "../../../Framework/FrameworkUtilities/COE/DatePicker/DatePickerLocators";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import{CommonKeyword}  from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { strict } from 'assert';
import { logger } from '../../../Framework/FrameworkUtilities/Logger/logger';

export class eventSettingsFlow{
 static async clickOnEventSettingsTab(){
   I.click(eventSettingsLocators.eventSettingTab);
    
}
static async clickOnEditSettings(){
    await CommonKeyword.clickLabel(await iSourcelmt.getLabel("EditSettingsLabel")as string);
   
   await I.waitForText(await iSourcelmt.getLabel("Event Settings"));
    
}

static async resetCloseDateAndTimeToCurrentDateAndTime(minutesToaddInCurrentMinute : string){
await eventSettingsFlow.clickOnEditSettings();
await eventSettingsFlow.resetCloseDate();
await eventSettingsFlow.resetTime(minutesToaddInCurrentMinute);
await eventSettingsFlow.saveEditSettingsRFx();
await eventSettingsFlow.clickOnSend();
await I.waitForText(await iSourcelmt.getLabel("PauseButtonLabel"));
}
static async resetStartDateAndTimeToCurrentDateAndTime(minutesToaddInCurrentMinute : string){
    await eventSettingsFlow.clickOnEditSettings();
    await eventSettingsFlow.resetStartDate();
    await eventSettingsFlow.resetTime(minutesToaddInCurrentMinute);
    await eventSettingsFlow.saveEditSettingsAuction();
    await I.waitForText(await iSourcelmt.getLabel("PauseButtonLabel"));
}
static async resetStartDate(date ? : string){
   
    if(date){
     await eventSettingsFlow.setStartDate(date);
    }else{
       await eventSettingsFlow.setStartDateToCurrentDate();
    }
    
}
static async resetCloseDateAndTimeToCurrentDateAndTimeauction(minutesToaddInCurrentMinute : string){
    await eventSettingsFlow.clickOnEditSettings();
    await eventSettingsFlow.resetCloseDate();
    await eventSettingsFlow.resetTime(minutesToaddInCurrentMinute);
    await eventSettingsFlow.saveEditSettingsAuction();
    await I.waitForText(await iSourcelmt.getLabel("EditSettingsLabel"));

    }
static async resetTime(minutesToaddInCurrentMinute : string){
   await eventSettingsFlow.setCurrentHour();
    await eventSettingsFlow.setCurrentMinute(minutesToaddInCurrentMinute);
    await eventSettingsFlow.setCurrentMeridiem();
    // await this.clickOK();
}
static async resetCloseDate(date ?: string){
    await eventSettingsFlow.verifyAndClickOnNonpricingTypeTabUnderEventSettings();
    if(date){
     await eventSettingsFlow.setCloseDate(date);
    }else{
       await eventSettingsFlow.setCloseDateToCurrentDate();
    }
}


static async clickOnSend(){
    let element=await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.footerButtonXpath) as string, await iSourcelmt.getLabel("SendButtonLabel")as string);
    await I.seeElement(element);
     await CommonKeyword.clickElement(element);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
     I.wait(prop.DEFAULT_WAIT)
}
static async saveEditSettingsAuction(){
    I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.saveBtnRescheduleEvent) as string, await iSourcelmt.getLabel("SaveButtonLabel")as string));
  await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_WAIT)
    await I.waitForText(await iSourcelmt.getLabel("DoneButtonLabel")as string);
   await CommonKeyword.clickLabel(await iSourcelmt.getLabel("DoneButtonLabel")as string);
    I.wait(prop.DEFAULT_WAIT)
    
  
}
static async saveEditSettingsRFx(){
    I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.saveBtnRescheduleEvent) as string, await iSourcelmt.getLabel("SaveButtonLabel")as string));
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_WAIT)
    I.fillField(Startup.uiElements.get(eventSettingsLocators.externalCommnetTextarea),Startup.testData.get("externalCommnet"));
    I.fillField(Startup.uiElements.get(eventSettingsLocators.internalCommnetTextarea),Startup.testData.get("internalCommnet"));
    I.click(await iSourcelmt.getLabel("DoneButtonLabel"));
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_WAIT)
    
  
}
static async selectPrimaryCurrency(option : string){
await eventSettingsFlow.openPrimaryCurrencyDropDown();
await eventSettingsFlow.selectPrimaryCurrencyoption(option);
await eventSettingsFlow.checkPrimaryCurrencyIsSet(option);
}
static async checkPrimaryCurrencyIsSet(option : string){
    let dropdDownXpath:string= ( await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.PrimaryCurrency_DropDown)as string,await iSourcelmt.getLabel("Primary Currency")as string));


    console.log("option evaluattion  "+(await I.grabAttributeFrom(dropdDownXpath,"value")).includes(option) as string);
    console.log("primary "+await I.grabAttributeFrom(dropdDownXpath,"value")+" option "+option);
    let val : string = await I.grabAttributeFrom(dropdDownXpath,"value");
    console.log("Modified Test Result :   " + val.includes(option) as string)
      if( val.includes(option)){
          logger.info("Primary currency is set as "+option);
      }else{
          throw new Error("Primary currency is not set as expcted.");
      }

// }else{
//     let defaultOption:string=Startup.testData.get("primaryCurrencyOption")as string;
//     if( (await I.grabAttributeFrom(dropdDownXpath,"value")).includes(defaultOption)){
//         logger.info("Primary currency is set as "+option);
//     }else{
//         throw new Error("Primary currency is not set as expcted.");
//     }

// }
}
static async checkEventStatus(status:string){

    I.refreshPage();
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
      I.wait(prop.DEFAULT_WAIT);
    await I.waitForText(await iSourcelmt.getLabel(status));
}
static async openPrimaryCurrencyDropDown(){
   
     let dropdDownXpath:string= ( await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.PrimaryCurrency_DropDown)as string,await iSourcelmt.getLabel("Primary Currency")as string));
     await CommonKeyword.clickElement(dropdDownXpath);
     I.clearField(dropdDownXpath);
     I.fillField(dropdDownXpath," ");
     I.seeElement(Startup.uiElements.get(eventSettingsLocators.dropDownDiv));
}
static async selectPrimaryCurrencyoption(option  : string){
    
    if(option){
    I.scrollIntoView(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.primaryCurrencyOption)as string,option));
    I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.primaryCurrencyOption)as string,option));
    }else{
        let defaultOption:string=Startup.testData.get("primaryCurrencyOption")as string;
        I.scrollIntoView(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.primaryCurrencyOption)as string,defaultOption));
    I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.primaryCurrencyOption)as string,defaultOption));
    }
}
 static async verifyAndClickOnNonpricingTypeTabUnderEventSettings(){
 try {
  I.wait(5);
   var array=await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CommonLocators.tabXpath) as string, await iSourcelmt.getLabel("NonpricingTypeTabLabel")as string));
    console.log("non pricing tab "+array);
     
    if(array==1){
        I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.nonActivePricingTabXpath) as string, await iSourcelmt.getLabel("NonpricingTypeTabLabel")as string));
        I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.nonActivePricingTabXpath) as string, await iSourcelmt.getLabel("NonpricingTypeTabLabel")as string));
       
      }
  
} catch (error) {
    throw error;
 }
}


static async selectMultiCurrencyOptions(option : string){
    eventSettingsFlow.selectMultiCurrencyRadioButton();
let dopdownXpath : string=await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.multiCurrencyDroDownInput) as string, await iSourcelmt.getLabel("SelectCurrencyPlaceholder")as string);
await COE.SelectFromDropDownMultipleCheckBoxValuesProvidexXpath(dopdownXpath,Startup.uiElements.get(eventSettingsLocators.MultiCurrencyOptionXpath) as string,option);
    eventSettingsFlow.selectMultiCurrencyRadioButton();
    eventSettingsFlow.checkCurrencyAdded(option);

}

static async checkCurrencyAdded(option : string){
    let  options : string []=option.split(",");
    I.seeElement(Startup.uiElements.get(eventSettingsLocators.currencyGridXpath));
    I.scrollIntoView(Startup.uiElements.get(eventSettingsLocators.currencyGridXpath));
    options.forEach(async (value) => {
        var element=await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.currencyGridEntries) as string,value);
        I.seeElement(element);
        console.log("currency is added as "+value);
    });
    
}
static async clickOnMulticurrencyDropdown(){
    var element=await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.multiCurrencyDroDownInput) as string,await iSourcelmt.getLabel("SelectCurrencyPlaceholder") as string);
   I.seeElement(element);
   console.log("Multi currency drop down is present.");
   I.click(element);
   I.seeElement(Startup.uiElements.get(eventSettingsLocators.currencyDropDownContainer) as string);
   console.log("Currency drop down is opened succesfully");

}
static async selectMultiCurrencyRadioButton(){
  I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(eventSettingsLocators.muylticurrencyRadioButtonXpath) as string, await iSourcelmt.getLabel("YesLabelKey") as string));
}
setLatencyBetweenLots(hour : string,minute : string){

}
static async setLatencyHour(hour : string){
I.seeElement(Startup.uiElements.get(eventSettingsLocators.latencyHourXpath));
await COE.SelectFromDropDown(Startup.uiElements.get(eventSettingsLocators.latencyHourXpath) as string,hour,"02")
}
static async setCloseDate(userDate : string){
    await eventSettingsFlow.verifyAndClickOnNonpricingTypeTabUnderEventSettings();
    await DatePickerFlow.setDate(Startup.uiElements.get(eventSettingsLocators.closeDate) as string,userDate)
}

static async setCloseDateToCurrentDate(){
   await eventSettingsFlow.verifyAndClickOnNonpricingTypeTabUnderEventSettings();
   console.log("user date "+await eventSettingsFlow.getCurrentDate(0));
      await DatePickerFlow.setDate(Startup.uiElements.get(eventSettingsLocators.closeDate) as string,(await eventSettingsFlow.getCurrentDate(0))); 
    }
static async setStartDateToCurrentDate(){
   
    await DatePickerFlow.setDate(Startup.uiElements.get(eventSettingsLocators.startDatePickerInput) as string,(await eventSettingsFlow.getCurrentDate(0)));
}
static async setStartDate(date : string){
    await DatePickerFlow.setDate(Startup.uiElements.get(eventSettingsLocators.startDatePickerInput) as string,date);
}

static async getCurrentDate(dayToAdd : number){
    const now = new Date();
    now.setDate(now.getDate() + dayToAdd);
    const month_names =["Jan","Feb","Mar",
                          "Apr","May","Jun",
                          "Jul","Aug","Sep",
                          "Oct","Nov","Dec"];
    return `${now.getFullYear()}-${month_names[now.getMonth()]}-${now.getDate()}`;
    //var today:DateConstructor = new Date();
    // Date.prototype.toShortFormat = function() {

    //     const month_names =["Jan","Feb","Mar",
    //                       "Apr","May","Jun",
    //                       "Jul","Aug","Sep",
    //                       "Oct","Nov","Dec"];
        
    //     var day = (this.getDate());
    //     if(dayToAdd){
    //         if(day===30){
    //             day=dayToAdd;
        
    //             }else{
    //                 day=day+dayToAdd;
    //                 if(day>30){
    //                     day=day-29;
    //                 }
    //             }
    //     }
    //     var month_index = this.getMonth();
    //     var year = this.getFullYear();
    //     console.log("final date " + year + "-" + month_names[month_index] + "-" + day);
    //     return "" + year + "-" + month_names[month_index] + "-" + day;
    // }
    
    // Now any Date object can be declared 

    // var indiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Kolkata"});
    //    indiaTime = new Date(indiaTime);
    
    //   var date=indiaTime.toLocaleString().split(",");
    //   console.log("************Current date "+date[0]);
    // var today=date[0];
    // and it can represent itself in the custom format defined above.

    //function getCurrentDate(dayToAdd : number){
       
      }

//    return today.prototype.toShortFormat(); 

static async setCurrentHour(){
    var CurrentHour=  (await eventSettingsFlow.getCurrentTime())[0];
   
    await DatePickerFlow.settHour(CurrentHour);
    }

static async setUserdefinedHour(hour : string){
        
        await DatePickerFlow.settHour(hour);
        }
    
static async setCurrentMinute(plusMinutes : string ){
    var status= await eventSettingsFlow.checkFormat();
    let CurrentMinute:number=parseInt( plusMinutes)+parseInt((await eventSettingsFlow.getCurrentTime())[2]);
    if(CurrentMinute>59){
        CurrentMinute=CurrentMinute-60;
        var CurrentHour= parseInt((await eventSettingsFlow.getCurrentTime())[0]);
        if(CurrentHour==12&&status==12){
            CurrentHour=1;
        }else{
            CurrentHour=CurrentHour+1
        }
        await DatePickerFlow.settHour(CurrentHour.toString())
    }
    await DatePickerFlow.setMinute(CurrentMinute.toString());
    
    }
   static async setCurrentMeridiem(){
        var CurrentMeridiem= (await this.getCurrentTime())[1];
      await  DatePickerFlow.setMeridiem(CurrentMeridiem);
    }
   static async setCurrentTime(plusMinutes : string){
    await eventSettingsFlow.setCurrentHour()
    await eventSettingsFlow.setCurrentMinute(plusMinutes)
    await eventSettingsFlow.setCurrentMeridiem()
    }


    static async timeConvertor(time : string) {
        var PM = time.match('PM') ? true : false
        var array=new Array();
        let timefun : string[] = time.split(':')
        let min : number = parseInt(timefun[1])
        let hour : number;
        if (PM) {
             hour = 12 + parseInt(timefun[0],10)
           
        } else {
             hour = parseInt(timefun[0])
                
        }

        if(hour<10){
            hour=parseInt((0+""+hour).replace(/(\s+)/g,""));
        }
        if(min<10){
            min=parseInt((0+""+min).replace(/(\s+)/g,""));
      }

      if(hour==24){
          hour=12;
      }
        array.push(hour);
       
        array.push((timefun[2].replace(/[0-9]/g,"")).replace(/(\s+)/g,""));
        array.push(min);
        return array
    }
    static async getCurrentTime(){
        var timeArray=new Array();
        var status= await eventSettingsFlow.checkFormat();
        console.log("current format "+status);
        let  indiaTime : string = new Date().toLocaleString("en-US", {timeZone: "Asia/Kolkata"});
       let indiaTime1 : Date = new Date(indiaTime);
       var date=indiaTime1.toLocaleString().split(",");
      
     
     if(status==24){
     timeArray=await (eventSettingsFlow.timeConvertor(date[1]));
     console.log("inside 24 hour frmat if")
      }

    else if(status==12){
      var time=date[1].split(":");
      
      if(parseInt(time[0])<10){
        time[0]=(0+""+time[0]).replace(/(\s+)/g,"");
      }
      if(parseInt(time[1])<10){
        time[1]=(0+""+time[1]).replace(/(\s+)/g,"");
    }
              timeArray.push(time[0]);
               timeArray.push(time[2].split(" ")[1]);
               timeArray.push(time[1]);
               console.log("inside 12 hour frmat if")
       
       }

       console.log("************Current hour "+timeArray[0]);
      console.log("************Current minute"+timeArray[1]);
      console.log("************Current meridiem"+timeArray[2]);
       return timeArray;
    }
   static async getCurrentTimeOld(){
       var status= await eventSettingsFlow.checkFormat();
       var time = new Date();
       var timeArray=new Array();
       var split=  time.toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(/(\s+)/);
      if(status==12){
         
         timeArray.push( split[0]);
         timeArray.push( split[2]);
         timeArray.push( time.getMinutes());
      
      }else if(status==24){
       timeArray.push(time.getHours());
         timeArray.push( split[2]);
         timeArray.push( time.getMinutes());
       
      
      }
      console.log("************Currentb date picker staryus"+timeArray[0]);
      console.log("************Currentb date picker staryus"+timeArray[1]);
      console.log("************Currentb date picker staryus"+timeArray[2]);
      return timeArray;
    }
    static async checkFormat(){
        var count=await DewElement.grabNumberOfVisibleElements(Startup.commonuiElements.get(DatePickerLocators.meridiemDropdown) as string);
        console.log("merididum******** "+count);
        if(count==1){
            return 12;
        }else{
            return 24;
        }
    }


}
 

// module.exports = new eventSettingsFlow();