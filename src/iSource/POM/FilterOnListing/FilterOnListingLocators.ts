export let FilterOnListingLocators = { 
    OwnerIndex:'FilterOnListing/OwnerIndex',
    Owner : 'FilterOnListing/Owner',
    TypeIndex : 'FilterOnListing/TypeIndex',
    Type : 'FilterOnListing/Type',
    SearchCheckBox:'FilterOnListing/SearchCheckBox',
    VerifyCheckBox:'FilterOnListing/VerifyCheckBox',
    Header:'FilterOnListing/Header',
    ClearAll:"FilterOnListing/ClearAll",
    VerifyFilter:"FilterOnListing/VerifyFilter",
    Fullsource:'FilterOnListing/Fullsource',
    QuickSourceTab:"FilterOnListing/QuickSourceTab",
    FullSourceTab:'FilterOnListing/FullSourceTab',
    FilterDraftInProgress:'FilterOnListing/FilterDraftInProgress'
}
