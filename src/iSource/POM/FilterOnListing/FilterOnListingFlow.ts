import { iSourcelmt } from './../../../Framework/FrameworkUtilities/i18nutil/readI18NProp';
import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { Filter } from "dd-cc-zycus-automation/dist/components/filter";
import { DateFilter } from "dd-cc-zycus-automation/dist/components/dateFilter";
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';

import { CommonFunctions } from '../../isourceCommonFunctions/CommonFunctions';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { Checkbox } from 'dd-cc-zycus-automation/dist/components/checkbox';
import { FilterOnListingLocators } from './FilterOnListingLocators';
import { logger } from '../../../Framework/FrameworkUtilities/Logger/logger';
export class FilterOnListingFlow {



    static async ClickOnfullsource(){
    I.wait(prop.DEFAULT_WAIT)
  // await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.Fullsource) as string, await iSourcelmt.getLabel('FULLSOURCE_TABNAME') as string) as string);
  await CommonKeyword.clickLabel(await iSourcelmt.getLabel('FULLSOURCE_TABNAME') as string);
  iSourcelmt.waitForLoadingSymbolNotDisplayed(); 
  I.wait(prop.DEFAULT_WAIT)

    }
    // =========Other column=========

    static async filterOnGrid(columnName: string) {
        I.wait(prop.DEFAULT_WAIT)
        // await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await Filter.applyFilter(await iSourcelmt.getLabel(columnName) as string);
        I.wait(prop.DEFAULT_WAIT)
    }

    static async checkboxSearch(columnName: string, dataToFilter: string) {
        I.wait(prop.DEFAULT_WAIT)
        //await I.fillField(".//dew-checklist//input[@placeholder[normalize-space()='Search']]", dataToFilter);
        //var element = await DewElement.grabNumberOfVisibleElements("//dew-checkbox//label[contains(@class,'custom-control')]");
        await I.fillField(Startup.uiElements.get(FilterOnListingLocators.SearchCheckBox), dataToFilter);
        var element = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(FilterOnListingLocators.VerifyCheckBox)as string);

        if (element > 1) {
            await Checkbox.searchSelect(columnName, dataToFilter);
        } else {
            await I.refreshPage();
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.wait(prop.DEFAULT_WAIT)
        }
    }

    static async verifyOwner() {
        I.wait(prop.DEFAULT_LOW_WAIT)
        let totalOwner = await DewElement.getNumberOfElementsPresentInDom(await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.Owner) as string, await iSourcelmt.getLabel("OwnerLabel") as string));
        console.log(totalOwner);
        let owner = Startup.testData.get("Owner_Name") as string;
        console.log(owner);
        for (let index = 1; index <= totalOwner; index++) {
            let entry = (await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.OwnerIndex) as string, await iSourcelmt.getLabel("OwnerLabel") as string)).replace("<<index>>", "" + index);
            if ((await I.grabTextFrom(entry)).includes(owner)) {
                console.log(entry);
                await FilterOnListingFlow.totalEventCount();

            }
        }
    }


    static async verifyType(dataToFilter: string) {
        I.wait(prop.DEFAULT_LOW_WAIT)
        let totaltype = await DewElement.getNumberOfElementsPresentInDom(await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.Type) as string, await iSourcelmt.getLabel("TypeLabel") as string));
        console.log(totaltype);

        for (let index = 1; index <= totaltype; index++) {
            let entry = (await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.TypeIndex) as string, await iSourcelmt.getLabel("TypeLabel") as string)).replace("<<index>>", "" + index);
            if ((await I.grabTextFrom(entry)).includes(dataToFilter)) {
                console.log(entry);
                await FilterOnListingFlow.totalEventCount();

            }
        }
    }

    static async verifyStatus(dataToFilter: string) {
        I.wait(prop.DEFAULT_LOW_WAIT)
        let totaltype = await DewElement.getNumberOfElementsPresentInDom(await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.Owner) as string, await iSourcelmt.getLabel("Stage-StatusLabel") as string));
        console.log(totaltype);

        for (let index = 1; index <= totaltype; index++) {
            let entry = (await CommonFunctions.getFinalLocator(Startup.uiElements.get(FilterOnListingLocators.OwnerIndex) as string, await iSourcelmt.getLabel("Stage-StatusLabel") as string)).replace("<<index>>", "" + index);
            if ((await I.grabTextFrom(entry)).includes(dataToFilter)) {
                console.log(entry);
                await FilterOnListingFlow.totalEventCount();

            }
        }
    }

    static async verifyOnDateFilter(dataToFilter: string) {
        I.wait(prop.DEFAULT_LOW_WAIT)
        if ((await I.grabTextFrom(Startup.uiElements.get(FilterOnListingLocators.VerifyFilter) as string)).includes(dataToFilter)) {
            await FilterOnListingFlow.totalEventCount();
            console.log("xyz")
        }
    }



    static async totalEventCount() {
        let AllSourcingEvent = await I.grabTextFrom(Startup.uiElements.get(FilterOnListingLocators.Header) as string)
        let x = await iSourcelmt.getLabel("AllSourcingEventLabel") as string;
        let totalFilteredEventCount = AllSourcingEvent.replace(x, "");
        console.log(totalFilteredEventCount + " Total Filtered Event count")
    }

    static async clearFilter() {
        var element = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(FilterOnListingLocators.ClearAll) as string);
        console.log(element)
        if (element > 0) {
            console.log("Clear Filter")
            await CommonKeyword.clickElement(Startup.uiElements.get(FilterOnListingLocators.ClearAll) as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed()
            I.wait(prop.DEFAULT_LOW_WAIT)
        }

    }


    // =========Date=========

    static async filterOnGridCreateDateInDate(columnName: string, dataToFilter: string) {
        await Filter.applyFilter(columnName);
        await DateFilter.selectCreateDate(dataToFilter)
        await DewButton.click(await iSourcelmt.getLabel("Apply") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    }

    // =========DateWithin=========

    static async filterOnGridCreateDateInDateWithin(columnName: string, dataToFilter: string) {
        await Filter.applyFilter(columnName);
        await DateFilter.selectDateWithin(dataToFilter)
        await DewButton.click(await iSourcelmt.getLabel("Apply") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
    }


    // =========DatePeriod=========

    static async filterOnGridCreateDateInDatePeriod(columnName: string, fromDate: string, toDate: string) {
        await Filter.applyFilter(columnName);
        await DateFilter.selectDatePeriod(fromDate, toDate);
        await DewButton.click(await iSourcelmt.getLabel("Apply") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_WAIT);
    }

    static async toggleBetweenTabs(){
        await CommonKeyword.clickElement(Startup.uiElements.get(FilterOnListingLocators.QuickSourceTab)as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(FilterOnListingLocators.FullSourceTab)as string);
    }

    static async verifyFilterRetain(){
        let filterText = await DewElement.grabTextFrom(Startup.uiElements.get(FilterOnListingLocators.FilterDraftInProgress)as string)
        // logger.info('=============>',filterText);
        if(filterText === 'Status: Draft In Progress'){
            logger.info('==========>Filters are Retained')
        }else{
            logger.info('==========>Filters are Not Retained')
        }
    }

    
}

