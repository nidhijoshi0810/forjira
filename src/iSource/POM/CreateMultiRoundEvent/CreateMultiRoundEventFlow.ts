import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { coe } from "../../../Framework/FrameworkUtilities/COE/COE";
import {CommonFunctions} from "../../../iSource/isourceCommonFunctions/CommonFunctions";
// import { lmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { createMultiRoundElements } from "./MultiRoundLocator";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { Filter} from "dd-cc-zycus-automation/dist/components/filter";
import { Checkbox} from "dd-cc-zycus-automation/dist/components/checkbox";
import { DewCheckbox} from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { SecondaryAction} from "dd-cc-zycus-automation/dist/components/SecondaryAction";

export class MultiRoundsFlow {

    static async createEvent() {
        I.wait(prop.DEFAULT_LOW_WAIT);
        await I.fillField(Startup.uiElements.get(createMultiRoundElements.eventName) as string, Startup.testData.get("eventName") as string);
        await TextArea.enterTextUsingLocator(Startup.uiElements.get(createMultiRoundElements.eventDesc) as string, Startup.testData.get("eventDesc") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.clickRFIEvent) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.clickTestEvent) as string);
       
        await MultiRoundsFlow.clickNext();
        
    }

    static async attachmentPage(){
       I.seeElement(Startup.uiElements.get(createMultiRoundElements.attachmentLabel) as string);
        // I.seeElement("(//div[contains(text(),'Attachments (')])[1]");
    }

    static async selectAttachmentPage(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // await CommonKeyword.clickElement("//dew-col[@class='px-0 rounded-top col-2']//dew-checkbox[@class='mb-2 ng-untouched ng-pristine ng-valid']");
       
        await Checkbox.iSCheckBoxSelected(Startup.uiElements.get(createMultiRoundElements.checkAttachment) as string);
        
    }

    static async sectionPage(){
        I.seeElement(Startup.uiElements.get(createMultiRoundElements.sectionLabel) as string);
        // I.seeElement("//div[contains(text(),'Section (')]");
 
    }

    static async selectNoOfSection(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // await CommonKeyword.clickElement("//dew-col[@class='px-0 rounded-top first-block col-1']//dew-row[@class='list-head mx-0 row']//dew-checkbox[@class='mb-2 ng-untouched ng-pristine ng-valid']");
       
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.selectSectionCheckBox) as string);
    
        I.wait(prop.DEFAULT_LOW_WAIT);
        await MultiRoundsFlow.clickNext();
        
    }

    static async TnCAttchPage(){
        // I.seeElement("(//div[contains(text(),'Attachments (')])[2]");
        I.seeElement(Startup.uiElements.get(createMultiRoundElements.tncAttachLabel) as string);
    }

    static async selectTnCAttch(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // await CommonKeyword.clickElement("//dew-tab[@id='taba3']//dew-row[@class='list-head mx-0 row']//dew-checkbox[@class='mb-2 ng-untouched ng-pristine ng-valid']");
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.selectTnCAttchCheckBox) as string);
        await MultiRoundsFlow.clickNext();
        I.wait(prop.DEFAULT_LOW_WAIT);    
    }

    static async supplierPage(){
        // I.seeElement("(//div[contains(text(),'Suppliers (')])[1]");
        I.seeElement(Startup.uiElements.get(createMultiRoundElements.supplierPageLabel) as string);
    }

    static async selectSupplierPage(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // await CommonKeyword.clickElement("//dew-col[@class='align-items-center rounded-top pl-3 pr-4 col']//dew-checkbox[@class='au--l-checkbox ng-untouched ng-pristine ng-valid']");
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.selectSupplierResponseCheckBox) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await MultiRoundsFlow.sendRegretMail();
        I.wait(prop.DEFAULT_LOW_WAIT);
        await MultiRoundsFlow.clickNext();
        I.wait(prop.DEFAULT_LOW_WAIT);
        // await MultiRoundsFlow.clickFinish();
        I.waitForLoadingSymbolNotDisplayed();
    }

    static async sendRegretMail(){
        // await CommonKeyword.clickElement("//dew-checkbox[@class='mt-3 ng-untouched ng-pristine ng-valid']");
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.selectSendRegretMailCheckBox) as string);
        
    }

    // static async scoreSheetPage(){
    //     I.seeElement("(//div[contains(text(),'Scoresheets (')])[1]");
    // }
    // static async selectScoreSheetPage(){
    //     // I.wait(prop.DEFAULT_LOW_WAIT);
    //     // await CommonKeyword.clickElement("(//dew-col[@class='align-items-center rounded-top col']//dew-checkbox)[4]");
    //     // await CommonKeyword.clickElement("//dew-col[@class='align-items-center rounded-top col']//dew-checkbox[@class='mb-2 ng-untouched ng-pristine ng-valid']");
    //     // await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.selectScoreSheetToCarryForward) as string);
    //     I.wait(prop.DEFAULT_LOW_WAIT);
    //     await MultiRoundsFlow.clickFinish();
    // }

    static async verifyMultiRoundEventCreated(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // I.seeElement("//div[contains(text(),'Basic details')]");
        I.seeElement(Startup.uiElements.get(createMultiRoundElements.verifyMultiroundCreateLabel) as string);
    }

    static async verifyNumberOfSection(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // I.seeElement("//p[contains(text(),'Sections')]");
        I.seeElement(Startup.uiElements.get(createMultiRoundElements.NumberofSectionLabel) as string);
    }

    static async verifyAttchCarryForward(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        // I.seeElement("//div[contains(text(),'Terms & Conditions')]");
        I.seeElement(Startup.uiElements.get(createMultiRoundElements.attchCarryForwardLabel) as string);
    }

    static async clickNext(){
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.clickNextButton) as string);
        I.wait(10);
    }

    static async clickFinish(){
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.clickFinishButton) as string);
        // await CommonKeyword.clickElement("//span[contains(text(),'Finish')]");
    }  

    static async clickCancelIcon(){
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.clickCancelIcon) as string);
    }
    
    static async clickOnAnalyzeTab(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(createMultiRoundElements.NavigateToAnalyze) as string);
    }
    
}