import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import "codeceptjs"; 
declare const inject: any;
const { I } = inject ();
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { eventSettingsFlow } from "./../../POM/eventSettings/eventSettingsFlow";
// import { DatePickerFlow } from "./../../../Framework/FrameworkUtilities/COE/DatePicker/DatePickerFlow";
import { sectionAction } from "./../../POM/SupplierResponse/SupplierResponseAction";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker"
import { prop } from '../../../Framework/FrameworkUtilities/config';
import { commonLandingFlow } from '../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { iSourceDDHome } from './DDHomeLocators';
import { logger } from '../../../Framework/FrameworkUtilities/Logger/logger';
import { DewDropdown } from 'dd-cc-zycus-automation/dist/components/dewDropdown';
import { ModalPopUp } from 'dd-cc-zycus-automation/dist/components/modalPopUp';
import { Footer } from 'dd-cc-zycus-automation/dist/components/footer';
import { DewCheckbox } from 'dd-cc-zycus-automation/dist/components/dewCheckbox';

export class DDHomeIsource{
 static async DDHomeSearch(dataToSearch:string){
  await I.fillField( await Startup.uiElements.get(iSourceDDHome.SearchBar) as string, dataToSearch);
  I.wait(3);
 } 

 static async verifyText(textToVerify : string){
    await DewElement.verifyIfISeeText(await iSourcelmt.getLabel(textToVerify) as string);
 }

 static async clickSourcingEvent(){
     await CommonKeyword.clickElement(await Startup.uiElements.get(iSourceDDHome.SourcingEvent) as string);
 }

 static async seeAllSourcingEvents(){
    await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("AllSourcingEventLabel") as string);
 }

 static async iSourceTabSetNavigation(tabsetName : string){
    // await CommonKeyword.clickElement("//dew-tabset//dew-default-tab-head[contains(text(),'Full Source Events')]");
    // await CommonKeyword.clickElement("//dew-tabset//dew-default-tab-head[contains(text(),'Quick Source Events')]");
    // let isiLogixVisible = await I.grabNumberOfVisibleElements("//dew-tabset//dew-default-tab-head[contains(text(),'iLogix')]");
    // logger.info('isiLogixVisible ::' + isiLogixVisible)
    // if(isiLogixVisible>0){
    //     await CommonKeyword.clickElement("//dew-tabset//dew-default-tab-head[contains(text(),'iLogix')]");
    // }
    let istabsetNameVisible = await I.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(iSourceDDHome.Tabset) as string,
                                                                                                        await iSourcelmt.getLabel(tabsetName) as string));
    logger.info('No of Visible' + istabsetNameVisible + 'elements ::' + istabsetNameVisible)
    if(istabsetNameVisible>0){
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(await Startup.uiElements.get(iSourceDDHome.Tabset) as string,
                                                                                await iSourcelmt.getLabel(tabsetName) as string)); 
        await I.wait(5);
    }
 }

 static async clickQuicklinkAndNavigateCustomizePopup(){
     await DewDropdown.selectByText( await Startup.uiElements.get(iSourceDDHome.QuickLink) as string,await iSourcelmt.getLabel('Customize') as string);
 }

 static async addFullSourceToQuickLink(){
    await Footer.clickModalFooterButton(await iSourcelmt.getLabel('ResetAll') as string);
    await DewCheckbox.selectCheckbox(await iSourcelmt.getLabel('FULLSOURCE_TABNAME') as string);
    await Footer.clickModalFooterButton(await iSourcelmt.getLabel('ApplyLabelKey') as string);
 }
 
 static async NavigateUsingQuickLink(){
    // await DewDropdown.selectByText(await Startup.uiElements.get(iSourceDDHome.QuickLink) as string,'Full Source (iSource)');
    await DewDropdown.selectFirst(await Startup.uiElements.get(iSourceDDHome.QuickLink) as string);
 } 
}


// module.exports=new BidReco();