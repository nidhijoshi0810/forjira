export let ExportLocators = {
    SupplierNameCheckBox:"ManageSupplier/SpplierNameCheckbox",
    ExportButton:"ManageSupplier/Export_Button",
    AllExportOption:"ManageSupplier/AllExportOption",
    exportOptionLength:"ManageSupplier/AllExportOptionLength",
    ViewResponseButton:"ManageSupplier/ViewResponseButton",
    ResponseReceived:"ManageSupplier/ResponseReceived",
    QuestionSectionVerify:"ManageSupplier/QuestionSectionVerify",
    ViewResponseTableButton:"ManageSupplier/ViewResponseTableButton",
    VerifyFullTableData:"ManageSupplier/VerifyFullTableData"
}