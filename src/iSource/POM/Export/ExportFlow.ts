import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import {DatabaseOperations} from "../../../Framework/FrameworkUtilities/DatabaseOperations/databaseOperations";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
//const dbKeys = require("../../../Framework/FrameworkUtilities/DatabaseOperations/dbKeys");
//const elements = require("./SurrogateBidLocators");
import {ExportLocators} from"./ExportLocators";
//const CreateEventFlow = require("../../POM/CreateEvent/CreateEventFlow");
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
export class ExportFlow{

    static async selectCheckbox(){
        await CommonKeyword.clickElement(Startup.uiElements.get(ExportLocators.SupplierNameCheckBox) as string);
        // I.click(Startup.uiElements.get(ExportLocators.SupplierNameCheckBox));
        logger.info("clicked on checkbox");
        // var checkOption=new Array(await I.grabNumberOfVisibleElements(Startup.uiElements.get(ExportLocators.SupplierNameCheckBox)));
        // for(let i=2;i<=checkOption.length;i++){
        //     I.click(locate(Startup.uiElements.get(ExportLocators.SupplierNameCheckBox)).at(i));
        // }
    }

   static async clickOnExport(){
       logger.info("Export Butoon Clicked")
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ExportButton)as string,Startup.testData.get("ExportButton")as string));
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ExportButton)as string,Startup.testData.get("ExportButton")as string));
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        var exportOptions = new Array(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(ExportLocators.exportOptionLength) as string));
       // logger.info("dhgggggggggggggggggggg"+exportOptions.length)
        for(let i=1;i<=exportOptions.length; i++){
           
            //logger.info("djgfffffffffffffffffffffffffffffffff22222222222222"+exportOptions.length);
            I.click(locate(Startup.uiElements.get(ExportLocators.exportOptionLength) as string).at(i));
            if(exportOptions.length<=6){
                await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ExportButton)as string,Startup.testData.get("ExportButton")as string));
                // I.click(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ExportButton)as string,Startup.testData.get("ExportButton")as string));
            }  
        }
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        logger.info("For Loop Doneeeeeeeeeeeee");
        
    }
    static async clickOnViewResponse(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ViewResponseButton)as string,await iSourcelmt.getLabel("ViewResponseButton")as string));
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ViewResponseButton)as string,await iSourcelmt.getLabel("ViewResponseButton")as string));
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }
    static async verifyResponseReceived()
    {
        logger.info("ResponseReceived    "+await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ResponseReceived)as string, await iSourcelmt.getLabel("ResponseReceivedStatus")as string))
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.ResponseReceived)as string, await iSourcelmt.getLabel("ResponseReceivedStatus")as string));
        // logger.info("Response Received Status===============>"+iSourcelmt.getLabel("ResponseReceived"))
    }
    static async verifyViewResponseForRFI(){
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.QuestionSectionVerify)as string, await iSourcelmt.getLabel("QuestionsText")as string))
    }
    static async verifyViewResponseForNonRFI()
    {
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.QuestionSectionVerify)as string, await iSourcelmt.getLabel("QuestionsText")as string));
        I.scrollIntoView(Startup.uiElements.get(ExportLocators.ViewResponseTableButton));
        await CommonKeyword.clickElement(Startup.uiElements.get(ExportLocators.ViewResponseTableButton) as string);
        // I.click(Startup.uiElements.get(ExportLocators.ViewResponseTableButton));
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(ExportLocators.VerifyFullTableData)as string , await iSourcelmt.getLabel("ItemInformation")as string));
    }


}
// module.exports = new ExportFlow();