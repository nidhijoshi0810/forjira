
declare const inject: any; 
const { I } = inject();

import {iSourcelmt} from "../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../../Framework/FrameworkUtilities/config";
import { editEventDetailsLocator } from "./editEventDetailsLocator";
import { CommonFunctions } from "../../../isourceCommonFunctions/CommonFunctions";
import { Filter} from "dd-cc-zycus-automation/dist/components/filter"
import { Checkbox} from "dd-cc-zycus-automation/dist/components/checkbox"
import { SecondaryAction} from "dd-cc-zycus-automation/dist/components/SecondaryAction"
import { createEventElements } from "../../CreateEvent/CreateEventLocator";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { TextArea } from 'dd-cc-zycus-automation/dist/components/textArea';

export class editEventDetailsFlow{
        static async secondaryAction(ButtonName : string) {
        SecondaryAction.clickSecondaryActions(await iSourcelmt.getLabel("AllSourcingEventLabel") as string, await iSourcelmt.getLabel( ButtonName) as string );
    }

    

    // stage

    static async editDetails(){
        I.fillField(await Startup.uiElements.get(createEventElements.EventName),'eventNameEdited&**(%$_kp');
        // await TextArea.enterTextWithPlaceHolder(await iSourcelmt.getLabel('Event Name')as string, 'eventNameEdited&**(%$_kp') 
        I.wait(prop.DEFAULT_WAIT);
        
        
    }

    static async checkChanges(){
        I.wait(prop.DEFAULT_WAIT);
        
        // await I.waitForText('eventNameEdited&**(%$_kp');
        // await DewElement.verifyIfISeeText('eventNameEdited&**(%$_kp');
    }

    static async applyingFilter(eventType : string,eventStatus : string) {
        await Filter.applyFilter(await iSourcelmt.getLabel( "OwnerLabel" ) as string);
        await Checkbox.selectAll(await iSourcelmt.getLabel( "OwnerLabel" ) as string);
        await Filter.applyFilter(await iSourcelmt.getLabel( "TypeLabel" ) as string);
        await Checkbox.searchSelect(await iSourcelmt.getLabel( "TypeLabel" ) as string,await iSourcelmt.getLabel( eventType ) as string );
        await Filter.applyFilter(await iSourcelmt.getLabel( "StatusLabel" ) as string);
        await Checkbox.searchSelect(await iSourcelmt.getLabel( "StatusLabel" ) as string,await iSourcelmt.getLabel( eventStatus ) as string);
    }

}
