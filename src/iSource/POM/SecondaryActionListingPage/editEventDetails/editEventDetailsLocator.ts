export let editEventDetailsLocator = {
    SecondaryActionButton : "ListingPage/SecondaryActionButton",
    clickButton : "ListingPage/SecondaryAction/clickButton",
    filterIcon : "ListingPage/filterIconClick",
    SearchInFilter : "ListingPage/filterIcon/SearchBar",
    clearAllButton : "ListingPage/filterIcon/clearAllButton",
    applyButton : "ListingPage/filterIcon/applyButton",
    eventType : "ListingPage/filterIcon/eventType",
    eventStatus : "ListingPage/filterIcon/eventStatus",
    checkLabel : "ListingPage/filterIcon/checkLabel",
    BackArrow : "ISource/BackArrow",
    FilterIcon : "CommonComponent/Grid Component/Filter icon OR/FilterIcon",
    saveAsDraft: "ISource/createEventPage/saveAsDraft"
}

