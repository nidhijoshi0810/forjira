
declare const inject: any; 
const { I } = inject();
import { SecondaryAction} from "dd-cc-zycus-automation/dist/components/SecondaryAction"
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { ViewAuctionSummaryLocator } from "./ViewAuctionSummaryLocator";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { BidAnalysis } from "../BidAnalysis/BidAnalysisLocators";
import { AwardWordFlow } from "../AwardWorkFlow/AwardWorkFlow";
import { BidAnalysisFlow } from "../BidAnalysis/BidAnalysisFlow";

export class ViewAuctionSummaryFlow{
    
    static async ViewAuctionSummaryCheck() {

        await DewElement.checkIfElementPresent(await Startup.uiElements.get(ViewAuctionSummaryLocator.bidDetailsLabel) as string);

    }
    static async performSecondaryAction(actionType:string){
        await SecondaryAction.clickSecondaryActions(await iSourcelmt.getLabel("AllSourcingEventLabel") as string,actionType);
    }
    static async clickOnYes(){
        await I.wait(prop.DEFAULT_LOW_WAIT)
        await I.waitForText(Startup.testData.get("YesButton"));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.YesPopUpButton) as string,await iSourcelmt.getLabel("YesLabelKey") as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async awardEvent(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.awardButtonXpath)as string, await iSourcelmt.getLabel("awardButtonLabel")as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await AwardWordFlow.awardEventUsingWorkFlow();
        await ViewAuctionSummaryFlow.clickOnYes();
       await BidAnalysisFlow.navigateToCommentsAndAttachmentsSection();
       await BidAnalysisFlow.enterAwardingComments();
       await BidAnalysisFlow.attachFileInAwarding();
       await BidAnalysisFlow.awardAndSend();
    }
}
