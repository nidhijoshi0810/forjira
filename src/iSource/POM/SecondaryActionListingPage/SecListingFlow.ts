
declare const inject: any; 
const { I } = inject();

import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { SecListingLocator } from "./SecListingLocator";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { Filter} from "dd-cc-zycus-automation/dist/components/filter"
import { Checkbox} from "dd-cc-zycus-automation/dist/components/checkbox"
import { SecondaryAction} from "dd-cc-zycus-automation/dist/components/SecondaryAction"
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter"
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword"
import { listingPageElements } from "../ListingPageFullSource/ListingPagePrimaryLocators";
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker";
import { Locators } from './../BayerSideSupplier/ViewAllSupplierLocators';
import { ListingPagePrimaryFlow } from './../../POM/ListingPageFullSource/ListingPagePrimaryFlow';
import {AwardWordFlow} from "../../POM/AwardWorkFlow/AwardWorkFlow";
import { BidAnalysisFlow } from "../../POM/BidAnalysis/BidAnalysisFlow";
import { BidAnalysis } from "../../POM/BidAnalysis/BidAnalysisLocators";
import { ViewAllSuppliersSteps } from "../../POM/BayerSideSupplier/ViewAllSupplierFlow";

export class SecListingFlow{
    static async secondaryAction(ButtonName : string) {
        await SecondaryAction.clickSecondaryActions(await iSourcelmt.getLabel("AllSourcingEventLabel") as string, await iSourcelmt.getLabel(ButtonName) as string );
    }

    static async applyingFilter(eventType : string,eventStatus : string) {
        await Filter.applyFilter(await iSourcelmt.getLabel( "OwnerLabel" ) as string);
        await Checkbox.selectAll(await iSourcelmt.getLabel( "OwnerLabel" ) as string);
        await Filter.applyFilter(await iSourcelmt.getLabel( "TypeLabel" ) as string);
        await Checkbox.searchSelect(await iSourcelmt.getLabel( "TypeLabel" ) as string,await iSourcelmt.getLabel( eventType ) as string );
        await Filter.applyFilter(await iSourcelmt.getLabel( "StatusLabel" ) as string);
        await Checkbox.searchSelect(await iSourcelmt.getLabel( "StatusLabel" ) as string,await iSourcelmt.getLabel( eventStatus ) as string);
    }

    static async searchEvent(eventID:string){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events")as string,eventID,await iSourcelmt.getLabel("EventColumnName")as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        
      }

      
      static async FullSourceListingPage(){
        DewElement.verifyIfISeeText("AllSourcingEventLabel");
    }

    static async CreateCopyCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let cName:string=await I.grabValueFrom('//input[@placeholder="Type here…"]');
        console.log(cName);
    }

    static async ViewActivityCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("EventSummary") as string);
    }

    static async MultiroundCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "Create a new Round" ) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("Cancel") as string);
    }

    static async ConcludeCheck(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "AnalyzeTab" ) as string);
    }

    static async AnalysisClick(){
       
       await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "AnalysisLabelKey" ) as string);
    }

    static async ConcludeClick(){
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ConcludeEventButtonLabel")as string);
    }

    static async ConcludeComments(){
       await I.fillField(Startup.uiElements.get(SecListingLocator.ConcludeComments),"Automation Comments");
        await I.fillField(Startup.uiElements.get(SecListingLocator.ConcludeComments1),"Automation Comments1");
    }

    static async ConcludeBtnClick(){
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(SecListingLocator.ConcludeBtn) as string, await iSourcelmt.getLabel("Conclude")as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey")as string);
    }

    static async BackToManageSuppBackArr(){
        await CommonKeyword.clickElement(Startup.uiElements.get(SecListingLocator.BackArrow) as string);
    }

    static async ReopenEvent11(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ReopenEventLabel") as string);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(listingPageElements.reopenEventModalHeader) as string,await iSourcelmt.getLabel("ReopenEventLabel") as string));
        await DatePicker.selectDateAndTime((Startup.uiElements.get(listingPageElements.pauseDateTime) as string),(await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(9)));
         I.wait(prop.DEFAULT_WAIT);
         await I.fillField(await Startup.uiElements.get(listingPageElements.supplierCommentReopen) as string, await Startup.testData.get("externalCommnet"));
         await I.fillField(await Startup.uiElements.get(listingPageElements.stakeHolderCommentReopen) as string, await Startup.testData.get("externalCommnet"));
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
        
    }

    static async AwardingActions(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(BidAnalysis.awardButtonXpath)as string, await iSourcelmt.getLabel("awardButtonLabel")as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        // I.see( await iSourcelmt.getLabel("EventNotAwardedLabelKey"));
        await AwardWordFlow.awardEventUsingWorkFlow();
       await  await BidAnalysisFlow.navigateToCommentsAndAttachmentsSection();
       await BidAnalysisFlow.enterAwardingComments();
       await BidAnalysisFlow.attachFileInAwarding();
       await BidAnalysisFlow.awardAndSend();
    
    }



    static async selectExtraSuppliers(supplierName ?: string){
        let supplierColmnLabel=await iSourcelmt.getLabel("supplierColumnName");
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
        I.wait(prop.DEFAULT_HIGH_WAIT)
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,Startup.testData.get("SuppExtra") as string,supplierColmnLabel as string);
        I.wait(prop.DEFAULT_HIGH_WAIT)
        await ViewAllSuppliersSteps.selectAllSupplierCheckbox();
        await ViewAllSuppliersSteps.clickOnAddSupplierButton();
      }


    static async UnfreezeEvent(){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('unfreezeLabelKey') as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }

    

    static async RepublishButtonClick(){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('RepublishEventButtonLabel') as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(SecListingLocator.RepublishBtnOnPopup) as string, await iSourcelmt.getLabel("RepublishLabelKey")as string));
        // await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('RepublishLabelKey') as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
    
    }

    static async viewLotDetails(){
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('View Lot Details') as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
    }

    static async viewLotDetailsVerify(){
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "ItemTabKey" ) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
    }

    static async viewLotSettings(){
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('View Lot Settings') as string);
        I.wait(prop.DEFAULT_WAIT);
    }

    static async viewLotSettingsVerify(){
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "Start Date" ) as string);
        I.wait(prop.DEFAULT_WAIT);
    }

    static async closeDefineSettingsPopup(){
        I.wait(prop.DEFAULT_WAIT);
        await I.pressKey(`Escape`);  
    }

    static async restartBidRecoButton(){
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('Restart Bid Reconciliation') as string);
        I.wait(prop.DEFAULT_WAIT);
    }


}
