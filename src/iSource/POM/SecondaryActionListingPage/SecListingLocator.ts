export let SecListingLocator = {
    SecondaryActionButton : "ListingPage/SecondaryActionButton",
    clickButton : "ListingPage/SecondaryAction/clickButton",
    filterIcon : "ListingPage/filterIconClick",
    SearchInFilter : "ListingPage/filterIcon/SearchBar",
    clearAllButton : "ListingPage/filterIcon/clearAllButton",
    applyButton : "ListingPage/filterIcon/applyButton",
    eventType : "ListingPage/filterIcon/eventType",
    eventStatus : "ListingPage/filterIcon/eventStatus",
    checkLabel : "ListingPage/filterIcon/checkLabel",
    BackArrow : "ISource/BackArrow",
    FilterIcon : "CommonComponent/Grid Component/Filter icon OR/Filter Icon",
    BackToListing : "CommonComponent/Page header/Back to listing Page - button",
    ConcludeComments: "iSource/Conclude/ConcludeComments/CommentsForConcludingEvent",
    ConcludeComments1: "iSource/Conclude/ConcludeComments/CommentsForConcludingEvent1",
    ConcludeBtn: "Analyze/ConcludeEvent/ConcludeBtnOnPopup",
    RepublishBtnOnPopup: "EventDetails/RepublishEvent/RepublishBtnOnPopup"
    
}