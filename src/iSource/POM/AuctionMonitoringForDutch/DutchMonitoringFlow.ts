import "codeceptjs";
import { DutchMonitoringLocators } from "./DutchMonitoringLocator";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
declare const inject: any;
const { I } = inject();
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import { DewCheckbox } from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";


export class DutchAuctionMonitoring {
    static async searchAuctionEvent() {
        I.wait(prop.DEFAULT_WAIT)
        // HeaderFilter.applyGridSearch(await iSourcelmt.getLabel('Search Events')as string,'1610062877',await iSourcelmt.getLabel('EventColumnName')as string)
        I.fillField(await Startup.uiElements.get(DutchMonitoringLocators.EventSearchBox)as string,Startup.testData.get('DutchAuctionEvent')as string)
        // I.fillField(`//span//input[contains(@placeholder,'Search Events')]`, '1610062877')
        // I.click(Startup.uiElements.get(DutchMonitoringLocators.searchByEventId));
        await CommonKeyword.clickElement(Startup.uiElements.get(DutchMonitoringLocators.searchByEventId)as string)

    }

    static async navigateToAuctionSummary() {
        I.wait(prop.DEFAULT_WAIT)
        // I.click(await Startup.uiElements.get(DutchMonitoringLocators.viewAuctionSummaryOption) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(DutchMonitoringLocators.viewAuctionSummaryOption)as string)
        I.wait(prop.DEFAULT_WAIT)
    }


    static async viewStartingBidPrice() {
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.StartBidPrice)as string);
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.StartBidPrice)as string)
    }

    static async viewReverseBidPrice() {
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.ReverseBidPrice)as string);
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.ReverseBidPrice)as string)
    }

    static async viewCurrentBidPrice() {
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.CurrentBidPrice)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.CurrentBidPrice)as string)
        
    }

    static async clickChangeBidBtn() {
        console.log(4545,await iSourcelmt.getLabel('ChangeBidPriceBtn')as string);
        
        await DewButton.click(await iSourcelmt.getLabel('ChangeBidPriceBtn')as string)
    }

    static async enterBidPrice() {
        // await I.clearField(`//input[contains(@formcontrolname,'manualPrice')]`)
        // await I.fillField(`//input[contains(@formcontrolname,'manualPrice')]`,'250')
        await TextArea.enterTextUsingLocator(await Startup.uiElements.get(DutchMonitoringLocators.ManualPriceInput)as string, '250')
    }

    static async enterBidPercentage() {
        // I.clearField(`//input[contains(@formcontrolname,'percentPrice')]`)
        // I.fillField(`//input[contains(@formcontrolname,'percentPrice')]`,'7')
        await TextField.enterTextUsingLocator(await Startup.uiElements.get(DutchMonitoringLocators.PercentPriceInput)as string, '4')
    }

    static async clickSaveBtn() {
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(await Startup.uiElements.get(DutchMonitoringLocators.SaveBidLabel)as string)
        I.wait(prop.DEFAULT_WAIT)
    }

    static async verifySavedBidPrice() {
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.CurrentBidLabel)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.CurrentBidPrice)as string)
        
    }

    static async viewSupplierDetail() {
        // I.scrollIntoView(await Startup.uiElements.get(DutchMonitoringLocators.SupplierNameLabel)as string)
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.SupplierNameLabel)as string)
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.LoggedInLabel)as string)
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.AcceptanceLabel)as string)

        await CommonKeyword.scrollIntoView(await Startup.uiElements.get(DutchMonitoringLocators.SupplierNameLabel)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.SupplierNameLabel)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.LoggedInLabel)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.AcceptanceLabel)as string)
    }

    static async verifySupplier() {
        // I.dontSeeElement(await Startup.uiElements.get(DutchMonitoringLocators.NoDataLabel)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.NoDataLabel)as string)
    }

    static async pauseAuction() {
        await DutchAuctionMonitoring.clickPauseBtn();
        await DutchAuctionMonitoring.clickOkBtn();
    }

    static async clickPauseBtn() {
        await DewButton.click(await iSourcelmt.getLabel('PauseButtonLabel') as string)
    }

    static async clickOkBtn() {
        await DewButton.click(await iSourcelmt.getLabel('OkbuttonText') as string)
    }

    static async verifyAuctionIsPaused(){
        // I.seeElement(await Startup.uiElements.get(DutchMonitoringLocators.AuctionPausedLabel)as string)
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.AuctionPausedLabel)as string)
    }

    static async enableOrDisableAutoExtention(){
        await DewCheckbox.selectCheckbox(await iSourcelmt.getLabel('extendCheckBoxKey')as string)
        await DewButton.click(await iSourcelmt.getLabel('YesLabelKey')as string)
    } 

    static async verifyDisableAutoExtention(){
        I.wait(prop.DEFAULT_WAIT)
        let isCheck = await Checkbox.isCheckboxChecked(await Startup.uiElements.get(DutchMonitoringLocators.AutoExtentionCheckbox)as string)

        if(!isCheck){
            logger.info('Check box is disable');
        }
        // await I.dontSeeCheckboxIsChecked(await Startup.uiElements.get(DutchMonitoringLocators.AutoExtentionCheckbox)as string)
    } 

    static async verifyEnableAutoExtention(){
        I.wait(prop.DEFAULT_WAIT)
        // await I.seeCheckboxIsChecked(await Startup.uiElements.get(DutchMonitoringLocators.AutoExtentionCheckbox)as string)

        let isCheck = await Checkbox.isCheckboxChecked(await Startup.uiElements.get(DutchMonitoringLocators.AutoExtentionCheckbox)as string)

        if(isCheck){
            logger.info('Check box is Enabled');
        }
    } 

    static async navigateToMontoringTab(){
        I.wait(prop.DEFAULT_HIGH_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(DutchMonitoringLocators.NavigateToMonitoringAuction)as string);
    }

    static async checkForCurrentTab(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await DewElement.checkIfElementVisible(await Startup.uiElements.get(DutchMonitoringLocators.ActiveTabLabel)as string)
    }

    static async setDurationPerLot(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(DutchMonitoringLocators.DurationPerLotMin)as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(DutchMonitoringLocators.SelectFiveMin)as string);
    }


}