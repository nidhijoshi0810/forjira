import "codeceptjs";
declare const inject: any;
const { I } = inject();

import { prop } from "../../../Framework/FrameworkUtilities/config";

import { QuestionnaireFlow } from "../Questionnaire/QuestionnaireFlow";
import { DewCheckbox } from "dd-cc-zycus-automation/dist/components/dewCheckbox";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { BUMasterLocators } from "./BUMasterLocators";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

export class BUMasterFlow {

    static async AddPricingTableBUMaster() {
        await QuestionnaireFlow.clickAddPricingTable();
        await QuestionnaireFlow.clickDoneOnPricingTablePopup();
        await  QuestionnaireFlow.checkCustomColumnCheckBox();
        await QuestionnaireFlow.clickDoneCreateStandardItem();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)
        await  QuestionnaireFlow.importFileforRFX();
        await  QuestionnaireFlow.exportFiles();
        // await   this.editSomeFieldsinItemTable();
        await  QuestionnaireFlow.hideCustomColumns();
        await  QuestionnaireFlow.incumbentSupplier();
           
    }

    static async AddSegmentationPricingTableBUMaster() {
        await CommonKeyword.clickElement("//dew-icon[contains(@class,'delete-icon pointer')]") 
        await BUMasterFlow.clickOnSegmentationGroup();
        await QuestionnaireFlow.clickDoneCreateStandardItem();
    }
    static async clickOnSegmentationGroup() {
        await DewCheckbox.selectCheckbox(await iSourcelmt.getLabel("SegmentationGroupLabelKey") as string);
    }

    static async selectBU() {
        I.wait(prop.DEFAULT_LOW_WAIT)

        I.dragSlider(Startup.uiElements.get(BUMasterLocators.DragSliderItemtable) as string, 100);
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("BUSelectorLabel") as string)
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)

        var element = await DewElement.grabTextFrom(Startup.uiElements.get(BUMasterLocators.ModalDataForBUCU) as string);
        console.log(element)

        if (element.includes("No Data Available")) {

            await CommonKeyword.clickElement(Startup.uiElements.get(BUMasterLocators.ModalCloseElement) as string);
        } else {
            await CommonKeyword.clickElement(Startup.uiElements.get(BUMasterLocators.BUSelect) as string);
            I.wait(prop.DEFAULT_LOW_WAIT)

            await CommonKeyword.clickElement(Startup.uiElements.get(BUMasterLocators.BuCheckbox) as string);

            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("BUSelectLabelKey") as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed()
            I.wait(prop.DEFAULT_LOW_WAIT)
        }

    }

    static async checkBU() {

        await DewElement.checkIfElementPresent(await iSourcelmt.getLabel("BUSelectorLabel") as string)

    }

    static async selectCategory() {
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("No Category Selected") as string)
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()

        var element = await DewElement.grabTextFrom(Startup.uiElements.get(BUMasterLocators.ModalDataForBUCU) as string);
        console.log(element)
        if (element == "No Data Available") {

            await CommonKeyword.clickElement(Startup.uiElements.get(BUMasterLocators.ModalCloseElement) as string);

        } else {
            await CommonKeyword.clickElement(Startup.uiElements.get(BUMasterLocators.BUSelect) as string);
            I.wait(prop.DEFAULT_LOW_WAIT)

            await CommonKeyword.clickElement(Startup.uiElements.get(BUMasterLocators.BuCheckbox) as string);

            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("BUSelectLabelKey") as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed()
            I.wait(prop.DEFAULT_LOW_WAIT)
        }

    }

    static async checkCategory() {

        await DewElement.checkIfElementPresent(await iSourcelmt.getLabel("No Category Selected") as string)

    }

}