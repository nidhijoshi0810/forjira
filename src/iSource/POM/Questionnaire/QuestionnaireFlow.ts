import { DewFileUpload } from 'dd-cc-zycus-automation/dist/components/dewFileUpload';
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { DragAndDrop } from 'dd-cc-zycus-automation/dist/components/dragAndDrop';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {QuestionnaireLocators} from "./QuestionnaireLocators";
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { CommonLocators } from '../../isourceCommonFunctions/CommonLocators';

export class QuestionnaireFlow{
    static async clickCreateQuestionnaire(){
     
        //I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(attachmentsElements.tabXpath),iSourcelmt.getLabel("AttachmentsTabLabel")));
        await CommonKeyword.clickElement("//a/dew-default-tab-head[contains(text(),'Questionnaires')]");
        // I.click("//a/dew-default-tab-head[contains(text(),'Questionnaires')]")
        I.wait(prop.DEFAULT_LOW_WAIT)
        I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.CreateQuestionnaireButton));
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.CreateQuestionnaireButton) as string);
        // I.click(Startup.uiElements.get(QuestionnaireLocators.CreateQuestionnaireButton));
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
      
    }
    static async checkPricingTableIsNotPresent(){
        I.dontSee(await iSourcelmt.getLabel("AddPricingTableLabelKey"));
        console.log("Add pricingtable is not present.");
    }
    static async randomString(length : number) {
        return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
    }
 
    static async selectQuestionTypeWithMandatory(questionType : string){ 
       var questionTypeTranslated=await iSourcelmt.getLabel(questionType) as string;
       await  QuestionnaireFlow.addQuestion(questionTypeTranslated);
       I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        if(questionTypeTranslated=="Comments"||questionTypeTranslated=="Table"){
            logger.info("QuestionType is Comments"+questionType);
        }else{
        I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.MandatoryIcon));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.MandatoryIcon) as string);
    //    I.click(Startup.uiElements.get(QuestionnaireLocators.MandatoryIcon))
       await iSourcelmt.waitForLoadingSymbolNotDisplayed();
       //await QuestionnaireFlow.fillQuestionField();
        }
        
     }
     static async addQuestion(questionType: string){
     await CommonFunctions.scrollToButton(await iSourcelmt.getLabel("AddQuestionLabel")as string);
       
       await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddQuestionLabel")as string);
        logger.info("Click on Add Question Button");
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.labelXpath) as string , await iSourcelmt.getLabel("SelectQuestionTypeTextLabel")as string));
        I.scrollIntoView(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.questionOption) as string ,questionType));
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.questionOption) as string , questionType));
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.questionOption) as string , questionType));
     }
     static async addQuestionTypeTableForMandatory(){
              await  QuestionnaireFlow.addQuestion(await iSourcelmt.getLabel("QuestionType_Table") as string);
                I.fillField(Startup.uiElements.get(QuestionnaireLocators.RowCount_Table),Startup.testData.get("RowCount_Table"));
                I.fillField(Startup.uiElements.get(QuestionnaireLocators.ColumnCount_Table),Startup.testData.get("ColumnCount_Table"))
               await QuestionnaireFlow.clickOncreateQuestionBtn();
         iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.MandatoryIcon));
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.MandatoryIcon) as string);
            // I.click(Startup.uiElements.get(QuestionnaireLocators.MandatoryIcon))
             iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.wait(prop.DEFAULT_MEDIUM_WAIT)
        } 

        static async clickOncreateQuestionBtn(){
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("CreateQuestionTextLabel") as string);
            // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.CreateQuestionButton_Table),iSourcelmt.getLabel("CreateQuestionTextLabel")));
            iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.wait(prop.DEFAULT_LOW_WAIT)
        }
        static async addQuestionTypeTableForNonMandatory(){
                   await QuestionnaireFlow.addQuestion(await iSourcelmt.getLabel("QuestionType_Table") as string);
                    I.fillField(Startup.uiElements.get(QuestionnaireLocators.RowCount_Table),Startup.testData.get("RowCount_Table"));
                    I.fillField(Startup.uiElements.get(QuestionnaireLocators.ColumnCount_Table),Startup.testData.get("ColumnCount_Table"));
                  await QuestionnaireFlow.clickOncreateQuestionBtn();
                
             
               
            } 

     static async selectQuestionTypeWithNonMandatory(questionType : string){
         await QuestionnaireFlow.addQuestion(await iSourcelmt.getLabel(questionType) as string);
         //await QuestionnaireFlow.fillQuestionField();
      }

      static async executeJSClick(){
         await I.executeScript(function() {
        // document.evaluate("//p", document, null, 9, null). singleNodeValue.innerText="Question added by automation";
         
        }); 
       
    }
    static async fillQuestionField(){
      await CommonKeyword.clickElement("//p[contains(text(),'Enter Question')]");
    //   I.click("//p[contains(text(),'Enter Question')]");
      await QuestionnaireFlow.switchTockEditorTextAreaFrame();
      await CommonKeyword.clickElement("//p");
    //   I.click("//p");
      await QuestionnaireFlow.executeJSClick();
      I.switchTo();
    }
    static async switchTockEditorTextAreaFrame(){

     I.switchTo("//iframe[@class='cke_wysiwyg_frame cke_reset']");

        
	}

    static async AddPricingTableAwardWorkflow(){
        await  QuestionnaireFlow.clickAddPricingTable();
        await  QuestionnaireFlow.clickDoneOnPricingTablePopup();
        await  QuestionnaireFlow.clickDoneCreateStandardItem();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT);
        await  QuestionnaireFlow.importFileforAuction();
        I.wait(prop.DEFAULT_LOW_WAIT);
    }

    static async AddPricingTableRFPandRFQ(){
    await  QuestionnaireFlow.clickAddPricingTable();
    await  QuestionnaireFlow.clickDoneOnPricingTablePopup();
    await  QuestionnaireFlow.checkCustomColumnCheckBox();
    await  QuestionnaireFlow.clickDoneCreateStandardItem();
    iSourcelmt.waitForLoadingSymbolNotDisplayed()
    I.wait(prop.DEFAULT_LOW_WAIT)
    await  QuestionnaireFlow.importFileforRFX();
    await  QuestionnaireFlow.exportFiles();
    // await   this.editSomeFieldsinItemTable();
    await  QuestionnaireFlow.hideCustomColumns();
    await  QuestionnaireFlow.incumbentSupplier();
       
    }
    static async AddPricingTableEngAuctionlineItem(){
        await   QuestionnaireFlow.clickAddPricingTable();
            
            await   QuestionnaireFlow.clickDoneOnPricingTablePopup();
            await   QuestionnaireFlow.checkLineItembidding();
            await   QuestionnaireFlow.clickDoneCreateStandardItem();
             await   iSourcelmt.waitForLoadingSymbolNotDisplayed()
             await   QuestionnaireFlow.importFileforAuction();
             await   QuestionnaireFlow.exportFiles();
             iSourcelmt.waitForLoadingSymbolNotDisplayed()
    }
    static async AddPricingTableEnglishAuction(){
     await   QuestionnaireFlow.clickAddPricingTable();
     await   QuestionnaireFlow.clickDoneOnPricingTablePopup();
     //await   this.checkLineItembidding();
     await   QuestionnaireFlow.clickDoneCreateStandardItem();
     await   iSourcelmt.waitForLoadingSymbolNotDisplayed()
    //  I.wait(prop.DEFAULT_LOW_WAIT)
     await   QuestionnaireFlow.importFileforAuction();
    //  await   this.editSomeFieldsinItemTable();
     await   QuestionnaireFlow.exportFiles();
     iSourcelmt.waitForLoadingSymbolNotDisplayed()
    
    }

    static async AddPricingTableDutchandJapneseAuction(){
     await   QuestionnaireFlow.clickAddPricingTable();
     await   QuestionnaireFlow.clickDoneOnPricingTablePopup();
     I.wait(prop.DEFAULT_MEDIUM_WAIT)
     await   QuestionnaireFlow.clickDoneCreateStandardItem();
     await  iSourcelmt.waitForLoadingSymbolNotDisplayed()
     I.wait(prop.DEFAULT_LOW_WAIT)
     await   QuestionnaireFlow.importFileforAuction();
    // await    this.editSomeFieldsinItemTable();
    await    QuestionnaireFlow.exportFiles();
     await  iSourcelmt.waitForLoadingSymbolNotDisplayed()
     I.wait(prop.DEFAULT_MEDIUM_WAIT)
       
    }


    static async addSection(secName ?:string){
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddSectionLabelKey")as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)
        if(secName){
        I.fillField(Startup.uiElements.get(QuestionnaireLocators.sectionInputFiled) as string,secName);
    }else{
        I.fillField(Startup.uiElements.get(QuestionnaireLocators.sectionInputFiled),Startup.testData.get("sectionName"));

    }
}
    static async clickOnDone(){
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("DoneButtonLabel")as string);
       await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async clickAddPricingTable(){
       await CommonKeyword.clickLabel(await  iSourcelmt.getLabel("AddPricingTableLabelKey")as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(prop.DEFAULT_LOW_WAIT)
    }
   static async clickDoneOnPricingTablePopup(){
       await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.DonePricingTablePopup) as string);
    }
  static async  fillDefaultNoOfRows(){
    I.fillField(Startup.uiElements.get(QuestionnaireLocators.NumOfDefaultRows),Startup.testData.get("NumberOfRows"));
    }
   static async checkCustomColumnCheckBox(){
    await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.CustomColumnCheckBox) as string);
    }

    static async checkLineItembidding(){
    I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.LineItemBiddingCheckBox)as string,await iSourcelmt.getLabel("LineItemBiddingCheckBox")as string));
    await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.LineItemBiddingCheckBox)as string, await iSourcelmt.getLabel("LineItemBiddingCheckBox")as string));
    }
 static async clickDoneCreateStandardItem(){
    iSourcelmt.waitForLoadingSymbolNotDisplayed()
  
    await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("DoneButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
       
    }
   static async importFileforRFX(){
        I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ImportExcelButton)as string, await iSourcelmt.getLabel("ImportExcel")as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ImportExcelButton)as string, await iSourcelmt.getLabel("ImportExcel")as string));
       await iSourcelmt.waitForLoadingSymbolNotDisplayed()
       
        I.attachFile(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ExternalFileButton)as string, await iSourcelmt.getLabel("ExternalFile")as string),"/DataFiles/iSource/Copy of ItemTableExport_281954_1583247522884.xlsx")
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
       
    }
   static async importFileforAuction(){
       I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ImportExcelButton)as string, await iSourcelmt.getLabel("ImportExcel")as string));
       await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ImportExcelButton) as string,await iSourcelmt.getLabel("ImportExcel")as string));
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
       
        await I.attachFile(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ExternalFileButton)as string, await iSourcelmt.getLabel("ExternalFile") as string),"/DataFiles/iSource/ItemTableExport_281954_1583248069616.xlsx")
       await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        
    }
   static async exportFiles(){
        I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ExportExcelButton)as string, await iSourcelmt.getLabel("ExportExcel") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ExportExcelButton)as string, await iSourcelmt.getLabel("ExportExcel") as string));
    }
  static async  editSomeFieldsinItemTable(){
       I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.ItemNameField));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.ItemNameField) as string);
       I.fillField(Startup.uiElements.get(QuestionnaireLocators.ItemNameField),Startup.testData.get("ItemNameField"));
       await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SaveIconButton) as string);
    }
 static async hideCustomColumns(){
        I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("OKButton") as string);
        I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.HiddenCustomEyeIcon));
        I.wait(prop.DEFAULT_LOW_WAIT)
    }
  static async  incumbentSupplier(){
    I.wait(prop.DEFAULT_LOW_WAIT)
        I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ZeroSuppliers)as string, await iSourcelmt.getLabel("0Suppliers") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ZeroSuppliers)as string, await iSourcelmt.getLabel("0Suppliers") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.ViewAllSuppliers)as string, await iSourcelmt.getLabel("ViewAllSuppliers") as string));
        
        I.seeElement(Startup.uiElements.get(QuestionnaireLocators.SupplierSearchField));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SupplierSearchField) as string);
        I.fillField(Startup.uiElements.get(QuestionnaireLocators.SupplierSearchField),Startup.testData.get("defaultSupplierText"))
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.SupplierColumnName)as string, await iSourcelmt.getLabel("supplierColumnNameIncubmentSupplier") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.SupplierColumnName)as string, await iSourcelmt.getLabel("supplierColumnNameIncubmentSupplier") as string));
        //SelectSuppliers
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SupplierNameCheckBox) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.incumbentAddButton) as string,await iSourcelmt.getLabel("AddButton") as string));

        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.ApplytoAllItemsCheckBox) as string);
        
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.incumbentAddButton) as string,await iSourcelmt.getLabel("AddButton") as string));
      
    }
  static async  fillCurrentPrice(){
    I.wait(prop.DEFAULT_LOW_WAIT)
        I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.CurrentPriceField));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.CurrentPriceField) as string);
      
        var EventDescription = QuestionnaireFlow.randomString(10);
        I.fillField((Startup.uiElements.get(QuestionnaireLocators.CurrentPriceField),EventDescription));
    }
    
    static async clickSaveAsDraft(){
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.SaveAsDraftButton) as string);
        // I.click(Startup.uiElements.get(QuestionnaireLocators.SaveAsDraftButton));
        I.seeElement(Startup.uiElements.get(QuestionnaireLocators.alertMessage))
    }
    static async clickFreeze(){
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.FreezeButton) as string);
    // I.click(Startup.uiElements.get(QuestionnaireLocators.FreezeButton));
        I.wait(prop.DEFAULT_LOW_WAIT)
        var status=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuestionnaireLocators.GetUpdatedInformationButtonXpath) as string);
        if(status==1)
        {   
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.GetUpdatedInformationButtonXpath) as string);
            // I.click(Startup.uiElements.get(QuestionnaireLocators.GetUpdatedInformationButtonXpath));
        }
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.DoneButtonAfterFreeze) as string,Startup.testData.get("DoneButton") as string));
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.DoneButtonAfterFreeze) as string,Startup.testData.get("DoneButton") as string))
        I.wait(prop.DEFAULT_LOW_WAIT);
        I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(QuestionnaireLocators.buttonSpanXpath) as string , await iSourcelmt.getLabel("unfreezeLabelKey") as string));
      console.log("Event freezed.");
    }
   
//----------------------------------------DefineSettings------------------------------------------------
    static async clickDefineSetting(){
                await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.DefineSettingButton) as string);
        //         I.click(Startup.uiElements.get(QuestionnaireLocators.DefineSettingButton))
            }
   static async  addAuctionStartingPrice(){
                I.fillField(Startup.uiElements.get(QuestionnaireLocators.DefineSetting_AuctionPriceField),Startup.testData.get("AuctionPrice"))
            }
   static async  addJapneseAuctionstartingPrice(){
            I.fillField(Startup.uiElements.get(QuestionnaireLocators.JapneseAuctionPriceField),Startup.testData.get("AuctionPrice"))
        }
   static async addAuctionreservePrice(){
            I.fillField(Startup.uiElements.get(QuestionnaireLocators.DutchReservPriceField),Startup.testData.get("ReservePrice"))
        }
   static async addBidIncrementPercentage(){
                I.fillField(Startup.uiElements.get(QuestionnaireLocators.DefineSetting_BidIncrementField),Startup.testData.get("BidIncrementField"))
            }
static async  clickSaveOnDefineSetting(){
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.SaveOnDefineSetting)as string, await iSourcelmt.getLabel("SaveButton") as string));
        //         I.click(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.SaveOnDefineSetting)as string, await iSourcelmt.getLabel("SaveButton") as string))
            } 
 static async  setDurationPerRound(option: string){
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.DurationPerRoundMinute) as string);
            // I.click(Startup.uiElements.get(QuestionnaireLocators.DurationPerRoundMinute));
            I.clearField(Startup.uiElements.get(QuestionnaireLocators.DurationPerRoundMinute));
        //    I.fillField(Startup.uiElements.get(QuestionnaireLocators.DurationPerRoundMinute),option);
            I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.minuteOption) as string,option));
            await CommonKeyword.clickElement((await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.minuteOption) as string,option)));
            // I.click(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.minuteOption) as string,option));
        }
    
   
    
static async modifyQuestionnaire(){
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await CommonKeyword.clickElement("//a/dew-default-tab-head[contains(text(),'Questionnaires')]");
    I.wait(prop.DEFAULT_WAIT);
   await CommonKeyword.clickElement("//button[@aria-label='Modify Questionnaires']");
    // I.click("//button[@aria-label='Modify Questionnaires']");
    
}

    static async deleteAllQuestionsFromCurrentSection(){
     let totalQuestionsWithinSection:number = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuestionnaireLocators.totalQuestionsWithinSection) as string);
     console.log("totalQuestionsWithinSection ::",totalQuestionsWithinSection);
        
        if(totalQuestionsWithinSection > 0){
            for(let i=totalQuestionsWithinSection; i > 0; i--){
                let deleteIconVisible:boolean = await DewElement.checkIfElementPresent(Startup.uiElements.get(QuestionnaireLocators.deleteQuestionIcon) as string);
                console.log("deleteIconVisible",deleteIconVisible);

                if(deleteIconVisible){
                    await DewElement.checkIfElementPresent(Startup.uiElements.get(QuestionnaireLocators.questionInFocus) as string);
                    await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.deleteQuestionIcon) as string);
                    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('YesLabelKey') as string);
                    I.wait(prop.DEFAULT_WAIT);
                }else{
                    console.log("No question availble to delete");
                }
            }
        }
    }
    
    static async verifyDeleteAllQuestionsFromCurrentSection(){
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            await DewElement.verifyIfIDontSeeElement(Startup.uiElements.get(QuestionnaireLocators.questionInFocus) as string);
    }

    static async deleteSection(){
        I.wait(prop.DEFAULT_WAIT);
        await DewElement.checkIfElementPresent(Startup.uiElements.get(QuestionnaireLocators.actionMenuForSection) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.actionMenuForSection) as string);
        await DewElement.checkIfElementPresent(Startup.uiElements.get(QuestionnaireLocators.deleteActionForSection) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.deleteActionForSection) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('YesLabelKey') as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }

    static async verifySectionDeletion(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfIDontSeeElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.sectionNameWithCustomText) as string, await Startup.testData.get("sectionNameDeletionQuestionnaire") as string));        
    }

    static async copySection(){
        await DewElement.checkIfElementPresent(Startup.uiElements.get(QuestionnaireLocators.actionMenuForSection) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.actionMenuForSection) as string);
        await DewElement.checkIfElementPresent(Startup.uiElements.get(QuestionnaireLocators.copyActionForSection) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.copyActionForSection) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);    
    }

    static async verifyCopySection(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let totalSections: number =  await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuestionnaireLocators.totalSections) as string);
        console.log("totalSections :: ", totalSections);

        let parentSectionIndex:number = totalSections - 1;
        console.log("parentSectionIndex :: ", parentSectionIndex);

        await DewElement.checkIfElementPresent((Startup.uiElements.get(QuestionnaireLocators.sectionInFocus) as string).replace("<<index>>",parentSectionIndex.toString()));
        await CommonKeyword.clickElement((Startup.uiElements.get(QuestionnaireLocators.sectionInFocus) as string).replace("<<index>>",parentSectionIndex.toString()));

        let questionEntriesInParentSection: number =  await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(QuestionnaireLocators.totalQuestionsWithinSection) as string);
        console.log("questionsInParentSection :: ", questionEntriesInParentSection);

        await DewElement.checkIfElementPresent((Startup.uiElements.get(QuestionnaireLocators.sectionInFocus) as string).replace("<<index>>",totalSections.toString()));
        await CommonKeyword.clickElement((Startup.uiElements.get(QuestionnaireLocators.sectionInFocus) as string).replace("<<index>>",totalSections.toString()));

        await DewElement.verifyIfISeeNumberOfElements(Startup.uiElements.get(QuestionnaireLocators.totalQuestionsWithinSection) as string,questionEntriesInParentSection)
    }

    static async exportQuestionnaireToExcel(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('ExportExcel') as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ExportExcel') as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.exportQuestionnaireRadioOption) as string,await iSourcelmt.getLabel('ExportToExcel') as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel('ExportsButtonLabelKey') as string));
    }

    static async exportQuestionnaireToWord(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('ExportExcel') as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ExportExcel') as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.exportQuestionnaireRadioOption) as string,await iSourcelmt.getLabel('ExportToWord') as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel('ExportsButtonLabelKey') as string));
    }

    static async downloadBlankTemplateForQuestionnaire(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('ExportExcel') as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ExportExcel') as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.exportQuestionnaireRadioOption) as string,await iSourcelmt.getLabel('DownloadBlankTemplate') as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CommonLocators.buttonXpath) as string,await iSourcelmt.getLabel('ExportsButtonLabelKey') as string));
    }

    static async selectQuestionTypeWithoutSupplierComments(questionType : string){
        var questionTypeTranslated=await iSourcelmt.getLabel(questionType) as string;
        await  QuestionnaireFlow.addQuestion(questionTypeTranslated);
        I.wait(prop.DEFAULT_LOW_WAIT);
         
         if(questionTypeTranslated=="Comments"||questionTypeTranslated=="Table"){
             logger.info("QuestionType is Comments"+questionType);
         }else{
        await I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.AllowSupplierCommentsIcon));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.AllowSupplierCommentsIcon) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         }
}

static async addQuestionTypeTableWithoutSupplierComments(){
        await  QuestionnaireFlow.addQuestion(await iSourcelmt.getLabel("QuestionType_Table") as string);
        I.fillField(Startup.uiElements.get(QuestionnaireLocators.RowCount_Table),Startup.testData.get("RowCount_Table"));
        I.fillField(Startup.uiElements.get(QuestionnaireLocators.ColumnCount_Table),Startup.testData.get("ColumnCount_Table"))
        await QuestionnaireFlow.clickOncreateQuestionBtn();
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.AllowSupplierCommentsIcon));
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.AllowSupplierCommentsIcon) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT)
}

    static async markedAsHidden(questionType : string){
            var questionTypeTranslated=await iSourcelmt.getLabel(questionType) as string;
            await  QuestionnaireFlow.addQuestion(questionTypeTranslated);
            I.wait(prop.DEFAULT_LOW_WAIT);
             
             if(questionTypeTranslated=="Comments"||questionTypeTranslated=="Table"){
                 logger.info("QuestionType is Comments"+questionType);
             }else{
             I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.HideFromSupplierIcon));
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.AllowSupplierCommentsIcon) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.HideFromSupplierIcon) as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
             }
    }

    static async addQuestionTypeTableMarkedAsHidden(){
            await  QuestionnaireFlow.addQuestion(await iSourcelmt.getLabel("QuestionType_Table") as string);
            I.fillField(Startup.uiElements.get(QuestionnaireLocators.RowCount_Table),Startup.testData.get("RowCount_Table"));
            I.fillField(Startup.uiElements.get(QuestionnaireLocators.ColumnCount_Table),Startup.testData.get("ColumnCount_Table"))
            await QuestionnaireFlow.clickOncreateQuestionBtn();
            iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.scrollIntoView(Startup.uiElements.get(QuestionnaireLocators.HideFromSupplierIcon));
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.AllowSupplierCommentsIcon) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.HideFromSupplierIcon) as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            I.wait(prop.DEFAULT_LOW_WAIT);
    }

    static async importUsingAppendToCurrentDraftOption(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('ImportOption') as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ImportOption') as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.importQuestionnaireRadioOption) as string, await iSourcelmt.getLabel('AppendToCurrentDraft') as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.importQuestionnaireRadioOption) as string, await iSourcelmt.getLabel('AppendToCurrentDraft') as string))
        await DewFileUpload.uploadFile("Event_Draft_Template.xlsx","Event_Draft_Template.xlsx");
        await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("DoneButtonLabel") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.importQuestionnaireModalCloseBtn) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }

    static async importUsingOverwriteToCurrentDraftOption(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('ImportOption') as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ImportOption') as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await DewElement.checkIfElementPresent(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.importQuestionnaireRadioOption) as string, await iSourcelmt.getLabel('OverwriteToCurrentDraft') as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(QuestionnaireLocators.importQuestionnaireRadioOption) as string, await iSourcelmt.getLabel('OverwriteToCurrentDraft') as string))
        await DewFileUpload.uploadFile("Event_Draft_Template.xlsx","Event_Draft_Template.xlsx");
        await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("DoneButtonLabel") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(QuestionnaireLocators.importQuestionnaireModalCloseBtn) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }




// ************************craete Event***********************************
}

// module.exports = new QuestionnaireFlow();