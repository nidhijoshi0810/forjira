import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
const { I } = inject();
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import {QuestionnaireLocators} from "./QuestionnaireLocators";
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";

import {TEBLocators} from "./TEBLocators";
export class TEBFlow{

   static async selectSectionType(sectionType:string){
        I.seeElement(Startup.uiElements.get(TEBLocators.TEBsectionDropdown));
        await CommonKeyword.clickElement(Startup.uiElements.get(TEBLocators.TEBsectionDropdown)as string);
        I.seeElement(Startup.uiElements.get(TEBLocators.dopdownDiv));
        logger.log("Section type drop down is opned.");
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(TEBLocators.sectiontypeoption)as string,await iSourcelmt.getLabel(sectionType)as string));
        // I.click(await CommonFunctions.getFinalLocator(I.getElement(TEBLocators.sectiontypeoption),await iSourcelmt.getLabel(sectionType)as string));

    }
}
// module.exports=new TEBFlow();