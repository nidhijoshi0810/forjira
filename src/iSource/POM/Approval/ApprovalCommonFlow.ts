import "codeceptjs"; 
declare const inject: any; 

const { I } = inject();
import { ApprovalLocators } from "./ApprovalLocators";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger"
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker";
import {DewLoader} from "dd-cc-zycus-automation/dist/components/dewLoader";
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { TextField } from "dd-cc-zycus-automation/dist/components/textfield";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";

export class ApprovalCommonFlow{

    static async clickOnApprove(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.approve) as string);
    }
    static async clickOnReject(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.reject) as string);
    }
    static async clickOnEventTitle(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.eventDetailsButton) as string);
    }
    static async clickOnRecall(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.recall) as string);
        TextField.enterTextUsingLocator(await Startup.uiElements.get(ApprovalLocators.commentTextBox) as string,"Any Comments");
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.recallCommentButton) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async clickOnView(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.view) as string);
        await I.wait(prop.DEFAULT_MEDIUM_WAIT);
    }
    static async fillDetailsOnCommentsBox(){
        TextField.enterTextUsingLocator(await Startup.uiElements.get(ApprovalLocators.commentTextBox) as string,"Any Comments");
    }

    static async clickOnCommentbutton(actionType:string){
        
        await DewLoader.waitToProcess();
        await DewLoader.waitForSpinner();
        let tempActivity: string =await Startup.uiElements.get(ApprovalLocators.activityType) as string
        let final : string = await CommonFunctions.getFinalLocator(tempActivity,actionType) as string;
        await CommonKeyword.clickElement(final);
        await I.wait(prop.DEFAULT_Standard_WAIT);
    }
    static async clickOnOk(){
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.doneButton) as string);
        await I.wait(prop.DEFAULT_Standard_WAIT);
    }

    static async ViewEventDetails(){
        I.seeElement(await Startup.uiElements.get(ApprovalLocators.basicDetailsLevel) as string);
    }

    static async verifyEventStatus(status: string){
        
        let eventStatus: string = await I.grabTextFrom(await Startup.uiElements.get(ApprovalLocators.approvalStatus) as string); 
        if(eventStatus.localeCompare(status) == 0 )
        {
            logger.info("Event "+eventStatus+" Successfully");
        }
         else{
            throw new Error("There is an Exception");
        }
    }
    static async verifyapproveawardEventStatus(status: string){
        I.seeElement(`//div[contains(@class, "node-section")]//*[contains(@class, "workflow-status") and contains(text(), "APPROVED")]`);
    }
    static async verifyrecallawardEventStatus(status: string){
        I.seeElement(`//div[contains(@class, "node-section")]//*[contains(@class, "workflow-status") and contains(text(), "RECALLED")]`);
    }

    static async verifyRecalledStatus(status: string){
        
        let recalledStatus: string = await I.grabTextFrom(await Startup.uiElements.get(ApprovalLocators.recalledStatus) as string); 
        logger.info("Recalled Status :>>"+ recalledStatus);
        if(recalledStatus.localeCompare(status) == 0 )
        {
            logger.info("Event Recalled Successfully");
        }
         else{
            throw new Error("There is an Exception to recall the event");
        }
    }

    static async ClickOnSecondaryView(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.secondaryAction) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.secondaryView) as string);
    }
    static async ClickOnSecondaryAction(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.secondaryAction) as string);
    }


    static async ClickOnDelegate(){
        
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.delegate) as string );
    }

    static async selectUser(){
        
        TextField.enterTextUsingLocator(await Startup.uiElements.get(ApprovalLocators.delegateUserDropDown) as string,await Startup.testData.get("delegateUserName") as string);
        let emailID : string = await Startup.testData.get("delegateUserEmail") as string;
        let dropdownPath : string = await Startup.uiElements.get(ApprovalLocators.userDropdownSelection) as string;
        let final : string = await CommonFunctions.getFinalLocator(dropdownPath,emailID)+"";
        await CommonKeyword.clickElement(final);
    }

    static async clickSaveandGotoWorkflow(){

        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.saveandGotoWorkflow) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.CONDITIONAL_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.sendForApproval) as string);
        await TextField.enterTextUsingLocator(await Startup.uiElements.get(ApprovalLocators.commentTextBox) as string,"Any Comments");
        await CommonKeyword.clickElement(await Startup.uiElements.get(ApprovalLocators.workflowApprove) as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_Standard_WAIT);
    }


    static async searchEvent(eventType:string){

        let eventID:string= await Startup.eventId_Map.get(eventType)as string;
        await DewLoader.waitToProcess();
        await DewLoader.waitForSpinner();
        await HeaderFilter.applyGridSearch("Search",eventID,"Event ID");
    }

    static async selectDate(){

        let todayDate: string = await CommonFunctions.getTodaysDateInMMDDYYYYFormat()+"";
        await DatePicker.selectDate(await Startup.uiElements.get(ApprovalLocators.delegateStartDate) as string,todayDate);
        let tomorrowDate: string = await CommonFunctions.getTodaysDateInMMDDYYYYFormat(1)+"";
        await DatePicker.selectDate(await Startup.uiElements.get(ApprovalLocators.delegateClosetDate) as string,tomorrowDate);
      
        }

}