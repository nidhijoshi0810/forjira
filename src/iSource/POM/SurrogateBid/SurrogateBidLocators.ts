export let surrogateBidlocator = {
    SearchOnGrid:"SearchOnGrid/SearchOnGrid",
    SearchByColumnName:"SearchByColumnName/SearchByColumnName",
    OpenEvent:"SearchAndOpenEvent/EventOpen",
    ManageSupplier:"Tab_ManageSuppliers/ManageSuppliers",
    SurrogateBidButton:"SurrogateBid/PlaceSurrogateBid",
    Yes_PopUp:"PopUpButton/Yes",
    AcceptButton:"SupplierPage/AcceptButton",
    unacceptedAceptButton:"SupplierPage/unacceptedAceptButton",
    ConfirmParticipation:"SupplierPage/ConfirmParticipation",
    Ok_button:"SupplierPage/Ok_Popup",
    buttonSpanXpath:"BA/buttonSpanXpath",
    ReturnToCollect:"SupplierPage/ReturnToCollect",
    SupplierNamesRows:"SurrogateBid/SupplierNamesRows",
    specificRowOfSupplier:"SurrogateBid/specificRowOfSupplier",
    placeSurrogateBidLink:"SurrogateBid/placeSurrogateBidLink"


    
}