import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonFunctions } from './../../isourceCommonFunctions/CommonFunctions';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import "codeceptjs"; 
declare const inject: any; 
import { surrogateBidlocator } from "./SurrogateBidLocators";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";

const { I } = inject();

export class SurrogateBidFlow{
    static async searchOnGrid(EventID? : string)
    {
        
        I.seeElement(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid));
        await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid) as string);
        // I.click(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid));
        I.fillField(Startup.uiElements.get(surrogateBidlocator.SearchOnGrid),EventID);
        I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName) as string,await iSourcelmt.getLabel("EventColumnName") as string));
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName) as string,await iSourcelmt.getLabel("EventColumnName") as string));
        // I.click(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SearchByColumnName) as string,await iSourcelmt.getLabel("EventColumnName") as string));
        I.wait(prop.DEFAULT_LOW_WAIT)
    }
    
static async clickOnManagerSupplierTab()
{
    I.wait(prop.DEFAULT_LOW_WAIT);
    await CommonFunctions.navigateToiosurceTabs(Startup.testData.get("ManageSpplierTab") as string);
    //  I.click(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.ManageSupplier) as string,Startup.testData.get("ManageSpplierTab") as string));
    I.wait(prop.DEFAULT_LOW_WAIT);
}
static async surrogateBid()
{       I.wait(prop.DEFAULT_LOW_WAIT);
            await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SurrogateBidButton) as string,Startup.testData.get("SupplierName") as string));
            // I.click(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SurrogateBidButton) as string,Startup.testData.get("SupplierName") as string));
            logger.info("*****************************************************************"+Startup.testData.get("SupplierName"))
            await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.Yes_PopUp) as string,Startup.testData.get("YesButton")as string));
            // I.click(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.Yes_PopUp) as string,Startup.testData.get("YesButton")as string));
            I.wait(prop.DEFAULT_LOW_WAIT);

            //await COE.searchOnGrid(evID,col);
        }


        static async clickOnPlaceSurrogateBid(){
            await I.waitForText(await iSourcelmt.getLabel("PlaceSurrogateBidLabelKey"));
           var supplierEnteries=await DewElement.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.SupplierNamesRows) as string,await iSourcelmt.getLabel("supplierColumnNameIncubmentSupplier") as string));
           var link=await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.placeSurrogateBidLink) as string,await iSourcelmt.getLabel("PlaceSurrogateBidLabelKey")as string);
           for (let index = 1; index <= supplierEnteries; index++) {
             console.log("link"+link)
            
              var entry=(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.specificRowOfSupplier)as string,
              await iSourcelmt.getLabel("supplierColumnNameIncubmentSupplier") as string)) .replace("<<index>>",""+index) ;
             
              if( (await I.grabTextFrom(entry)).includes(Startup.testData.get("SupplierName"))){
               
               await CommonKeyword.clickElement(link.replace("<<index>>",""+index));
                  break;
              
           }
        }
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(surrogateBidlocator.Yes_PopUp) as string,Startup.testData.get("YesButton") as string));
          await iSourcelmt.waitForLoadingSymbolNotDisplayed();
          await I.wait(prop.DEFAULT_WAIT);
        
    }
    static async clickOnAccept(){
           I.wait(prop.DEFAULT_MEDIUM_WAIT);
           I.seeElement(Startup.uiElements.get(surrogateBidlocator.AcceptButton));
            var acceptButton = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(surrogateBidlocator.AcceptButton) as string);
           
            for(let k=0;k<acceptButton; k++){
                console.log("inside of for loop");
                await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.unacceptedAceptButton) as string);
                 I.wait(prop.DEFAULT_MEDIUM_WAIT);
                 await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.Ok_button) as string);
                I.wait(prop.DEFAULT_MEDIUM_WAIT);
            }
        }

        static async clickOnConfirmParticipation(){
               await  I.seeElement(Startup.uiElements.get(surrogateBidlocator.ConfirmParticipation));
               await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.ConfirmParticipation) as string);
               I.wait(prop.DEFAULT_LOW_WAIT);
               await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.Ok_button) as string);
               I.wait(prop.DEFAULT_LOW_WAIT);
        }
        static async clickOnReturnToCollect(){
            await CommonKeyword.clickElement(Startup.uiElements.get(surrogateBidlocator.ReturnToCollect) as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        }
        
}

// module.exports = new SurrogateBidFlow();