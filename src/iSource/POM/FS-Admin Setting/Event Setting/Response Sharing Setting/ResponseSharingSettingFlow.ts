import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
import { SurrogateBidSettingLocator } from "../Surrogate Bidding Setting/SurrogateBidSettinglocator";
import { ResponseSharingSettingLocator } from "./ResponseSharingSettingLocator";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
declare const inject: any; 
const { I } = inject();
export class RespSharingFlow{

    // static async ClickRespSharingAndCheckOption() {
    //     //check
    //     await CommonKeyword.clickElement('//option[@id="responseSharing"]');
    //     await CommonKeyword.clickElement('//input[@id="mandatorySealedBid"]');    
    //     await CommonKeyword.clickElement('//input[@id="saveBtnId"]');
    //     await CommonKeyword.clickElement('//input[@id="popup_ok"]');
    //     I.wait(10);
    // }

    // static async ClickRespSharingAndCheckOption() {
    //     //Uncheck
    //     await CommonKeyword.clickElement('//option[@id="responseSharing"]');
    //     await CommonKeyword.clickElement('//input[@id="mandatorySealedBid"]');    
    //     await CommonKeyword.clickElement('//input[@id="saveBtnId"]');
    //     await CommonKeyword.clickElement('//input[@id="popup_ok"]');
    //     I.wait(10);
    // }
    static async ClickRespSharingAndCheckOption() {
        //check
        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(ResponseSharingSettingLocator.MandatorySealingCheckbox) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            await CommonKeyword.clickElement(Startup.uiElements.get(ResponseSharingSettingLocator.MandatorySealingCheckbox) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        }
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async ClickRespSharingAndUncheckOption() {
        //Uncheck
        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(ResponseSharingSettingLocator.MandatorySealingCheckbox) as string);
        console.log( "flag value :: ", isChecked)
        if(isChecked){
            await CommonKeyword.clickElement(Startup.uiElements.get(ResponseSharingSettingLocator.MandatorySealingCheckbox) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        }
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async seeSealBidDisabled(){
        await DewElement.verifyIfISeeElementInDOM(Startup.uiElements.get(SurrogateBidSettingLocator.disabledSealBid) as string);

    }
    

}