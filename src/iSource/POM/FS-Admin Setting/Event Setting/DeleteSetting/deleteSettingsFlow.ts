import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import {Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
// delete setting locators
 import { DeleteSettingLocators } from "./DeleteSettingLocators";
 import{NegotiationLocators} from "../../../Negotiation/NegotiationLocators"
 import{SurrogateBidSettingLocator} from "../Surrogate Bidding Setting/SurrogateBidSettinglocator"
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { TextArea } from 'dd-cc-zycus-automation/dist/components/textArea'

declare const inject: any; 
const { I } = inject();
export class deleteSettingsFlow{

    static async subDeleteSetting(labelToBeClicked : string) {
        await I.wait(prop.DEFAULT_WAIT);
        console.log(Startup.uiElements.get("AdminSettings/DeleteSettings/subDeleteOptions"));
        await CommonKeyword.clickElement(Startup.uiElements.get(DeleteSettingLocators.subDelDropdown) as string);
        // await CommonKeyword.clickLabel(await iSourcelmt.getLabel(labelToBeClicked) as string);
        
        await CommonKeyword.clickLabel(labelToBeClicked);
        I.wait(prop.DEFAULT_WAIT);
       
    }

    static async checkOwnerDelete(){
        console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/DeleteSettings/AllowDelete/OwnerDelete"));  

        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(DeleteSettingLocators.ownerDelete) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            await CommonKeyword.clickElement(Startup.uiElements.get(DeleteSettingLocators.ownerDelete) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
            }
        await I.wait(prop.DEFAULT_WAIT);

    }

    static async noRecordsFound(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("No records found") as string)
    }

     static async checkMandatoryComments(){

        console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/DeleteSettings/AllowDelete/mandatoryComments"));
         let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(DeleteSettingLocators.mandatoryComments) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            await CommonKeyword.clickElement(Startup.uiElements.get(DeleteSettingLocators.mandatoryComments) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
            }
        await I.wait(prop.DEFAULT_WAIT);

    }

    static async enterDeleteComments(){
        await I.wait(prop.DEFAULT_WAIT);
        // await I.fillField(await iSourcelmt.getLabel('Enter a comment for Event deletion')as string, 'deleted by kp_automation');
        console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/DeleteSettings/CommentBox")); 
        
        I.fillField(Startup.uiElements.get(DeleteSettingLocators.CommentBox),Startup.testData.get("EventName"));
        // await TextArea.enterTextWithPlaceHolder(await iSourcelmt.getLabel('Enter a comment for Event deletion')as string, 'deleted by kp_automation'));
    }


    static async secondaryActionsButton(){
        await I.wait(prop.DEFAULT_WAIT);
        
        console.log("xpath&&&&"+Startup.uiElements.get("FullSourceListingPage/SecondaryActionButton")); 
        await CommonKeyword.clickElement(Startup.uiElements.get(DeleteSettingLocators.secondaryActionsButton) as string);
    }

    static async deleteButtonVerify(){
        await I.wait(prop.DEFAULT_WAIT);
        await DewElement.verifyIfIDontSeeText(await iSourcelmt.getLabel("Delete event") as string);
    }

    static async saveDeleteSettings(){
        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
    }

    static async deleteEvent(){
        console.log("xpath&&&&"+Startup.uiElements.get("FullSource/DeleteEvent/deleteButton")); 
        await I.wait(prop.DEFAULT_WAIT);
        
        await CommonKeyword.clickElement(Startup.uiElements.get(DeleteSettingLocators.deleteButton) as string);
    }

    

}