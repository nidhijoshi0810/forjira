export let DeleteSettingLocators = {
    subDelDropdown:"AdminSettings/DeleteSettings/subDeleteOptions",
    deleteButton:"FullSource/DeleteEvent/deleteButton",
    ownerDelete:"AdminSettings/DeleteSettings/AllowDelete/OwnerDelete",
    mandatoryComments:"AdminSettings/DeleteSettings/mandatoryComments",
    secondaryActionsButton:"FullSourceListingPage/SecondaryActionButton",
    CommentBox:"FullSource/DeleteEvent/CommentBox"
   }


////textarea[@placeholder="Type here…"]