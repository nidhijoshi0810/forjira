import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { SurrogateBidSettingLocator } from "../Surrogate Bidding Setting/SurrogateBidSettinglocator";
import { EventRulesSettingLocator } from "../Event Rules Setting/EventRulesSettingLocator"
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { ViewAllSuppliersSteps } from "../../../BayerSideSupplier/ViewAllSupplierFlow";
import { CreateQuickSourceEvent } from "../../../CreateQuickSourceEvent/CreateQuickSourceEventLocators";
import { COE } from "../../../../../Framework/FrameworkUtilities/COE/COE";
import { Footer } from "dd-cc-zycus-automation/dist/components/Footer"
// import { DewButton } from "dd-cc-zycus-automation/components/dewButton";
declare const inject: any; 
const { I } = inject();
export class EventRulesFlow{
    // static async ClickEventRulesAndSelectYes() {
    //     //check
    //     await CommonKeyword.clickElement('//option[@id="potenSuppEvtRuleSetting"]');
    //     await CommonKeyword.clickElement('//input[@id="award_potenSupp"]');    
    //     await CommonKeyword.clickElement('//input[@id="saveBtnId"]');
    //     await CommonKeyword.clickElement('//input[@id="popup_ok"]');
    //     I.wait(10);
    // }

    // static async ClickEventRulesAndSelectNo() {
    //     //Uncheck
    //     await CommonKeyword.clickElement('//option[@id="potenSuppEvtRuleSetting"]');
    //     await CommonKeyword.clickElement('//input[@id="no_award_potenSupp"]');    
    //     await CommonKeyword.clickElement('//input[@id="saveBtnId"]');
    //     await CommonKeyword.clickElement('//input[@id="popup_ok"]');
    //     I.wait(10);
    // }
    
    static async ClickEventRulesAndSelectNo() {
        //no on event Rules
        await CommonKeyword.clickElement(Startup.uiElements.get(EventRulesSettingLocator.selectNO) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async ClickEventRulesAndSelectYes() {
        //yes on event Rules
        await CommonKeyword.clickElement(Startup.uiElements.get(EventRulesSettingLocator.selectYES) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async CreatePotentialSupplier(){
        await ViewAllSuppliersSteps.clickOnAddSuppliersTab();
        let  supplierName:string = await COE.randomString("10") as string;
        var supplierMailid = (supplierName+"@gmail.com");
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.enterTextUsingPlaceHolder(await iSourcelmt.getLabel("SearchAndSelect:") as string,supplierName);
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await COE.replaceValueInAString(Startup.uiElements.get(CreateQuickSourceEvent.SuggestNewSupplierXpath)  as string,await iSourcelmt.getLabel("SuggestNewSupplierButton") as string)); 
        await EventRulesFlow.setSupplierName(supplierName);
        await EventRulesFlow.setSupplieContact();
        await EventRulesFlow.setMailId(supplierMailid);
        I.wait(prop.DEFAULT_WAIT);
        await EventRulesFlow.clickDoneOnCreateNewSupplier();
        I.wait(prop.DEFAULT_WAIT);
    }
    
    static async setSupplierName(supplierName : string){
       await CommonKeyword.enterText(await iSourcelmt.getLabel("supplierColumnNameIncubmentSupplier") as string,supplierName);
   }

    static async setSupplieContact(){
       await CommonKeyword.enterText(await iSourcelmt.getLabel("supplierContactName") as string,Startup.testData.get("ContactInput") as string);
    }

    static async setMailId(supplierMailid : string){
       await CommonKeyword.enterText(await iSourcelmt.getLabel("SupplierEmailID") as string,supplierMailid);
    }
    
    static async clickDoneOnCreateNewSupplier(){
        await Footer.clickModalFooterButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
}