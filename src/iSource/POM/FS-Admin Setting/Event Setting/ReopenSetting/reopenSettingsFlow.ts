import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import {Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
// reopen locators
//  import { negoSettingLocators } from "./negoSettingLocators";
 import{reopenSettingLocators} from "./reopenSettingLocators"
 import{SurrogateBidSettingLocator} from "../Surrogate Bidding Setting/SurrogateBidSettinglocator"
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

declare const inject: any; 
const { I } = inject();
export class reopenSettingsFlow{
    static async externalComments(){
       //external comments yes
       console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/ReopenSettings/externalYES")); 
        await CommonKeyword.clickElement(Startup.uiElements.get(reopenSettingLocators.externalYES) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async internalComments(){
//internal comments yes
console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/ReopenSettings/internalYES")); 
        await CommonKeyword.clickElement(Startup.uiElements.get(reopenSettingLocators.internalYES) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }
}

