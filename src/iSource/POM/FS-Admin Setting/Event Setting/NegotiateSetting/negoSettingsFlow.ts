import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import {Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
// NegotiateLocators
 import { negoSettingLocators } from "./negoSettingLocators";
 import{NegotiationLocators} from "../../../Negotiation/NegotiationLocators"
 import{SurrogateBidSettingLocator} from "../Surrogate Bidding Setting/SurrogateBidSettinglocator"
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

declare const inject: any; 
const { I } = inject();
export class negoSettingsFlow{

    static async enableNego() {
         console.log("xpath&&&&"+Startup.uiElements.get("Admin/EventSettingDropdownOption/NegotiateCheckbox/enableNego")); 
        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(negoSettingLocators.enableNego) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            await CommonKeyword.clickElement(Startup.uiElements.get(negoSettingLocators.enableNego) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        }
        await I.wait(prop.DEFAULT_WAIT);
    }

    

    static async checkNegoAction() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("Negotiate") as string)
    }

    // stage
    
    static async enableBAFO() {

         console.log("xpath&&&&"+Startup.uiElements.get("Admin/EventSettingDropdownOption/negotiateSettings/enableBAFO")); 
        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(negoSettingLocators.enableBAFO) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            await CommonKeyword.clickElement(Startup.uiElements.get(negoSettingLocators.enableBAFO) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
            }
        await I.wait(prop.DEFAULT_WAIT);
        
    }

    static async checkBAFO(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(NegotiationLocators.Negotiatebutton) as string);
        console.log("xpath&&&&"+Startup.uiElements.get("ManageSuppliers/Negotiation/requestforOffer")); 
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(negoSettingLocators.requestforOffer) as string);
    }

}