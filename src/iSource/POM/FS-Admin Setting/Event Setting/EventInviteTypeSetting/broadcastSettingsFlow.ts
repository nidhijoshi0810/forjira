import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import {Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
// broadcast locators
import { broadcastLocators } from "./broadcastLocators";
 import{NegotiationLocators} from "../../../Negotiation/NegotiationLocators"
 import{SurrogateBidSettingLocator} from "../Surrogate Bidding Setting/SurrogateBidSettinglocator"
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

declare const inject: any; 
const { I } = inject();
export class broadcastSettingsFlow{

    static async allowBroadcast(){
        console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/BroadcastSettings/allowBroadcast")); 
        // await I.waitForElement(Startup.uiElements.get(broadcastLocators.allowBroadcast)); 
        await I.waitForElement(Startup.uiElements.get(broadcastLocators.AB));
        // let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(broadcastLocators.allowBroadcast) as string);
        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(broadcastLocators.AB) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            // await CommonKeyword.clickElement(Startup.uiElements.get(broadcastLocators.allowBroadcast) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(broadcastLocators.AB) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
            await I.wait(prop.DEFAULT_WAIT);
        }
        else{
        await I.wait(prop.DEFAULT_WAIT);
        }
        
        
    }


    static async broadcastToSuppliers(){
        console.log("xpath&&&&"+Startup.uiElements.get("EventDetails/EventSettings/broadcastYES")); 
        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(broadcastLocators.broadcastYES) as string);

    }

    static async broadcastDefault(){
        console.log("xpath&&&&"+Startup.uiElements.get("AdminSettings/BroadcastSettings/defaultBroadcast")); 
        // let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(broadcastLocators.defaultBroadcast) as string);
        let isChecked : boolean = await Checkbox.isCheckboxChecked(Startup.uiElements.get(broadcastLocators.DB) as string);
        console.log( "flag value :: ", !(isChecked))
        if(!(isChecked)){
            // await CommonKeyword.clickElement(Startup.uiElements.get(broadcastLocators.defaultBroadcast) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(broadcastLocators.DB) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
            await I.wait(prop.DEFAULT_WAIT);
        }
        else{
            await I.wait(prop.DEFAULT_WAIT);
            }
    }

    static async checkYES(){
        console.log("xpath&&&&"+Startup.uiElements.get("EventDetails/EventSettings/broadcastYES")); 
        await I.wait(prop.DEFAULT_WAIT);
         await CommonKeyword.clickElement(Startup.uiElements.get(broadcastLocators.broadcastYES) as string);
        
    }

}

// stage