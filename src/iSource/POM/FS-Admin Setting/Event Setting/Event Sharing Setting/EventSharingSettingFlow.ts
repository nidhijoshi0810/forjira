import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {Checkbox } from "dd-cc-zycus-automation/components/checkbox";
import { SurrogateBidSettingLocator } from "../Surrogate Bidding Setting/SurrogateBidSettinglocator";
import { EventSharingSettingLocator } from "./EventSharingSettingLocator";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
declare const inject: any; 
const { I } = inject();
export class EventSharingFlow{

    // static async ClickEventSharingAndSelectAllUser() {
    //     //check
    //     I.click('//option[@id="evtSharingSetting"]');
    //     I.click('//input[@id="all_user"]');    
    //     I.click('//input[@id="saveBtnId"]');
    //     I.click('//input[@id="popup_ok"]');
    //     I.wait(10);
    // }

    // static async ClickEventSharingAndSelectNoOtherUser() {
    //     //Uncheck
    //     await CommonKeyword.clickElement('//option[@id="evtSharingSetting"]');
    //     await CommonKeyword.clickElement('//input[@id="no_other_user"]');    
    //     await CommonKeyword.clickElement('//input[@id="saveBtnId"]');
    //     await CommonKeyword.clickElement('//input[@id="popup_ok"]');
    //     I.wait(10);
    // }
    static async ClickEventSharingAndSelectAllUser() {
        //all user
        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(EventSharingSettingLocator.allUser) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async ClickEventSharingAndSelectNoOtherUser() {
        //no other user
        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(EventSharingSettingLocator.noOtherUser) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async seeShared(){
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel( "SharedLabel" ) as string);
    }
    
}