export let SurrogateBidSettingLocator = {
 eventSettingTab : "Admin/EventSettings",
 eventSettingDropdown : "Admin/EventSettingDropDown",
 SurrogateBidSetting : "Admin/EventSettingDropdownOption/SurrogateBidSetting",
 saveButton : "Admin/EventSettingDropdownOption/SaveButton",
 OKButton : "Admin/EventSettingDropdownOption/OKButton",
 selectYES : "Admin/EventSetting/SurrogateBidSetting/YES",
 selectNO : "Admin/EventSetting/SurrogateBidSetting/NO",
 disabledSealBid : "EventSettings/SealBidDisabled"
}
