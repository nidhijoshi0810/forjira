import "codeceptjs"; 
import { Startup } from "../../../../../Framework/FrameworkUtilities/Startup/Startup";
import { iSourcelmt } from "../../../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { prop } from "../../../../../Framework/FrameworkUtilities/config";
import { SurrogateBidSettingLocator } from "./SurrogateBidSettinglocator";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
declare const inject: any; 
const { I } = inject();
export class SurrogateBidSettingFlow{
    static async clickOnEventSetting(labelToBeClicked : string) {
        await I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.eventSettingTab) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.eventSettingDropdown) as string);
        // await CommonKeyword.clickLabel(await iSourcelmt.getLabel(labelToBeClicked) as string);
        await CommonKeyword.clickLabel(labelToBeClicked);
        I.wait(prop.DEFAULT_WAIT);
    }

    static async switchTab(){
        I.wait(prop.DEFAULT_WAIT);
        await I.switchToNextTab();
    }

    static async switchToPreviousTab(){
        I.wait(prop.DEFAULT_WAIT);
        await I.switchToPreviousTab();
    }
    // static async ClickSurrogateBidAndSelectNo() {
    //     //no on surrogate bid
    //     await I.click('//option[@id="surrogateBiddingSetting"]');
    //     await I.click('//input[@id="disallowedSurrogate"]');    
    //     await I.click('//input[@id="saveBtnId"]');
    //     await I.click('//input[@value="OK"][@id="popup_ok"]');
    //     I.wait(10);
    // }

    // static async ClickSurrogateBidAndSelectYes() {
    //     //Yes on surrogate bid
    //     await I.click('//option[@id="surrogateBiddingSetting"]');
    //     await I.click('//input[@id="allowedSurrogate"]');    
    //     await I.click('//input[@id="saveBtnId"]');
    //     await I.click('//input[@value="OK"][@id="popup_ok"]');
    //     I.wait(10);
    // }

    static async ClickSurrogateBidAndSelectNo() {
        //no on surrogate bid
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.selectNO) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async ClickSurrogateBidAndSelectYes() {
        //Yes on surrogate bid
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.selectYES) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.saveButton) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(SurrogateBidSettingLocator.OKButton) as string);
        await I.wait(prop.DEFAULT_WAIT);
    }

    static async ISeeAllSourcingEvent(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("AllSourcingEventLabel") as string)
    }

    static async dontSeePlaceSurrogatebid(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await DewElement.verifyIfIDontSeeText(await iSourcelmt.getLabel("PlaceSurrogateBidLabelKey") as string);
    }

    
}