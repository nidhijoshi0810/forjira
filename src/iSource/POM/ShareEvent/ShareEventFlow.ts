
declare const inject: any; 
const { I } = inject();
//import {ViewSummaryLocators} from "./ViewSummaryLocators";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { ShareEventLocator } from "./ShareEventLocator";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { Filter} from "dd-cc-zycus-automation/dist/components/filter"
import { Checkbox} from "dd-cc-zycus-automation/dist/components/checkbox"
import { SecondaryAction} from "dd-cc-zycus-automation/dist/components/SecondaryAction"
import { HeaderFilter } from "dd-cc-zycus-automation/dist/components/headerFilter";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup";




export class ShareEventFlow{
    static async secondaryAction(ButtonName : string) {
        await SecondaryAction.clickSecondaryActions(await iSourcelmt.getLabel("AllSourcingEventLabel") as string, await iSourcelmt.getLabel( ButtonName) as string );
    }

    static async seeClearAllButton(){
       await DewElement.verifyIfISeeText(await iSourcelmt.getLabel("clearAllButton") as string);
    //    await ModalPopUp.clickCloseButton();
       await I.pressKey(`Escape`);      
       await iSourcelmt.waitForLoadingSymbolNotDisplayed(); 
    }


    // static async applyingFilter(eventType : string,eventStatus : string) {
    //     iSourcelmt.waitForLoadingSymbolNotDisplayed()
    //     I.wait (prop.DEFAULT_WAIT)
    //     I.click(await Startup.uiElements.get(ShareEventLocator.filterIcon) as string);
    //     I.click(await Startup.uiElements.get(ShareEventLocator.clearAllButton) as string); 
    //     await I.fillField(await Startup.uiElements.get(ShareEventLocator.SearchInFilter) as string,"Owner");
    //     I.checkOption('//label[contains(text(),"Select All")]');
    //     await I.fillField(await Startup.uiElements.get(ShareEventLocator.SearchInFilter) as string,'Type');
    //      I.scrollIntoView(Startup.uiElements.get(ShareEventLocator.checkLabel as string).replace("<<dataname>>",await iSourcelmt.getLabel(eventType)));
    //      I.checkOption( await CommonFunctions.getFinalLocator(Startup.uiElements.get(ShareEventLocator.checkLabel as string),await iSourcelmt.getLabel(eventType)));
    //     await I.fillField(await Startup.uiElements.get(ShareEventLocator.SearchInFilter) as string,'Status');
    //     I.scrollIntoView('//label[contains(text(),"'+eventStatus+'")]');
    //     I.checkOption('//label[contains(text(),"'+eventStatus+'")]');
    //     I.click((Startup.uiElements.get(ShareEventLocator.clickButton)).replace("<<dataname>>",await iSourcelmt.getLabel("ApplyLabelKey") as string));
    // } 
    // static async applyingFilter(eventType : string,eventStatus : string) {
    //     await Filter.applyFilter(await iSourcelmt.getLabel("Owner") as string );
    //     await Checkbox.selectAll(await iSourcelmt.getLabel("Owner") as string);
    //     await Filter.applyFilter(await iSourcelmt.getLabel("Owner") as string);
    //     await Checkbox.searchSelect(await iSourcelmt.getLabel("Owner") as string,await iSourcelmt.getLabel(eventType) as string );
    //     await Filter.applyFilter(await iSourcelmt.getLabel("Owner") as string);
    //     await Checkbox.searchSelect(await iSourcelmt.getLabel("Owner") as string,await iSourcelmt.getLabel(eventStatus) as string);
    // }
    static async applyingFilter(eventType : string,eventStatus : string) {
        await Filter.applyFilter(await iSourcelmt.getLabel( "OwnerLabel" ) as string);
        await Checkbox.selectAll(await iSourcelmt.getLabel( "OwnerLabel" ) as string);
        await Filter.applyFilter(await iSourcelmt.getLabel( "TypeLabel" ) as string);
        await Checkbox.searchSelect(await iSourcelmt.getLabel( "TypeLabel" ) as string,await iSourcelmt.getLabel( eventType ) as string );
        await Filter.applyFilter(await iSourcelmt.getLabel( "StatusLabel" ) as string);
        await Checkbox.searchSelect(await iSourcelmt.getLabel( "StatusLabel" ) as string,await iSourcelmt.getLabel( eventStatus ) as string);
    }

    static async searchEvent(eventID : string){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT); 
        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search Events")as string,eventID,await iSourcelmt.getLabel("EventColumnName")as string);
    await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await I.see(eventID);
    }
}
