export let ShareEventLocator = {
    SecondaryActionButton : "ListingPage/SecondaryActionButton",
    clickButton : "ListingPage/SecondaryAction/clickButton",
    filterIcon : "ListingPage/filterIconClick",
    SearchInFilter : "ListingPage/filterIcon/SearchBar",
    clearAllButton : "ListingPage/filterIcon/clearAllButton",
    applyButton : "ListingPage/filterIcon/applyButton",
    eventType : "ListingPage/filterIcon/eventType",
    eventStatus : "ListingPage/filterIcon/eventStatus",
    checkLabel : "ListingPage/filterIcon/checkLabel"
    
}