import "codeceptjs";
const { I } = inject();

import { ScoreSheetLocators } from "./ScoreSheetLocators";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";

import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { TextArea } from "dd-cc-zycus-automation/dist/components/textArea";
import { DewDropdown } from "dd-cc-zycus-automation/dist/components/dewDropdown";

export class WhatIfAnalysisFlow {

  static async clickOnScoresheet() {

    await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScoresheetLabelKey") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)

    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ManageScoresheetsLabelKey") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)
  }

  static async clickOnWhatIfAnalysis() {
    await CommonKeyword.clickElement(Startup.uiElements.get(ScoreSheetLocators.WhatIfAnalyis) as string);
    I.wait(prop.DEFAULT_LOW_WAIT)

    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("CreateScenarioLAbelKey") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)
  }

  static async fillScenarioDetails() {
    await DewDropdown.selectFirst(Startup.uiElements.get(ScoreSheetLocators.SelectSenario) as string)
    I.wait(prop.DEFAULT_LOW_WAIT)

    await DewDropdown.selectFirst(Startup.uiElements.get(ScoreSheetLocators.SelectScoresheet) as string)
    I.wait(prop.DEFAULT_LOW_WAIT)

    await I.fillField(Startup.uiElements.get(ScoreSheetLocators.SenarioName), Startup.testData.get("WhatIfScenarioName"));
    //textarea textfield

    await TextArea.enterTextWithPlaceHolder("Type here…", Startup.testData.get("Description") as string);
    //await CommonKeyword.clickElement(Startup.uiElements.get(ScoreSheetLocators.CreateButton) as string);
    await CommonKeyword.clickLabel( await iSourcelmt.getLabel('CreateLabelKey') as string)
    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_LOW_WAIT)

    await DewElement.checkIfElementPresent(await iSourcelmt.getLabel("CumulativeScoresheet") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)
  }


  static async seeScoresandSupplierAdvantage() {
    I.wait(prop.DEFAULT_LOW_WAIT)
    await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("QuestionsNavLabelKey") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)
    await DewElement.checkIfElementPresent(Startup.uiElements.get(ScoreSheetLocators.Questions) as string);

  }

  static async changeScore() {
    I.wait(prop.DEFAULT_LOW_WAIT)
    await I.fillField(Startup.uiElements.get(ScoreSheetLocators.FillScore), Startup.testData.get("NewScore"));
    I.wait(prop.DEFAULT_LOW_WAIT)

    let value = await I.grabValueFrom(Startup.uiElements.get(ScoreSheetLocators.FillScore)as string)
    if((value).includes(Startup.testData.get("NewScore"))){
      console.log("Score changed")
    }
  }
  static async changeSupplierAdvantages() {
    I.wait(prop.DEFAULT_LOW_WAIT)

    await I.dragSlider(await CommonFunctions.getFinalLocator(Startup.uiElements.get(ScoreSheetLocators.Slider) as string, await iSourcelmt.getLabel('SupplierAdvantageKey') as string), 13);
    I.wait(prop.DEFAULT_LOW_WAIT)

  }

  static async saveScore() {

    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveLabelKey") as string)

    iSourcelmt.waitForLoadingSymbolNotDisplayed();
    I.wait(prop.DEFAULT_LOW_WAIT)
    await DewElement.checkIfElementPresent(Startup.uiElements.get(ScoreSheetLocators.NewScore) as string);
  }

}

