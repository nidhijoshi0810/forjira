
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {coe} from "../../../Framework/FrameworkUtilities/COE/COE";
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {eventSettingsFlow} from "../eventSettings/eventSettingsFlow.js";
import {DatePickerFlow} from "../../../Framework/FrameworkUtilities/COE/DatePicker/DatePickerFlow";

import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import {logger} from "./../../../Framework/FrameworkUtilities/Logger/logger";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker";
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element"; //clickElement
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";  
import { CreateScoreSheetLocator } from "./CreateScoreSheetLocator";
import {TextField} from "dd-cc-zycus-automation/dist/components/textfield"


export class PublishScoreSheetFlow{


    static async clickOnCreateScoreSheetbutton() { 
        
       
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("EventDetailsTab") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScoreSheetLabelKey") as string);
        //await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("CreateScoreSheetButton") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
     }

    static async changeScoresheetName(){ 
       
        I.wait(prop.DEFAULT_LOW_WAIT);
        // let scoresheet : string = " Vaishnavi"; 
        // I.clearField(Startup.uiElements.get(CreateScoreSheetLocator.ScoreSheetNameInputBox) as string);              
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.ScoreSheetNameInputBox) as string , Startup.testData.get("ScoreSheetName") as string);
        
     }
    static async changeScoresheetDescription(){ 
        I.wait(prop.DEFAULT_LOW_WAIT);
        await TextField.enterTextUsingLocator((Startup.uiElements.get(CreateScoreSheetLocator.DescriptionTextBox) as string), Startup.testData.get("DescriptionTextBox") as string);
        
     }
    static async setMaxScoreValue(){ 
        
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.MaximumScoreDropdown) as string));
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.MaximumScoreValue) as string ).replace("<<dataname>>",(( Startup.testData.get("MaxScore")) as string)));

     }
    static async setScoringTypeAsQuantitative(){ 
        
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SetDefaultScoringPatternButtonLabelKey") as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ApplyLabelKey") as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("YesLabelKey") as string);
        
     }
     static async setScoringTypeAsQualitativeIndividually(){
        I.wait(prop.DEFAULT_LOW_WAIT);
        logger.info("   clicked on tab  "+await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);;
        let x : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox) as string));
        for(var i : number = 1; i <=x; i++){
            await CommonKeyword.clickElement(((Startup.uiElements.get(CreateScoreSheetLocator.SectionScoringPatternDropdown)) as string)+"["+i+"]");
            await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.ChangeSectionScorePattern) as string);
        }
        await this.saveScoreSheetOnMainPage();
        await this.setScoringTypeAsQuantitative();
     }


     static async setScoringTypeAsQualitative(){ 
        
        I.wait(prop.DEFAULT_LOW_WAIT);
        logger.info("   clicked on tab  "+await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        let x : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox) as string));
        console.log("last row no is "+x);
        await CommonKeyword.clickElement(((Startup.uiElements.get(CreateScoreSheetLocator.SectionScoringPatternDropdown)) as string)+"["+x+"]");
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.ChangeSectionScorePattern) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("SetScorerRulesLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.DeleteLastQualitativeLabel)) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);

        await this.duplicateQualitativeLabelHandling();

        for(var i : number =2; i<=5; i++){
        
            await TextField.enterTextUsingLocator((((Startup.uiElements.get(CreateScoreSheetLocator.QualitativeLabelInputBox)) as string)+"["+i+"]"), "Label"+i);
            await TextField.enterTextUsingLocator((((Startup.uiElements.get(CreateScoreSheetLocator.QualitativeValueInputBox)) as string)+"["+i+"]"), i  as any);
            
            if(i==5)
            {
                break;
            }
            I.wait(prop.DEFAULT_LOW_WAIT);
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddNewQualitativeLabel") as string);
            I.wait(prop.DEFAULT_LOW_WAIT);
        }
        I.wait(prop.DEFAULT_LOW_WAIT);

     }

     static async duplicateQualitativeLabelHandling(){
         var errorMessage : any = await DewElement.grabTextFrom((Startup.uiElements.get(CreateScoreSheetLocator.SameQualitativeLabelErrorMessage)) as string);
         if((errorMessage.toString())===(await iSourcelmt.getLabel("QualitativeLabelErrorMessage") as string))
         {
            console.log("dupicae qualitative label handling is done successfully "+errorMessage);
         }
     }


    static async saveScoreSheetOnMainPage(){ 

        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        
     }

     static async validateChangesOnMainScoreSheetPage(){

        var maxScore : string = await DewElement.grabValueFrom((Startup.uiElements.get(CreateScoreSheetLocator.MaximumScoreDropdown)) as string);
        var lastSectionScoringType : string = await DewElement.grabValueFrom("("+((Startup.uiElements.get(CreateScoreSheetLocator.SectionScoringPatternDropdown)) as string)+")[last()]");
        var scoreSheetDescription : string = await DewElement.grabValueFrom((Startup.uiElements.get(CreateScoreSheetLocator.DescriptionTextBox)) as string);

        if((maxScore.toString())===((Startup.testData.get("MaxScore")) as string))
        {
            console.log("Value of Maximum Score is set correctly");
        }
        if((lastSectionScoringType.toString())===(await iSourcelmt.getLabel("QualitativeLabelKey") as string))
        {
            console.log("Scoring pattern of lasy section is set to Qualitative");
        }
        if((scoreSheetDescription.toString())===(Startup.testData.get("DescriptionTextBox") as string))
        {
            console.log("The scoresheet description has been set successfully");
        }

     }

    static async assignSectionWeightages(){
        
        await this.saveScoreSheetOnMainPage();
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        let no_weightages : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox)) as string);
        console.log("No of weightage boxes are "+no_weightages);
        let remainder : number = 100 % no_weightages;
        let cal : number = 100 - remainder;
        let eachweightage : number = cal/no_weightages;
        let firstboxweightage : number = eachweightage + remainder;
        let x : number = no_weightages-(no_weightages-1);
        
        for (let i : number = 1; i <= no_weightages; i++) {

            if((100%no_weightages)==0){
                await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox)) as string)+"["+i+"]", eachweightage  as any);
                I.wait(prop.DEFAULT_LOW_WAIT);
            }
            else {
                await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox)) as string)+"["+i+"]", eachweightage as any);
                await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox)) as string)+"["+x+"]", firstboxweightage  as any);
                I.wait(prop.DEFAULT_LOW_WAIT);
            }
        }

        await this.saveScoreSheetOnMainPage();

    }

    static async validateRemainingSectionWeightage(){

        var remainingSectionWeightage : string = await DewElement.grabValueFrom((Startup.uiElements.get(CreateScoreSheetLocator.RemainingSectionWeightage)) as string);
        
        if((remainingSectionWeightage.toString())===((await iSourcelmt.getLabel("RemainingSectionWeightage") as string)+" : 0.0"))
        {
            console.log("Section weightages have been set succesfully"); 
        }
        else{
            console.log("Remaining weighatges are "+remainingSectionWeightage);
        }

    }

    static async assignQuestionWeightages(){
        
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
      
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        console.log("click on assg tab on pag top"); 
        I.wait(prop.DEFAULT_LOW_WAIT);
        let no_weightages : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SectionWeightagesInputBox)) as string);
        console.log(no_weightages);

        for (let i : number = 1; i <=no_weightages; i++) {
            I.wait(prop.DEFAULT_LOW_WAIT);
            await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string)
            await CommonKeyword.clickElement(((Startup.uiElements.get(CreateScoreSheetLocator.AssignQuestionWeightagesLink)) as string)+"["+i+"]"); 
            I.wait(prop.DEFAULT_LOW_WAIT); 
            
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            await this.individualQuestionWeightage();

            let rowNum : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string);
            
            let remainder : number = 100%rowNum;
            let cal : number = 100 - remainder;
            var eachQuesWeightage : number = cal/rowNum;
            
            I.wait(prop.DEFAULT_LOW_WAIT);
            let questionType : string;
            
            var noOfCommentQues : number=0;

            for(let j : number = 1; j <= rowNum; j++)
            {
                
                questionType = await DewElement.grabTextFrom(((Startup.uiElements.get(CreateScoreSheetLocator.TypeOfQuestion)) as string)+"["+j+"]");
               
                if(questionType==(await iSourcelmt.getLabel("QuestionType_Default") as string))
                {
                        await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                        await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                        
                }
                else if(questionType==(await iSourcelmt.getLabel("QuestionType_Numeric") as string))
                {
                    
                    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetScoreRulesLink) as string).replace("<<index>>", String(j) )), await iSourcelmt.getLabel("SetScoreRulesLinkKey") as string));
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.NumericSetRuleMinScoreValue)) as string), Startup.testData.get("SetRulesScoreValue1") as any);
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.NumericSetRuleMaxScoreValue)) as string), Startup.testData.get("SetRulesScoreValue2") as any);
                    await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUpSaveButton)) as string);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                }
                else if(questionType==(await iSourcelmt.getLabel("QuestionType_MultiChoice") as string))
                { 
                    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetScoreRulesLink) as string).replace("<<index>>", String(j) )), await iSourcelmt.getLabel("SetScoreRulesLinkKey") as string));
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp1)) as string), Startup.testData.get("SetRulesScoreValue1") as any);
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp2)) as string), Startup.testData.get("SetRulesScoreValue2") as any);
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp3)) as string), Startup.testData.get("SetRulesScoreValue3") as any);
                    await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUpSaveButton)) as string);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                }
                else if(questionType==(await iSourcelmt.getLabel("QuestionType_YesNo") as string))
                {
                    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetScoreRulesLink) as string).replace("<<index>>", String(j) )), await iSourcelmt.getLabel("SetScoreRulesLinkKey") as string));
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp1)) as string), Startup.testData.get("SetRulesScoreValue1") as any);
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp2)) as string), Startup.testData.get("SetRulesScoreValue2") as any);
                    await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUpSaveButton)) as string);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                }
                else if(questionType==(await iSourcelmt.getLabel("QuestionType_SingleChoice") as string))
                {
                    await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetScoreRulesLink) as string).replace("<<index>>", String(j) )), await iSourcelmt.getLabel("SetScoreRulesLinkKey") as string));
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp1)) as string), Startup.testData.get("SetRulesScoreValue1") as any);
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp2)) as string), Startup.testData.get("SetRulesScoreValue2") as any);
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUp3)) as string), Startup.testData.get("SetRulesScoreValue3") as any);
                    await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.SetRulesPopUpSaveButton)) as string);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                }
                else if(questionType==(await iSourcelmt.getLabel("ScoreSheetQuesTypeWeightage") as string))
                {
                    logger.info("inside attachments" + questionType);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                    
                }
                else if(questionType==(await iSourcelmt.getLabel("QuestionType_ItemTable") as string))
                {   
                    logger.info("inside item table"  + questionType);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                    
                }
                else if(questionType==(await iSourcelmt.getLabel("QuestionType_Table") as string))
                {
                    logger.info("inside table" + questionType);
                    await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]");
                    await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+j+"]", eachQuesWeightage as any);
                    
                } 
                else if(questionType.trim().toString()==(await iSourcelmt.getLabel("QuestionType_Comment") as string))
                {
                    
                    noOfCommentQues=noOfCommentQues+1;
                }
                if(noOfCommentQues<=0)
                {
                    if(((rowNum%2)!=0)){
                        logger.info("when odd quest with no comments"  );
                        let x=(rowNum-(rowNum-1));
                        let y=(eachQuesWeightage+remainder);
                        await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]");
                        await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]", y as any);
                        I.wait(prop.DEFAULT_LOW_WAIT);;
                    }
                    else if(((rowNum%2)==0)){
                        logger.info("when even quest with no comments"  );
                        let x=(rowNum-(rowNum-1));
                        let y=(eachQuesWeightage+remainder);
                        await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]");
                        await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]", y  as any);
                        I.wait(prop.DEFAULT_LOW_WAIT);;
                    }
                }
                else{
                    
                    if(((rowNum%2)!=0)){
                        logger.info("when odd quest with comments");
                        let x : number =(rowNum-(rowNum-1));
                        let y : number =((eachQuesWeightage*(noOfCommentQues+1))+remainder);
                        await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]");
                        await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]", y as any);
                        I.wait(prop.DEFAULT_LOW_WAIT);;
                    }
                    else if(((rowNum%2)==0)){
                        logger.info("when even quest with comments");
                        let x : number =(rowNum-(rowNum-1));
                        let y : number =((eachQuesWeightage*(noOfCommentQues+1))+remainder);
                        await I.clearField(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]");
                        await TextField.enterTextUsingLocator(((Startup.uiElements.get(CreateScoreSheetLocator.EachQuestionWeightageTextBox)) as string)+"["+x+"]", y as any);
                        I.wait(prop.DEFAULT_LOW_WAIT);;
                    }
                }                
            }
 
                await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveLabelKey") as string);
                await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.BackToCreateSSPageArrow)) as string);

        }

    }

    static async validateIndividualSectionWeightage(){
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        let SectionWeightageStatus : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.IndividualSectionWeightageStatus)) as string);
            for(var i : number = 1; i <= SectionWeightageStatus; i++ ){
                var IndividualSectionStatus : string = await DewElement.grabTextFrom(((Startup.uiElements.get(CreateScoreSheetLocator.IndividualSectionWeightageStatus)) as string)+"["+i+"]");
                if((IndividualSectionStatus.trim().toString())===(await iSourcelmt.getLabel("IndividualSectionStatus") as string))
                {
                    console.log("Individual Section weightages have been completed succesfully "+IndividualSectionStatus);
                }
            }
    }
    static async individualQuestionWeightage(){
        //await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.AssignEqualWeightageToQuestion) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AssignEqualWeightageToQuestion") as string);
        var remainingQuestionWeightage : string = await DewElement.grabTextFrom((Startup.uiElements.get(CreateScoreSheetLocator.RemainingIndividualQuesWeightage)) as string);
        if((remainingQuestionWeightage.toString())===((await iSourcelmt.getLabel("RemainingSectionWeightage") as string)+" : 0.0"))
        {
            console.log("Question weightages have been set succesfully"); 
        }
        else{
            console.log("Remaining weighatges are "+remainingQuestionWeightage);
        }

    }

    static async selectScorers()
    {
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.AddScorersFirstLink) as string);
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.SearchScorerTextBox) as string, Startup.testData.get("SelectScorersFirstTime") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SearchScorerEveryWhere) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorersCheckBox) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.AddScorersToAllSections) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddScorersButtonLabelKey") as string);
        
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.AddScorersFirstLink) as string);
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.SearchScorerTextBox) as string, Startup.testData.get("SelectScorersSecondTime") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SearchScorerEveryWhere) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorersCheckBox) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectedScorersLink) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddScorersButtonLabelKey") as string);
        
    
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        await this.saveScoreSheetOnMainPage();
        

    }
    static async deselectScorers()
    {
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.AddScorersFirstLink) as string);
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.SearchScorerTextBox) as string, Startup.testData.get("DeselectScorers") as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SearchScorerEveryWhere) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorersCheckBox) as string);
        //await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.ShowAllScorersLink) as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectedScorersLink) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("AddScorersButtonLabelKey") as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        await this.saveScoreSheetOnMainPage();

    }

    static async validateScorersAddition(){

        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AssignWeightagesAndScorersLabelKey") as string);
        //await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.AddScorersFirstLink) as string);
        var noOfScorers : string = await DewElement.grabTextFrom(Startup.uiElements.get(CreateScoreSheetLocator.AddScorersFirstLink) as string);
        if((noOfScorers.toString())!==(await iSourcelmt.getLabel("AddScorersLabelKey") as string))
        {
            console.log("Scorers addition is successful and no is "+noOfScorers);
        }

       
        
    } 

    static async publishScoreSheet(){

            I.wait(prop.DEFAULT_LOW_WAIT);
            await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScheduleLabelKey") as string);

            await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.EndDateTime) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.NextMonth) as string);
            await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.DateOfPublish) as string);

            let currentDate:string = await this.getTodaysDateInMMDDYYYYFormat();
            let currentTimePlusMinutes:string = await this.addMinutesToCurrentDate(3);

            await DatePicker.selectDateAndTime((Startup.uiElements.get(CreateScoreSheetLocator.StartDateTime) as string), currentDate, currentTimePlusMinutes);
           
            // var startDate : any = eventSettingsFlow.getCurrentDate(1);
            // DatePickerFlow.setDate(Startup.uiElements.get(CreateScoreSheetLocator.StartDateTime) as string,startDate);
            //await this.saveScoreSheetOnMainPage();
        
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("PublishLabelKey") as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SendLabelKey") as string);
            await iSourcelmt.waitForLoadingSymbolNotDisplayed();
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);


    }
    static async getTodaysDateInMMDDYYYYFormat(){
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth()+1; 
        let yyyy = today.getFullYear();
        if(dd<10) 
        {
            dd=parseInt('0'+dd.toString());
        } 
        
        if(mm<10) 
        {
            mm=parseInt('0'+mm.toString());
        } 
        console.log(mm+'/'+dd+'/'+yyyy);
        return mm+'/'+dd+'/'+yyyy;  
    }

    static async addMinutesToCurrentDate(minutesToAdd:number) {
                let d = new Date();
                let h = d.getHours();
                let m = d.getMinutes() + minutesToAdd;
                if(m>59)
                {
                    h=h+1;
                    m=m-60;
                    return  h.toString() + ':' + m.toString();
                } 
                return  h.toString() + ':' + m.toString();    
    } 
    
    static async validateWhetherOnEventDetailsPage(){
        
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("Questionnaires") as string);
        console.log("Score Sheet is published successfully");

    }

}
//module.exports=new CreateScoreSheetFlow()

// new line



