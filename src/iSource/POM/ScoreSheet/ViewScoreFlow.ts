import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
// import commondd from "dd-cc-zycus-automation/components/CommonKeyword";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import {CreateScoreSheetLocator} from "./CreateScoreSheetLocator";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
//import { getLabel } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter";
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup"
import {DewButton} from "dd-cc-zycus-automation/dist/components/dewButton";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";
import {DewElement} from "dd-cc-zycus-automation/dist/components/element";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import {TextField} from "dd-cc-zycus-automation/dist/components/textfield"

export class ViewScoreFlow{

    static async clickOnViewScoreButton() { 

        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.ViewScoreButton) as string).replace("<<dataname>>",(await iSourcelmt.getLabel("ViewScoreLinkLabelKey") as string)));
    }

    static async checkViewScore() {
        logger.info("after view scores");
        I.wait(prop.DEFAULT_WAIT);
        
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('WeightedScoreLabel') as string);
        await DewElement.verifyIfISeeText(await iSourcelmt.getLabel('SuppliersTotalScoresLabel') as string);
    }

    static async clickOnAbsoluteScoreTab() { 

        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("AbsoluteScoreLabelKey") as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);

    }
    static async clickOnWeightedScoreTab() { 

        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("WeightedScoreLabelKey") as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);

    }
    
    static async clickOnSettingsIcon() { 

        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIcon) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIconSearchBox) as string, Startup.testData.get("SearchRandomText") as any);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SearchByScorerName) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        I.wait(10);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("OKButton") as string);
        // await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIconCross) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);

    }

    static async remindScorersWhoHaveNotScored(){

        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        // await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIcon) as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorersCheckBox) as string);
        //I.click("//dew-popover-body//dew-checkbox//input[contains(@id,'selectAllScorers')]");
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("RemindScorerLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("sendLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        //CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIcon) as string);
    }
    
    static async useSearchTextBox(){

        I.wait(prop.DEFAULT_LOW_WAIT);
        //CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIcon) as string);
        //I.clearFieldI(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIconSearchBox));
        await TextField.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIconSearchBox) as string, Startup.testData.get("SelectScorersFirstTime") as any);
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SearchByScorerName) as string);
        I.wait(prop.DEFAULT_LOW_WAIT);
        var x : number = await DewElement.grabNumberOfVisibleElements(await CommonFunctions.getFinalLocator(Startup.uiElements.get(CreateScoreSheetLocator.buttonXpath) as string, await iSourcelmt.getLabel("OKButton")as string));
        if(x>0)
        {
            await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("OKButton") as string);
        }
        
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIconCross) as string);
        //CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIcon) as string);
    

    }
    static async applyResetButtons(){

        //CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SettingsIcon) as string);
        // if (await Checkbox.iSCheckBoxSelected((Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorersCheckBox))as string))
        // {
        //     console.log("dont check");
        // }
        // else
        // {
        //     await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorersCheckBox))as string);
        // }
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ApplyLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ResetScorerListViewScoreLabelKey") as string);
        await I.refreshPage();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        
    }  
    static async exportScoreButton() { 
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(10);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ExportButtonLabelKey") as string);
        logger.info(" export " + (await iSourcelmt.getLabel("ExportButtonLabelKey") as string));
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.backArrowOnViewScorePage) as string);
        
    }  

    static async revokeScore() {
        I.wait(prop.DEFAULT_WAIT)
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT)
        await ViewScoreFlow.selectRevokeScoreOption();
        await ViewScoreFlow.selectScorer();
        await ViewScoreFlow.sendRevokeMail();
    }

    static async selectRevokeScoreOption(){
        I.wait(prop.DEFAULT_WAIT)
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SecondaryMenu) as string)
        I.wait(prop.DEFAULT_WAIT)
        await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator((Startup.uiElements.get(CreateScoreSheetLocator.RevokeScoresLabel) as string), await iSourcelmt.getLabel("RevokeScoresDropdownOption") as string));     //RevokeScoresDropdownOption
    }

    static async selectScorer(){
        logger.info("inside 2nd block");
        await TextField.enterTextUsingLocator((Startup.uiElements.get(CreateScoreSheetLocator.RevokeScoreSearchTextBox) as string), Startup.testData.get("SelectScorersFirstTime") as any);
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.RevokeScoreSearchEverywhere)as string);
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SelectAllScorer) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('DoneButtonLabel')as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }   
     
    static async sendRevokeMail(){
        logger.info("inside 3rd block");
        I.wait(prop.DEFAULT_WAIT); 
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SendButtonLabel") as string);
        I.wait(prop.DEFAULT_WAIT)
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);

    }
    static async exportImportScoreButton(){
        
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT)
        logger.info(" import export"+(await iSourcelmt.getLabel("ImportExportLableKey")as string));
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ImportExportLableKey")as string)
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.ViewScoreExportIcon) as string);
        await CommonKeyword.clickElement(Startup.testData.get("CrossImpExpOnManageSSPage") as string);
    } 


}
//module.exports=new ViewScoreFlow()