import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
//D:\QC_Intermediate_TS\node_modules\dd-cc-zycus-automation
import {iSourcelmt} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
//import {CommonKeyword} from "dd-cc-zycus-automation/components/CommonKeyword";
//import {dropdown} from "dd-cc-zycus-automation/components/dewDropdown";
import {COE} from "../../../Framework/FrameworkUtilities/COE/COE";
import {CreateScoreSheetLocator} from "./CreateScoreSheetLocator";
import {CommonFunctions} from "../../isourceCommonFunctions/CommonFunctions";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { commonLandingFlow } from "../../../Framework/FrameworkUtilities/COE/CommonLandingPage/commonLandingFlow";
import {HeaderFilter} from "dd-cc-zycus-automation/dist/components/headerFilter";
import {DewButton} from "dd-cc-zycus-automation/dist/components/dewButton";
import {TextArea} from "dd-cc-zycus-automation/dist/components/textArea";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";
// import {getLabel} from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
// import logout from "../../POM/SupplierLogin/SupplierLoginFlow";
import {logger} from "../../../Framework/FrameworkUtilities/Logger/logger";
import {TextField} from "dd-cc-zycus-automation/dist/components/textfield"
import { ScoreSheetLocators } from "./ScoreSheetLocators";

// import elements from "../../POM/SurrogateBid/SurrogateBidLocators";

export class SubmitScoresFlow{

    static async clickOnManageScoresheetButton() {

        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScoreSheetLabelKey") as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ManageScoresheetsLabelKey") as string);

    }

    static async clickOnScoringButton() {

        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.testData.get("StartScoringButtonXpath") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();

    }

    static async clickOnClearScoreButton() {

        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ClearScoreLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("YesLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
         
    }

    // static async scoreResponses() {

    //     await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    //     I.wait(prop.DEFAULT_MEDIUM_WAIT);
    //     await this.collapseSection();
    //     await this.expandSection();
    //     await this.quantitativeScoring();
    //     await this.qualitativeScoring();
        
    // }

    static async collapseSection(){
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        let noOfDroparrows : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SubmitScoreSectionCollapseArrow))as string)
        logger.info(noOfDroparrows);
        for(var i : number =1; i <=noOfDroparrows; i++)
        {
            await CommonKeyword.clickElement("("+(Startup.uiElements.get(CreateScoreSheetLocator.SubmitScoreSectionCollapseArrow) as string)+")["+i+"]");
        }
    }

    static async expandSection(){

        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        let noOfDroparrows : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SubmitScoreSectionCollapseArrow))as string)
        for(var i : number =noOfDroparrows; i <=noOfDroparrows; i--)
        {   
            if(i==0){
                break;
            }
            await CommonKeyword.clickElement("("+(Startup.uiElements.get(CreateScoreSheetLocator.SubmitScoreSectionCollapseArrow) as string)+")["+i+"]");
            
        }
    }

    static async qualitativeScoring(){

        I.wait(prop.DEFAULT_MEDIUM_WAIT);

        let noOfDropdowns =new Array(await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.SetQualitativeLabelScore))as string));

        if(noOfDropdowns.length>0){
        console.log("value of dropdwon box "+noOfDropdowns.length);
        for(let k=1; k<=2; k++)
        {   
            for(var i=1; i<=noOfDropdowns.length; i++)
            {
                await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.SetQualitativeLabelScore) as string)+"["+i+"]");
                await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.SetQualitativeLabel3) as string);
                
            }
            // await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.RightArrowOnScoringPage) as string);
        }
        }
        await this.clickOnSaveSubmittedScoreButton();
    } 
    
    static async quantitativeScoring(){

        I.wait(prop.DEFAULT_MEDIUM_WAIT);

        let noOfInputBoxes =new Array(await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(CreateScoreSheetLocator.EnterManualScoreInputBox))as string));
        logger.info(noOfInputBoxes.length +" input boxes");
        if(noOfInputBoxes.length>0){
            
            var a : Boolean= false; 
        for(let i=1; i<=2; i++)
        {  logger.info("outside inner for loop");
            for(var j=1; j<=noOfInputBoxes.length; j++)
            {
                a = await DewElement.grabAttributeFrom(("("+(Startup.uiElements.get(CreateScoreSheetLocator.EnterManualScoreInputBox) as string)+")["+j+"]"), "disabled");
                logger.info(a);
                if(a==false)
                {   
                    console.log(a);
                    await CommonKeyword.clickElement(("("+(Startup.uiElements.get(CreateScoreSheetLocator.EnterManualScoreInputBox)as string)+")["+j+"]"));
                    I.wait(prop.DEFAULT_LOW_WAIT);
                    I.clearField(("("+(Startup.uiElements.get(CreateScoreSheetLocator.EnterManualScoreInputBox))+")["+j+"]"));
                    await TextField.enterTextUsingLocator(("("+(Startup.uiElements.get(CreateScoreSheetLocator.EnterManualScoreInputBox))+")["+j+"]"), Startup.testData.get("MaxScore") as any);
                }
            }
            // await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.RightArrowOnScoringPage) as string);
        }
        }
        await this.clickOnSaveSubmittedScoreButton();
    }
    
    static async clickOnSaveSubmittedScoreButton() {

        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SaveLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        
    }

    static async viewFullItemTable(){
        
        let noOfItemTables =new Array(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(CreateScoreSheetLocator.ViewFullItemTable) as string));
        for(var i : number =0; i<=noOfItemTables.length; i++){
            if(i==1){
        I.scrollIntoView((Startup.uiElements.get(CreateScoreSheetLocator.ViewFullItemTable) as string)+"["+i+"]");
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.ViewFullItemTable) as string)+"["+i+"]");
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.BackToCreateSSPageArrow) as string);
            }
        }
    }

    static async viewFullTable(){
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let noOfTables =new Array(await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(CreateScoreSheetLocator.ViewFullTable) as string));
        for(var i : number =0; i<=noOfTables.length; i++){
            if(i==1){
        I.scrollIntoView((Startup.uiElements.get(CreateScoreSheetLocator.ViewFullTable) as string)+"["+i+"]");
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.ViewFullTable) as string)+"["+i+"]");
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.BackToCreateSSPageArrow) as string);
            }
        }
    }

    static async readSupplierComment(){

        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT);
        var x = await CommonFunctions.getFinalLocator(Startup.uiElements.get(CreateScoreSheetLocator.ShowCommentsLink) as string,(await iSourcelmt.getLabel("ShowComments")as string))
        console.log(x +"xpath");
        let noOfComments =new Array(await DewElement.grabNumberOfVisibleElements(x));
        for(var i : number =1; i<=noOfComments.length; i++){
            if(i==1){

        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ShowComments")as string)
        I.wait(prop.DEFAULT_WAIT);
        I.refreshPage();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        break;
        }
    }
    }

    static async enterScorerComment(){
        
        var x = (Startup.uiElements.get(CreateScoreSheetLocator.scorerCommentlink) as string);
        let manualScorerRuleComment =new Array(await DewElement.grabNumberOfVisibleElements(x));
        for(var i : number =1; i<=manualScorerRuleComment.length; i++){
            if(i==1){
        I.scrollIntoView((Startup.uiElements.get(CreateScoreSheetLocator.scorerCommentlink) as string));
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.scorerCommentlink) as string));
        await TextArea.enterTextUsingLocator(Startup.uiElements.get(CreateScoreSheetLocator.ScorerCommentTextbox)as string,'qwerty');
        I.wait(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement((Startup.uiElements.get(CreateScoreSheetLocator.FooterButtonOnScorerCommentPopUp)as string));
        break;
            }

        }
        
    }

    static async viewAutomatedScoreRule(){
        var x = (Startup.uiElements.get(CreateScoreSheetLocator.ViewAutomatedRuleIcon) as string);
        let automatedRulesIcon =new Array(await DewElement.grabNumberOfVisibleElements(x));
        for(var i : number =1; i<=automatedRulesIcon.length; i++){
            if(automatedRulesIcon.length>0){
        I.scrollIntoView(Startup.uiElements.get(CreateScoreSheetLocator.ViewAutomatedRuleIcon));
        await CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.ViewAutomatedRuleIcon) as string);
        //await DewElement.checkIfElementVisible(Startup.uiElements.get(CreateScoreSheetLocator.SupplierCommentHeader)as string);
        I.wait(prop.DEFAULT_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('DoneButtonLabel')as string);
        break;
            }
        }
    }
    static async searchBySectionName() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search")as string,"Added by automation",await iSourcelmt.getLabel("SectionName")as string);
        I.refreshPage();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }

    static async searchByQuestionName() {
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
        await HeaderFilter.applyGridSearch(await iSourcelmt.getLabel("Search")as string,"Question",await iSourcelmt.getLabel("QuestionName")as string);
        I.refreshPage();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async checkSearchResult(){
        await DewElement.verifyIfISeeText("Added by automation")
        await DewElement.verifyIfISeeText("Question")
    }

    static async submitScoreButton() {
        
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SubmitScoresLabelKey") as string);
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("YesLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        
    }

    static async checkViewResponses() {
        try {
            let text = await Startup.testData.get("defaultQuestionTypeAnswer");
            let numeric = await Startup.testData.get("defaultNumericValue");
            let option = await Startup.testData.get("optionForScoresheet");
            let yes = await Startup.testData.get("yesForScoresheet");
            let itemTable = await Startup.testData.get("fullItemTable");
            await I.scrollIntoView((await Startup.uiElements.get(ScoreSheetLocators.questionText) as string))
            let grabbedText = await DewElement.grabTextFrom((await Startup.uiElements.get(ScoreSheetLocators.questionText) as string));

            await I.scrollIntoView((await Startup.uiElements.get(ScoreSheetLocators.questionNumeric) as string))
            let grabbedNumericFromScoresheet = await DewElement.grabTextFrom((await Startup.uiElements.get(ScoreSheetLocators.questionNumeric) as string));
            let grabbedNumeric = await grabbedNumericFromScoresheet.toString().split(".")[0]

            await I.scrollIntoView((await Startup.uiElements.get(ScoreSheetLocators.questionMultipleChoice) as string))
            let grabbedMultiChoiceFromScoresheet = await DewElement.grabTextFrom((await Startup.uiElements.get(ScoreSheetLocators.questionMultipleChoice) as string))
            let grabbedMultiChoice = await grabbedMultiChoiceFromScoresheet.split(" ")[0]

            await I.scrollIntoView((await Startup.uiElements.get(ScoreSheetLocators.questionSingleChoice) as string))
            let grabbedSingleChoiceFromScoresheet = await DewElement.grabTextFrom((await Startup.uiElements.get(ScoreSheetLocators.questionSingleChoice) as string))
            let grabbedSingleChoice = await grabbedSingleChoiceFromScoresheet.split(" ")[0]

            await I.scrollIntoView((await Startup.uiElements.get(ScoreSheetLocators.questionYesNo) as string))
            let grabbedYesNo = await DewElement.grabTextFrom((await Startup.uiElements.get(ScoreSheetLocators.questionYesNo) as string))

            await I.scrollIntoView((await Startup.uiElements.get(ScoreSheetLocators.questionItemTable) as string))
            let grabbedItemtable = await DewElement.grabTextFrom((await Startup.uiElements.get(ScoreSheetLocators.questionItemTable) as string))


            if (text == grabbedText) {
                logger.info("Success ==>Text Question")
            }
            else {
                logger.info("Failed ==>Text Question")
            }
            if (numeric == grabbedNumeric) {
                logger.info("Success ==>Numeric Question")
            } else {
                logger.info("Failed ==>Numeric Question")
            }
            if (option == grabbedMultiChoice) {
                logger.info("Success ==>Multi choice Question")
            }
            else {
                logger.info("Failed ==>Multi choice Question")
            }
            if (yes == grabbedYesNo) {
                logger.info("Success ==>Yes/No Question")
            } else {
                logger.info("Failed ==>Yes/No Question")
            }
            if (option == grabbedSingleChoice) {
                logger.info("Success ==>Single choice Question")
            }
            else {
                logger.info("Failed ==>Single choice Question")
            }
            if (itemTable == grabbedItemtable) {
                logger.info("Success ==>itemTable")
            }
            else {
                logger.info("Failed ==>itemTable")
            }

        } catch (err) {
            console.log("skipped the view score response check due to the given error ===>"+ err)
        }
    }
}

//module.exports=new SubmitScoresFlow()