import "codeceptjs";
const { I } = inject();


import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import {PublishScoreSheetFlow} from "../../POM/ScoreSheet/PublishScoreSheetFlow";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { ScoreSheetLocators } from "./ScoreSheetLocators";
import { DatePicker } from "dd-cc-zycus-automation/dist/components/datePicker";
import { CreateScoreSheetLocator } from "./CreateScoreSheetLocator";


export class ManageScoresheetFlow {

static async clickOnScoresheet() {

    await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScoresheetLabelKey") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)

    await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ManageScoresheetsLabelKey") as string);
    I.wait(prop.DEFAULT_LOW_WAIT)
  }

    static async remindScorer() {
        await CommonKeyword.clickLabel(await iSourcelmt.getLabel("Remind Scorers") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT)
        await DewElement.checkIfElementPresent(Startup.uiElements.get(ScoreSheetLocators.RemindMail) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async sendReminder() {
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("ReminderSendLabelKey") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async clickOnThreeDotButton() {
        await CommonKeyword.clickElement(Startup.uiElements.get(ScoreSheetLocators.ManageSSDropDown) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
        await DewElement.checkIfElementPresent(Startup.uiElements.get(ScoreSheetLocators.CopyDropdown) as string);
        I.wait(prop.DEFAULT_LOW_WAIT)
    }

    static async copyScoresheet() {
        await CommonKeyword.clickElement(Startup.uiElements.get(ScoreSheetLocators.CopyDropdown) as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT)
        await DewElement.checkIfElementPresent(await iSourcelmt.getLabel("ScheduleLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT)

        await PublishScoreSheetFlow.publishScoreSheet();

    }


    static async clickOnRescheduleDate() {
        await CommonKeyword.clickLabel((await iSourcelmt.getLabel("Reschedule Date") as string));

        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT)
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScheduleLabelKey") as string);
        I.wait(prop.DEFAULT_LOW_WAIT)

    }

    static async rescheduleScoresheet() {

        I.wait(prop.DEFAULT_LOW_WAIT);
            await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScheduleLabelKey") as string);

            CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.EndDateTime) as string);
            CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.NextMonth) as string);
            CommonKeyword.clickElement(Startup.uiElements.get(CreateScoreSheetLocator.DateOfPublish) as string);

            let currentDate:string = await PublishScoreSheetFlow.getTodaysDateInMMDDYYYYFormat();
            let currentTimePlusMinutes:string = await PublishScoreSheetFlow.addMinutesToCurrentDate(3);

            await DatePicker.selectDateAndTime((Startup.uiElements.get(CreateScoreSheetLocator.StartDateTime) as string), currentDate, currentTimePlusMinutes);
           

        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("RescheduleLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SendLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);

    }


    static async clickOnExport() {
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_WAIT)
       // await CommonKeyword.clickElement(Startup.uiElements.get(ScoreSheetLocators.ImportExport) as string);
       await CommonKeyword.clickLabel( await iSourcelmt.getLabel('ExportExcel') as string)

    }

    static async clickOnImport() {
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.wait(prop.DEFAULT_WAIT)
       // await CommonKeyword.clickElement(Startup.uiElements.get(ScoreSheetLocators.ImportExport2) as string);
        await CommonKeyword.clickLabel( await iSourcelmt.getLabel('ImportExcel') as string)


        await I.attachFile("//dew-file-upload//input[@id='file1']", "/DataFiles/iSource/BrowseScoresheet.xlsx");
        I.wait(prop.DEFAULT_MEDIUM_WAIT)
    }

    static async reschedule360ScoreSheet() {
        I.wait(prop.DEFAULT_LOW_WAIT);
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("ScheduleLabelKey") as string);
        let currentDate: string = await PublishScoreSheetFlow.getTodaysDateInMMDDYYYYFormat();
        let currentStartTimePlusMinutes: string = await PublishScoreSheetFlow.addMinutesToCurrentDate(2);
        let currentendTimePlusMinutes: string = await PublishScoreSheetFlow.addMinutesToCurrentDate(3);
        
        await DatePicker.selectDateAndTime((Startup.uiElements.get(CreateScoreSheetLocator.StartDateTime) as string), currentDate, currentStartTimePlusMinutes);
        await DatePicker.selectDateAndTime((Startup.uiElements.get(CreateScoreSheetLocator.EndDateTime) as string), currentDate, currentendTimePlusMinutes);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("RescheduleLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("SendLabelKey") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("DoneLabelKey") as string);
    }


}