import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import {EventLevelAttachmentLocators} from"./EventLevelAttachmentLocators";
import {CommonFunctions} from "../../../iSource/isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {DewFileUpload} from "dd-cc-zycus-automation/dist/components/dewFileUpload";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";

var attachmentBeforeDelete;
var attachmentAfterDelete;
export class AttachmentsFlow{

    static async clickOnAttachmentsTab(){
         I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventLevelAttachmentLocators.tabXpath) as string,await iSourcelmt.getLabel("AttachmentsTabLabel")as string));
      
    }
    static async attachFile(name  : string, filepathAsInput?:string){

        let uploadFailedLabel:string=await iSourcelmt.getLabel("UploadFailedLabelKey")as string;
        if(name != ' '){
            let names : string []=name.split(",");
            if(names.length>1){
            names.forEach(async (filename) => {
                // I.attachFile("//dew-tab[@id='attachments']//dew-add-attachments//dew-file-upload//input[@id='file1']",fileName);
                let filepath : string = filepathAsInput? (filepathAsInput + '/' +  filename) : filename;
                await DewFileUpload.uploadFile(filepath,filename);
                I.wait(prop.DEFAULT_MEDIUM_WAIT);
                I.dontSeeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventLevelAttachmentLocators.uploadFailedXpathInattachment)as string,uploadFailedLabel));
                // I.seeElement(await CommonFunctions.getFinalLocator("(//dew-tab[@id='attachments']//dew-add-attachments//span[contains(@title,'<<dataname>>')])[last()]",fileName));
            });
        }else{
            //I.attachFile(Startup.uiElements.get(EventLevelAttachmentLocators.AttachmentsBrowseButton),"AutomationPresentation.pptx");//selector needs to be typeof `string` or `function`
            let filepath : string = filepathAsInput? (filepathAsInput + '/' +  name) : name;
            await DewFileUpload.uploadFile(filepath,name);
            I.wait(prop.DEFAULT_MEDIUM_WAIT);
            await I.dontSeeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventLevelAttachmentLocators.uploadFailedXpathInattachment)as string,uploadFailedLabel));
          // await I.seeElement("(//dew-tab[@id='attachments']//dew-add-attachments//span[contains(@title,'AutomationPresentation.pptx')])[last()]");
         }
     
        }else{
       //I.attachFile(Startup.uiElements.get(EventLevelAttachmentLocators.AttachmentsBrowseButton),"AutomationPresentation.pptx");//selector needs to be typeof `string` or `function`
       let filepath : string = "/DataFiles/iSource/WORD FILE.docx";
    //    await DewFileUpload.uploadFile(filepath,"WORD FILE.docx");
       I.attachFile("//dew-tab[@id='attachments']//dew-add-attachments//dew-file-upload//input[@id='file1']","/DataFiles/iSource/WORD FILE.docx");
       I.wait(prop.DEFAULT_MEDIUM_WAIT);
   
       await I.dontSeeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(EventLevelAttachmentLocators.uploadFailedXpathInattachment)as string,uploadFailedLabel));
 // await I.seeElement("(//dew-tab[@id='attachments']//dew-add-attachments//span[contains(@title,'AutomationPresentation.pptx')])[last()]");
    }
}

static async downloadMultipleAttachment(){
    attachmentBeforeDelete = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(EventLevelAttachmentLocators.listOfAttachment)as string);

    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.firstAttachment)as string);
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.secondAttachment)as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.downloadAttachment)as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
    // I.amInPath('./DataFiles/iSource');
    // I.seeFileNameMatching('WORD FILE');

}

static async downloadAllAttachment(){

    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.firstAttachment)as string);
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.secondAttachment)as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.downloadAllAttachment)as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
}

static async deleteMultipleAttachment(){
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.firstAttachment)as string);
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.secondAttachment)as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT);
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.deleteAttachment)as string)
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
}

static async deleteAllAttachment(){
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
    await CommonKeyword.clickElement(await Startup.uiElements.get(EventLevelAttachmentLocators.deleteAllAttachment)as string);
    I.wait(prop.DEFAULT_MEDIUM_WAIT)
}

static async verifyDeleteAttachment(value:string){
   attachmentAfterDelete = await DewElement.grabNumberOfVisibleElements(await Startup.uiElements.get(EventLevelAttachmentLocators.listOfAttachment)as string);
    // console.log('attachmentAfterDelete==>',attachmentAfterDelete);
    // console.log('attachmentBeforeDelete==>',attachmentBeforeDelete);
    if(value === 'selective'){
        if(attachmentAfterDelete < attachmentBeforeDelete){
            logger.info('========>Attachment deleted successfully')
        }else{
            logger.info('========>Attachment Not deleted successfully')
        }
    }else{
        if(attachmentAfterDelete < 1){
            logger.info('========>All Attachment deleted successfully')
        }else{
            logger.info('========>Attachment Not deleted successfully')
        }
    }

}



}
// module.exports = new AttachmentsFlow();