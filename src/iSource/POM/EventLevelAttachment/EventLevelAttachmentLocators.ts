export let EventLevelAttachmentLocators={
    tabXpath:"EventDetailsPage/tabXpath",
    AttachmentsBrowseButton:"Attachments And Terms and Conditions/AttachmentsBrowseButton",
    uploadFailedXpathInattachment:"attachment/uploadFailedXpathInattachment",
    listOfAttachment:"attachment/listOfAttachment",
    firstAttachment:"attachment/firstAttachment",
    secondAttachment:"attachment/secondAttachment",
    downloadAttachment:"attachment/downloadAttachment",
    downloadAllAttachment:"attachment/downloadAllAttachment",
    deleteAttachment:"attachment/deleteAttachment",
    deleteAllAttachment:"attachment/deleteAllAttachment"
}