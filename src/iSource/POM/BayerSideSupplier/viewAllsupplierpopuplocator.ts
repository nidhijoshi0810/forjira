const { I } = inject();

export let viewAllSupplierPopuplocator = {
     verifyCopmanyOrContactName:"viewAllsupplierpopup/verifyCompanyNameorContactName",
     viewAllSupplierPopup:"viewAllsupplierpopup/verifyViewAllSupplier",
     deleteSupplier:"viewAllsupplierpopup/deleteSupplier",
     grabFirstElementInSupplier:"viewAllsupplierpopup/grabTheElement",   
     gobacktoAddSupplierPage:"viewAllsupplierpopup/gobacktoAddSupplierPage",
     viewAllSupplierPopupqs:"viewAllsupplierpopup/viewAllSupplierPopupqs"
}