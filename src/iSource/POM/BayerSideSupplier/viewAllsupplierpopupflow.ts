import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { CommonKeyword } from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { DewElement } from "dd-cc-zycus-automation/dist/components/element"
import { Wait } from "dd-cc-zycus-automation/dist/components/dewWait"
import { ModalPopUp } from "dd-cc-zycus-automation/dist/components/modalpopup";
import { Checkbox } from "dd-cc-zycus-automation/dist/components/checkbox";
import { viewAllSupplierPopuplocator } from "./viewAllsupplierpopuplocator"
import { ViewAllSuppliersSteps } from "../../POM/BayerSideSupplier/ViewAllSupplierFlow";


export class ViewAllSupplierPopupflow {
    static async viewAllSupplierPopup() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(viewAllSupplierPopuplocator.viewAllSupplierPopup) as string);
    }
    static async viewAllSupplierPopupQS() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await CommonKeyword.clickElement(await Startup.uiElements.get(viewAllSupplierPopuplocator.viewAllSupplierPopupqs) as string);
    }


    static async searchByCompnyContactName(name: string) {
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        let Search = await iSourcelmt.getLabel("Search") as string;
        let supplierData = await Startup.testData.get("defaultSupplierText") as string;
        let supplierColumn = await iSourcelmt.getLabel(name) as string;
        await ModalPopUp.applyGridSearch(Search, supplierData, supplierColumn);
    }
 

    static async verifyCompnyContactName(name: string) {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await I.seeElement(await Startup.uiElements.get(viewAllSupplierPopuplocator.verifyCopmanyOrContactName) as string);
    }

    static async goBackToAddSupplierPage() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await CommonKeyword.clickElement(await Startup.uiElements.get(viewAllSupplierPopuplocator.gobacktoAddSupplierPage) as string);
    }

    static async removeSupplier() {
        let firstSupplier = await I.grabTextFrom(await Startup.uiElements.get(viewAllSupplierPopuplocator.grabFirstElementInSupplier) as string);
        await CommonKeyword.clickElement(await Startup.uiElements.get(viewAllSupplierPopuplocator.deleteSupplier) as string);
        await Wait.waitForDefaultTimeout(prop.DEFAULT_LOW_WAIT);
        await I.dontSeeElement(firstSupplier);
    }

    static async moveTOViewAllSupplier() {
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await Wait.waitForDefaultTimeout(prop.DEFAULT_WAIT);
        await ViewAllSuppliersSteps.clickOnAddSuppliersTab();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed()
        await ViewAllSuppliersSteps.navigateToViewAllSupliersPopup();
    }

}