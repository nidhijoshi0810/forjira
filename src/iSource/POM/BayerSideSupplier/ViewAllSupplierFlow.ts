import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import "codeceptjs"; 
declare const inject: any; 
const { I } = inject();
import { Locators } from "./ViewAllSupplierLocators";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { COE } from "../../../Framework/FrameworkUtilities/COE/COE";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { logger } from "../../../Framework/FrameworkUtilities/Logger/logger";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import {DewCheckbox} from "dd-cc-zycus-automation/dist/components/dewCheckbox";


export class ViewAllSuppliersSteps{

    static async clickOnAddSuppliersTab(){
        await CommonFunctions.navigateToiosurceTabs(await iSourcelmt.getLabel("addSuppliersLabel") as string);
        // I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.tabXpath) as string,Locators.addSuppliersLabel));
    }

    static async navigateToViewAllSupliersPopup(){
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
        // await CommonKeyword.clickElement(Startup.uiElements.get(Locators.View_all_Suppliers_button) as string);
        /* If supplier shows no records found
        Close supplier pop up
        click on button again*/
        I.wait(prop.DEFAULT_MEDIUM_WAIT);
        const suppcount = await I.grabNumberOfVisibleElements(Startup.uiElements.get(Locators.norecordsfound) as string)
        console.log("Count of grabbed element from the UI is ------------------" + suppcount);
        if (suppcount == 1) {
        await CommonKeyword.clickElement(Startup.uiElements.get(Locators.closeViewSuppliersPopup) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
        }
               await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await I.seeElement(Startup.uiElements.get(Locators.All_Supplier_popup));
    }

    static async selectSuppliersFromGrid(supplierName ?: string){
        let supplierColmnLabel=await iSourcelmt.getLabel("supplierColumnName");
        I.wait(5)
     if(supplierName!="" || supplierName){
        if(!supplierName){
            let suppcount = await I.grabNumberOfVisibleElements(Startup.uiElements.get(Locators.norecordsfound) as string)
            console.log("Count of grabbed element from the UI is while searching supplier ------------------" + suppcount);
            if (suppcount == 1) {
        await CommonKeyword.clickElement(Startup.uiElements.get(Locators.closeViewSuppliersPopup) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
        I.wait(prop.DEFAULT_HIGH_WAIT)
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,supplierName as string,supplierColmnLabel as string);
        }
        else { 
            await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,supplierName as string,supplierColmnLabel as string);}
            I.wait(5)
        await ViewAllSuppliersSteps.selectAllSupplierCheckbox();
        await ViewAllSuppliersSteps.clickOnAddSupplierButton();
        }else{
            let suppcount = await I.grabNumberOfVisibleElements(Startup.uiElements.get(Locators.norecordsfound) as string)
            console.log("Count of grabbed element from the UI is while searching supplier ------------------" + suppcount);
            if (suppcount == 1) {
        await CommonKeyword.clickElement(Startup.uiElements.get(Locators.closeViewSuppliersPopup) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
        I.wait(prop.DEFAULT_HIGH_WAIT)
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,Startup.testData.get("defaultSupplierText") as string,supplierColmnLabel as string);
        }
        else {await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,Startup.testData.get("defaultSupplierText") as string,supplierColmnLabel as string);}
        I.wait(prop.DEFAULT_HIGH_WAIT)   
        await ViewAllSuppliersSteps.selectAllSupplierCheckbox();
        await ViewAllSuppliersSteps.clickOnAddSupplierButton(); 
        }
      }else{
        let suppcount = await I.grabNumberOfVisibleElements(Startup.uiElements.get(Locators.norecordsfound) as string)
        console.log("Count of grabbed element from the UI is while searching supplier ------------------" + suppcount);
        console.log("Count of grabbed element from the UI is while searching supplier ------------------" + suppcount);
        if (suppcount == 1) {
        await CommonKeyword.clickElement(Startup.uiElements.get(Locators.closeViewSuppliersPopup) as string);
        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel('ViewAllSuppliers') as string);
        I.wait(prop.DEFAULT_HIGH_WAIT)
        await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,Startup.testData.get("defaultSupplierText") as string,supplierColmnLabel as string);
        }
        else {await COE.searchOnGridProvidedLocator(Startup.uiElements.get(Locators.searchSuppliersInputOfGridPopup) as string,Startup.testData.get("defaultSupplierText") as string,supplierColmnLabel as string);}
        I.wait(prop.DEFAULT_HIGH_WAIT)
        await ViewAllSuppliersSteps.selectAllSupplierCheckbox();
        await ViewAllSuppliersSteps.clickOnAddSupplierButton();
      }
    }

    static async clickOnAddSupplierButton(){
        I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.AddSupplierBtn) as string,Locators.addSuppliersLabel));
        logger.info("Clicked on Add supplier button.");
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async selectAllSupplierCheckbox(){
        I.wait(5)
        await CommonKeyword.clickElement(Startup.uiElements.get(Locators.Select_all_checkbox_All_Suppliers) as string);
        // I.checkOption(Startup.uiElements.get(Locators.Select_all_checkbox_All_Suppliers));
        //I.seeCheckboxIsChecked(Startup.uiElements.get(Locators.Select_all_checkbox_All_Suppliers));//ask
        logger.info("Select all checkbox is checked.");
    }
    
    static async performPublish(){
        var isRepublishFlow : boolean = false;
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        var buttonElement = await DewElement.grabNumberOfVisibleElements(await COE.replaceValueInAString(Startup.uiElements.get(Locators.publishBtnXpath) as string, await iSourcelmt.getLabel("RepublishEventButtonLabel") as string));
        console.log("buttonElem ::",buttonElement);
        
        isRepublishFlow =  buttonElement > 0 ? true : false;
        console.log("isRepublishFlow ::",isRepublishFlow);

        if(isRepublishFlow){
            var element=await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.publishBtnXpath) as string, await iSourcelmt.getLabel("RepublishEventButtonLabel") as string);
        }else{
            var element=await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.publishBtnXpath) as string,Locators.PublishLabelKey);
        }
        await I.seeElement(element);
        logger.info("Publish button is present.");
        await CommonKeyword.clickElement(element);
        logger.info("Clicked on publish button.");
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        I.wait(5)
       await ViewAllSuppliersSteps.clickOnYesForSupplierValidationPopup();
        await ViewAllSuppliersSteps.clickOnSend(isRepublishFlow);
        logger.info("Event is published.");
        
    }

    static async clickOnSend(isRepublishFlow?:boolean){
        iSourcelmt.waitForLoadingSymbolNotDisplayed()
        if(isRepublishFlow){
            await I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.modalDialogue) as string,await iSourcelmt.getLabel("RepublishEventButtonLabel") as string));
            I.wait(prop.DEFAULT_WAIT);
            await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,await iSourcelmt.getLabel("RepublishLabelKey") as string));
            logger.info("Clicked on send button of Republish email popup.");
        }else{
            await I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.modalDialogue) as string,Locators.PublishLabelKey));
            I.wait(prop.DEFAULT_WAIT);
            await CommonKeyword.clickElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.footerButtonXpath) as string,Locators.sendLabelKey));
            logger.info("Clicked on send button of publish email popup.");
        }
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_LOW_WAIT)
    }
    static async clickOnYesForSupplierValidationPopup(){
        var number=await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(Locators.supplierConfirmationPopup) as string);
        if(number==1){
            I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(Locators.confirmationBtnXpath) as string,Locators.YesLabelKey));
        }
    }
}

// module.exports=new ViewAllSuppliersSteps();