export let Locators = {
    // hhjhg
    tabXpath:"EventDetailsPage/tabXpath",
    searchSuppliersInputOfGridPopup:"ISource/Suppliers_Objects/searchSuppliersInputOfGrid",
    View_all_Suppliers_button:"ISource/Suppliers_Objects/View_all_Suppliers_button",
    All_Supplier_popup:"ISource/Suppliers_Objects/All_Supplier_pop-up",
    ClauseTitlePresentCheck:"Listing/ClauseTitlePresentCheck",
    supplierColumnName :"Supplier Name",
    AddSupplierBtn:"ViewAllSupplier/AddSupplierBtn",
    Select_all_checkbox_All_Suppliers:"ISource/Event_details_Page_After_Freeze/Select_all_checkbox_All_Suppliers",
    addSuppliersLabel:"Add Suppliers",
    PublishLabelKey:"Publish",
    YesLabelKey:"Yes",
    footerButtonXpath:"footer/footerButtonXpath",
    publishBtnXpath:"footer/publishBtnXpath",
    supplierConfirmationPopup:"Publish/emailPopup",
    confirmationBtnXpath:"ConfirmationPopup/confirmationBtnXpath",
    sendLabelKey:"Send",
    modalDialogue:"modalDialogue/modalDialoguepopupTitleXpath",
    norecordsfound:"iSource/Suppliers_Objects/norecordsfound",
    closeViewSuppliersPopup:"iSource/Suppliers_Objects/closeViewSuppliersPopup"
}