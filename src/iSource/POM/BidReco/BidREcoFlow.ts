import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import "codeceptjs"; 
declare const inject: any;
const { I } = inject ();
import { bidrecoLocators } from "./BidRecoLocators";
import { CommonFunctions } from "../../isourceCommonFunctions/CommonFunctions";
import { iSourcelmt } from "../../../Framework/FrameworkUtilities/i18nutil/readI18NProp";
import { eventSettingsFlow } from "./../../POM/eventSettings/eventSettingsFlow";
// import { DatePickerFlow } from "./../../../Framework/FrameworkUtilities/COE/DatePicker/DatePickerFlow";
import { sectionAction } from "./../../POM/SupplierResponse/SupplierResponseAction";
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import {DatePicker} from "dd-cc-zycus-automation/dist/components/datePicker"
import { prop } from '../../../Framework/FrameworkUtilities/config';

export class BidReco{

    static async clickOnEditBidReco(){

        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("EditBidReconciliationLabelKey")as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.seeElement(await CommonFunctions.getFinalLocator(Startup.uiElements.get(bidrecoLocators.labelXpath)as string,await iSourcelmt.getLabel("RescheduleBidRecoLabel")as string));
    }
    static async clickOnBidReco(){

        await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("StartBidReconciliationLabel") as string);
        iSourcelmt.waitForLoadingSymbolNotDisplayed();
        await DewElement.checkIfElementPresent(await CommonFunctions.getFinalLocator(Startup.uiElements.get(bidrecoLocators.labelXpath) as string, await iSourcelmt.getLabel("RescheduleBidRecoLabel") as string));
    }

    static async closeBidRecoDatePicker(){
        I.click((await CommonFunctions.getFinalLocator(Startup.uiElements.get(bidrecoLocators.labelXpath) as string, await iSourcelmt.getLabel("RescheduleBidRecoLabel") as string)));
    }
    static async setBidRecoStartDateToCurrentDate(addMinutesToCurrentDate:string){

       await DatePicker.selectDateAndTime(Startup.uiElements.get(bidrecoLocators.startDateInputField) as string,
       (await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(parseInt(addMinutesToCurrentDate))));
       
    }

    static async setBidRecoEnndDateToCutrrentDateAndTime(addMinutesToCurrentMinute:string){
       await DatePicker.selectDateAndTime(Startup.uiElements.get(bidrecoLocators.endDateInputField) as string,
       (await CommonFunctions.getTodaysDateInMMDDYYYYFormat(0)),(await CommonFunctions.addMinutesToCurrentDate(parseInt(addMinutesToCurrentMinute))));
    }

    static async clickOnSchedule(){
      await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("scheduleScenario") as string);
      await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async clickOnSend(){
        await CommonFunctions.clickOnModalFooterButton(await iSourcelmt.getLabel("SendButtonLabel") as string);
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    }
    static async fetchAuctionValue(){
      var currentValue= await I.grabAttributeFrom(Startup.uiElements.get(bidrecoLocators.auctionValue),"value");
      return currentValue;

    }
    static async enterUnitCostBidreco(){
        // var currentauctionValue= await I.grabAttributeFrom(global.uiElements.get(bidrecoLocators.auctionValue),"value");
        var currentauctionValue= await I.grabTextFrom(Startup.uiElements.get(bidrecoLocators.auctionValue));
        console.log("Current auction value "+currentauctionValue);

        var currentPriceUnitBox = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(sectionAction.unitCostInputXpath) as string);
      
        var price=(parseInt(currentauctionValue)/currentPriceUnitBox);
        console.log("current rpice to be netered "+price)
        for (let index = 0; index < currentPriceUnitBox; index++) {
           
        
        for (let index = 0; index < currentPriceUnitBox; index++) {
           I.fillField(locate(Startup.uiElements.get(sectionAction.unitCostInputXpath) as string).at(index+1),price.toString());
          }
       }
    }
}


// module.exports=new BidReco();