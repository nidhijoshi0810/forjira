export let bidrecoLocators = {

    startDateInputField:"BidReco/startDateInputField",
    endDateInputField:"BidReco/endDateInputField",
    auctionValue:"BidReco/supplierSide/auctionValue",
    labelXpath:"iSource/labelXpath"
}