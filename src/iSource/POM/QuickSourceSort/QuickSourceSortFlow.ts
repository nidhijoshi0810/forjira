import { iSourcelmt } from './../../../Framework/FrameworkUtilities/i18nutil/readI18NProp';
import "codeceptjs";
declare const inject: any;
const { I } = inject();
import { Startup } from "../../../Framework/FrameworkUtilities/Startup/Startup";
import { prop } from "../../../Framework/FrameworkUtilities/config";
import { Filter } from "dd-cc-zycus-automation/dist/components/filter";
import { DateFilter } from "dd-cc-zycus-automation/dist/components/dateFilter";
import { DewButton } from "dd-cc-zycus-automation/dist/components/dewButton";
import { DewElement } from 'dd-cc-zycus-automation/dist/components/element';
import { CommonFunctions } from '../../isourceCommonFunctions/CommonFunctions';
import { CommonKeyword } from 'dd-cc-zycus-automation/dist/components/commonKeyword';
import { Checkbox } from 'dd-cc-zycus-automation/dist/components/checkbox';
import { FilterOnListingLocators } from '../FilterOnListing/FilterOnListingLocators';
import { QuickSourceSortLocators } from "./QuickSourceSortLocator"

export class QuickSourceSortFlow {

static async filterOnGrid(columnName: string) {
    I.wait(prop.DEFAULT_WAIT)
    // await iSourcelmt.waitForLoadingSymbolNotDisplayed();
    await Filter.applyFilter(await iSourcelmt.getLabel(columnName) as string);
    I.wait(prop.DEFAULT_WAIT)
}

static async checkboxSearch(columnName: string, dataToFilter: string) {
    I.wait(prop.DEFAULT_WAIT)
    //await I.fillField(".//dew-checklist//input[@placeholder[normalize-space()='Search']]", dataToFilter);
    //var element = await DewElement.grabNumberOfVisibleElements("//dew-checkbox//label[contains(@class,'custom-control')]");
    await I.fillField(Startup.uiElements.get(FilterOnListingLocators.SearchCheckBox), dataToFilter);
    var element = await DewElement.grabNumberOfVisibleElements(Startup.uiElements.get(FilterOnListingLocators.VerifyCheckBox)as string);

    if (element > 1) {
        await Checkbox.searchSelect(columnName, dataToFilter);
    } else {
        await I.refreshPage();
        await iSourcelmt.waitForLoadingSymbolNotDisplayed();
        I.wait(prop.DEFAULT_WAIT)
    }
}
}