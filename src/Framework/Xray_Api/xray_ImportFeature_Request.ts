
var request = require('request');
var fs = require('fs');

export class xray_ImportCucumberApiRequest {

    static async importCucumberFeatureRequest() {

        var options = {
            'method': 'POST',
            'url': 'https://xray.cloud.xpand-it.com/api/v1/import/feature?projectKey=TSTNG',
            'headers': {
                'Content-Type': 'multipart/form-data',
                // nidhi joshi gmail user for xray
                // 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnQiOiI0MGFiOGEwNS1kOWM4LTNiYjYtODk3ZS01ZmQ2ZjBjNjFmMTkiLCJhY2NvdW50SWQiOiI1Yjg2YTY3YjMzMmRmNTJhNWQ5NDY4MGUiLCJpc1hlYSI6ZmFsc2UsImlhdCI6MTU5NDY1MDY5OSwiZXhwIjoxNTk0NzM3MDk5LCJhdWQiOiJDNDkwMEMwNzAyQUQ0Q0I0OUM2NjUxMkQ4NzVCODM5NiIsImlzcyI6ImNvbS54cGFuZGl0LnBsdWdpbnMueHJheSIsInN1YiI6IkM0OTAwQzA3MDJBRDRDQjQ5QzY2NTEyRDg3NUI4Mzk2In0.rt6FwlxCpOE1BCZgsgSd1kbLQpXOX2e3tsvE9t1GJTw'

                //Jira bot zycus user for xray

                // 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnQiOiJqaXJhOjAwMzdkMzdlLTVhNzYtNDU5MC04ODFiLWQ1NDJmOGYyMzM2ZiIsImFjY291bnRJZCI6IjVjYTViZTVmOWEwMDBjMTE4MDk1NmEzMyIsImlzWGVhIjpmYWxzZSwiaWF0IjoxNTk0MzY2MDEyLCJleHAiOjE1OTQ0NTI0MTIsImF1ZCI6Ijc5Mjc5MTY4RDVDQjRENzI4NEZBOTkwRDEzMjI3RjhBIiwiaXNzIjoiY29tLnhwYW5kaXQucGx1Z2lucy54cmF5Iiwic3ViIjoiNzkyNzkxNjhENUNCNEQ3Mjg0RkE5OTBEMTMyMjdGOEEifQ.hAAY_86HzvZY7qJymTUcwTMkX9YGQSMc5sqgb6n3IZA'
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5hbnQiOiJqaXJhOjAwMzdkMzdlLTVhNzYtNDU5MC04ODFiLWQ1NDJmOGYyMzM2ZiIsImFjY291bnRJZCI6IjVjYTViZTVmOWEwMDBjMTE4MDk1NmEzMyIsImlzWGVhIjpmYWxzZSwiaWF0IjoxNTk0NjUxMTQzLCJleHAiOjE1OTQ3Mzc1NDMsImF1ZCI6Ijc5Mjc5MTY4RDVDQjRENzI4NEZBOTkwRDEzMjI3RjhBIiwiaXNzIjoiY29tLnhwYW5kaXQucGx1Z2lucy54cmF5Iiwic3ViIjoiNzkyNzkxNjhENUNCNEQ3Mjg0RkE5OTBEMTMyMjdGOEEifQ.M6Nxf900Jx1W_4HuRM04c8f-EbVF5bC4EfSyJ0RiyBU'


            },
            formData: {
                'file': {
                    'value': fs.createReadStream('D:/POC_framework/Codecept_iContract/src/iContract/features/Clauses/Clauses.feature'),
                    'options': {
                        // 'filename': '/D:/POC_framework/Codecept_iContract/src/iContract/features/Clauses/clausesBulkActions.feature',
                        'filename': 'D:/POC_framework/Codecept_iContract/src/iContract/features/Clauses/Clauses.feature',
                        'contentType': null
                    }
                }
            }
        };
        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(JSON.parse(response.body));
        });

    }
}
xray_ImportCucumberApiRequest.importCucumberFeatureRequest();

