// const mysql = require('mysql');
// const logger = require("../Logger/logger");

import parser from "mssql-connection-string";
import { logger } from "../Logger/logger";
import { prop } from "../config";
import { createConnection, Connection, MysqlError } from "mysql";
import { Startup } from "../Startup/Startup";
import { random } from "faker";


export class DatabaseOperations {
    static async getTestData(): Promise<Map<string, string>> {
        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        logger.info("connectionString  : " + connectionString);

        const connectionObj = parser(connectionString);

        const columnName = process.env.SETUP + "_" + process.env.TENANT;
        logger.info(columnName);

        const query = `SELECT FIELD_NAME, ${columnName} FROM ${prop.testdataTable}`;

        logger.info("getTestData query : " + query);

        return new Promise((resolve, reject) => {
            let testDataMap: Map<string, string> = new Map();

            logger.info("Creating sql connection");

            const connection = createConnection(connectionObj);

            logger.info("Checking sql connection");

            connection.connect((error: any) => {
                if (!!error) {
                    logger.info("Error1");
                }
                else {
                    logger.info("Connected");
                    logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            logger.info("Error in the query");
                        }
                        else {
                            logger.info("SUCCESS!");

                            for (let i = 0; i < rows.length; i++) {
                                let mapKey: string = "";
                                let mapValue: string = "";

                                for (let [key, value] of Object.entries(rows[i])) {
                                    if (key === "FIELD_NAME") {
                                        mapKey = value as string;
                                    }
                                    else if (key === columnName) {
                                        mapValue = value as string;
                                    }
                                }
                                testDataMap.set(mapKey, mapValue);
                            }
                            connection.destroy();
                            logger.info(`Data map size --> ${testDataMap.size}`);
                            resolve(testDataMap);
                        }
                    });
                }
            });
        });
    }
    static async getLMTKeys(): Promise<Map<string, string>> {
        // const prop = global.confi_prop;

        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        logger.info("connectionString  : " + connectionString)

        const connectionObj = parser(connectionString);

        const query = `SELECT * FROM LMT`;

        return new Promise((resolve, reject) => {
            let getLMTKeys: Map<string, string> = new Map();

            logger.info("Creating sql connection");
            let connection = createConnection(connectionObj);

            logger.info("Checking sql connection");
            connection.connect(function (error: any) {
                if (!!error) {
                    logger.info("Error");
                }
                else {
                    logger.info("Connected");
                    logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            logger.info("Error in the query");
                        }
                        else {
                            logger.info("SUCCESS!");

                            for (let i = 0; i < rows.length; i++) {
                                let mapKey: string = "";
                                let mapValue: string = "";
                                let LMTValueMap = new Map();
                                for (let [key, value] of Object.entries(rows[i])) {
                                    if (key === "en") {
                                        mapKey = value as string;
                                    } else {
                                        if (key === "Key") {
                                            mapValue = value as string;
                                        }
                                    }
                                }
                                getLMTKeys.set(mapKey, mapValue);
                            }
                            connection.destroy();
                            logger.info(`LMT map size --> ${getLMTKeys.size}`);
                            resolve(getLMTKeys);
                        }
                    });
                }
            });
        });
    }

    static async getLMTDetails(): Promise<Map<string, Map<string, string>>> {
        // const prop = global.confi_prop;

        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        logger.info("connectionString  : " + connectionString);

        const connectionObj = parser(connectionString);

        const query = "SELECT * FROM LMT";

        return new Promise((resolve, reject) => {
            let LMTMap: Map<string, Map<string, string>> = new Map();

            logger.info("Creating sql connection");
            let connection = createConnection(connectionObj);

            logger.info("Checking sql connection");
            connection.connect(function (error: any) {
                if (!!error) {
                    logger.info("Error");
                }
                else {
                    logger.info("Connected");
                    logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            logger.info("Error in the query");
                        }
                        else {
                            logger.info("SUCCESS!");

                            for (let i = 0; i < rows.length; i++) {
                                let mapKey: string = "";
                                let LMTValueMap: Map<string, string> = new Map();
                                for (let [key, value] of Object.entries(rows[i])) {
                                    if (key === "Key") {
                                        mapKey = value as string;
                                    } else {
                                        LMTValueMap.set(key, value as string);
                                    }
                                }
                                LMTMap.set(mapKey, LMTValueMap);
                            }
                            connection.destroy();
                            logger.info(`LMT map size --> ${LMTMap.size}`);
                            resolve(LMTMap);
                        }
                    });
                }
            });
        });
    }
    static async getCommonUiElementXpath(): Promise<Map<string, string>> {
        // const prop = Startup.confi_prop;


        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        logger.info("connectionString  : " + connectionString)

        const connectionObj = parser(connectionString);

        const query = `SELECT Page_Name, Element_Name, Element_Value FROM CommonComponentOR`;

        return new Promise((resolve, reject) => {
            let testDataMap = new Map();

            logger.info("Creating sql connection");
            const connection = createConnection(connectionObj);

            logger.info("Checking sql connection");
            connection.connect(function (error: any) {
                if (!!error) {
                    logger.info("Error");
                }
                else {
                    logger.info("Connected");
                    logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            logger.info("Error in the query");
                        }
                        else {
                            logger.info("SUCCESS!");

                            for (let i = 0; i < rows.length; i++) {
                                let mapKey: string = "";
                                let mapValue: string = "";

                                for (let [key, value] of Object.entries(rows[i])) {
                                    if (key === "Page_Name") {
                                        mapKey = value as string;
                                    }
                                    else if (key === "Element_Name") {
                                        mapKey = `${mapKey}/${value}`;
                                    }
                                    else if (key === "Element_Value") {
                                        mapValue = value as string;
                                    }

                                }
                                testDataMap.set(mapKey, mapValue);
                            }
                            connection.destroy();
                            logger.info(`UiElement map size --> ${testDataMap.size}`);
                            resolve(testDataMap);
                        }
                    });
                }
            });
        });
    }


    static async getUiElementXpath(): Promise<Map<string, string>> {
        let tablename = "uiElementTable"

        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        logger.info("connectionString  : " + connectionString);

        const connectionObj = parser(connectionString);
        if (process.env.SETUP) {
            tablename = process.env.SETUP + "_UIElements";
        }
        else {
            tablename = prop.SETUP + "_UIElements";
        }
        logger.info("table name    " + tablename)
        const query = `SELECT PAGE_NAME, ELEMENT_NAME, XPATH FROM ${Startup.configmap.get(tablename)}`;
        logger.info("---=-==-=-=-=-=-=-=>" + query);
        return new Promise((resolve, reject) => {
            let elementMap: Map<string, string> = new Map();

            logger.info("Creating sql connection");
            let connection = createConnection(connectionObj);

            logger.info("Checking sql connection");
            connection.connect(function (error: any) {
                if (!!error) {
                    logger.info("Error2");
                }
                else {
                    logger.info("Connected");
                    logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            logger.info("Error in the query");
                        }
                        else {
                            logger.info("SUCCESS!");

                            for (let i = 0; i < rows.length; i++) {
                                let mapKey: string = "";
                                let mapValue: string = "";

                                for (let [key, value] of Object.entries(rows[i])) {
                                    if (key === "PAGE_NAME") {
                                        mapKey = value as string;
                                    }
                                    else if (key === "ELEMENT_NAME") {
                                        mapKey = `${mapKey}/${value}`;
                                    }
                                    else if (key === "XPATH") {
                                        mapValue = value as string;
                                    }
                                }
                                elementMap.set(mapKey, mapValue);
                            }
                            connection.destroy();
                            logger.info(`UiElement map size --> ${elementMap.size}`);
                            resolve(elementMap);
                        }
                    });
                }
            });
        });
    }
    static async getAutomationKey(): Promise<Map<string, string>> {
        // const prop = Startup.confi_prop;

        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        logger.info("connectionString  : " + connectionString)

        const connectionObj = parser(connectionString);

        const columnName = process.env.SETUP + "_" + process.env.TENANT;
        logger.info(columnName);

        const query = "SELECT `Key`, MST_key FROM LMT";
        logger.info(query);

        return new Promise((resolve, reject) => {
            let testDataMap = new Map();

            logger.info("Creating sql connection");

            const connection = createConnection(connectionObj);

            logger.info("Checking sql connection");
            connection.connect(function (error: any) {
                if (!!error) {
                    logger.info("Error");
                }
                else {
                    logger.info("Connected");
                    logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            logger.info("Error in the query");
                        }
                        else {
                            logger.info("SUCCESS!");

                            for (let i = 0; i < rows.length; i++) {
                                let mapKey: string = "";
                                let mapValue: string = "";

                                for (let [key, value] of Object.entries(rows[i])) {
                                    if (key === "Key") {
                                        mapKey = value as string;
                                    }
                                    else {//if(key === "en") {
                                        mapValue = value as string;
                                    }
                                }
                                testDataMap.set(mapKey, mapValue);
                            }
                            connection.destroy();
                            logger.info(`Automation Keys size --> ${testDataMap.size}`);
                            resolve(testDataMap);
                        }
                    });
                }
            });
        });
    }
    static async getUser(): Promise<Map<string, string>> {
        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";

        const connectionObj = parser(connectionString);

        //logger.info(columnName);
        const query = `SELECT USERNAME,PASSWORD,DISPLAY_NAME FROM ${prop.UserTable} WHERE SETUP_NAME='${process.env.SETUP}' AND TENANT_NAME='${process.env.TENANT}'`;
        logger.info("getUser query : " + query);

        let timeout = 0;
        if (process.env.GRID) {
            timeout = random.number({ min: 60000, max: 300000 });
        }
        logger.info(`waiting for timeout --> ${timeout}`);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                logger.info(`waited for timeout --> ${timeout}`);
                let testDataMap: Map<string, string> = new Map();
                //logger.info("Creating sql connection");
                let connection = createConnection(connectionObj);
                //logger.info("Checking sql connection");
                connection.connect(function (error: any) {
                    if (!!error) {
                        //logger.info("Error1");
                    }
                    else {
                        //logger.info("Connected");
                        //logger.info("Triggering sql query");
                        connection.query(query, function (error: any, rows: any, fields: any) {
                            let displayName: string = "";
                            if (!!error) {
                                //logger.info("Error in the query");
                            }
                            else {
                                //logger.info("SUCCESS!");
                                for (let i = 0; i < rows.length; i++) {
                                    testDataMap.set("USERNAME", rows[i].USERNAME as string);
                                    testDataMap.set("PASSWORD", rows[i].PASSWORD as string);
                                    displayName = rows[i].DISPLAY_NAME;
                                }
                                connection.destroy();
                                logger.info(`user map size --> ${testDataMap.size}`);
                                Startup.displayName = displayName;
                                resolve(testDataMap);
                            }
                        });
                    }
                });
            }, timeout);
        });
    }

    static async updateUSER(userName: string, status: string) {
        // const prop = global.confi_prop;
        //logger.info("userName  : " + userName);
        const connectionString = "Data Source=tcp:" + prop.DBhost + ",3306;Initial Catalog=" + prop.DBdatabase + ";User Id=" + prop.DBuser + ";Password=" + prop.DBpassword + ";";
        //logger.info("connectionString  : " + connectionString);
        const connectionObj = parser(connectionString);
        const query = `UPDATE eProc_credentials SET FLAG='${status}' WHERE USERNAME='${userName}'`;
        //logger.info(query);
        return new Promise((resolve, reject) => {
            let testDataMap = new Map();
            //logger.info("Creating sql connection");
            let connection = createConnection(connectionObj);
            //logger.info("Checking sql connection");
            connection.connect(function (error: any) {
                if (!!error) {
                    //logger.info("Error1");
                }
                else {
                    //logger.info("Connected");
                    //logger.info("Triggering sql query");
                    connection.query(query, function (error: any, rows: any, fields: any) {
                        if (!!error) {
                            //logger.info("Error in the query");
                            connection.destroy();
                        }
                        else {
                            //logger.info("SUCCESS!");
                            connection.destroy();
                            resolve("RESOLVED");
                        }
                    });
                }
            });
        });
    }

    static async getAndUpdateUser() {
        let testDataMap = await this.getUser();
        await this.updateUSER(testDataMap.get("USERNAME") as string, "false");
        return testDataMap;
    }
}