import { Startup } from "../Startup/Startup";
import { Codecept } from "codeceptjs";
import { logger } from "../Logger/logger";
declare const inject: any;
import { Request } from "node-fetch";
const fetch = require("node-fetch");
// const {getStepData, setStepData} =  require("../../../../Share_data/client")
const { I } = inject();

export class iSourcelmt {
    static async getLabel(key: string) {

        if (typeof Startup.mst_final === 'undefined') {
            await this.matchKeys().then(async function (map) {
                Startup.mst_final = map;
                console.log("Final map", Startup.mst_final)
            })
        }
        // console.log((await getStepData("mstMap")).has(key))
        if ((Startup.mst_final).has(key)) {
            console.log("mst key taken" + (Startup.mst_final).get(key))
            return (Startup.mst_final).get(key);
        } else {
            console.log("lmt key taken" + ((Startup.lmt).get(key) as Map<string, string>).get(Startup.lang))
            return ((Startup.lmt).get(key) as Map<string, string>).get(Startup.lang);
        }

        // return (await client.getStepData("mstMap")).get(key);
    }

    // async getLabel(key){
    //     return Startup.lmt.get(key).get((await client.getStepData("UserLang")).toString());
    //    }

    static async getLabelFromKey(label: string) {
        if (Startup.allkeys.get(label)) {
            return this.getLabel(Startup.allkeys.get(label) as string)
        }
        else {
            logger.info("exception occured as key may not happened")
        }
    }

    static async getUserLangCode() {
        return new Promise(async function (resolve, reject) {
            await I.getCookie().then(async function (SAAS_TOKEN: string) {
                console.log(SAAS_TOKEN)
                fetch(Startup.testData.get("getDetailsAPI") as string, {
                    method: 'GET',
                    headers: {
                        'cookie': 'SAAS_COMMON_BASE_TOKEN_ID=' + SAAS_TOKEN,
                        'Content-Type': 'application/json'
                    },
                })
                    .then(async function (response: any) {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                        );
                        return response.json();
                    })
                    .then(async function (json: any) {
                        // await setStepData("UserLang",json.langCode);
                        // console.log(json.langCode)
                        resolve(json.langCode)
                    })
                    .catch(async function (err: any) { console.error(err) });
            })
        })
    }

    static async get_USER_MST_keys() {
        // let langCode = await this.getUserLangCode();
        // console.log(langCode);
        let MST_USER: Map<string, string> = new Map();;
        return await this.getUserLangCode().then(async function (userLang) {
            console.log("User Language " + userLang)
            return new Promise(async function (resolve, reject) {
                fetch(Startup.testData.get("languageAPI") as string + userLang + '.json', {
                    method: 'GET'
                })
                    .then(async function (response: any) {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                        );
                        return response.json();
                    })
                    .then(async function (json: any) {
                        MST_USER = json;
                        resolve(MST_USER);
                    });
            })
        })
    }

    static async matchKeys() {
        let AutoToMstValue = new Map();
        console.log("inside matchkeys")
        return await this.get_USER_MST_keys().then(async function (MST_user: any) {
            Startup.automationLMT_Pair.forEach(async function (val, key) {
                if (val != undefined) {
                    let FinalValue: string = "";
                    let i: number = 1;
                    if (val.indexOf !== undefined) {
                        let keyArray: string[] = val.split(",");
                        keyArray.forEach(async (keyAuto) => {
                            FinalValue = FinalValue + MST_user[keyAuto];
                            if (i < keyArray.length) {
                                FinalValue = FinalValue + " ";
                                i = i + 1;
                            }
                        });
                    }
                    else {
                        FinalValue = MST_user[val];
                    }
                    AutoToMstValue.set(key, FinalValue.trim());
                }
            })
            return AutoToMstValue;
        })
    }

    static async getData(key: string) {
        const I = this;
        var value: string = "";
        var index = 0;
        if (key.includes("[") && key.includes("]")) {
            let startindex = key.indexOf("[");
            let endtindex = key.indexOf("]");
            index = key.substring(startindex + 1, endtindex) as unknown as number;
            key = key.substring(0, startindex);
        }
        else {
            //logger.info("warning : no index in key so it will return 0 index value");
        }

        let mapValue = Startup.testData.get(key);
        if (typeof mapValue == 'undefined') {
            logger.info("Error : Getting null from testData for given field : " + key);
        }
        else {
            let arrayVal = mapValue.split("||");
            if (index < arrayVal.length) {
                value = arrayVal[index];
            }
            else {
                logger.info("warning : index is greater then size returning 0 index value");
                value = arrayVal[0];
            }
        }
        return value;
    }

    static async getElement(elementKey: string) {
        const I = this;
        var element: string = "";
        element = Startup.uiElements.get(elementKey) as string;
        return element;
    }

    static async  getRandomText(length: string) {
        const I = this;
        return Randomstring.generate(length);
    }

    static async waitForLoadingSymbolNotDisplayed() {

        I.waitForInvisible("//div[@class='spinner']", 60);
        // logger.info("Waited for Loading Symbol to go off");
    }
}

// module.exports= new lmt;w