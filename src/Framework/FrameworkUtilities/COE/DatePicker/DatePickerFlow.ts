import "codeceptjs";
import { Startup } from "../../Startup/Startup";
import { DatePickerLocators } from "./DatePickerLocators";
import { logger } from "../../Logger/logger"
import { DewElement } from "dd-cc-zycus-automation/dist/components/element";
//declare const inject: any;
const { I } = inject();

export class DatePickerFlow{

    static async setDateAndTime(datePickerLocator : string,userDate : string,hour : string,minute : string,meridiem : string){
        await DatePickerFlow.setDate(datePickerLocator,userDate);
        await DatePickerFlow.settHour(hour);
        await DatePickerFlow.setMinute(minute);
        await DatePickerFlow.setMeridiem(meridiem);
        logger.info("Date is set successfully.");
    }

    static async clickOnDatepicker( element : string){
     I.click(element);
      await I.seeElement(Startup.commonuiElements.get(DatePickerLocators.calenderTopDivXapth));
        logger.info("Clicked on datepicker.");
    }

    static async setDate(datePickerLocator  : string,userDate : string){
        console.log("inside datepicker"+userDate);
        var date=userDate.toString().split("-");
        await DatePickerFlow.clickOnDatepicker(datePickerLocator);
        await DatePickerFlow.selectYear(date[0]);
        await DatePickerFlow.selectMonth(date[1]);
        await DatePickerFlow.selectDay(date[2]);
        
    }
    

    /**
 * To set year or month(since locators are same, same method is being used.)
 * @param {*} matchingText 
 */
static async selectYear(matchingText : string) {
      I.click(Startup.commonuiElements.get(DatePickerLocators.yearDiv));
      const webElement = await DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.yearOrMonth) as string,matchingText);
      I.click(webElement)
   }

   static async  selectMonth(matchingText : string) {
       I.click(Startup.commonuiElements.get(DatePickerLocators.monthDiv));
       const webElement = DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.yearOrMonth) as string,matchingText);
       I.click(await webElement)
   }
   static async selectYearOrMonthFromDatePicker(matchingText : string) {
    const webElement = DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.yearOrMonth) as string,matchingText);
      I.click(await webElement)
   }

   static async selectDay(day : string){
    const webElement = DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.dayLocator) as string,day);
    I.click(await webElement) 
   }
   /**
  * To set hour
  * @param {*} hour 
  */
 static async settHour(hour : string){
    
    await I.seeElement(Startup.commonuiElements.get(DatePickerLocators.hourInputXpath));
    await I.click(Startup.commonuiElements.get(DatePickerLocators.hourInputXpath));
    const element=await DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.getHourOrMinuteOption) as string,hour);

    await I.scrollIntoView(element)
    await I.click(element)
    const value= await I.grabAttributeFrom(Startup.commonuiElements.get(DatePickerLocators.hourInputXpath),"value")
   
   if(parseInt(value,10)==parseInt(hour)){
    logger.info("Hour is set as "+hour);
   }else{
    throw new Error("Hour is not getting set,kindly check.");
       
    }
    
    }

     /**
     * To set minute
     * @param {*} Minute 
     */
    static async setMinute(Minute : string){
        I.seeElement(Startup.commonuiElements.get(DatePickerLocators.minuteInputXpath));
        I.click(Startup.commonuiElements.get(DatePickerLocators.minuteInputXpath))
        const element=await DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.getHourOrMinuteOption) as string,Minute);
       await I.scrollIntoView(element)
       await I.click(element)
        const value= await I.grabAttributeFrom(Startup.commonuiElements.get(DatePickerLocators.minuteInputXpath),"value")
       if(parseInt(value,10)==parseInt(Minute)){
        logger.info("Minute is set as "+Minute);
       }else{
        throw new Error("Minute is not getting set,kindly check.");
        }
       
    }
    /**
 * To set Meridiem
 * @param {*} meridiem 
 */
static async setMeridiem(meridiem : string){
    const count=await DewElement.grabNumberOfVisibleElements(Startup.commonuiElements.get(DatePickerLocators.meridiemDropdown) as string);
    console.log("merididum******** "+count);
    if(count==1){
    //if(I.checkIfVisible(global.commonuiElements.get(DatePickerLocators.meridiemDropdown))){
    I.seeElement(Startup.commonuiElements.get(DatePickerLocators.meridiemDropdown));
    I.click(Startup.commonuiElements.get(DatePickerLocators.meridiemDropdown));
    console.log("*********clicked on merideim***********")
    const element=DatePickerFlow.getFinalLocator(Startup.commonuiElements.get(DatePickerLocators.getHourOrMinuteOption) as string,meridiem);
    I.click(await element);
    const value= await I.grabAttributeFrom(Startup.commonuiElements.get(DatePickerLocators.optionSetXpathMeridiem),"title")
   if(value==meridiem){
        logger.info("Meridiem is set as "+meridiem)
       }else{
          throw new Error("meridiem is not getting set,kindly check.");
    }
  }else{
      console.log("Meridium field is not present.")
  }
    }

    /**
     * Internal method.
     * @param {*} locator 
     * @param {*} stringToreplace 
     */
    static async getFinalLocator(locator : string,stringToreplace : string){
        const finalLocator=locator.replace("<<dataname>>",stringToreplace);
        return finalLocator;
    }
}
