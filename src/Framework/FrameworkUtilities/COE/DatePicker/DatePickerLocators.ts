
export let DatePickerLocators = {


calenderTopDivXapth:"DatePicker/calenderTopDivXapth",
yearOrMonth:"DatePicker/yearOrMonth",
dayLocator:"DatePicker/dayLocator",
meridiemDropdown:"DatePicker/meridiemDropdown",
MeridiemOption : "DatePicker/MeridiemOption",
optionSetXpathMeridiem:"DatePicker/optionSetXpathMeridiem",
hourInputXpath : "DatePicker/hourInputXpath",
minuteInputXpath: "DatePicker/minuteInputXpath",
getHourOrMinuteOption :"DatePicker/getHourOrMinuteOption",
buttonXpath:"DatePicker/buttonXpath",
monthDiv:"DatePicker/monthDiv",
yearDiv:"DatePicker/yearDiv"
}