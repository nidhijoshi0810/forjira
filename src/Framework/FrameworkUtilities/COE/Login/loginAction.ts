export let loginAction= {
    EMAIL_ADDRESS_GHOST_TEXTBOX: "Login_Page/userName",
    EMAIL_ADDRESS_TEXTBOX: "Login_Page/userName",
    PASSWORD_GHOST_TEXTBOX: "Login/PASSWORD_GHOST_TEXTBOX",
    PASSWORD_TEXTBOX: "Login_Page/password",
    LOGIN_BUTTON: "Login_Page/signInButton",
    DDS_LOGIN_PAGE: "Login/DDS_LOGIN_PAGE",
    REQ_TABLE_OPTION_ICON: "OnlineStore/REQ_TABLE_OPTION_ICON",
    SEARCH_TEXTBOX: "OnlineStore/SEARCH_TEXTBOX",
    profileDropdown:"Profile/profileDropdown",
    logoutDropdownOption:"Profile/logoutDropdownOption",
    dopdownDiv:"common/dopdownDiv",
    usernameInputFiled:"legacySupplierSide/usernameInputFiled",
    usernameEmailInputFiled:"legacySupplierSide/usernameEmailInputFiled"
}