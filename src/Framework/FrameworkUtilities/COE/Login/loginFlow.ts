import "codeceptjs";
import { Startup } from "../../Startup/Startup";
import { loginAction } from "./loginAction";
import {CommonFunctions} from "./../../../../iSource/isourceCommonFunctions/CommonFunctions";
import {iSourcelmt} from "../../../FrameworkUtilities/i18nutil/readI18NProp";
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword";
import { prop } from "../../config";
import { logger } from "../../Logger/logger";
import { z } from "actionbot-wrapper/z";
import{DewElement}  from "dd-cc-zycus-automation/dist/components/element";

declare const inject: any;
const { I } = inject();

 
 export class loginFlow {
    static async enterUserName(username?:string) {
        if(username){
            I.fillField(Startup.uiElements.get(loginAction.EMAIL_ADDRESS_TEXTBOX), username);
        }
        else{
            I.fillField(Startup.uiElements.get(loginAction.EMAIL_ADDRESS_TEXTBOX), Startup.users.get("USERNAME"));

        }
    }
    static async  enterPassword(password?:string) {
        if(password){
        I.fillField(Startup.uiElements.get(loginAction.PASSWORD_TEXTBOX),password);
        }
        else{
            I.fillField(Startup.uiElements.get(loginAction.PASSWORD_TEXTBOX), Startup.users.get("PASSWORD"));
        }
    } 
    static async clickOnLogin (){
        I.click(Startup.uiElements.get(loginAction.LOGIN_BUTTON));
        // I.clickIfVisible(Startup.uiElements.get(loginAction.LOGIN_BUTTON));
    }
    static async logoutDDiSource(){
      await  CommonKeyword.clickElement(Startup.uiElements.get(loginAction.profileDropdown)as string);
        var noOfDD : number = await DewElement.grabNumberOfVisibleElements((Startup.uiElements.get(loginAction.dopdownDiv)) as string);
        if(noOfDD > 0)
        {
            I.wait(5);
            I.click(await CommonFunctions.getFinalLocator(Startup.uiElements.get(loginAction.logoutDropdownOption)as string,await iSourcelmt.getLabel("LogoutLabelKey")as string));
            (await CommonFunctions.clickOnButton(await iSourcelmt.getLabel("YesLabelKey")as string));
            console.log("clicked on Yes button");
            await iSourcelmt.waitForLoadingSymbolNotDisplayed()
            I.wait(5);
        }

        // I.seeElement(Startup.uiElements.get(loginAction.EMAIL_ADDRESS_TEXTBOX));
    }
   static  async loginToApp(username?:string,password?:string){
        I.amOnPage(Startup.testData.get("url"));
        // I.wait(prop.DEFAULT_LOW_WAIT)
        if(username){
            await loginFlow.enterUserName(username);  
        }
        else{
            await loginFlow.enterUserName();
        }
        if(password){
            await loginFlow.enterPassword(password);
        }
        else{
            loginFlow.enterPassword();
        }
        loginFlow.clickOnLogin();
        I.wait(10)
    }

    static async loginWithDSSO()
    {
        let USERNAME = "//input[@id='okta-signin-username']";
        let PASSWORD = "//input[@id='okta-signin-password']";
        let SIGN_IN_BUTTON = "//input[@id='okta-signin-submit']";
        let LOADER = "//img[@class='new-img']";
        let DDS_HOME = "//input[@id='input-text-area']";

        await z.amOnPage(await iSourcelmt.getData("DSSO_URL"));
        logger.info("Navigated to DSSO page");

        await z.waitForVisible(USERNAME);

        await z.fillField(USERNAME, "archana.maheshwari@zycus.com");
        await z.fillField(PASSWORD, "Pass@123");
        await z.click(SIGN_IN_BUTTON);
        logger.info("Clicked on Sign in button");

        await z.waitForInvisible(LOADER);
        await z.waitForVisible(DDS_HOME);
    }
}
