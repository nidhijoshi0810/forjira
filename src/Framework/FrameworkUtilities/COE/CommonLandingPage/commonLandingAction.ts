export let CommonLandingAction = {
    DEWDROPS_HOME : "CommonPage/homePage",
    HAMBURGER_MENU : "CommonPage/Hamburger Menu",
    SIDE_MENU_PANE : "CommonPage/side menu pane",
    APPLICATION_MENU : "CommonPage/Application Menu",
    MENU_LOCATOR : "CommonPage/Menu Locator",
    SUBMENU_LOCATOR : "CommonPage/SubMenu Locator"
}