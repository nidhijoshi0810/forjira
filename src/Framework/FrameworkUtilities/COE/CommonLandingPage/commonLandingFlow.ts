import {CommonLandingAction} from "../CommonLandingPage/commonLandingAction"
import 'codeceptjs';
import { Startup } from "../../Startup/Startup";
import { logger } from "../../Logger/logger"
import {iSourcelmt } from "../../i18nutil/readI18NProp"
import {CommonKeyword} from "dd-cc-zycus-automation/dist/components/commonKeyword"
declare const inject: any;

const { I } = inject();

export class commonLandingFlow{

    
    static async navigateToTab(applicationNameKey :string, tabKey  : string, subtabKey : string){
       
        await CommonKeyword.clickElement(Startup.uiElements.get(CommonLandingAction.HAMBURGER_MENU)as string);
        I.seeElement(Startup.uiElements.get(CommonLandingAction.SIDE_MENU_PANE));
        
        await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.APPLICATION_MENU) as string).replace("<<applicationName>>",await iSourcelmt.getLabel(applicationNameKey) as string));
        await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.MENU_LOCATOR) as string).replace("<<menuName>>",await iSourcelmt.getLabel(tabKey) as string))
        if( subtabKey !== ""){
            await CommonKeyword.clickElement((Startup.uiElements.get(CommonLandingAction.SUBMENU_LOCATOR) as string ).replace("<<subMenuName>>",await iSourcelmt.getLabel(subtabKey as string) as string))
        }
    }
}