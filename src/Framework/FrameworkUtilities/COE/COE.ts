import "codeceptjs";
import { Startup } from "../Startup/Startup";
import {generate} from "randomstring"
declare const inject: any;
const { I } = inject();

let map = new Map();

export let coe = {
    EMAIL_ADDRESS_GHOST_TEXTBOX: "Login/EMAIL_ADDRESS_GHOST_TEXTBOX",
    EMAIL_ADDRESS_TEXTBOX: "Login/EMAIL_ADDRESS_TEXTBOX",
    PASSWORD_GHOST_TEXTBOX: "Login/PASSWORD_GHOST_TEXTBOX",
    PASSWORD_TEXTBOX: "Login/PASSWORD_TEXTBOX",
    LOGIN_BUTTON: "Login/LOGIN_BUTTON",
    MENU_ICON: "Home/MENU_ICON",
    CREATEBUTTON:"CLAUSESlISING/CREATEBUTTON",
    CLAUSETITLE:"CLAUSETITLE/ClauseTitle",
    clickDropdown:"Create_A_New_Clause/clickDropdown",
    DropDownText: "Create_A_New_Clause/DropDownText",
    AssociatedBaseType:"Create_A_New_Clause/AssociatedBaseType",
    DRISCRIPTION:"Create_A_New_Clause/Description",
    CREATEBUTTON_FOOTER:"Create_A_New_Clause/CREATEBUTTON_FOOTER",
    NAVIGATEVIATABLISTING:"ListingPage/NavigateViaTabListing",
    NAVIGATESUBMENUTABS:"ClauseBasicDetails/NavigateTOSubMenuTab",
    SearchOnGrid:"Listing/SearchOnGrid",
    searchByColumnName:"Listing/searchByColumnName",
    ClauseTitlePresentCheck:"Listing/ClauseTitlePresentCheck",
    BackArrow:"BasicDetailsClauses/BackArrow",
    AddDefault:"Listing/AddDefault",
    SaveAndGoTOAllClauses:"BasicDetailsClauses/SaveAndGoTOAllClauses",
    SearchBoxReviewer:"SearchBoxReviewer/SearchTextBox",
    SearchReviewerColumnNAme:"AddReviewer/SearchReviewerColumnNAme",
    SelectReviewerByValue:"AddReviewersClauses/SelectReviewerByValue",
    AddButtonClauses:"AddReviewersClauses/AddButton",
    crossIcon:"AddReviewersClauses/crossButton",
    SecondaryOptionThreeDot:"Listing/SecondaryOptionThreeDot",
    SecondaryOptionTextListing:"SecondaryOptionTextListing/SecondaryOptionText",
    
}
export class COE{
    async navigateViaTabsListing(TabName:string)
        {
                     I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.NAVIGATEVIATABLISTING) as string,TabName));
                      I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.NAVIGATEVIATABLISTING) as string,TabName));
        } 
static async SelectFromDropDown(locatorOfDropdown:string,ValueToSelect:string,TestDataDefaultValueKeyFromDB:string)
{
    
    I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,locatorOfDropdown));
    if(ValueToSelect)
                     {
                        
                                 I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,TestDataDefaultValueKeyFromDB));
                                 I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,TestDataDefaultValueKeyFromDB));
                               //  return TestDataDefaultValueKeyFromDB;
                     }else
                            {
                               
                             
                                 I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,ValueToSelect));
                                 I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,ValueToSelect));
                                //return ValueToSelect;
                                }
}
async SelectFromDropDownProvidedXpath(locatorOfDropdown:string,ValueToSelect:string,DefaultValue:string)
{
    
    I.click(locatorOfDropdown);
    if(ValueToSelect)
                     {
                        
                                 I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,DefaultValue));
                                 I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,DefaultValue));
                               //  return TestDataDefaultValueKeyFromDB;
                     }else
                            {
                               
                             
                                 I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,ValueToSelect));
                                 I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.DropDownText) as string,ValueToSelect));
                                //return ValueToSelect;
                                }
}
static async SelectFromDropDownMultipleCheckBoxValuesProvidexXpath(dropDownlocator:string,optionsLocator:string,DropDownOptionsTobeSelected:string){
    if(DropDownOptionsTobeSelected)
    {
    var listofOptions= DropDownOptionsTobeSelected.split(',');
    I.scrollIntoView(dropDownlocator);
    I.waitForElement(dropDownlocator);
    I.click(dropDownlocator);
    
                for(var i=0;i<listofOptions.length;i++)
                         {
                             var element=await COE.replaceValueInAString(optionsLocator,listofOptions[i]);
                            // I.fillField(element);
                             I.seeElement(element);
                             I.scrollIntoView(element);
                             I.click(element);
                            
                         }
                        }
}
async SelectFromDropDownMultipleCheckBoxValues(formControlName:string,DropDownOptionsTobeSelected:string)
{
    if(DropDownOptionsTobeSelected)
    {
    var listofelements= DropDownOptionsTobeSelected.split(',');
    I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName));
                for(var i=0;i<=listofelements.length-1;i++)
                         {
                             I.fillField(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName),listofelements[i]);
                             I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]));
                             I.waitForElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]))
                             I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]));
                            
                         }
    }
    else
    {
        let listofElements=Startup.testData.get("AssociatedBseTypeDropDown_DefaultValue") as string;
        if(listofElements.includes(','))
                {
                let listofelements=Startup.testData.get("AssociatedBseTypeDropDown_DefaultValue") as string;
                I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName));
                 for(var i=0;i<=listofelements.length-1;i++)
                                     {
                                          I.fillField(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName),listofelements[i]);
                                          
                                          I.waitForElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]));
                                          I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]));
                                          I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]));
                                          I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements[i]));

                                     }
                 }
                    else 
                        {
                             let listofelements=Startup.testData.get("AssociatedBseTypeDropDown_DefaultValue");
                             I.fillField(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName),listofelements);
                             I.waitForElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName));
                             I.scrollIntoView(await COE.replaceValueInAString(Startup.uiElements.get(coe.clickDropdown) as string,formControlName));
                             I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements as string));
                             I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.AssociatedBaseType) as string,listofelements as string));

        }

    }


}

      static async  navigateToSunMenuTabs(SubTabText : string)
            {
                 I.seeElement(await COE.replaceValueInAString(Startup.uiElements.get(coe.NAVIGATESUBMENUTABS) as string,SubTabText));
                 I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.NAVIGATESUBMENUTABS) as string,SubTabText));
            }
          static async searchOnGridProvidedLocator(searchLocator : string,dataTosearch: string,colunmName:string)
            {
               
                I.seeElement(searchLocator);
                 I.click(searchLocator);
                I.fillField(searchLocator,dataTosearch);
                 I.seeElement(await COE.replaceValueInAString(Startup.commonuiElements.get(coe.searchByColumnName) as string,colunmName));
                I.click(await COE.replaceValueInAString(Startup.commonuiElements.get(coe.searchByColumnName) as string,colunmName));
                I.wait(2);
                I.seeElement(await COE.replaceValueInAString(Startup.commonuiElements.get(coe.ClauseTitlePresentCheck) as string,dataTosearch));

            }

       static async searchOnGrid(dataTosearch? : string,colunmName? : string)
            {
                
                I.seeElement(Startup.uiElements.get(coe.SearchOnGrid));
                 I.click(Startup.uiElements.get(coe.SearchOnGrid));
                I.fillField(Startup.uiElements.get(coe.SearchOnGrid),dataTosearch);
                I.seeElement(await COE.replaceValueInAString(Startup.commonuiElements.get(coe.searchByColumnName) as string,colunmName as string));
                I.click(await COE.replaceValueInAString(Startup.commonuiElements.get(coe.searchByColumnName) as string,colunmName as string));
                I.seeElement(await COE.replaceValueInAString(Startup.commonuiElements.get(coe.ClauseTitlePresentCheck) as string,dataTosearch as string));

            }
           
            // static async replaceValueInAString(xpath,valueToReplacewith)
            // {
            //         return await COE.replaceValueInAString(xpath,valueToReplacewith)
            // }
           static async selectSecondaryOption(ValueToSelect : string)
            {
                I.waitForElement(Startup.uiElements.get(coe.SecondaryOptionThreeDot));
                I.click(Startup.uiElements.get(coe.SecondaryOptionThreeDot));
                I.click(await COE.replaceValueInAString(Startup.uiElements.get(coe.SecondaryOptionTextListing) as string,ValueToSelect));
               // pause();
            }
            static async replaceValueInAString(xpath : string,valueToReplacewith: string)
            {
               return  xpath.replace("<<dataname>>",valueToReplacewith);
            }

            // {
            //     return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
            // }


            static  randomString(length : string | number) :string|number{
                let re : any ;
                if(typeof length === 'string')
                re= generate(parseInt(length));
                else if(typeof length == 'number')
                re=  Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
                return re;
             }
}
