export class Startup {
    static confi_prop: Object;
    static configmap: Map<string, string | number | boolean> = new Map();
    static displayName: string;
    static testData: Map<string, string> = new Map();
    static uiElements: Map<string, string> = new Map();
    static users: Map<string, string> = new Map();
    static lmt: Map<string, Map<string, string>> = new Map();
    static allkeys: Map<string, string> = new Map();
    static lang: string;
    static commonuiElements: Map<string, string> = new Map();
    static automationLMT_Pair: Map<string, string> = new Map();
    static mst_final: Map<string, string> = new Map();
    static eventId_Map: Map<string, string> = new Map();
    // static email_Test: Map<number, Map<string, string>> = new Map();
    static email_Test: Map<any, any> = new Map();

}