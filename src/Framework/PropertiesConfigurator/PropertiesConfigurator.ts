import { Startup } from "../FrameworkUtilities/Startup/Startup";
import { prop } from "../FrameworkUtilities/config";
import { logger } from "../FrameworkUtilities/Logger/logger";

export async function parseJson() {
  for (const [key, value] of Object.entries(prop)) {
    Startup.configmap.set(key, value);
  }

  // for (const [key, value] of Startup.configmap.entries()) {
  //   logger.info(`key --> ${key} || value --> ${value}`);
  // }

}