const Webdriver = require('./node_modules/codeceptjs/lib/helper/WebDriver');
const containers = require('./node_modules/codeceptjs/lib/container');
const event = require('./node_modules/codeceptjs/lib/event');
const path = require('path');
const config = require('./Resources/config.json')[process.env.PRODUCT +process.env.SETUP+process.env.TENANT];
const request = require('sync-request');

const urlRequestScreen = (session) => `http://${config.host}:${config.port}/wd/hub/session/${session}/screenshot`
let newBrowserSesssion = null;
function initWorkAround() {
  event.dispatcher.on(event.step.failed, () => {
    if(newBrowserSesssion) {
      const resp = request('GET', urlRequestScreen(newBrowserSesssion))
      const allureReporter = containers.plugins('allure') || {addAttachment: () => {}};
      const fileName = path.resolve(global.output_dir, `${newBrowserSesssion}.png`)
      const a = JSON.parse(resp.body.toString()).value
      require('fs').writeFileSync(fileName, a, 'base64')
      const readFromfs = require('fs').readFileSync(fileName)
      allureReporter.addAttachment(`Other browser ${newBrowserSesssion}`, readFromfs, 'image/png');
    }
  })

  const initialSession = Webdriver.prototype._session
  Webdriver.prototype._session = function() {
    const session = initialSession.call(this)
    const initialStartResult = session.start
    return {
      ...session,
      start: (...opts) =>
        initialStartResult(...opts).then(br => {
          newBrowserSesssion = br.sessionId;
          console.log(newBrowserSesssion)
          return br
        })
    }
  }
}

module.exports = {
  initWorkAround
}
